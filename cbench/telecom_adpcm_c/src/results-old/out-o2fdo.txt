
real	0m3.166s
user	0m3.158s
sys	0m0.008s

 Performance counter stats for './__run 1':

       3167.888944      task-clock (msec)         #    1.000 CPUs utilized          
                 5      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.079 K/sec                  
    10,294,437,642      cycles                    #    3.250 GHz                    
     1,853,863,720      stalled-cycles-frontend   #   18.01% frontend cycles idle   
    22,203,644,996      instructions              #    2.16  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     5,342,879,109      branches                  # 1686.574 M/sec                  
        32,982,409      branch-misses             #    0.62% of all branches        

       3.168234702 seconds time elapsed


real	0m3.170s
user	0m3.169s
sys	0m0.000s

 Performance counter stats for './__run 1':

       3171.754145      task-clock (msec)         #    1.000 CPUs utilized          
                22      context-switches          #    0.007 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.080 K/sec                  
    10,269,474,807      cycles                    #    3.238 GHz                    
     1,839,921,888      stalled-cycles-frontend   #   17.92% frontend cycles idle   
    22,200,876,276      instructions              #    2.16  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     5,342,443,809      branches                  # 1684.381 M/sec                  
        34,326,050      branch-misses             #    0.64% of all branches        

       3.172489501 seconds time elapsed


real	0m3.149s
user	0m3.149s
sys	0m0.000s

 Performance counter stats for './__run 1':

       3151.270717      task-clock (msec)         #    1.000 CPUs utilized          
                13      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.080 K/sec                  
    10,204,857,755      cycles                    #    3.238 GHz                    
     1,809,373,497      stalled-cycles-frontend   #   17.73% frontend cycles idle   
    22,203,471,241      instructions              #    2.18  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     5,342,851,712      branches                  # 1695.459 M/sec                  
        31,231,602      branch-misses             #    0.58% of all branches        

       3.151973516 seconds time elapsed


real	0m0.061s
user	0m0.061s
sys	0m0.000s

 Performance counter stats for './__run 2':

         63.092920      task-clock (msec)         #    0.993 CPUs utilized          
                 6      context-switches          #    0.095 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               249      page-faults               #    0.004 M/sec                  
       180,836,763      cycles                    #    2.866 GHz                    
        41,542,433      stalled-cycles-frontend   #   22.97% frontend cycles idle   
       339,632,751      instructions              #    1.88  insn per cycle         
                                                  #    0.12  stalled cycles per insn
        81,501,605      branches                  # 1291.771 M/sec                  
         1,442,729      branch-misses             #    1.77% of all branches        

       0.063539277 seconds time elapsed


real	0m0.059s
user	0m0.059s
sys	0m0.000s

 Performance counter stats for './__run 2':

         61.318744      task-clock (msec)         #    0.995 CPUs utilized          
                 1      context-switches          #    0.016 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.004 M/sec                  
       181,709,739      cycles                    #    2.963 GHz                    
        44,074,030      stalled-cycles-frontend   #   24.26% frontend cycles idle   
       339,534,796      instructions              #    1.87  insn per cycle         
                                                  #    0.13  stalled cycles per insn
        81,482,845      branches                  # 1328.841 M/sec                  
         1,446,260      branch-misses             #    1.77% of all branches        

       0.061649524 seconds time elapsed


real	0m0.062s
user	0m0.061s
sys	0m0.000s

 Performance counter stats for './__run 2':

         64.072012      task-clock (msec)         #    0.994 CPUs utilized          
                 3      context-switches          #    0.047 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               250      page-faults               #    0.004 M/sec                  
       180,887,788      cycles                    #    2.823 GHz                    
        42,216,903      stalled-cycles-frontend   #   23.34% frontend cycles idle   
       339,654,813      instructions              #    1.88  insn per cycle         
                                                  #    0.12  stalled cycles per insn
        81,505,376      branches                  # 1272.090 M/sec                  
         1,456,374      branch-misses             #    1.79% of all branches        

       0.064486737 seconds time elapsed


real	0m0.855s
user	0m0.850s
sys	0m0.004s

 Performance counter stats for './__run 3':

        857.149359      task-clock (msec)         #    0.999 CPUs utilized          
                14      context-switches          #    0.016 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.296 K/sec                  
     2,763,016,613      cycles                    #    3.223 GHz                    
       544,154,003      stalled-cycles-frontend   #   19.69% frontend cycles idle   
     5,855,134,452      instructions              #    2.12  insn per cycle         
                                                  #    0.09  stalled cycles per insn
     1,408,814,768      branches                  # 1643.605 M/sec                  
        10,540,149      branch-misses             #    0.75% of all branches        

       0.857779029 seconds time elapsed


real	0m0.870s
user	0m0.869s
sys	0m0.000s

 Performance counter stats for './__run 3':

        870.565421      task-clock (msec)         #    0.998 CPUs utilized          
                46      context-switches          #    0.053 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
               251      page-faults               #    0.288 K/sec                  
     2,779,022,891      cycles                    #    3.192 GHz                    
       550,307,977      stalled-cycles-frontend   #   19.80% frontend cycles idle   
     5,856,056,817      instructions              #    2.11  insn per cycle         
                                                  #    0.09  stalled cycles per insn
     1,408,979,722      branches                  # 1618.465 M/sec                  
        10,299,346      branch-misses             #    0.73% of all branches        

       0.871885662 seconds time elapsed


real	0m0.841s
user	0m0.841s
sys	0m0.000s

 Performance counter stats for './__run 3':

        843.621397      task-clock (msec)         #    0.999 CPUs utilized          
                 8      context-switches          #    0.009 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.300 K/sec                  
     2,725,513,606      cycles                    #    3.231 GHz                    
       491,612,134      stalled-cycles-frontend   #   18.04% frontend cycles idle   
     5,855,159,671      instructions              #    2.15  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     1,408,818,533      branches                  # 1669.965 M/sec                  
        10,131,245      branch-misses             #    0.72% of all branches        

       0.844093938 seconds time elapsed


real	0m0.446s
user	0m0.446s
sys	0m0.000s

 Performance counter stats for './__run 4':

        448.815839      task-clock (msec)         #    0.998 CPUs utilized          
                 6      context-switches          #    0.013 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.564 K/sec                  
     1,450,099,295      cycles                    #    3.231 GHz                    
       255,576,056      stalled-cycles-frontend   #   17.62% frontend cycles idle   
     3,079,298,464      instructions              #    2.12  insn per cycle         
                                                  #    0.08  stalled cycles per insn
       740,797,346      branches                  # 1650.560 M/sec                  
         6,311,845      branch-misses             #    0.85% of all branches        

       0.449624477 seconds time elapsed


real	0m0.448s
user	0m0.448s
sys	0m0.000s

 Performance counter stats for './__run 4':

        450.467704      task-clock (msec)         #    0.999 CPUs utilized          
                 5      context-switches          #    0.011 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.557 K/sec                  
     1,450,721,010      cycles                    #    3.220 GHz                    
       263,421,222      stalled-cycles-frontend   #   18.16% frontend cycles idle   
     3,079,263,791      instructions              #    2.12  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       740,790,145      branches                  # 1644.491 M/sec                  
         6,423,770      branch-misses             #    0.87% of all branches        

       0.450907393 seconds time elapsed


real	0m0.457s
user	0m0.455s
sys	0m0.000s

 Performance counter stats for './__run 4':

        458.032517      task-clock (msec)         #    0.996 CPUs utilized          
                11      context-switches          #    0.024 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               253      page-faults               #    0.552 K/sec                  
     1,461,030,922      cycles                    #    3.190 GHz                    
       278,955,536      stalled-cycles-frontend   #   19.09% frontend cycles idle   
     3,079,354,621      instructions              #    2.11  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       740,807,102      branches                  # 1617.368 M/sec                  
         6,161,471      branch-misses             #    0.83% of all branches        

       0.459800949 seconds time elapsed


real	0m0.490s
user	0m0.486s
sys	0m0.004s

 Performance counter stats for './__run 5':

        492.889213      task-clock (msec)         #    0.999 CPUs utilized          
                 4      context-switches          #    0.008 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.513 K/sec                  
     1,588,342,402      cycles                    #    3.223 GHz                    
       290,087,087      stalled-cycles-frontend   #   18.26% frontend cycles idle   
     3,376,759,283      instructions              #    2.13  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       812,377,993      branches                  # 1648.196 M/sec                  
         6,841,659      branch-misses             #    0.84% of all branches        

       0.493348584 seconds time elapsed


real	0m0.497s
user	0m0.497s
sys	0m0.000s

 Performance counter stats for './__run 5':

        500.020070      task-clock (msec)         #    0.999 CPUs utilized          
                 2      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.508 K/sec                  
     1,595,270,671      cycles                    #    3.190 GHz                    
       293,749,235      stalled-cycles-frontend   #   18.41% frontend cycles idle   
     3,376,783,393      instructions              #    2.12  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       812,380,261      branches                  # 1624.695 M/sec                  
         6,601,920      branch-misses             #    0.81% of all branches        

       0.500457559 seconds time elapsed


real	0m0.491s
user	0m0.491s
sys	0m0.000s

 Performance counter stats for './__run 5':

        493.534270      task-clock (msec)         #    0.999 CPUs utilized          
                 6      context-switches          #    0.012 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.513 K/sec                  
     1,586,828,451      cycles                    #    3.215 GHz                    
       289,685,787      stalled-cycles-frontend   #   18.26% frontend cycles idle   
     3,376,876,926      instructions              #    2.13  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       812,396,870      branches                  # 1646.080 M/sec                  
         6,630,894      branch-misses             #    0.82% of all branches        

       0.494039990 seconds time elapsed


real	0m0.194s
user	0m0.190s
sys	0m0.004s

 Performance counter stats for './__run 6':

        197.379135      task-clock (msec)         #    0.997 CPUs utilized          
                 6      context-switches          #    0.030 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.001 M/sec                  
       617,984,092      cycles                    #    3.131 GHz                    
       120,743,259      stalled-cycles-frontend   #   19.54% frontend cycles idle   
     1,279,151,273      instructions              #    2.07  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       307,570,316      branches                  # 1558.272 M/sec                  
         3,120,829      branch-misses             #    1.01% of all branches        

       0.197916891 seconds time elapsed


real	0m0.193s
user	0m0.189s
sys	0m0.004s

 Performance counter stats for './__run 6':

        195.483316      task-clock (msec)         #    0.998 CPUs utilized          
                 3      context-switches          #    0.015 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               256      page-faults               #    0.001 M/sec                  
       616,861,054      cycles                    #    3.156 GHz                    
       115,451,126      stalled-cycles-frontend   #   18.72% frontend cycles idle   
     1,279,019,612      instructions              #    2.07  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       307,547,971      branches                  # 1573.270 M/sec                  
         3,144,942      branch-misses             #    1.02% of all branches        

       0.195878986 seconds time elapsed


real	0m0.195s
user	0m0.183s
sys	0m0.012s

 Performance counter stats for './__run 6':

        197.314375      task-clock (msec)         #    0.997 CPUs utilized          
                 6      context-switches          #    0.030 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.001 M/sec                  
       617,119,085      cycles                    #    3.128 GHz                    
       119,512,585      stalled-cycles-frontend   #   19.37% frontend cycles idle   
     1,279,096,863      instructions              #    2.07  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       307,560,926      branches                  # 1558.736 M/sec                  
         3,163,382      branch-misses             #    1.03% of all branches        

       0.197849583 seconds time elapsed


real	0m0.410s
user	0m0.410s
sys	0m0.000s

 Performance counter stats for './__run 7':

        412.542870      task-clock (msec)         #    0.999 CPUs utilized          
                10      context-switches          #    0.024 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               252      page-faults               #    0.611 K/sec                  
     1,324,772,371      cycles                    #    3.211 GHz                    
       263,468,214      stalled-cycles-frontend   #   19.89% frontend cycles idle   
     2,769,189,554      instructions              #    2.09  insn per cycle         
                                                  #    0.10  stalled cycles per insn
       666,148,743      branches                  # 1614.738 M/sec                  
         5,658,159      branch-misses             #    0.85% of all branches        

       0.413063550 seconds time elapsed


real	0m0.401s
user	0m0.401s
sys	0m0.000s

 Performance counter stats for './__run 7':

        403.795091      task-clock (msec)         #    0.999 CPUs utilized          
                 4      context-switches          #    0.010 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               253      page-faults               #    0.627 K/sec                  
     1,304,815,562      cycles                    #    3.231 GHz                    
       231,743,087      stalled-cycles-frontend   #   17.76% frontend cycles idle   
     2,769,190,081      instructions              #    2.12  insn per cycle         
                                                  #    0.08  stalled cycles per insn
       666,147,816      branches                  # 1649.717 M/sec                  
         5,546,177      branch-misses             #    0.83% of all branches        

       0.404146562 seconds time elapsed


real	0m0.405s
user	0m0.405s
sys	0m0.000s

 Performance counter stats for './__run 7':

        407.397923      task-clock (msec)         #    0.999 CPUs utilized          
                 4      context-switches          #    0.010 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               253      page-faults               #    0.621 K/sec                  
     1,308,716,592      cycles                    #    3.212 GHz                    
       241,505,374      stalled-cycles-frontend   #   18.45% frontend cycles idle   
     2,769,433,604      instructions              #    2.12  insn per cycle         
                                                  #    0.09  stalled cycles per insn
       666,192,661      branches                  # 1635.238 M/sec                  
         5,799,829      branch-misses             #    0.87% of all branches        

       0.407807934 seconds time elapsed


real	0m0.704s
user	0m0.698s
sys	0m0.004s

 Performance counter stats for './__run 8':

        704.710369      task-clock (msec)         #    0.997 CPUs utilized          
                 4      context-switches          #    0.006 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.360 K/sec                  
     2,282,887,115      cycles                    #    3.239 GHz                    
       409,608,850      stalled-cycles-frontend   #   17.94% frontend cycles idle   
     4,877,127,910      instructions              #    2.14  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     1,173,433,418      branches                  # 1665.129 M/sec                  
         9,284,778      branch-misses             #    0.79% of all branches        

       0.707038012 seconds time elapsed


real	0m0.708s
user	0m0.708s
sys	0m0.000s

 Performance counter stats for './__run 8':

        710.613028      task-clock (msec)         #    0.999 CPUs utilized          
                 2      context-switches          #    0.003 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.359 K/sec                  
     2,291,938,071      cycles                    #    3.225 GHz                    
       413,438,930      stalled-cycles-frontend   #   18.04% frontend cycles idle   
     4,878,393,346      instructions              #    2.13  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     1,173,643,517      branches                  # 1651.593 M/sec                  
         9,464,948      branch-misses             #    0.81% of all branches        

       0.711040340 seconds time elapsed


real	0m0.714s
user	0m0.714s
sys	0m0.000s

 Performance counter stats for './__run 8':

        716.336077      task-clock (msec)         #    0.999 CPUs utilized          
                 3      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.356 K/sec                  
     2,314,246,716      cycles                    #    3.231 GHz                    
       455,216,867      stalled-cycles-frontend   #   19.67% frontend cycles idle   
     4,878,380,154      instructions              #    2.11  insn per cycle         
                                                  #    0.09  stalled cycles per insn
     1,173,642,850      branches                  # 1638.397 M/sec                  
         9,117,664      branch-misses             #    0.78% of all branches        

       0.716758553 seconds time elapsed


real	0m2.920s
user	0m2.920s
sys	0m0.000s

 Performance counter stats for './__run 9':

       2922.618778      task-clock (msec)         #    1.000 CPUs utilized          
                19      context-switches          #    0.007 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               258      page-faults               #    0.088 K/sec                  
     9,509,974,931      cycles                    #    3.254 GHz                    
     1,716,980,139      stalled-cycles-frontend   #   18.05% frontend cycles idle   
    20,578,460,813      instructions              #    2.16  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     4,951,958,089      branches                  # 1694.356 M/sec                  
        31,102,473      branch-misses             #    0.63% of all branches        

       2.923127744 seconds time elapsed


real	0m2.916s
user	0m2.916s
sys	0m0.000s

 Performance counter stats for './__run 9':

       2918.324688      task-clock (msec)         #    1.000 CPUs utilized          
                16      context-switches          #    0.005 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.087 K/sec                  
     9,490,464,832      cycles                    #    3.252 GHz                    
     1,680,412,261      stalled-cycles-frontend   #   17.71% frontend cycles idle   
    20,581,423,970      instructions              #    2.17  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     4,952,454,627      branches                  # 1697.020 M/sec                  
        30,954,508      branch-misses             #    0.63% of all branches        

       2.919441173 seconds time elapsed


real	0m2.956s
user	0m2.956s
sys	0m0.000s

 Performance counter stats for './__run 9':

       2958.441557      task-clock (msec)         #    1.000 CPUs utilized          
                45      context-switches          #    0.015 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.086 K/sec                  
     9,567,196,159      cycles                    #    3.234 GHz                    
     1,778,719,420      stalled-cycles-frontend   #   18.59% frontend cycles idle   
    20,578,337,313      instructions              #    2.15  insn per cycle         
                                                  #    0.09  stalled cycles per insn
     4,951,944,693      branches                  # 1673.836 M/sec                  
        32,050,117      branch-misses             #    0.65% of all branches        

       2.959768711 seconds time elapsed


real	0m1.663s
user	0m1.655s
sys	0m0.008s

 Performance counter stats for './__run 10':

       1665.994773      task-clock (msec)         #    1.000 CPUs utilized          
                25      context-switches          #    0.015 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.152 K/sec                  
     5,369,665,561      cycles                    #    3.223 GHz                    
       951,509,850      stalled-cycles-frontend   #   17.72% frontend cycles idle   
    11,592,224,246      instructions              #    2.16  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     2,789,414,983      branches                  # 1674.324 M/sec                  
        19,467,575      branch-misses             #    0.70% of all branches        

       1.666776751 seconds time elapsed


real	0m1.655s
user	0m1.650s
sys	0m0.004s

 Performance counter stats for './__run 10':

       1657.275367      task-clock (msec)         #    0.999 CPUs utilized          
                37      context-switches          #    0.022 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               256      page-faults               #    0.154 K/sec                  
     5,357,960,320      cycles                    #    3.233 GHz                    
       932,980,955      stalled-cycles-frontend   #   17.41% frontend cycles idle   
    11,592,269,020      instructions              #    2.16  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     2,789,421,784      branches                  # 1683.137 M/sec                  
        18,992,561      branch-misses             #    0.68% of all branches        

       1.658217659 seconds time elapsed


real	0m1.662s
user	0m1.662s
sys	0m0.000s

 Performance counter stats for './__run 10':

       1664.069903      task-clock (msec)         #    1.000 CPUs utilized          
                29      context-switches          #    0.017 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               250      page-faults               #    0.150 K/sec                  
     5,367,136,185      cycles                    #    3.225 GHz                    
       941,504,266      stalled-cycles-frontend   #   17.54% frontend cycles idle   
    11,592,247,440      instructions              #    2.16  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     2,789,420,886      branches                  # 1676.264 M/sec                  
        19,531,776      branch-misses             #    0.70% of all branches        

       1.664739426 seconds time elapsed

