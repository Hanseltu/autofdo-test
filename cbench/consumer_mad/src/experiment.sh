#!/bin/bash

echo "This is a test in autofdo with cbenchmark"

: << 'COMMENT'

echo "Start pass in O2"
rm results/*
gcc -DFPM_DEFAULT -DHAVE_CONFIG_H -DLOCALEDIR=\"/usr/local/share/locale\" -O2 *.c -lm -lz -lesd
#for var in 1 2 3 4 5 6 7 8 9 10
for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o2.txt
sudo perf stat ./__run $var 2>>results/out-o2.txt
sudo perf stat ./__run $var 2>>results/out-o2.txt
done
echo "End pass in O2"


echo "Start pass in O3"
gcc -DFPM_DEFAULT -DHAVE_CONFIG_H -DLOCALEDIR=\"/usr/local/share/locale\" -O3 *.c -lm -lz -lesd
#for var in 1 2 3 4 5 6 7 8 9 10
for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o3.txt
sudo perf stat ./__run $var 2>>results/out-o3.txt
sudo perf stat ./__run $var 2>>results/out-o3.txt
done
echo "End pass in O3"

COMMENT
#: << 'COMMENT'

echo "Start pass in O2+fdo"
gcc -DFPM_DEFAULT -DHAVE_CONFIG_H -DLOCALEDIR=\"/usr/local/share/locale\" *.c -lm -lz -lesd -fprofile-generate
#gcc *.c -fprofile-generate -lm
./a.out --time=100000 --output=wave:output.wav ~/bitbucket/autofdo-test/cbench/consumer_data/1.mp3
gcc -DFPM_DEFAULT -DHAVE_CONFIG_H -DLOCALEDIR=\"/usr/local/share/locale\" -O2 *.c -fprofile-use=*.gcda -lm
#for var in 1 2 3 4 5 6 7 8 9 10
for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o2fdo.txt
sudo perf stat ./__run $var 2>>results/out-o2fdo.txt
sudo perf stat ./__run $var 2>>results/out-o2fdo.txt
done
echo "End pass in O2+fdo"

: << 'COMMENT'

echo "Start pass in O3+fdo"
gcc *.c -fprofile-generate -lm
./a.out ~/bitbucket/autofdo-test/cbench/consumer_data/1.wav output_large.mp3
gcc -DFPM_DEFAULT -DHAVE_CONFIG_H -DLOCALEDIR=\"/usr/local/share/locale\" -O3 *.c -fprofile-use=*.gcda -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o3fdo.txt
sudo perf stat ./__run $var 2>>results/out-o3fdo.txt
sudo perf stat ./__run $var 2>>results/out-o3fdo.txt
done
echo "End pass in O3+fdo"

#COMMENT

echo "Start pass in O2+autofdo"
gcc -DFPM_DEFAULT -DHAVE_CONFIG_H -DLOCALEDIR=\"/usr/local/share/locale\" -O2 *.c -lm -lz -lesd
sudo ~/github/AutoFDO-Test/pmu-tools/ocperf.py record -b -e br_inst_retired.near_taken:pp -- ./a.out ~/bitbucket/autofdo-test/cbench/consumer_data/1.wav output_large.mp3
sudo ~/github/AutoFDO-Test/autofdo/create_gcov --binary=./a.out --profile=perf.data --gcov=a.gcov -gcov_version=1
gcc -O2 -fauto-profile=a.gcov *.c -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o2autofdo.txt
sudo perf stat ./__run $var 2>>results/out-o2autofdo.txt
sudo perf stat ./__run $var 2>>results/out-o2autofdo.txt
done
echo "End pass in O2+autofdo"


echo "Start pass in O3+autofdo"
gcc -DFPM_DEFAULT -DHAVE_CONFIG_H -DLOCALEDIR=\"/usr/local/share/locale\" -O2 *.c -lm -lz -lesd
sudo ~/github/AutoFDO-Test/pmu-tools/ocperf.py record -b -e br_inst_retired.near_taken:pp -- ./a.out ~/bitbucket/autofdo-test/cbench/consumer_data/1.wav output_large.mp3
sudo ~/github/AutoFDO-Test/autofdo/create_gcov --binary=./a.out --profile=perf.data --gcov=a.gcov -gcov_version=1
gcc -O3 -fauto-profile=a.gcov *.c -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o3autofdo.txt
sudo perf stat ./__run $var 2>>results/out-o3autofdo.txt
sudo perf stat ./__run $var 2>>results/out-o3autofdo.txt
done
echo "End pass in O3+autofdo"

COMMENT
