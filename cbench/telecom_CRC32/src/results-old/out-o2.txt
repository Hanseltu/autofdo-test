
real	0m4.797s
user	0m4.711s
sys	0m0.084s

 Performance counter stats for './__run 1':

       4797.056852      task-clock (msec)         #    0.999 CPUs utilized          
                15      context-switches          #    0.003 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
               253      page-faults               #    0.053 K/sec                  
    15,029,001,480      cycles                    #    3.133 GHz                    
     5,294,420,579      stalled-cycles-frontend   #   35.23% frontend cycles idle   
    24,617,200,164      instructions              #    1.64  insn per cycle         
                                                  #    0.22  stalled cycles per insn
     6,932,847,725      branches                  # 1445.229 M/sec                  
           850,395      branch-misses             #    0.01% of all branches        

       4.799560697 seconds time elapsed


real	0m4.426s
user	0m4.357s
sys	0m0.068s

 Performance counter stats for './__run 1':

       4426.489816      task-clock (msec)         #    1.000 CPUs utilized          
                11      context-switches          #    0.002 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.057 K/sec                  
    13,691,010,402      cycles                    #    3.093 GHz                    
     3,806,210,596      stalled-cycles-frontend   #   27.80% frontend cycles idle   
    24,616,634,213      instructions              #    1.80  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     6,932,751,430      branches                  # 1566.196 M/sec                  
           838,410      branch-misses             #    0.01% of all branches        

       4.427996365 seconds time elapsed


real	0m5.703s
user	0m5.577s
sys	0m0.124s

 Performance counter stats for './__run 1':

       5703.369857      task-clock (msec)         #    0.999 CPUs utilized          
                17      context-switches          #    0.003 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
               256      page-faults               #    0.045 K/sec                  
    18,398,053,854      cycles                    #    3.226 GHz                    
     9,049,310,940      stalled-cycles-frontend   #   49.19% frontend cycles idle   
    24,612,087,333      instructions              #    1.34  insn per cycle         
                                                  #    0.37  stalled cycles per insn
     6,932,014,207      branches                  # 1215.424 M/sec                  
           849,143      branch-misses             #    0.01% of all branches        

       5.706990903 seconds time elapsed


real	0m0.064s
user	0m0.060s
sys	0m0.004s

 Performance counter stats for './__run 2':

         65.997281      task-clock (msec)         #    0.991 CPUs utilized          
                 8      context-switches          #    0.121 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               257      page-faults               #    0.004 M/sec                  
       203,924,987      cycles                    #    3.090 GHz                    
        69,934,920      stalled-cycles-frontend   #   34.29% frontend cycles idle   
       335,046,821      instructions              #    1.64  insn per cycle         
                                                  #    0.21  stalled cycles per insn
        93,953,731      branches                  # 1423.600 M/sec                  
            59,923      branch-misses             #    0.06% of all branches        

       0.066595168 seconds time elapsed


real	0m0.088s
user	0m0.088s
sys	0m0.000s

 Performance counter stats for './__run 2':

         90.330335      task-clock (msec)         #    0.996 CPUs utilized          
                 4      context-switches          #    0.044 K/sec                  
                 3      cpu-migrations            #    0.033 K/sec                  
               253      page-faults               #    0.003 M/sec                  
       293,038,498      cycles                    #    3.244 GHz                    
       168,656,039      stalled-cycles-frontend   #   57.55% frontend cycles idle   
       334,935,418      instructions              #    1.14  insn per cycle         
                                                  #    0.50  stalled cycles per insn
        93,933,485      branches                  # 1039.889 M/sec                  
            55,140      branch-misses             #    0.06% of all branches        

       0.090738161 seconds time elapsed


real	0m0.062s
user	0m0.054s
sys	0m0.008s

 Performance counter stats for './__run 2':

         63.887769      task-clock (msec)         #    0.994 CPUs utilized          
                 8      context-switches          #    0.125 K/sec                  
                 1      cpu-migrations            #    0.016 K/sec                  
               253      page-faults               #    0.004 M/sec                  
       197,444,004      cycles                    #    3.090 GHz                    
        62,702,646      stalled-cycles-frontend   #   31.76% frontend cycles idle   
       334,988,536      instructions              #    1.70  insn per cycle         
                                                  #    0.19  stalled cycles per insn
        93,945,246      branches                  # 1470.473 M/sec                  
            59,072      branch-misses             #    0.06% of all branches        

       0.064274509 seconds time elapsed


real	0m0.980s
user	0m0.960s
sys	0m0.020s

 Performance counter stats for './__run 3':

        981.439480      task-clock (msec)         #    0.999 CPUs utilized          
                 9      context-switches          #    0.009 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
               256      page-faults               #    0.261 K/sec                  
     3,035,419,015      cycles                    #    3.093 GHz                    
       867,959,738      stalled-cycles-frontend   #   28.59% frontend cycles idle   
     5,419,875,130      instructions              #    1.79  insn per cycle         
                                                  #    0.16  stalled cycles per insn
     1,526,201,300      branches                  # 1555.064 M/sec                  
           228,896      branch-misses             #    0.01% of all branches        

       0.982014925 seconds time elapsed


real	0m0.925s
user	0m0.917s
sys	0m0.008s

 Performance counter stats for './__run 3':

        927.019429      task-clock (msec)         #    0.999 CPUs utilized          
                20      context-switches          #    0.022 K/sec                  
                 2      cpu-migrations            #    0.002 K/sec                  
               252      page-faults               #    0.272 K/sec                  
     3,019,249,221      cycles                    #    3.257 GHz                    
       841,151,955      stalled-cycles-frontend   #   27.86% frontend cycles idle   
     5,419,888,842      instructions              #    1.80  insn per cycle         
                                                  #    0.16  stalled cycles per insn
     1,526,203,988      branches                  # 1646.356 M/sec                  
           224,709      branch-misses             #    0.01% of all branches        

       0.927726744 seconds time elapsed


real	0m0.926s
user	0m0.894s
sys	0m0.032s

 Performance counter stats for './__run 3':

        928.482335      task-clock (msec)         #    0.999 CPUs utilized          
                 2      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.274 K/sec                  
     3,017,334,157      cycles                    #    3.250 GHz                    
       838,565,640      stalled-cycles-frontend   #   27.79% frontend cycles idle   
     5,420,944,250      instructions              #    1.80  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     1,526,376,583      branches                  # 1643.948 M/sec                  
           219,603      branch-misses             #    0.01% of all branches        

       0.928992322 seconds time elapsed


real	0m0.539s
user	0m0.535s
sys	0m0.004s

 Performance counter stats for './__run 4':

        541.206142      task-clock (msec)         #    0.999 CPUs utilized          
                 9      context-switches          #    0.017 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               256      page-faults               #    0.473 K/sec                  
     1,743,820,827      cycles                    #    3.222 GHz                    
       482,601,044      stalled-cycles-frontend   #   27.67% frontend cycles idle   
     3,125,120,860      instructions              #    1.79  insn per cycle         
                                                  #    0.15  stalled cycles per insn
       879,818,978      branches                  # 1625.663 M/sec                  
           151,715      branch-misses             #    0.02% of all branches        

       0.541808814 seconds time elapsed


real	0m0.541s
user	0m0.525s
sys	0m0.016s

 Performance counter stats for './__run 4':

        543.447806      task-clock (msec)         #    0.999 CPUs utilized          
                 2      context-switches          #    0.004 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               255      page-faults               #    0.469 K/sec                  
     1,748,312,452      cycles                    #    3.217 GHz                    
       492,168,043      stalled-cycles-frontend   #   28.15% frontend cycles idle   
     3,124,981,566      instructions              #    1.79  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       879,795,546      branches                  # 1618.915 M/sec                  
           149,503      branch-misses             #    0.02% of all branches        

       0.543895161 seconds time elapsed


real	0m0.540s
user	0m0.535s
sys	0m0.004s

 Performance counter stats for './__run 4':

        542.008859      task-clock (msec)         #    0.999 CPUs utilized          
                 8      context-switches          #    0.015 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.470 K/sec                  
     1,745,035,892      cycles                    #    3.220 GHz                    
       489,026,060      stalled-cycles-frontend   #   28.02% frontend cycles idle   
     3,125,104,436      instructions              #    1.79  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       879,817,465      branches                  # 1623.253 M/sec                  
           152,709      branch-misses             #    0.02% of all branches        

       0.542617367 seconds time elapsed


real	0m0.531s
user	0m0.527s
sys	0m0.004s

 Performance counter stats for './__run 5':

        533.701361      task-clock (msec)         #    0.999 CPUs utilized          
                 3      context-switches          #    0.006 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               256      page-faults               #    0.480 K/sec                  
     1,694,490,212      cycles                    #    3.175 GHz                    
       484,742,166      stalled-cycles-frontend   #   28.61% frontend cycles idle   
     3,018,838,455      instructions              #    1.78  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       849,742,790      branches                  # 1592.169 M/sec                  
           148,275      branch-misses             #    0.02% of all branches        

       0.534153352 seconds time elapsed


real	0m0.518s
user	0m0.506s
sys	0m0.012s

 Performance counter stats for './__run 5':

        520.430718      task-clock (msec)         #    0.999 CPUs utilized          
                 2      context-switches          #    0.004 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               254      page-faults               #    0.488 K/sec                  
     1,681,839,946      cycles                    #    3.232 GHz                    
       472,108,971      stalled-cycles-frontend   #   28.07% frontend cycles idle   
     3,017,594,701      instructions              #    1.79  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       849,533,728      branches                  # 1632.367 M/sec                  
           145,414      branch-misses             #    0.02% of all branches        

       0.520924259 seconds time elapsed


real	0m0.518s
user	0m0.514s
sys	0m0.004s

 Performance counter stats for './__run 5':

        520.258765      task-clock (msec)         #    0.999 CPUs utilized          
                13      context-switches          #    0.025 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.488 K/sec                  
     1,684,676,750      cycles                    #    3.238 GHz                    
       475,528,456      stalled-cycles-frontend   #   28.23% frontend cycles idle   
     3,017,740,083      instructions              #    1.79  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       849,559,660      branches                  # 1632.956 M/sec                  
           148,587      branch-misses             #    0.02% of all branches        

       0.520911959 seconds time elapsed


real	0m0.218s
user	0m0.206s
sys	0m0.012s

 Performance counter stats for './__run 6':

        221.187555      task-clock (msec)         #    0.998 CPUs utilized          
                 1      context-switches          #    0.005 K/sec                  
                 1      cpu-migrations            #    0.005 K/sec                  
               256      page-faults               #    0.001 M/sec                  
       700,656,084      cycles                    #    3.168 GHz                    
       202,382,971      stalled-cycles-frontend   #   28.88% frontend cycles idle   
     1,239,216,785      instructions              #    1.77  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       348,600,472      branches                  # 1576.040 M/sec                  
            89,835      branch-misses             #    0.03% of all branches        

       0.221535685 seconds time elapsed


real	0m0.221s
user	0m0.213s
sys	0m0.008s

 Performance counter stats for './__run 6':

        223.940528      task-clock (msec)         #    0.998 CPUs utilized          
                 4      context-switches          #    0.018 K/sec                  
                 1      cpu-migrations            #    0.004 K/sec                  
               254      page-faults               #    0.001 M/sec                  
       699,272,680      cycles                    #    3.123 GHz                    
       201,929,600      stalled-cycles-frontend   #   28.88% frontend cycles idle   
     1,239,280,570      instructions              #    1.77  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       348,611,396      branches                  # 1556.714 M/sec                  
            91,131      branch-misses             #    0.03% of all branches        

       0.224303071 seconds time elapsed


real	0m0.222s
user	0m0.205s
sys	0m0.016s

 Performance counter stats for './__run 6':

        224.197626      task-clock (msec)         #    0.998 CPUs utilized          
                 3      context-switches          #    0.013 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.001 M/sec                  
       704,114,243      cycles                    #    3.141 GHz                    
       204,495,644      stalled-cycles-frontend   #   29.04% frontend cycles idle   
     1,239,195,884      instructions              #    1.76  insn per cycle         
                                                  #    0.17  stalled cycles per insn
       348,597,626      branches                  # 1554.868 M/sec                  
            92,983      branch-misses             #    0.03% of all branches        

       0.224649118 seconds time elapsed


real	0m0.462s
user	0m0.446s
sys	0m0.016s

 Performance counter stats for './__run 7':

        464.391798      task-clock (msec)         #    0.999 CPUs utilized          
                 6      context-switches          #    0.013 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               257      page-faults               #    0.553 K/sec                  
     1,470,917,652      cycles                    #    3.167 GHz                    
       416,414,296      stalled-cycles-frontend   #   28.31% frontend cycles idle   
     2,630,827,276      instructions              #    1.79  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       740,551,275      branches                  # 1594.669 M/sec                  
           139,987      branch-misses             #    0.02% of all branches        

       0.464933951 seconds time elapsed


real	0m0.463s
user	0m0.458s
sys	0m0.004s

 Performance counter stats for './__run 7':

        465.448794      task-clock (msec)         #    0.999 CPUs utilized          
                 3      context-switches          #    0.006 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.544 K/sec                  
     1,470,650,737      cycles                    #    3.160 GHz                    
       419,403,799      stalled-cycles-frontend   #   28.52% frontend cycles idle   
     2,630,739,029      instructions              #    1.79  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       740,537,675      branches                  # 1591.019 M/sec                  
           138,028      branch-misses             #    0.02% of all branches        

       0.465968062 seconds time elapsed


real	0m0.464s
user	0m0.452s
sys	0m0.012s

 Performance counter stats for './__run 7':

        467.243512      task-clock (msec)         #    0.999 CPUs utilized          
                 4      context-switches          #    0.009 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.546 K/sec                  
     1,470,081,340      cycles                    #    3.146 GHz                    
       410,900,088      stalled-cycles-frontend   #   27.95% frontend cycles idle   
     2,630,768,554      instructions              #    1.79  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       740,540,979      branches                  # 1584.914 M/sec                  
           138,520      branch-misses             #    0.02% of all branches        

       0.467762172 seconds time elapsed


real	0m0.876s
user	0m0.856s
sys	0m0.020s

 Performance counter stats for './__run 8':

        879.101894      task-clock (msec)         #    0.999 CPUs utilized          
                18      context-switches          #    0.020 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.290 K/sec                  
     2,848,403,036      cycles                    #    3.240 GHz                    
       788,941,153      stalled-cycles-frontend   #   27.70% frontend cycles idle   
     5,133,884,841      instructions              #    1.80  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     1,445,584,494      branches                  # 1644.388 M/sec                  
           217,096      branch-misses             #    0.02% of all branches        

       0.879988791 seconds time elapsed


real	0m0.877s
user	0m0.864s
sys	0m0.012s

 Performance counter stats for './__run 8':

        879.424373      task-clock (msec)         #    0.999 CPUs utilized          
                10      context-switches          #    0.011 K/sec                  
                 3      cpu-migrations            #    0.003 K/sec                  
               253      page-faults               #    0.288 K/sec                  
     2,846,299,613      cycles                    #    3.237 GHz                    
       780,363,992      stalled-cycles-frontend   #   27.42% frontend cycles idle   
     5,133,852,057      instructions              #    1.80  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     1,445,575,297      branches                  # 1643.774 M/sec                  
           213,092      branch-misses             #    0.01% of all branches        

       0.880302629 seconds time elapsed


real	0m0.872s
user	0m0.864s
sys	0m0.008s

 Performance counter stats for './__run 8':

        874.625666      task-clock (msec)         #    0.999 CPUs utilized          
                 7      context-switches          #    0.008 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               256      page-faults               #    0.293 K/sec                  
     2,845,427,721      cycles                    #    3.253 GHz                    
       786,123,297      stalled-cycles-frontend   #   27.63% frontend cycles idle   
     5,133,859,992      instructions              #    1.80  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     1,445,578,977      branches                  # 1652.797 M/sec                  
           211,491      branch-misses             #    0.01% of all branches        

       0.875271232 seconds time elapsed


real	0m3.652s
user	0m3.579s
sys	0m0.072s

 Performance counter stats for './__run 9':

       3654.149980      task-clock (msec)         #    1.000 CPUs utilized          
                23      context-switches          #    0.006 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.070 K/sec                  
    11,850,413,639      cycles                    #    3.243 GHz                    
     3,251,303,474      stalled-cycles-frontend   #   27.44% frontend cycles idle   
    21,434,948,234      instructions              #    1.81  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     6,037,153,676      branches                  # 1652.136 M/sec                  
           727,230      branch-misses             #    0.01% of all branches        

       3.655611797 seconds time elapsed


real	0m3.664s
user	0m3.610s
sys	0m0.052s

 Performance counter stats for './__run 9':

       3665.632845      task-clock (msec)         #    1.000 CPUs utilized          
                 4      context-switches          #    0.001 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.070 K/sec                  
    11,888,271,960      cycles                    #    3.243 GHz                    
     3,295,200,933      stalled-cycles-frontend   #   27.72% frontend cycles idle   
    21,438,753,140      instructions              #    1.80  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     6,037,788,628      branches                  # 1647.134 M/sec                  
           731,116      branch-misses             #    0.01% of all branches        

       3.666862438 seconds time elapsed


real	0m3.623s
user	0m3.550s
sys	0m0.072s

 Performance counter stats for './__run 9':

       3625.660153      task-clock (msec)         #    1.000 CPUs utilized          
                16      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.070 K/sec                  
    11,865,977,285      cycles                    #    3.273 GHz                    
     3,264,768,789      stalled-cycles-frontend   #   27.51% frontend cycles idle   
    21,434,953,088      instructions              #    1.81  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     6,037,152,149      branches                  # 1665.118 M/sec                  
           732,453      branch-misses             #    0.01% of all branches        

       3.627012288 seconds time elapsed


real	0m1.908s
user	0m1.871s
sys	0m0.036s

 Performance counter stats for './__run 10':

       1910.065855      task-clock (msec)         #    0.999 CPUs utilized          
                16      context-switches          #    0.008 K/sec                  
                 3      cpu-migrations            #    0.002 K/sec                  
               252      page-faults               #    0.132 K/sec                  
     6,242,796,095      cycles                    #    3.268 GHz                    
     1,702,586,062      stalled-cycles-frontend   #   27.27% frontend cycles idle   
    11,272,931,947      instructions              #    1.81  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     3,174,784,012      branches                  # 1662.133 M/sec                  
           409,047      branch-misses             #    0.01% of all branches        

       1.911077879 seconds time elapsed


real	0m1.914s
user	0m1.890s
sys	0m0.024s

 Performance counter stats for './__run 10':

       1916.902439      task-clock (msec)         #    1.000 CPUs utilized          
                 3      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.132 K/sec                  
     6,255,372,517      cycles                    #    3.263 GHz                    
     1,755,214,390      stalled-cycles-frontend   #   28.06% frontend cycles idle   
    11,275,444,280      instructions              #    1.80  insn per cycle         
                                                  #    0.16  stalled cycles per insn
     3,175,201,973      branches                  # 1656.423 M/sec                  
           407,852      branch-misses             #    0.01% of all branches        

       1.917733230 seconds time elapsed


real	0m1.920s
user	0m1.899s
sys	0m0.020s

 Performance counter stats for './__run 10':

       1922.662575      task-clock (msec)         #    1.000 CPUs utilized          
                12      context-switches          #    0.006 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.133 K/sec                  
     6,255,650,587      cycles                    #    3.254 GHz                    
     1,746,266,311      stalled-cycles-frontend   #   27.92% frontend cycles idle   
    11,273,047,902      instructions              #    1.80  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     3,174,803,394      branches                  # 1651.254 M/sec                  
           420,947      branch-misses             #    0.01% of all branches        

       1.923602117 seconds time elapsed

