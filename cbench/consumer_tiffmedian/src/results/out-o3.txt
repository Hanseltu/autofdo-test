
real	0m5.124s
user	0m4.442s
sys	0m0.549s

 Performance counter stats for './__run 1':

       4990.692256      task-clock (msec)         #    0.973 CPUs utilized          
             3,273      context-switches          #    0.656 K/sec                  
                 5      cpu-migrations            #    0.001 K/sec                  
            33,964      page-faults               #    0.007 M/sec                  
    16,116,335,219      cycles                    #    3.229 GHz                    
     5,311,945,630      stalled-cycles-frontend   #   32.96% frontend cycles idle   
    28,719,315,988      instructions              #    1.78  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     4,906,541,559      branches                  #  983.138 M/sec                  
       191,067,542      branch-misses             #    3.89% of all branches        

       5.126881724 seconds time elapsed


real	0m5.426s
user	0m2.943s
sys	0m0.416s

 Performance counter stats for './__run 2':

       3359.014460      task-clock (msec)         #    0.619 CPUs utilized          
             1,596      context-switches          #    0.475 K/sec                  
                21      cpu-migrations            #    0.006 K/sec                  
            22,382      page-faults               #    0.007 M/sec                  
    10,692,658,988      cycles                    #    3.183 GHz                    
     3,618,768,716      stalled-cycles-frontend   #   33.84% frontend cycles idle   
    18,818,870,949      instructions              #    1.76  insn per cycle         
                                                  #    0.19  stalled cycles per insn
     3,215,018,447      branches                  #  957.131 M/sec                  
       125,316,571      branch-misses             #    3.90% of all branches        

       5.428635064 seconds time elapsed


real	0m0.135s
user	0m0.119s
sys	0m0.016s

 Performance counter stats for './__run 3':

        136.711200      task-clock (msec)         #    0.997 CPUs utilized          
                 5      context-switches          #    0.037 K/sec                  
                 1      cpu-migrations            #    0.007 K/sec                  
             1,108      page-faults               #    0.008 M/sec                  
       414,864,447      cycles                    #    3.035 GHz                    
       154,146,656      stalled-cycles-frontend   #   37.16% frontend cycles idle   
       694,393,796      instructions              #    1.67  insn per cycle         
                                                  #    0.22  stalled cycles per insn
       118,772,200      branches                  #  868.782 M/sec                  
         4,629,588      branch-misses             #    3.90% of all branches        

       0.137172544 seconds time elapsed


real	0m1.821s
user	0m1.492s
sys	0m0.288s

 Performance counter stats for './__run 4':

       1782.364033      task-clock (msec)         #    0.977 CPUs utilized          
             1,029      context-switches          #    0.577 K/sec                  
                 6      cpu-migrations            #    0.003 K/sec                  
            11,949      page-faults               #    0.007 M/sec                  
     5,707,845,141      cycles                    #    3.202 GHz                    
     1,974,739,449      stalled-cycles-frontend   #   34.60% frontend cycles idle   
     9,932,975,692      instructions              #    1.74  insn per cycle         
                                                  #    0.20  stalled cycles per insn
     1,697,098,171      branches                  #  952.161 M/sec                  
        66,171,726      branch-misses             #    3.90% of all branches        

       1.823756191 seconds time elapsed


real	0m4.298s
user	0m3.548s
sys	0m0.548s

 Performance counter stats for './__run 5':

       4098.926038      task-clock (msec)         #    0.953 CPUs utilized          
             2,662      context-switches          #    0.649 K/sec                  
                 6      cpu-migrations            #    0.001 K/sec                  
            27,101      page-faults               #    0.007 M/sec                  
    13,138,335,946      cycles                    #    3.205 GHz                    
     4,551,499,583      stalled-cycles-frontend   #   34.64% frontend cycles idle   
    22,854,278,497      instructions              #    1.74  insn per cycle         
                                                  #    0.20  stalled cycles per insn
     3,904,611,169      branches                  #  952.594 M/sec                  
       152,361,815      branch-misses             #    3.90% of all branches        

       4.301721201 seconds time elapsed


real	0m4.830s
user	0m3.573s
sys	0m0.454s

 Performance counter stats for './__run 6':

       4027.294651      task-clock (msec)         #    0.833 CPUs utilized          
             2,392      context-switches          #    0.594 K/sec                  
                13      cpu-migrations            #    0.003 K/sec                  
            26,341      page-faults               #    0.007 M/sec                  
    12,832,902,958      cycles                    #    3.186 GHz                    
     4,495,412,481      stalled-cycles-frontend   #   35.03% frontend cycles idle   
    22,199,896,483      instructions              #    1.73  insn per cycle         
                                                  #    0.20  stalled cycles per insn
     3,792,777,499      branches                  #  941.768 M/sec                  
       148,188,025      branch-misses             #    3.91% of all branches        

       4.833751906 seconds time elapsed


real	0m1.985s
user	0m1.717s
sys	0m0.256s

 Performance counter stats for './__run 7':

       1974.833066      task-clock (msec)         #    0.994 CPUs utilized          
             1,193      context-switches          #    0.604 K/sec                  
                 3      cpu-migrations            #    0.002 K/sec                  
            13,635      page-faults               #    0.007 M/sec                  
     6,368,807,486      cycles                    #    3.225 GHz                    
     2,093,089,260      stalled-cycles-frontend   #   32.86% frontend cycles idle   
    11,368,350,372      instructions              #    1.79  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     1,942,329,103      branches                  #  983.541 M/sec                  
        75,669,783      branch-misses             #    3.90% of all branches        

       1.987679159 seconds time elapsed


real	0m2.854s
user	0m2.461s
sys	0m0.275s

 Performance counter stats for './__run 8':

       2737.115542      task-clock (msec)         #    0.958 CPUs utilized          
             1,621      context-switches          #    0.592 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
            19,082      page-faults               #    0.007 M/sec                  
     8,865,393,463      cycles                    #    3.239 GHz                    
     2,842,962,153      stalled-cycles-frontend   #   32.07% frontend cycles idle   
    16,009,537,216      instructions              #    1.81  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     2,735,191,208      branches                  #  999.297 M/sec                  
       106,419,827      branch-misses             #    3.89% of all branches        

       2.857081030 seconds time elapsed


real	0m2.586s
user	0m2.296s
sys	0m0.277s

 Performance counter stats for './__run 9':

       2574.180061      task-clock (msec)         #    0.994 CPUs utilized          
             1,477      context-switches          #    0.574 K/sec                  
                 5      cpu-migrations            #    0.002 K/sec                  
            18,105      page-faults               #    0.007 M/sec                  
     8,345,181,160      cycles                    #    3.242 GHz                    
     2,631,231,524      stalled-cycles-frontend   #   31.53% frontend cycles idle   
    15,177,839,392      instructions              #    1.82  insn per cycle         
                                                  #    0.17  stalled cycles per insn
     2,593,116,718      branches                  # 1007.356 M/sec                  
       100,837,653      branch-misses             #    3.89% of all branches        

       2.588765496 seconds time elapsed


real	0m3.809s
user	0m3.246s
sys	0m0.437s

 Performance counter stats for './__run 10':

       3682.447681      task-clock (msec)         #    0.966 CPUs utilized          
             2,760      context-switches          #    0.750 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            25,618      page-faults               #    0.007 M/sec                  
    11,918,900,131      cycles                    #    3.237 GHz                    
     3,792,765,462      stalled-cycles-frontend   #   31.82% frontend cycles idle   
    21,595,203,508      instructions              #    1.81  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     3,689,609,384      branches                  # 1001.945 M/sec                  
       143,595,493      branch-misses             #    3.89% of all branches        

       3.813094719 seconds time elapsed

