#!/bin/bash

echo "This is a test in autofdo with cbenchmark"

#: << 'COMMENT'

echo "Start pass in O2"
rm results/*
gcc -O2 -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o2.txt
#sudo perf stat ./__run $var 2>>results/out-o2.txt
#sudo perf stat ./__run $var 2>>results/out-o2.txt
done
echo "End pass in O2"


echo "Start pass in O3"
gcc -O3 -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o3.txt
#sudo perf stat ./__run $var 2>>results/out-o3.txt
#sudo perf stat ./__run $var 2>>results/out-o3.txt
done
echo "End pass in O3"


echo "Start pass in O2+fdo"
gcc -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -fprofile-generate
./a.out -fps -c ../../telecom_gsm_data/1.au > output_large.encode.gsm
gcc -O2 -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -fprofile-use=*.gcda -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o2fdo.txt
#sudo perf stat ./__run $var 2>>results/out-o2fdo.txt
#sudo perf stat ./__run $var 2>>results/out-o2fdo.txt
done
echo "End pass in O2+fdo"


echo "Start pass in O3+fdo"
gcc -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -fprofile-generate
./a.out -fps -c ../../telecom_gsm_data/1.au > output_large.encode.gsm
gcc -O3 -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -fprofile-use=*.gcda -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o3fdo.txt
#sudo perf stat ./__run $var 2>>results/out-o3fdo.txt
#sudo perf stat ./__run $var 2>>results/out-o3fdo.txt
done
echo "End pass in O3+fdo"

#COMMENT

echo "Start pass in O2+autofdo"
gcc -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -lm
sudo ~/github/AutoFDO-Test/pmu-tools/ocperf.py record -b -e br_inst_retired.near_taken:pp -- ./a.out -fps -c ../../telecom_gsm_data/1.au > output_large.encode.gsm
sudo ~/github/AutoFDO-Test/autofdo/create_gcov --binary=./a.out --profile=perf.data --gcov=a.gcov -gcov_version=1
gcc -O2 -fauto-profile=a.gcov -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -lm
#gcc -O2 -fauto-profile=a.gcov *.c -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o2autofdo.txt
#sudo perf stat ./__run $var 2>>results/out-o2autofdo.txt
#sudo perf stat ./__run $var 2>>results/out-o2autofdo.txt
done
echo "End pass in O2+autofdo"

#: << 'COMMENT'

echo "Start pass in O3+autofdo"
gcc -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -lm
sudo ~/github/AutoFDO-Test/pmu-tools/ocperf.py record -b -e br_inst_retired.near_taken:pp -- ./a.out -fps -c ../../telecom_gsm_data/1.au > output_large.encode.gsm
sudo ~/github/AutoFDO-Test/autofdo/create_gcov --binary=./a.out --profile=perf.data --gcov=a.gcov -gcov_version=1
gcc -O3 -fauto-profile=a.gcov -DSASR -DSTUPID_COMPILER -DNeedFunctionPrototypes=1 *.c -lm
for var in 1 2 3 4 5 6 7 8 9 10
#for var in 1
do
sudo perf stat ./__run $var 2>>results/out-o3autofdo.txt
#sudo perf stat ./__run $var 2>>results/out-o3autofdo.txt
#sudo perf stat ./__run $var 2>>results/out-o3autofdo.txt
done
echo "End pass in O3+autofdo"

#COMMENT
