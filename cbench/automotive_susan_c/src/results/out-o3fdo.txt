
real	0m4.958s
user	0m3.751s
sys	0m0.541s

 Performance counter stats for './__run 1':

       4291.630559      task-clock (msec)         #    0.865 CPUs utilized          
               783      context-switches          #    0.182 K/sec                  
                18      cpu-migrations            #    0.004 K/sec                  
           188,606      page-faults               #    0.044 M/sec                  
    13,647,696,150      cycles                    #    3.180 GHz                    
     4,407,060,034      stalled-cycles-frontend   #   32.29% frontend cycles idle   
    32,101,149,448      instructions              #    2.35  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     1,411,324,166      branches                  #  328.855 M/sec                  
        52,868,794      branch-misses             #    3.75% of all branches        

       4.961028689 seconds time elapsed


real	0m5.744s
user	0m3.737s
sys	0m0.576s

 Performance counter stats for './__run 1':

       4314.028278      task-clock (msec)         #    0.751 CPUs utilized          
               782      context-switches          #    0.181 K/sec                  
                23      cpu-migrations            #    0.005 K/sec                  
           188,606      page-faults               #    0.044 M/sec                  
    13,669,272,707      cycles                    #    3.169 GHz                    
     4,428,591,533      stalled-cycles-frontend   #   32.40% frontend cycles idle   
    32,103,214,341      instructions              #    2.35  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     1,412,194,892      branches                  #  327.349 M/sec                  
        52,894,680      branch-misses             #    3.75% of all branches        

       5.746437755 seconds time elapsed


real	0m5.282s
user	0m3.703s
sys	0m0.553s

 Performance counter stats for './__run 1':

       4255.950530      task-clock (msec)         #    0.805 CPUs utilized          
               800      context-switches          #    0.188 K/sec                  
                21      cpu-migrations            #    0.005 K/sec                  
           188,602      page-faults               #    0.044 M/sec                  
    13,542,255,792      cycles                    #    3.182 GHz                    
     4,306,101,826      stalled-cycles-frontend   #   31.80% frontend cycles idle   
    32,099,179,661      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     1,411,006,441      branches                  #  331.537 M/sec                  
        52,930,857      branch-misses             #    3.75% of all branches        

       5.284500450 seconds time elapsed


real	0m4.922s
user	0m3.375s
sys	0m0.459s

 Performance counter stats for './__run 2':

       3834.968868      task-clock (msec)         #    0.779 CPUs utilized          
               578      context-switches          #    0.151 K/sec                  
                35      cpu-migrations            #    0.009 K/sec                  
           166,769      page-faults               #    0.043 M/sec                  
    12,060,780,854      cycles                    #    3.145 GHz                    
     3,891,748,447      stalled-cycles-frontend   #   32.27% frontend cycles idle   
    28,369,745,986      instructions              #    2.35  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     1,247,771,908      branches                  #  325.367 M/sec                  
        46,854,945      branch-misses             #    3.76% of all branches        

       4.924889738 seconds time elapsed


real	0m4.792s
user	0m3.386s
sys	0m0.367s

 Performance counter stats for './__run 2':

       3753.842419      task-clock (msec)         #    0.783 CPUs utilized          
               625      context-switches          #    0.166 K/sec                  
                21      cpu-migrations            #    0.006 K/sec                  
           166,770      page-faults               #    0.044 M/sec                  
    11,890,608,360      cycles                    #    3.168 GHz                    
     3,731,727,008      stalled-cycles-frontend   #   31.38% frontend cycles idle   
    28,362,254,757      instructions              #    2.39  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     1,246,602,566      branches                  #  332.087 M/sec                  
        46,717,324      branch-misses             #    3.75% of all branches        

       4.794582965 seconds time elapsed


real	0m4.738s
user	0m3.201s
sys	0m0.557s

 Performance counter stats for './__run 2':

       3757.490394      task-clock (msec)         #    0.793 CPUs utilized          
               646      context-switches          #    0.172 K/sec                  
                17      cpu-migrations            #    0.005 K/sec                  
           166,768      page-faults               #    0.044 M/sec                  
    11,944,931,091      cycles                    #    3.179 GHz                    
     3,776,487,190      stalled-cycles-frontend   #   31.62% frontend cycles idle   
    28,361,384,041      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     1,246,214,126      branches                  #  331.661 M/sec                  
        46,664,236      branch-misses             #    3.74% of all branches        

       4.740233149 seconds time elapsed


real	0m1.334s
user	0m1.051s
sys	0m0.116s

 Performance counter stats for './__run 3':

       1168.300492      task-clock (msec)         #    0.874 CPUs utilized          
               193      context-switches          #    0.165 K/sec                  
                 2      cpu-migrations            #    0.002 K/sec                  
            53,009      page-faults               #    0.045 M/sec                  
     3,727,218,758      cycles                    #    3.190 GHz                    
     1,160,584,527      stalled-cycles-frontend   #   31.14% frontend cycles idle   
     8,911,475,137      instructions              #    2.39  insn per cycle         
                                                  #    0.13  stalled cycles per insn
       392,381,488      branches                  #  335.857 M/sec                  
        14,680,852      branch-misses             #    3.74% of all branches        

       1.336093629 seconds time elapsed


real	0m1.311s
user	0m1.035s
sys	0m0.157s

 Performance counter stats for './__run 3':

       1193.942331      task-clock (msec)         #    0.909 CPUs utilized          
               211      context-switches          #    0.177 K/sec                  
                 5      cpu-migrations            #    0.004 K/sec                  
            53,006      page-faults               #    0.044 M/sec                  
     3,766,038,373      cycles                    #    3.154 GHz                    
     1,200,581,428      stalled-cycles-frontend   #   31.88% frontend cycles idle   
     8,911,501,127      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
       392,304,092      branches                  #  328.579 M/sec                  
        14,684,778      branch-misses             #    3.74% of all branches        

       1.313908153 seconds time elapsed


real	0m1.433s
user	0m1.057s
sys	0m0.153s

 Performance counter stats for './__run 3':

       1211.682335      task-clock (msec)         #    0.843 CPUs utilized          
               199      context-switches          #    0.164 K/sec                  
                 9      cpu-migrations            #    0.007 K/sec                  
            53,009      page-faults               #    0.044 M/sec                  
     3,814,215,678      cycles                    #    3.148 GHz                    
     1,247,290,538      stalled-cycles-frontend   #   32.70% frontend cycles idle   
     8,912,480,335      instructions              #    2.34  insn per cycle         
                                                  #    0.14  stalled cycles per insn
       392,690,099      branches                  #  324.087 M/sec                  
        14,729,794      branch-misses             #    3.75% of all branches        

       1.436699830 seconds time elapsed


real	0m18.513s
user	0m13.086s
sys	0m1.834s

 Performance counter stats for './__run 4':

      14909.502388      task-clock (msec)         #    0.805 CPUs utilized          
             2,712      context-switches          #    0.182 K/sec                  
                80      cpu-migrations            #    0.005 K/sec                  
           658,527      page-faults               #    0.044 M/sec                  
    47,419,093,109      cycles                    #    3.180 GHz                    
    15,070,296,864      stalled-cycles-frontend   #   31.78% frontend cycles idle   
   112,449,973,851      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     4,940,042,348      branches                  #  331.335 M/sec                  
       185,127,372      branch-misses             #    3.75% of all branches        

      18.515674127 seconds time elapsed


real	0m19.029s
user	0m12.920s
sys	0m2.019s

 Performance counter stats for './__run 4':

      14930.102750      task-clock (msec)         #    0.784 CPUs utilized          
             2,672      context-switches          #    0.179 K/sec                  
                78      cpu-migrations            #    0.005 K/sec                  
           658,527      page-faults               #    0.044 M/sec                  
    47,397,628,350      cycles                    #    3.175 GHz                    
    15,049,385,002      stalled-cycles-frontend   #   31.75% frontend cycles idle   
   112,448,943,378      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     4,939,727,646      branches                  #  330.857 M/sec                  
       185,316,883      branch-misses             #    3.75% of all branches        

      19.031384161 seconds time elapsed


real	0m19.912s
user	0m13.710s
sys	0m1.922s

 Performance counter stats for './__run 4':

      15623.985612      task-clock (msec)         #    0.785 CPUs utilized          
             2,679      context-switches          #    0.171 K/sec                  
               112      cpu-migrations            #    0.007 K/sec                  
           658,528      page-faults               #    0.042 M/sec                  
    49,306,498,293      cycles                    #    3.156 GHz                    
    16,780,754,141      stalled-cycles-frontend   #   34.03% frontend cycles idle   
   112,445,646,707      instructions              #    2.28  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     4,938,783,649      branches                  #  316.103 M/sec                  
       185,213,023      branch-misses             #    3.75% of all branches        

      19.914325740 seconds time elapsed


real	0m2.185s
user	0m1.500s
sys	0m0.264s

 Performance counter stats for './__run 5':

       1765.404188      task-clock (msec)         #    0.807 CPUs utilized          
               312      context-switches          #    0.177 K/sec                  
                19      cpu-migrations            #    0.011 K/sec                  
            76,286      page-faults               #    0.043 M/sec                  
     5,530,293,626      cycles                    #    3.133 GHz                    
     1,816,814,127      stalled-cycles-frontend   #   32.85% frontend cycles idle   
    12,894,347,321      instructions              #    2.33  insn per cycle         
                                                  #    0.14  stalled cycles per insn
       567,813,638      branches                  #  321.634 M/sec                  
        21,334,386      branch-misses             #    3.76% of all branches        

       2.187149123 seconds time elapsed


real	0m2.264s
user	0m1.504s
sys	0m0.187s

 Performance counter stats for './__run 5':

       1692.423178      task-clock (msec)         #    0.747 CPUs utilized          
               245      context-switches          #    0.145 K/sec                  
                 8      cpu-migrations            #    0.005 K/sec                  
            76,286      page-faults               #    0.045 M/sec                  
     5,359,895,465      cycles                    #    3.167 GHz                    
     1,651,270,395      stalled-cycles-frontend   #   30.81% frontend cycles idle   
    12,891,634,217      instructions              #    2.41  insn per cycle         
                                                  #    0.13  stalled cycles per insn
       567,300,543      branches                  #  335.200 M/sec                  
        21,188,607      branch-misses             #    3.73% of all branches        

       2.266394637 seconds time elapsed


real	0m2.099s
user	0m1.512s
sys	0m0.225s

 Performance counter stats for './__run 5':

       1738.561224      task-clock (msec)         #    0.827 CPUs utilized          
               308      context-switches          #    0.177 K/sec                  
                 6      cpu-migrations            #    0.003 K/sec                  
            76,286      page-faults               #    0.044 M/sec                  
     5,545,506,322      cycles                    #    3.190 GHz                    
     1,833,087,337      stalled-cycles-frontend   #   33.06% frontend cycles idle   
    12,894,086,195      instructions              #    2.33  insn per cycle         
                                                  #    0.14  stalled cycles per insn
       567,593,240      branches                  #  326.473 M/sec                  
        21,341,792      branch-misses             #    3.76% of all branches        

       2.102101999 seconds time elapsed


real	0m1.597s
user	0m0.979s
sys	0m0.178s

 Performance counter stats for './__run 6':

       1159.530711      task-clock (msec)         #    0.724 CPUs utilized          
               201      context-switches          #    0.173 K/sec                  
                 2      cpu-migrations            #    0.002 K/sec                  
            52,288      page-faults               #    0.045 M/sec                  
     3,696,073,754      cycles                    #    3.188 GHz                    
     1,165,565,086      stalled-cycles-frontend   #   31.54% frontend cycles idle   
     8,789,312,603      instructions              #    2.38  insn per cycle         
                                                  #    0.13  stalled cycles per insn
       387,122,069      branches                  #  333.861 M/sec                  
        14,449,067      branch-misses             #    3.73% of all branches        

       1.601112856 seconds time elapsed


real	0m1.459s
user	0m0.984s
sys	0m0.185s

 Performance counter stats for './__run 6':

       1170.478170      task-clock (msec)         #    0.801 CPUs utilized          
               200      context-switches          #    0.171 K/sec                  
                 6      cpu-migrations            #    0.005 K/sec                  
            52,288      page-faults               #    0.045 M/sec                  
     3,716,452,912      cycles                    #    3.175 GHz                    
     1,184,724,555      stalled-cycles-frontend   #   31.88% frontend cycles idle   
     8,789,457,610      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
       387,118,373      branches                  #  330.735 M/sec                  
        14,492,592      branch-misses             #    3.74% of all branches        

       1.461625171 seconds time elapsed


real	0m1.340s
user	0m1.053s
sys	0m0.136s

 Performance counter stats for './__run 6':

       1189.556530      task-clock (msec)         #    0.887 CPUs utilized          
               203      context-switches          #    0.171 K/sec                  
                 9      cpu-migrations            #    0.008 K/sec                  
            52,287      page-faults               #    0.044 M/sec                  
     3,763,635,870      cycles                    #    3.164 GHz                    
     1,234,327,705      stalled-cycles-frontend   #   32.80% frontend cycles idle   
     8,788,019,588      instructions              #    2.33  insn per cycle         
                                                  #    0.14  stalled cycles per insn
       386,931,070      branches                  #  325.273 M/sec                  
        14,526,464      branch-misses             #    3.75% of all branches        

       1.341685263 seconds time elapsed


real	0m6.831s
user	0m4.582s
sys	0m0.702s

 Performance counter stats for './__run 7':

       5284.742626      task-clock (msec)         #    0.773 CPUs utilized          
               932      context-switches          #    0.176 K/sec                  
                39      cpu-migrations            #    0.007 K/sec                  
           232,768      page-faults               #    0.044 M/sec                  
    16,757,309,921      cycles                    #    3.171 GHz                    
     5,347,097,175      stalled-cycles-frontend   #   31.91% frontend cycles idle   
    39,648,221,561      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     1,742,102,927      branches                  #  329.648 M/sec                  
        65,345,991      branch-misses             #    3.75% of all branches        

       6.833692180 seconds time elapsed


real	0m6.251s
user	0m4.561s
sys	0m0.693s

 Performance counter stats for './__run 7':

       5253.178576      task-clock (msec)         #    0.840 CPUs utilized          
               847      context-switches          #    0.161 K/sec                  
                38      cpu-migrations            #    0.007 K/sec                  
           232,766      page-faults               #    0.044 M/sec                  
    16,626,809,344      cycles                    #    3.165 GHz                    
     5,225,648,674      stalled-cycles-frontend   #   31.43% frontend cycles idle   
    39,644,190,903      instructions              #    2.38  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     1,741,541,979      branches                  #  331.522 M/sec                  
        65,189,421      branch-misses             #    3.74% of all branches        

       6.254656536 seconds time elapsed


real	0m6.746s
user	0m4.562s
sys	0m0.736s

 Performance counter stats for './__run 7':

       5297.564163      task-clock (msec)         #    0.785 CPUs utilized          
               869      context-switches          #    0.164 K/sec                  
                34      cpu-migrations            #    0.006 K/sec                  
           232,767      page-faults               #    0.044 M/sec                  
    16,795,204,197      cycles                    #    3.170 GHz                    
     5,385,908,218      stalled-cycles-frontend   #   32.07% frontend cycles idle   
    39,650,426,418      instructions              #    2.36  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     1,743,382,759      branches                  #  329.091 M/sec                  
        65,368,664      branch-misses             #    3.75% of all branches        

       6.749580545 seconds time elapsed


real	0m24.500s
user	0m16.678s
sys	0m2.600s

 Performance counter stats for './__run 8':

      19268.911189      task-clock (msec)         #    0.786 CPUs utilized          
             3,548      context-switches          #    0.184 K/sec                  
               104      cpu-migrations            #    0.005 K/sec                  
           844,770      page-faults               #    0.044 M/sec                  
    61,233,863,243      cycles                    #    3.178 GHz                    
    19,719,032,171      stalled-cycles-frontend   #   32.20% frontend cycles idle   
   144,312,615,098      instructions              #    2.36  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     6,344,157,271      branches                  #  329.243 M/sec                  
       238,217,822      branch-misses             #    3.75% of all branches        

      24.503373436 seconds time elapsed


real	0m25.133s
user	0m16.771s
sys	0m2.487s

 Performance counter stats for './__run 8':

      19246.766853      task-clock (msec)         #    0.766 CPUs utilized          
             3,531      context-switches          #    0.183 K/sec                  
               127      cpu-migrations            #    0.007 K/sec                  
           844,767      page-faults               #    0.044 M/sec                  
    61,050,352,321      cycles                    #    3.172 GHz                    
    19,533,881,899      stalled-cycles-frontend   #   32.00% frontend cycles idle   
   144,282,708,775      instructions              #    2.36  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     6,335,881,076      branches                  #  329.192 M/sec                  
       238,317,747      branch-misses             #    3.76% of all branches        

      25.136087164 seconds time elapsed


real	0m25.014s
user	0m16.833s
sys	0m2.506s

 Performance counter stats for './__run 8':

      19329.449583      task-clock (msec)         #    0.773 CPUs utilized          
             3,484      context-switches          #    0.180 K/sec                  
               122      cpu-migrations            #    0.006 K/sec                  
           844,769      page-faults               #    0.044 M/sec                  
    61,270,744,135      cycles                    #    3.170 GHz                    
    19,743,565,194      stalled-cycles-frontend   #   32.22% frontend cycles idle   
   144,294,506,828      instructions              #    2.36  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     6,338,315,287      branches                  #  327.910 M/sec                  
       238,021,953      branch-misses             #    3.76% of all branches        

      25.017430021 seconds time elapsed


real	0m1.148s
user	0m0.717s
sys	0m0.109s

 Performance counter stats for './__run 9':

        828.702313      task-clock (msec)         #    0.720 CPUs utilized          
               131      context-switches          #    0.158 K/sec                  
                11      cpu-migrations            #    0.013 K/sec                  
            35,726      page-faults               #    0.043 M/sec                  
     2,541,701,490      cycles                    #    3.067 GHz                    
       825,473,951      stalled-cycles-frontend   #   32.48% frontend cycles idle   
     5,957,726,343      instructions              #    2.34  insn per cycle         
                                                  #    0.14  stalled cycles per insn
       262,764,676      branches                  #  317.080 M/sec                  
         9,844,175      branch-misses             #    3.75% of all branches        

       1.151273210 seconds time elapsed


real	0m1.256s
user	0m0.701s
sys	0m0.101s

 Performance counter stats for './__run 9':

        804.709747      task-clock (msec)         #    0.639 CPUs utilized          
               140      context-switches          #    0.174 K/sec                  
                 9      cpu-migrations            #    0.011 K/sec                  
            35,729      page-faults               #    0.044 M/sec                  
     2,504,898,849      cycles                    #    3.113 GHz                    
       788,811,582      stalled-cycles-frontend   #   31.49% frontend cycles idle   
     5,957,805,583      instructions              #    2.38  insn per cycle         
                                                  #    0.13  stalled cycles per insn
       262,769,973      branches                  #  326.540 M/sec                  
         9,857,868      branch-misses             #    3.75% of all branches        

       1.259227899 seconds time elapsed


real	0m0.980s
user	0m0.717s
sys	0m0.085s

 Performance counter stats for './__run 9':

        804.581196      task-clock (msec)         #    0.818 CPUs utilized          
               160      context-switches          #    0.199 K/sec                  
                 6      cpu-migrations            #    0.007 K/sec                  
            35,733      page-faults               #    0.044 M/sec                  
     2,560,004,396      cycles                    #    3.182 GHz                    
       844,136,632      stalled-cycles-frontend   #   32.97% frontend cycles idle   
     5,959,260,007      instructions              #    2.33  insn per cycle         
                                                  #    0.14  stalled cycles per insn
       263,081,677      branches                  #  326.980 M/sec                  
         9,879,227      branch-misses             #    3.76% of all branches        

       0.983222551 seconds time elapsed


real	0m17.168s
user	0m11.057s
sys	0m1.639s

 Performance counter stats for './__run 10':

      12689.710094      task-clock (msec)         #    0.739 CPUs utilized          
             2,335      context-switches          #    0.184 K/sec                  
                88      cpu-migrations            #    0.007 K/sec                  
           558,930      page-faults               #    0.044 M/sec                  
    40,190,466,442      cycles                    #    3.167 GHz                    
    12,744,068,705      stalled-cycles-frontend   #   31.71% frontend cycles idle   
    95,419,839,555      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     4,192,434,137      branches                  #  330.381 M/sec                  
       157,516,240      branch-misses             #    3.76% of all branches        

      17.171660401 seconds time elapsed


real	0m16.076s
user	0m11.166s
sys	0m1.615s

 Performance counter stats for './__run 10':

      12775.452065      task-clock (msec)         #    0.795 CPUs utilized          
             2,357      context-switches          #    0.184 K/sec                  
                84      cpu-migrations            #    0.007 K/sec                  
           558,928      page-faults               #    0.044 M/sec                  
    40,482,830,655      cycles                    #    3.169 GHz                    
    13,005,006,919      stalled-cycles-frontend   #   32.12% frontend cycles idle   
    95,426,272,102      instructions              #    2.36  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     4,194,874,860      branches                  #  328.354 M/sec                  
       157,366,370      branch-misses             #    3.75% of all branches        

      16.079085786 seconds time elapsed


real	0m16.368s
user	0m11.029s
sys	0m1.687s

 Performance counter stats for './__run 10':

      12711.938083      task-clock (msec)         #    0.776 CPUs utilized          
             2,280      context-switches          #    0.179 K/sec                  
                82      cpu-migrations            #    0.006 K/sec                  
           558,927      page-faults               #    0.044 M/sec                  
    40,315,894,774      cycles                    #    3.171 GHz                    
    12,863,760,560      stalled-cycles-frontend   #   31.91% frontend cycles idle   
    95,423,479,666      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     4,193,216,257      branches                  #  329.864 M/sec                  
       157,401,831      branch-misses             #    3.75% of all branches        

      16.371697377 seconds time elapsed


real	0m5.565s
user	0m3.677s
sys	0m0.618s

 Performance counter stats for './__run 1':

       4294.979958      task-clock (msec)         #    0.771 CPUs utilized          
               790      context-switches          #    0.184 K/sec                  
                30      cpu-migrations            #    0.007 K/sec                  
           188,603      page-faults               #    0.044 M/sec                  
    13,530,109,554      cycles                    #    3.150 GHz                    
     4,295,359,644      stalled-cycles-frontend   #   31.75% frontend cycles idle   
    32,099,459,432      instructions              #    2.37  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     1,411,229,445      branches                  #  328.576 M/sec                  
        52,922,519      branch-misses             #    3.75% of all branches        

       5.568120178 seconds time elapsed


real	0m5.595s
user	0m3.694s
sys	0m0.625s

 Performance counter stats for './__run 1':

       4318.453833      task-clock (msec)         #    0.771 CPUs utilized          
               776      context-switches          #    0.180 K/sec                  
                24      cpu-migrations            #    0.006 K/sec                  
           188,605      page-faults               #    0.044 M/sec                  
    13,722,552,186      cycles                    #    3.178 GHz                    
     4,484,942,343      stalled-cycles-frontend   #   32.68% frontend cycles idle   
    32,098,871,433      instructions              #    2.34  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     1,411,537,704      branches                  #  326.862 M/sec                  
        52,972,515      branch-misses             #    3.75% of all branches        

       5.597594215 seconds time elapsed


real	0m5.299s
user	0m3.619s
sys	0m0.667s

 Performance counter stats for './__run 1':

       4285.389306      task-clock (msec)         #    0.808 CPUs utilized          
               745      context-switches          #    0.174 K/sec                  
                28      cpu-migrations            #    0.007 K/sec                  
           188,608      page-faults               #    0.044 M/sec                  
    13,618,108,057      cycles                    #    3.178 GHz                    
     4,379,831,603      stalled-cycles-frontend   #   32.16% frontend cycles idle   
    32,097,357,951      instructions              #    2.36  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     1,410,914,836      branches                  #  329.238 M/sec                  
        52,837,813      branch-misses             #    3.74% of all branches        

       5.301704172 seconds time elapsed

