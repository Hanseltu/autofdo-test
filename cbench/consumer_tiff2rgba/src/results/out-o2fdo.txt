
real	0m26.614s
user	0m4.976s
sys	0m2.391s

 Performance counter stats for './__run 1':

       7254.766667      task-clock (msec)         #    0.273 CPUs utilized          
             6,629      context-switches          #    0.914 K/sec                  
                76      cpu-migrations            #    0.010 K/sec                  
           171,360      page-faults               #    0.024 M/sec                  
    21,295,962,458      cycles                    #    2.935 GHz                    
     8,783,649,991      stalled-cycles-frontend   #   41.25% frontend cycles idle   
    31,207,753,742      instructions              #    1.47  insn per cycle         
                                                  #    0.28  stalled cycles per insn
     5,427,510,432      branches                  #  748.130 M/sec                  
       285,947,971      branch-misses             #    5.27% of all branches        

      26.618200303 seconds time elapsed


real	0m18.306s
user	0m4.042s
sys	0m1.857s

 Performance counter stats for './__run 2':

       5793.132574      task-clock (msec)         #    0.316 CPUs utilized          
             4,529      context-switches          #    0.782 K/sec                  
                44      cpu-migrations            #    0.008 K/sec                  
           116,397      page-faults               #    0.020 M/sec                  
    14,497,192,385      cycles                    #    2.502 GHz                    
     6,004,677,855      stalled-cycles-frontend   #   41.42% frontend cycles idle   
    21,186,572,008      instructions              #    1.46  insn per cycle         
                                                  #    0.28  stalled cycles per insn
     3,684,980,454      branches                  #  636.095 M/sec                  
       193,657,016      branch-misses             #    5.26% of all branches        

      18.309241888 seconds time elapsed


real	0m0.140s
user	0m0.018s
sys	0m0.022s

 Performance counter stats for './__run 3':

         42.457830      task-clock (msec)         #    0.296 CPUs utilized          
                34      context-switches          #    0.801 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,190      page-faults               #    0.028 M/sec                  
       124,968,526      cycles                    #    2.943 GHz                    
        58,477,673      stalled-cycles-frontend   #   46.79% frontend cycles idle   
       166,674,188      instructions              #    1.33  insn per cycle         
                                                  #    0.35  stalled cycles per insn
        29,133,431      branches                  #  686.173 M/sec                  
         1,483,751      branch-misses             #    5.09% of all branches        

       0.143203719 seconds time elapsed


real	0m2.661s
user	0m0.595s
sys	0m0.264s

 Performance counter stats for './__run 4':

        846.147073      task-clock (msec)         #    0.318 CPUs utilized          
               651      context-switches          #    0.769 K/sec                  
                 8      cpu-migrations            #    0.009 K/sec                  
            16,764      page-faults               #    0.020 M/sec                  
     2,075,618,877      cycles                    #    2.453 GHz                    
       870,020,927      stalled-cycles-frontend   #   41.92% frontend cycles idle   
     3,008,596,050      instructions              #    1.45  insn per cycle         
                                                  #    0.29  stalled cycles per insn
       523,371,410      branches                  #  618.535 M/sec                  
        27,600,826      branch-misses             #    5.27% of all branches        

       2.664248765 seconds time elapsed


real	1m1.799s
user	0m11.048s
sys	0m5.313s

 Performance counter stats for './__run 5':

      16117.609346      task-clock (msec)         #    0.261 CPUs utilized          
            14,886      context-switches          #    0.924 K/sec                  
               204      cpu-migrations            #    0.013 K/sec                  
           382,713      page-faults               #    0.024 M/sec                  
    48,439,613,229      cycles                    #    3.005 GHz                    
    20,435,097,804      stalled-cycles-frontend   #   42.19% frontend cycles idle   
    69,729,149,374      instructions              #    1.44  insn per cycle         
                                                  #    0.29  stalled cycles per insn
    12,126,260,656      branches                  #  752.361 M/sec                  
       642,233,855      branch-misses             #    5.30% of all branches        

      61.801410593 seconds time elapsed


real	0m32.762s
user	0m6.211s
sys	0m3.054s

 Performance counter stats for './__run 6':

       9115.771849      task-clock (msec)         #    0.278 CPUs utilized          
             8,146      context-switches          #    0.894 K/sec                  
                87      cpu-migrations            #    0.010 K/sec                  
           209,998      page-faults               #    0.023 M/sec                  
    26,370,948,225      cycles                    #    2.893 GHz                    
    11,046,774,724      stalled-cycles-frontend   #   41.89% frontend cycles idle   
    38,234,005,590      instructions              #    1.45  insn per cycle         
                                                  #    0.29  stalled cycles per insn
     6,649,016,648      branches                  #  729.397 M/sec                  
       351,706,723      branch-misses             #    5.29% of all branches        

      32.764982869 seconds time elapsed


real	0m3.082s
user	0m0.574s
sys	0m0.236s

 Performance counter stats for './__run 7':

        798.734211      task-clock (msec)         #    0.259 CPUs utilized          
               735      context-switches          #    0.920 K/sec                  
                14      cpu-migrations            #    0.018 K/sec                  
            19,182      page-faults               #    0.024 M/sec                  
     2,408,997,477      cycles                    #    3.016 GHz                    
     1,022,706,224      stalled-cycles-frontend   #   42.45% frontend cycles idle   
     3,449,212,325      instructions              #    1.43  insn per cycle         
                                                  #    0.30  stalled cycles per insn
       599,997,486      branches                  #  751.185 M/sec                  
        31,794,186      branch-misses             #    5.30% of all branches        

       3.084552596 seconds time elapsed


real	0m10.331s
user	0m1.751s
sys	0m0.756s

 Performance counter stats for './__run 8':

       2471.537723      task-clock (msec)         #    0.239 CPUs utilized          
             2,280      context-switches          #    0.923 K/sec                  
                36      cpu-migrations            #    0.015 K/sec                  
            58,336      page-faults               #    0.024 M/sec                  
     7,324,078,854      cycles                    #    2.963 GHz                    
     3,092,042,027      stalled-cycles-frontend   #   42.22% frontend cycles idle   
    10,588,410,024      instructions              #    1.45  insn per cycle         
                                                  #    0.29  stalled cycles per insn
     1,841,532,785      branches                  #  745.096 M/sec                  
        97,501,095      branch-misses             #    5.29% of all branches        

      10.334418025 seconds time elapsed


real	0m7.701s
user	0m1.370s
sys	0m0.663s

 Performance counter stats for './__run 9':

       2007.934716      task-clock (msec)         #    0.261 CPUs utilized          
             1,933      context-switches          #    0.963 K/sec                  
                33      cpu-migrations            #    0.016 K/sec                  
            49,553      page-faults               #    0.025 M/sec                  
     6,210,721,301      cycles                    #    3.093 GHz                    
     2,604,148,424      stalled-cycles-frontend   #   41.93% frontend cycles idle   
     8,989,866,242      instructions              #    1.45  insn per cycle         
                                                  #    0.29  stalled cycles per insn
     1,563,761,536      branches                  #  778.791 M/sec                  
        83,281,255      branch-misses             #    5.33% of all branches        

       7.704600062 seconds time elapsed


real	0m14.792s
user	0m2.434s
sys	0m1.223s

 Performance counter stats for './__run 10':

       3599.996293      task-clock (msec)         #    0.243 CPUs utilized          
             3,299      context-switches          #    0.916 K/sec                  
                75      cpu-migrations            #    0.021 K/sec                  
            82,595      page-faults               #    0.023 M/sec                  
    10,313,649,607      cycles                    #    2.865 GHz                    
     4,296,724,418      stalled-cycles-frontend   #   41.66% frontend cycles idle   
    15,017,044,477      instructions              #    1.46  insn per cycle         
                                                  #    0.29  stalled cycles per insn
     2,611,810,541      branches                  #  725.504 M/sec                  
       138,439,979      branch-misses             #    5.30% of all branches        

      14.795367093 seconds time elapsed

