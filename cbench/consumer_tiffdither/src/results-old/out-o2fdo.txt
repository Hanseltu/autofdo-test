
real	0m28.798s
user	0m8.577s
sys	0m1.102s

 Performance counter stats for './__run 1':

       9614.635228      task-clock (msec)         #    0.334 CPUs utilized          
             9,310      context-switches          #    0.968 K/sec                  
             1,405      cpu-migrations            #    0.146 K/sec                  
               298      page-faults               #    0.031 K/sec                  
    29,337,645,675      cycles                    #    3.051 GHz                    
    18,231,663,526      stalled-cycles-frontend   #   62.14% frontend cycles idle   
    29,493,610,619      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     4,765,092,204      branches                  #  495.608 M/sec                  
       302,212,711      branch-misses             #    6.34% of all branches        

      28.799583968 seconds time elapsed


real	0m27.528s
user	0m8.253s
sys	0m1.018s

 Performance counter stats for './__run 1':

       9213.533577      task-clock (msec)         #    0.335 CPUs utilized          
             8,063      context-switches          #    0.875 K/sec                  
             1,328      cpu-migrations            #    0.144 K/sec                  
               298      page-faults               #    0.032 K/sec                  
    27,745,419,852      cycles                    #    3.011 GHz                    
    16,292,841,472      stalled-cycles-frontend   #   58.72% frontend cycles idle   
    29,483,020,352      instructions              #    1.06  insn per cycle         
                                                  #    0.55  stalled cycles per insn
     4,762,965,240      branches                  #  516.953 M/sec                  
       299,950,646      branch-misses             #    6.30% of all branches        

      27.532502033 seconds time elapsed


real	0m26.051s
user	0m8.166s
sys	0m1.001s

 Performance counter stats for './__run 1':

       9105.056144      task-clock (msec)         #    0.349 CPUs utilized          
             7,934      context-switches          #    0.871 K/sec                  
             1,264      cpu-migrations            #    0.139 K/sec                  
               296      page-faults               #    0.033 K/sec                  
    27,365,179,755      cycles                    #    3.005 GHz                    
    15,936,675,763      stalled-cycles-frontend   #   58.24% frontend cycles idle   
    29,483,551,572      instructions              #    1.08  insn per cycle         
                                                  #    0.54  stalled cycles per insn
     4,763,107,802      branches                  #  523.128 M/sec                  
       300,116,832      branch-misses             #    6.30% of all branches        

      26.053141422 seconds time elapsed


real	0m13.071s
user	0m4.105s
sys	0m0.507s

 Performance counter stats for './__run 2':

       4583.292998      task-clock (msec)         #    0.351 CPUs utilized          
             3,878      context-switches          #    0.846 K/sec                  
               622      cpu-migrations            #    0.136 K/sec                  
               302      page-faults               #    0.066 K/sec                  
    13,808,580,070      cycles                    #    3.013 GHz                    
     8,104,538,698      stalled-cycles-frontend   #   58.69% frontend cycles idle   
    14,742,622,285      instructions              #    1.07  insn per cycle         
                                                  #    0.55  stalled cycles per insn
     2,381,760,615      branches                  #  519.661 M/sec                  
       150,054,268      branch-misses             #    6.30% of all branches        

      13.073290803 seconds time elapsed


real	0m12.390s
user	0m4.085s
sys	0m0.511s

 Performance counter stats for './__run 2':

       4566.109906      task-clock (msec)         #    0.368 CPUs utilized          
             3,969      context-switches          #    0.869 K/sec                  
               619      cpu-migrations            #    0.136 K/sec                  
               298      page-faults               #    0.065 K/sec                  
    13,761,865,097      cycles                    #    3.014 GHz                    
     8,044,226,830      stalled-cycles-frontend   #   58.45% frontend cycles idle   
    14,743,083,084      instructions              #    1.07  insn per cycle         
                                                  #    0.55  stalled cycles per insn
     2,381,848,803      branches                  #  521.636 M/sec                  
       150,203,887      branch-misses             #    6.31% of all branches        

      12.392946398 seconds time elapsed


real	0m13.812s
user	0m4.072s
sys	0m0.455s

 Performance counter stats for './__run 2':

       4498.611901      task-clock (msec)         #    0.326 CPUs utilized          
             3,763      context-switches          #    0.836 K/sec                  
               617      cpu-migrations            #    0.137 K/sec                  
               297      page-faults               #    0.066 K/sec                  
    13,492,840,507      cycles                    #    2.999 GHz                    
     7,765,130,391      stalled-cycles-frontend   #   57.55% frontend cycles idle   
    14,741,259,967      instructions              #    1.09  insn per cycle         
                                                  #    0.53  stalled cycles per insn
     2,381,519,866      branches                  #  529.390 M/sec                  
       149,497,439      branch-misses             #    6.28% of all branches        

      13.814129383 seconds time elapsed


real	0m0.071s
user	0m0.037s
sys	0m0.000s

 Performance counter stats for './__run 3':

         40.325072      task-clock (msec)         #    0.534 CPUs utilized          
                40      context-switches          #    0.992 K/sec                  
                 9      cpu-migrations            #    0.223 K/sec                  
               301      page-faults               #    0.007 M/sec                  
       123,293,401      cycles                    #    3.057 GHz                    
        74,385,198      stalled-cycles-frontend   #   60.33% frontend cycles idle   
       123,522,198      instructions              #    1.00  insn per cycle         
                                                  #    0.60  stalled cycles per insn
        20,171,181      branches                  #  500.214 M/sec                  
         1,241,791      branch-misses             #    6.16% of all branches        

       0.075496663 seconds time elapsed


real	0m0.089s
user	0m0.025s
sys	0m0.011s

 Performance counter stats for './__run 3':

         37.626324      task-clock (msec)         #    0.412 CPUs utilized          
                38      context-switches          #    0.001 M/sec                  
                 7      cpu-migrations            #    0.186 K/sec                  
               298      page-faults               #    0.008 M/sec                  
       114,177,232      cycles                    #    3.035 GHz                    
        64,567,582      stalled-cycles-frontend   #   56.55% frontend cycles idle   
       123,312,831      instructions              #    1.08  insn per cycle         
                                                  #    0.52  stalled cycles per insn
        20,133,812      branches                  #  535.099 M/sec                  
         1,225,550      branch-misses             #    6.09% of all branches        

       0.091400172 seconds time elapsed


real	0m0.360s
user	0m0.035s
sys	0m0.005s

 Performance counter stats for './__run 3':

         41.841378      task-clock (msec)         #    0.115 CPUs utilized          
                40      context-switches          #    0.956 K/sec                  
                 5      cpu-migrations            #    0.119 K/sec                  
               296      page-faults               #    0.007 M/sec                  
       123,348,786      cycles                    #    2.948 GHz                    
        75,526,074      stalled-cycles-frontend   #   61.23% frontend cycles idle   
       123,269,987      instructions              #    1.00  insn per cycle         
                                                  #    0.61  stalled cycles per insn
        20,124,876      branches                  #  480.980 M/sec                  
         1,258,872      branch-misses             #    6.26% of all branches        

       0.362986305 seconds time elapsed


real	0m2.253s
user	0m0.837s
sys	0m0.092s

 Performance counter stats for './__run 4':

        925.878291      task-clock (msec)         #    0.410 CPUs utilized          
               737      context-switches          #    0.796 K/sec                  
               130      cpu-migrations            #    0.140 K/sec                  
               301      page-faults               #    0.325 K/sec                  
     2,785,518,563      cycles                    #    3.009 GHz                    
     1,652,240,121      stalled-cycles-frontend   #   59.32% frontend cycles idle   
     2,953,937,349      instructions              #    1.06  insn per cycle         
                                                  #    0.56  stalled cycles per insn
       477,408,013      branches                  #  515.627 M/sec                  
        30,113,328      branch-misses             #    6.31% of all branches        

       2.257295042 seconds time elapsed


real	0m3.097s
user	0m0.839s
sys	0m0.067s

 Performance counter stats for './__run 4':

        903.333983      task-clock (msec)         #    0.291 CPUs utilized          
               714      context-switches          #    0.790 K/sec                  
                93      cpu-migrations            #    0.103 K/sec                  
               299      page-faults               #    0.331 K/sec                  
     2,713,953,595      cycles                    #    3.004 GHz                    
     1,570,372,028      stalled-cycles-frontend   #   57.86% frontend cycles idle   
     2,952,144,451      instructions              #    1.09  insn per cycle         
                                                  #    0.53  stalled cycles per insn
       477,079,868      branches                  #  528.132 M/sec                  
        29,861,152      branch-misses             #    6.26% of all branches        

       3.098991598 seconds time elapsed


real	0m3.616s
user	0m0.843s
sys	0m0.074s

 Performance counter stats for './__run 4':

        912.845534      task-clock (msec)         #    0.252 CPUs utilized          
               731      context-switches          #    0.801 K/sec                  
                99      cpu-migrations            #    0.108 K/sec                  
               300      page-faults               #    0.329 K/sec                  
     2,712,659,589      cycles                    #    2.972 GHz                    
     1,569,574,393      stalled-cycles-frontend   #   57.86% frontend cycles idle   
     2,951,903,008      instructions              #    1.09  insn per cycle         
                                                  #    0.53  stalled cycles per insn
       477,036,564      branches                  #  522.582 M/sec                  
        29,976,922      branch-misses             #    6.28% of all branches        

       3.617802382 seconds time elapsed


real	1m2.760s
user	0m19.936s
sys	0m2.415s

 Performance counter stats for './__run 5':

      22208.329645      task-clock (msec)         #    0.354 CPUs utilized          
            19,854      context-switches          #    0.894 K/sec                  
             3,086      cpu-migrations            #    0.139 K/sec                  
               296      page-faults               #    0.013 K/sec                  
    67,123,761,086      cycles                    #    3.022 GHz                    
    39,791,589,828      stalled-cycles-frontend   #   59.28% frontend cycles idle   
    70,760,937,763      instructions              #    1.05  insn per cycle         
                                                  #    0.56  stalled cycles per insn
    11,431,504,859      branches                  #  514.740 M/sec                  
       722,526,216      branch-misses             #    6.32% of all branches        

      62.762901680 seconds time elapsed


real	1m1.972s
user	0m19.629s
sys	0m2.353s

 Performance counter stats for './__run 5':

      21829.410015      task-clock (msec)         #    0.352 CPUs utilized          
            19,311      context-switches          #    0.885 K/sec                  
             2,918      cpu-migrations            #    0.134 K/sec                  
               299      page-faults               #    0.014 K/sec                  
    65,758,110,166      cycles                    #    3.012 GHz                    
    38,366,614,688      stalled-cycles-frontend   #   58.35% frontend cycles idle   
    70,757,102,555      instructions              #    1.08  insn per cycle         
                                                  #    0.54  stalled cycles per insn
    11,430,766,689      branches                  #  523.641 M/sec                  
       718,563,854      branch-misses             #    6.29% of all branches        

      61.974910575 seconds time elapsed


real	1m3.748s
user	0m20.480s
sys	0m2.905s

 Performance counter stats for './__run 5':

      23245.706395      task-clock (msec)         #    0.365 CPUs utilized          
            22,641      context-switches          #    0.974 K/sec                  
             2,886      cpu-migrations            #    0.124 K/sec                  
               293      page-faults               #    0.013 K/sec                  
    71,168,762,706      cycles                    #    3.062 GHz                    
    44,023,019,449      stalled-cycles-frontend   #   61.86% frontend cycles idle   
    70,774,228,175      instructions              #    0.99  insn per cycle         
                                                  #    0.62  stalled cycles per insn
    11,434,122,266      branches                  #  491.881 M/sec                  
       732,301,237      branch-misses             #    6.40% of all branches        

      63.750789727 seconds time elapsed


real	0m31.476s
user	0m9.693s
sys	0m1.211s

 Performance counter stats for './__run 6':

      10835.732150      task-clock (msec)         #    0.344 CPUs utilized          
            10,080      context-switches          #    0.930 K/sec                  
             1,481      cpu-migrations            #    0.137 K/sec                  
               301      page-faults               #    0.028 K/sec                  
    32,653,180,142      cycles                    #    3.013 GHz                    
    18,896,000,080      stalled-cycles-frontend   #   57.87% frontend cycles idle   
    35,387,139,373      instructions              #    1.08  insn per cycle         
                                                  #    0.53  stalled cycles per insn
     5,716,959,320      branches                  #  527.602 M/sec                  
       360,771,003      branch-misses             #    6.31% of all branches        

      31.480630515 seconds time elapsed


real	0m34.219s
user	0m9.731s
sys	0m1.189s

 Performance counter stats for './__run 6':

      10854.759600      task-clock (msec)         #    0.317 CPUs utilized          
            10,306      context-switches          #    0.949 K/sec                  
             1,545      cpu-migrations            #    0.142 K/sec                  
               300      page-faults               #    0.028 K/sec                  
    32,707,161,105      cycles                    #    3.013 GHz                    
    18,982,325,352      stalled-cycles-frontend   #   58.04% frontend cycles idle   
    35,382,741,386      instructions              #    1.08  insn per cycle         
                                                  #    0.54  stalled cycles per insn
     5,716,270,342      branches                  #  526.614 M/sec                  
       361,014,763      branch-misses             #    6.32% of all branches        

      34.221450334 seconds time elapsed


real	0m29.811s
user	0m9.669s
sys	0m1.133s

 Performance counter stats for './__run 6':

      10731.131111      task-clock (msec)         #    0.360 CPUs utilized          
             9,999      context-switches          #    0.932 K/sec                  
             1,524      cpu-migrations            #    0.142 K/sec                  
               298      page-faults               #    0.028 K/sec                  
    32,276,840,134      cycles                    #    3.008 GHz                    
    18,531,304,131      stalled-cycles-frontend   #   57.41% frontend cycles idle   
    35,378,582,332      instructions              #    1.10  insn per cycle         
                                                  #    0.52  stalled cycles per insn
     5,715,500,302      branches                  #  532.609 M/sec                  
       361,044,654      branch-misses             #    6.32% of all branches        

      29.817725740 seconds time elapsed


real	0m2.897s
user	0m0.808s
sys	0m0.098s

 Performance counter stats for './__run 7':

        901.217487      task-clock (msec)         #    0.311 CPUs utilized          
               862      context-switches          #    0.956 K/sec                  
               136      cpu-migrations            #    0.151 K/sec                  
               297      page-faults               #    0.330 K/sec                  
     2,699,044,049      cycles                    #    2.995 GHz                    
     1,553,112,960      stalled-cycles-frontend   #   57.54% frontend cycles idle   
     2,955,220,322      instructions              #    1.09  insn per cycle         
                                                  #    0.53  stalled cycles per insn
       477,682,050      branches                  #  530.041 M/sec                  
        30,247,587      branch-misses             #    6.33% of all branches        

       2.899288541 seconds time elapsed


real	0m3.153s
user	0m0.843s
sys	0m0.078s

 Performance counter stats for './__run 7':

        917.659387      task-clock (msec)         #    0.291 CPUs utilized          
               769      context-switches          #    0.838 K/sec                  
               100      cpu-migrations            #    0.109 K/sec                  
               300      page-faults               #    0.327 K/sec                  
     2,734,207,594      cycles                    #    2.980 GHz                    
     1,599,141,295      stalled-cycles-frontend   #   58.49% frontend cycles idle   
     2,953,198,066      instructions              #    1.08  insn per cycle         
                                                  #    0.54  stalled cycles per insn
       477,308,983      branches                  #  520.137 M/sec                  
        30,208,054      branch-misses             #    6.33% of all branches        

       3.154975551 seconds time elapsed


real	0m2.244s
user	0m0.828s
sys	0m0.069s

 Performance counter stats for './__run 7':

        893.289277      task-clock (msec)         #    0.397 CPUs utilized          
               836      context-switches          #    0.936 K/sec                  
               102      cpu-migrations            #    0.114 K/sec                  
               295      page-faults               #    0.330 K/sec                  
     2,686,115,196      cycles                    #    3.007 GHz                    
     1,540,037,397      stalled-cycles-frontend   #   57.33% frontend cycles idle   
     2,952,949,301      instructions              #    1.10  insn per cycle         
                                                  #    0.52  stalled cycles per insn
       477,266,634      branches                  #  534.280 M/sec                  
        29,972,745      branch-misses             #    6.28% of all branches        

       2.247689896 seconds time elapsed


real	0m10.183s
user	0m3.278s
sys	0m0.371s

 Performance counter stats for './__run 8':

       3627.550528      task-clock (msec)         #    0.356 CPUs utilized          
             3,220      context-switches          #    0.888 K/sec                  
               508      cpu-migrations            #    0.140 K/sec                  
               302      page-faults               #    0.083 K/sec                  
    10,931,865,277      cycles                    #    3.014 GHz                    
     6,386,946,288      stalled-cycles-frontend   #   58.43% frontend cycles idle   
    11,798,196,589      instructions              #    1.08  insn per cycle         
                                                  #    0.54  stalled cycles per insn
     1,906,189,679      branches                  #  525.476 M/sec                  
       120,644,223      branch-misses             #    6.33% of all branches        

      10.187086453 seconds time elapsed


real	0m10.068s
user	0m3.367s
sys	0m0.324s

 Performance counter stats for './__run 8':

       3666.988281      task-clock (msec)         #    0.364 CPUs utilized          
             3,539      context-switches          #    0.965 K/sec                  
               506      cpu-migrations            #    0.138 K/sec                  
               300      page-faults               #    0.082 K/sec                  
    10,979,163,598      cycles                    #    2.994 GHz                    
     6,416,358,285      stalled-cycles-frontend   #   58.44% frontend cycles idle   
    11,798,587,608      instructions              #    1.07  insn per cycle         
                                                  #    0.54  stalled cycles per insn
     1,906,308,580      branches                  #  519.857 M/sec                  
       120,890,345      branch-misses             #    6.34% of all branches        

      10.071055552 seconds time elapsed


real	0m9.278s
user	0m3.134s
sys	0m0.444s

 Performance counter stats for './__run 8':

       3555.986344      task-clock (msec)         #    0.383 CPUs utilized          
             3,446      context-switches          #    0.969 K/sec                  
               494      cpu-migrations            #    0.139 K/sec                  
               300      page-faults               #    0.084 K/sec                  
    10,687,116,148      cycles                    #    3.005 GHz                    
     6,085,412,721      stalled-cycles-frontend   #   56.94% frontend cycles idle   
    11,799,898,404      instructions              #    1.10  insn per cycle         
                                                  #    0.52  stalled cycles per insn
     1,906,528,406      branches                  #  536.146 M/sec                  
       120,028,288      branch-misses             #    6.30% of all branches        

       9.282097372 seconds time elapsed


real	0m8.759s
user	0m2.515s
sys	0m0.316s

 Performance counter stats for './__run 9':

       2815.952169      task-clock (msec)         #    0.321 CPUs utilized          
             2,814      context-switches          #    0.999 K/sec                  
               399      cpu-migrations            #    0.142 K/sec                  
               295      page-faults               #    0.105 K/sec                  
     8,565,474,786      cycles                    #    3.042 GHz                    
     5,182,552,622      stalled-cycles-frontend   #   60.51% frontend cycles idle   
     8,851,702,260      instructions              #    1.03  insn per cycle         
                                                  #    0.59  stalled cycles per insn
     1,430,302,363      branches                  #  507.929 M/sec                  
        91,949,765      branch-misses             #    6.43% of all branches        

       8.762163261 seconds time elapsed


real	0m7.696s
user	0m2.427s
sys	0m0.343s

 Performance counter stats for './__run 9':

       2753.363497      task-clock (msec)         #    0.358 CPUs utilized          
             2,536      context-switches          #    0.921 K/sec                  
               386      cpu-migrations            #    0.140 K/sec                  
               296      page-faults               #    0.108 K/sec                  
     8,321,780,138      cycles                    #    3.022 GHz                    
     4,900,702,785      stalled-cycles-frontend   #   58.89% frontend cycles idle   
     8,848,132,930      instructions              #    1.06  insn per cycle         
                                                  #    0.55  stalled cycles per insn
     1,429,624,521      branches                  #  519.228 M/sec                  
        91,098,679      branch-misses             #    6.37% of all branches        

       7.698714992 seconds time elapsed


real	0m7.198s
user	0m2.461s
sys	0m0.262s

 Performance counter stats for './__run 9':

       2706.476481      task-clock (msec)         #    0.376 CPUs utilized          
             2,546      context-switches          #    0.941 K/sec                  
               387      cpu-migrations            #    0.143 K/sec                  
               301      page-faults               #    0.111 K/sec                  
     8,129,079,172      cycles                    #    3.004 GHz                    
     4,688,965,400      stalled-cycles-frontend   #   57.68% frontend cycles idle   
     8,855,960,126      instructions              #    1.09  insn per cycle         
                                                  #    0.53  stalled cycles per insn
     1,431,108,921      branches                  #  528.772 M/sec                  
        90,274,359      branch-misses             #    6.31% of all branches        

       7.200438165 seconds time elapsed


real	0m12.065s
user	0m4.029s
sys	0m0.456s

 Performance counter stats for './__run 10':

       4455.805566      task-clock (msec)         #    0.369 CPUs utilized          
             4,014      context-switches          #    0.901 K/sec                  
               599      cpu-migrations            #    0.134 K/sec                  
               297      page-faults               #    0.067 K/sec                  
    13,387,403,156      cycles                    #    3.004 GHz                    
     7,660,723,499      stalled-cycles-frontend   #   57.22% frontend cycles idle   
    14,742,372,383      instructions              #    1.10  insn per cycle         
                                                  #    0.52  stalled cycles per insn
     2,381,758,418      branches                  #  534.529 M/sec                  
       150,020,686      branch-misses             #    6.30% of all branches        

      12.070521005 seconds time elapsed


real	0m13.086s
user	0m4.060s
sys	0m0.463s

 Performance counter stats for './__run 10':

       4494.674966      task-clock (msec)         #    0.343 CPUs utilized          
             4,236      context-switches          #    0.942 K/sec                  
               655      cpu-migrations            #    0.146 K/sec                  
               295      page-faults               #    0.066 K/sec                  
    13,567,472,535      cycles                    #    3.019 GHz                    
     7,857,077,368      stalled-cycles-frontend   #   57.91% frontend cycles idle   
    14,745,703,419      instructions              #    1.09  insn per cycle         
                                                  #    0.53  stalled cycles per insn
     2,382,419,630      branches                  #  530.054 M/sec                  
       150,798,689      branch-misses             #    6.33% of all branches        

      13.090109961 seconds time elapsed


real	0m13.613s
user	0m4.151s
sys	0m0.504s

 Performance counter stats for './__run 10':

       4625.812927      task-clock (msec)         #    0.340 CPUs utilized          
             4,500      context-switches          #    0.973 K/sec                  
               687      cpu-migrations            #    0.149 K/sec                  
               294      page-faults               #    0.064 K/sec                  
    14,086,323,964      cycles                    #    3.045 GHz                    
     8,414,091,176      stalled-cycles-frontend   #   59.73% frontend cycles idle   
    14,750,660,164      instructions              #    1.05  insn per cycle         
                                                  #    0.57  stalled cycles per insn
     2,383,334,833      branches                  #  515.225 M/sec                  
       153,006,004      branch-misses             #    6.42% of all branches        

      13.615545166 seconds time elapsed

