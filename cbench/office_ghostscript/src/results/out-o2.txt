
real	0m4.485s
user	0m4.179s
sys	0m0.224s

 Performance counter stats for './__run 1':

       4403.684892      task-clock (msec)         #    0.978 CPUs utilized          
                44      context-switches          #    0.010 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            23,845      page-faults               #    0.005 M/sec                  
    14,288,691,625      cycles                    #    3.245 GHz                    
     2,456,246,496      stalled-cycles-frontend   #   17.19% frontend cycles idle   
    35,782,883,969      instructions              #    2.50  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     6,933,910,677      branches                  # 1574.570 M/sec                  
        17,636,044      branch-misses             #    0.25% of all branches        

       4.504820128 seconds time elapsed


real	0m4.467s
user	0m4.180s
sys	0m0.228s

 Performance counter stats for './__run 2':

       4410.379856      task-clock (msec)         #    0.987 CPUs utilized          
                70      context-switches          #    0.016 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            23,845      page-faults               #    0.005 M/sec                  
    14,321,653,735      cycles                    #    3.247 GHz                    
     2,517,059,497      stalled-cycles-frontend   #   17.58% frontend cycles idle   
    35,788,316,929      instructions              #    2.50  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     6,934,854,830      branches                  # 1572.394 M/sec                  
        17,585,046      branch-misses             #    0.25% of all branches        

       4.469514488 seconds time elapsed


real	0m4.455s
user	0m4.222s
sys	0m0.168s

 Performance counter stats for './__run 3':

       4391.475004      task-clock (msec)         #    0.985 CPUs utilized          
                73      context-switches          #    0.017 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            23,849      page-faults               #    0.005 M/sec                  
    14,254,144,962      cycles                    #    3.246 GHz                    
     2,445,237,599      stalled-cycles-frontend   #   17.15% frontend cycles idle   
    35,790,411,688      instructions              #    2.51  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     6,935,234,037      branches                  # 1579.249 M/sec                  
        17,638,066      branch-misses             #    0.25% of all branches        

       4.457181961 seconds time elapsed


real	0m0.231s
user	0m0.202s
sys	0m0.020s

 Performance counter stats for './__run 4':

        223.656891      task-clock (msec)         #    0.956 CPUs utilized          
                 4      context-switches          #    0.018 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,484      page-faults               #    0.007 M/sec                  
       717,415,340      cycles                    #    3.208 GHz                    
       128,845,966      stalled-cycles-frontend   #   17.96% frontend cycles idle   
     1,797,161,180      instructions              #    2.51  insn per cycle         
                                                  #    0.07  stalled cycles per insn
       348,281,430      branches                  # 1557.213 M/sec                  
           934,985      branch-misses             #    0.27% of all branches        

       0.233966853 seconds time elapsed


real	0m0.670s
user	0m0.641s
sys	0m0.024s

 Performance counter stats for './__run 5':

        668.066328      task-clock (msec)         #    0.992 CPUs utilized          
                33      context-switches          #    0.049 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             3,837      page-faults               #    0.006 M/sec                  
     2,146,825,741      cycles                    #    3.213 GHz                    
       386,867,725      stalled-cycles-frontend   #   18.02% frontend cycles idle   
     5,375,514,104      instructions              #    2.50  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     1,041,680,778      branches                  # 1559.248 M/sec                  
         2,716,159      branch-misses             #    0.26% of all branches        

       0.673190990 seconds time elapsed


real	0m4.845s
user	0m4.487s
sys	0m0.329s

 Performance counter stats for './__run 6':

       4818.557243      task-clock (msec)         #    0.994 CPUs utilized          
                70      context-switches          #    0.015 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            23,845      page-faults               #    0.005 M/sec                  
    15,406,222,869      cycles                    #    3.197 GHz                    
     3,608,270,406      stalled-cycles-frontend   #   23.42% frontend cycles idle   
    35,786,996,931      instructions              #    2.32  insn per cycle         
                                                  #    0.10  stalled cycles per insn
     6,934,674,963      branches                  # 1439.160 M/sec                  
        18,006,638      branch-misses             #    0.26% of all branches        

       4.849435405 seconds time elapsed


real	0m0.880s
user	0m0.845s
sys	0m0.028s

 Performance counter stats for './__run 7':

        875.490548      task-clock (msec)         #    0.991 CPUs utilized          
                 6      context-switches          #    0.007 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             5,015      page-faults               #    0.006 M/sec                  
     2,834,694,355      cycles                    #    3.238 GHz                    
       512,126,004      stalled-cycles-frontend   #   18.07% frontend cycles idle   
     7,163,805,995      instructions              #    2.53  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     1,388,219,754      branches                  # 1585.648 M/sec                  
         3,566,675      branch-misses             #    0.26% of all branches        

       0.883708895 seconds time elapsed


real	0m1.305s
user	0m1.160s
sys	0m0.116s

 Performance counter stats for './__run 8':

       1279.814299      task-clock (msec)         #    0.979 CPUs utilized          
                41      context-switches          #    0.032 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             6,780      page-faults               #    0.005 M/sec                  
     4,089,473,602      cycles                    #    3.195 GHz                    
       810,929,352      stalled-cycles-frontend   #   19.83% frontend cycles idle   
     9,849,124,823      instructions              #    2.41  insn per cycle         
                                                  #    0.08  stalled cycles per insn
     1,908,554,182      branches                  # 1491.274 M/sec                  
         4,959,505      branch-misses             #    0.26% of all branches        

       1.307890956 seconds time elapsed


real	0m4.445s
user	0m4.122s
sys	0m0.256s

 Performance counter stats for './__run 9':

       4380.824577      task-clock (msec)         #    0.985 CPUs utilized          
                14      context-switches          #    0.003 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            23,845      page-faults               #    0.005 M/sec                  
    14,228,935,852      cycles                    #    3.248 GHz                    
     2,435,953,441      stalled-cycles-frontend   #   17.12% frontend cycles idle   
    35,785,710,515      instructions              #    2.51  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     6,934,471,080      branches                  # 1582.915 M/sec                  
        17,613,563      branch-misses             #    0.25% of all branches        

       4.448239280 seconds time elapsed


real	0m2.183s
user	0m2.022s
sys	0m0.152s

 Performance counter stats for './__run 10':

       2177.065293      task-clock (msec)         #    0.996 CPUs utilized          
                 6      context-switches          #    0.003 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            12,074      page-faults               #    0.006 M/sec                  
     7,093,697,255      cycles                    #    3.258 GHz                    
     1,165,512,654      stalled-cycles-frontend   #   16.43% frontend cycles idle   
    17,897,413,163      instructions              #    2.52  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     3,468,150,931      branches                  # 1593.039 M/sec                  
         8,802,176      branch-misses             #    0.25% of all branches        

       2.186185527 seconds time elapsed

