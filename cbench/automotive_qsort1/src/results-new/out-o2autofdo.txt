
real	0m5.338s
user	0m5.293s
sys	0m0.044s

 Performance counter stats for './__run 1':

       5339.174334      task-clock (msec)         #    1.000 CPUs utilized          
                61      context-switches          #    0.011 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               476      page-faults               #    0.089 K/sec                  
    17,312,967,679      cycles                    #    3.243 GHz                    
     4,631,417,628      stalled-cycles-frontend   #   26.75% frontend cycles idle   
    32,326,870,193      instructions              #    1.87  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     8,136,093,814      branches                  # 1523.849 M/sec                  
       172,084,737      branch-misses             #    2.12% of all branches        

       5.341056221 seconds time elapsed


real	0m9.803s
user	0m9.684s
sys	0m0.116s

 Performance counter stats for './__run 2':

       9803.200293      task-clock (msec)         #    1.000 CPUs utilized          
               147      context-switches          #    0.015 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
               477      page-faults               #    0.049 K/sec                  
    31,831,450,697      cycles                    #    3.247 GHz                    
     8,242,444,283      stalled-cycles-frontend   #   25.89% frontend cycles idle   
    60,501,466,035      instructions              #    1.90  insn per cycle         
                                                  #    0.14  stalled cycles per insn
    15,227,370,760      branches                  # 1553.306 M/sec                  
       307,517,525      branch-misses             #    2.02% of all branches        

       9.806286518 seconds time elapsed


real	0m7.207s
user	0m7.106s
sys	0m0.096s

 Performance counter stats for './__run 3':

       7206.359778      task-clock (msec)         #    1.000 CPUs utilized          
               142      context-switches          #    0.020 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               473      page-faults               #    0.066 K/sec                  
    23,270,969,265      cycles                    #    3.229 GHz                    
     6,403,898,480      stalled-cycles-frontend   #   27.52% frontend cycles idle   
    43,340,839,075      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    10,908,090,074      branches                  # 1513.675 M/sec                  
       219,649,874      branch-misses             #    2.01% of all branches        

       7.209669592 seconds time elapsed


real	0m6.054s
user	0m5.987s
sys	0m0.064s

 Performance counter stats for './__run 4':

       6053.056222      task-clock (msec)         #    0.999 CPUs utilized          
               137      context-switches          #    0.023 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
               478      page-faults               #    0.079 K/sec                  
    19,734,162,373      cycles                    #    3.260 GHz                    
     5,085,813,762      stalled-cycles-frontend   #   25.77% frontend cycles idle   
    37,618,171,271      instructions              #    1.91  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     9,467,917,507      branches                  # 1564.155 M/sec                  
       187,975,804      branch-misses             #    1.99% of all branches        

       6.056967430 seconds time elapsed


real	0m8.590s
user	0m8.506s
sys	0m0.084s

 Performance counter stats for './__run 5':

       8592.049128      task-clock (msec)         #    1.000 CPUs utilized          
                14      context-switches          #    0.002 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.055 K/sec                  
    28,026,300,530      cycles                    #    3.262 GHz                    
     7,122,626,677      stalled-cycles-frontend   #   25.41% frontend cycles idle   
    53,633,625,569      instructions              #    1.91  insn per cycle         
                                                  #    0.13  stalled cycles per insn
    13,499,003,374      branches                  # 1571.104 M/sec                  
       268,243,347      branch-misses             #    1.99% of all branches        

       8.592668294 seconds time elapsed


real	0m30.150s
user	0m29.841s
sys	0m0.308s

 Performance counter stats for './__run 6':

      30151.032929      task-clock (msec)         #    1.000 CPUs utilized          
                46      context-switches          #    0.002 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               473      page-faults               #    0.016 K/sec                  
    98,036,893,865      cycles                    #    3.252 GHz                    
    25,619,790,110      stalled-cycles-frontend   #   26.13% frontend cycles idle   
   185,913,893,718      instructions              #    1.90  insn per cycle         
                                                  #    0.14  stalled cycles per insn
    46,793,415,037      branches                  # 1551.967 M/sec                  
       929,940,033      branch-misses             #    1.99% of all branches        

      30.152272916 seconds time elapsed


real	0m7.613s
user	0m7.528s
sys	0m0.076s

 Performance counter stats for './__run 7':

       7609.276069      task-clock (msec)         #    0.999 CPUs utilized          
               288      context-switches          #    0.038 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               477      page-faults               #    0.063 K/sec                  
    24,731,531,759      cycles                    #    3.250 GHz                    
     6,580,485,961      stalled-cycles-frontend   #   26.61% frontend cycles idle   
    46,490,342,889      instructions              #    1.88  insn per cycle         
                                                  #    0.14  stalled cycles per insn
    11,700,580,962      branches                  # 1537.673 M/sec                  
       243,143,847      branch-misses             #    2.08% of all branches        

       7.615714470 seconds time elapsed


real	0m11.067s
user	0m10.864s
sys	0m0.192s

 Performance counter stats for './__run 8':

      11060.950242      task-clock (msec)         #    0.999 CPUs utilized          
               265      context-switches          #    0.024 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.043 K/sec                  
    35,724,240,266      cycles                    #    3.230 GHz                    
    10,062,226,908      stalled-cycles-frontend   #   28.17% frontend cycles idle   
    65,962,643,334      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    16,599,793,300      branches                  # 1500.757 M/sec                  
       331,795,791      branch-misses             #    2.00% of all branches        

      10.770008293 seconds time elapsed


real	0m5.876s
user	0m5.769s
sys	0m0.105s

 Performance counter stats for './__run 9':

       5877.175156      task-clock (msec)         #    1.000 CPUs utilized          
                48      context-switches          #    0.008 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.081 K/sec                  
    19,202,538,456      cycles                    #    3.267 GHz                    
     4,928,533,119      stalled-cycles-frontend   #   25.67% frontend cycles idle   
    36,617,975,647      instructions              #    1.91  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     9,216,141,452      branches                  # 1568.124 M/sec                  
       183,256,611      branch-misses             #    1.99% of all branches        

       5.878839572 seconds time elapsed


real	0m6.773s
user	0m6.698s
sys	0m0.072s

 Performance counter stats for './__run 10':

       6771.659509      task-clock (msec)         #    0.999 CPUs utilized          
               140      context-switches          #    0.021 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               476      page-faults               #    0.070 K/sec                  
    22,087,073,033      cycles                    #    3.262 GHz                    
     5,706,252,552      stalled-cycles-frontend   #   25.84% frontend cycles idle   
    42,051,660,402      instructions              #    1.90  insn per cycle         
                                                  #    0.14  stalled cycles per insn
    10,583,801,940      branches                  # 1562.955 M/sec                  
       210,823,528      branch-misses             #    1.99% of all branches        

       6.775727489 seconds time elapsed

