
real	0m2.566s
user	0m2.161s
sys	0m0.399s

 Performance counter stats for './__run 1':

       2561.993469      task-clock (msec)         #    0.997 CPUs utilized          
               294      context-switches          #    0.115 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
           122,718      page-faults               #    0.048 M/sec                  
     8,332,187,133      cycles                    #    3.252 GHz                    
     2,292,751,312      stalled-cycles-frontend   #   27.52% frontend cycles idle   
    15,684,514,310      instructions              #    1.88  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     3,155,850,842      branches                  # 1231.795 M/sec                  
        20,855,539      branch-misses             #    0.66% of all branches        

       2.568714884 seconds time elapsed


real	0m1.571s
user	0m1.403s
sys	0m0.164s

 Performance counter stats for './__run 2':

       1569.157022      task-clock (msec)         #    0.997 CPUs utilized          
               208      context-switches          #    0.133 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            73,733      page-faults               #    0.047 M/sec                  
     5,101,654,199      cycles                    #    3.251 GHz                    
     1,439,737,369      stalled-cycles-frontend   #   28.22% frontend cycles idle   
     9,417,296,874      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     1,895,002,191      branches                  # 1207.656 M/sec                  
        15,661,333      branch-misses             #    0.83% of all branches        

       1.573827186 seconds time elapsed


real	0m1.025s
user	0m0.877s
sys	0m0.144s

 Performance counter stats for './__run 3':

       1023.209117      task-clock (msec)         #    0.997 CPUs utilized          
               104      context-switches          #    0.102 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            49,242      page-faults               #    0.048 M/sec                  
     3,344,609,821      cycles                    #    3.269 GHz                    
       927,905,864      stalled-cycles-frontend   #   27.74% frontend cycles idle   
     6,277,366,372      instructions              #    1.88  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     1,263,149,972      branches                  # 1234.498 M/sec                  
         8,322,765      branch-misses             #    0.66% of all branches        

       1.026724314 seconds time elapsed


real	0m0.776s
user	0m0.683s
sys	0m0.092s

 Performance counter stats for './__run 4':

        777.259171      task-clock (msec)         #    0.997 CPUs utilized          
                60      context-switches          #    0.077 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            36,991      page-faults               #    0.048 M/sec                  
     2,512,279,742      cycles                    #    3.232 GHz                    
       694,408,834      stalled-cycles-frontend   #   27.64% frontend cycles idle   
     4,708,493,366      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
       947,333,449      branches                  # 1218.813 M/sec                  
         6,259,304      branch-misses             #    0.66% of all branches        

       0.779309376 seconds time elapsed


real	0m0.525s
user	0m0.449s
sys	0m0.075s

 Performance counter stats for './__run 5':

        526.683943      task-clock (msec)         #    0.997 CPUs utilized          
                46      context-switches          #    0.087 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            24,748      page-faults               #    0.047 M/sec                  
     1,704,072,591      cycles                    #    3.235 GHz                    
       483,215,044      stalled-cycles-frontend   #   28.36% frontend cycles idle   
     3,141,750,691      instructions              #    1.84  insn per cycle         
                                                  #    0.15  stalled cycles per insn
       632,245,107      branches                  # 1200.426 M/sec                  
         5,209,004      branch-misses             #    0.82% of all branches        

       0.528263855 seconds time elapsed


real	0m0.264s
user	0m0.203s
sys	0m0.060s

 Performance counter stats for './__run 6':

        266.079732      task-clock (msec)         #    0.996 CPUs utilized          
                27      context-switches          #    0.101 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            12,504      page-faults               #    0.047 M/sec                  
       847,514,798      cycles                    #    3.185 GHz                    
       239,984,535      stalled-cycles-frontend   #   28.32% frontend cycles idle   
     1,573,869,440      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
       316,731,245      branches                  # 1190.362 M/sec                  
         2,134,540      branch-misses             #    0.67% of all branches        

       0.267129231 seconds time elapsed


real	0m0.210s
user	0m0.185s
sys	0m0.024s

 Performance counter stats for './__run 7':

        211.192743      task-clock (msec)         #    0.996 CPUs utilized          
                24      context-switches          #    0.114 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            10,050      page-faults               #    0.048 M/sec                  
       686,544,572      cycles                    #    3.251 GHz                    
       201,196,640      stalled-cycles-frontend   #   29.31% frontend cycles idle   
     1,260,186,913      instructions              #    1.84  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       253,552,700      branches                  # 1200.575 M/sec                  
         1,718,101      branch-misses             #    0.68% of all branches        

       0.212128778 seconds time elapsed


real	0m0.114s
user	0m0.093s
sys	0m0.020s

 Performance counter stats for './__run 8':

        116.673102      task-clock (msec)         #    0.993 CPUs utilized          
                10      context-switches          #    0.086 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             5,150      page-faults               #    0.044 M/sec                  
       357,254,314      cycles                    #    3.062 GHz                    
       112,581,679      stalled-cycles-frontend   #   31.51% frontend cycles idle   
       633,206,692      instructions              #    1.77  insn per cycle         
                                                  #    0.18  stalled cycles per insn
       127,421,680      branches                  # 1092.126 M/sec                  
           927,533      branch-misses             #    0.73% of all branches        

       0.117468429 seconds time elapsed


real	0m0.091s
user	0m0.074s
sys	0m0.016s

 Performance counter stats for './__run 9':

         93.430417      task-clock (msec)         #    0.992 CPUs utilized          
                14      context-switches          #    0.150 K/sec                  
                 1      cpu-migrations            #    0.011 K/sec                  
             3,929      page-faults               #    0.042 M/sec                  
       275,508,996      cycles                    #    2.949 GHz                    
        89,724,676      stalled-cycles-frontend   #   32.57% frontend cycles idle   
       476,793,643      instructions              #    1.73  insn per cycle         
                                                  #    0.19  stalled cycles per insn
        95,963,136      branches                  # 1027.108 M/sec                  
           847,693      branch-misses             #    0.88% of all branches        

       0.094162152 seconds time elapsed


real	0m0.059s
user	0m0.043s
sys	0m0.016s

 Performance counter stats for './__run 10':

         61.831503      task-clock (msec)         #    0.992 CPUs utilized          
                 5      context-switches          #    0.081 K/sec                  
                 1      cpu-migrations            #    0.016 K/sec                  
             2,702      page-faults               #    0.044 M/sec                  
       180,954,119      cycles                    #    2.927 GHz                    
        57,358,907      stalled-cycles-frontend   #   31.70% frontend cycles idle   
       319,959,574      instructions              #    1.77  insn per cycle         
                                                  #    0.18  stalled cycles per insn
        64,393,542      branches                  # 1041.436 M/sec                  
           474,489      branch-misses             #    0.74% of all branches        

       0.062342403 seconds time elapsed

