
real	0m25.278s
user	0m5.122s
sys	0m1.936s

 Performance counter stats for './__run 1':

       6899.315148      task-clock (msec)         #    0.273 CPUs utilized          
             8,986      context-switches          #    0.001 M/sec                  
               124      cpu-migrations            #    0.018 K/sec                  
               260      page-faults               #    0.038 K/sec                  
    18,691,409,635      cycles                    #    2.709 GHz                    
     7,464,701,608      stalled-cycles-frontend   #   39.94% frontend cycles idle   
    32,127,193,861      instructions              #    1.72  insn per cycle         
                                                  #    0.23  stalled cycles per insn
     2,873,670,906      branches                  #  416.515 M/sec                  
       138,547,356      branch-misses             #    4.82% of all branches        

      25.281907730 seconds time elapsed


real	0m16.196s
user	0m2.974s
sys	0m1.143s

 Performance counter stats for './__run 2':

       4031.808546      task-clock (msec)         #    0.249 CPUs utilized          
             5,201      context-switches          #    0.001 M/sec                  
                77      cpu-migrations            #    0.019 K/sec                  
               266      page-faults               #    0.066 K/sec                  
    11,459,241,332      cycles                    #    2.842 GHz                    
     4,772,982,745      stalled-cycles-frontend   #   41.65% frontend cycles idle   
    19,027,691,145      instructions              #    1.66  insn per cycle         
                                                  #    0.25  stalled cycles per insn
     1,701,473,114      branches                  #  422.012 M/sec                  
        84,076,906      branch-misses             #    4.94% of all branches        

      16.198480083 seconds time elapsed


real	0m0.094s
user	0m0.023s
sys	0m0.006s

 Performance counter stats for './__run 3':

         31.019922      task-clock (msec)         #    0.322 CPUs utilized          
                41      context-switches          #    0.001 M/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               265      page-faults               #    0.009 M/sec                  
        92,458,656      cycles                    #    2.981 GHz                    
        37,371,984      stalled-cycles-frontend   #   40.42% frontend cycles idle   
       155,785,540      instructions              #    1.68  insn per cycle         
                                                  #    0.24  stalled cycles per insn
        14,521,875      branches                  #  468.147 M/sec                  
           714,961      branch-misses             #    4.92% of all branches        

       0.096254615 seconds time elapsed


real	0m3.003s
user	0m0.690s
sys	0m0.202s

 Performance counter stats for './__run 4':

        875.606038      task-clock (msec)         #    0.291 CPUs utilized          
             1,045      context-switches          #    0.001 M/sec                  
                12      cpu-migrations            #    0.014 K/sec                  
               266      page-faults               #    0.304 K/sec                  
     2,270,644,805      cycles                    #    2.593 GHz                    
       930,876,569      stalled-cycles-frontend   #   41.00% frontend cycles idle   
     3,815,063,401      instructions              #    1.68  insn per cycle         
                                                  #    0.24  stalled cycles per insn
       341,723,301      branches                  #  390.271 M/sec                  
        16,838,812      branch-misses             #    4.93% of all branches        

       3.005426134 seconds time elapsed


real	1m3.052s
user	0m13.174s
sys	0m5.050s

 Performance counter stats for './__run 5':

      17813.037689      task-clock (msec)         #    0.283 CPUs utilized          
            23,340      context-switches          #    0.001 M/sec                  
               299      cpu-migrations            #    0.017 K/sec                  
               266      page-faults               #    0.015 K/sec                  
    49,381,323,443      cycles                    #    2.772 GHz                    
    19,783,288,283      stalled-cycles-frontend   #   40.06% frontend cycles idle   
    84,613,068,549      instructions              #    1.71  insn per cycle         
                                                  #    0.23  stalled cycles per insn
     7,569,642,909      branches                  #  424.950 M/sec                  
       362,763,055      branch-misses             #    4.79% of all branches        

      63.054757844 seconds time elapsed


real	0m33.909s
user	0m7.806s
sys	0m2.792s

 Performance counter stats for './__run 6':

      10335.442954      task-clock (msec)         #    0.305 CPUs utilized          
            12,819      context-switches          #    0.001 M/sec                  
                83      cpu-migrations            #    0.008 K/sec                  
               264      page-faults               #    0.026 K/sec                  
    27,360,005,788      cycles                    #    2.647 GHz                    
    10,980,166,906      stalled-cycles-frontend   #   40.13% frontend cycles idle   
    46,744,061,453      instructions              #    1.71  insn per cycle         
                                                  #    0.23  stalled cycles per insn
     4,177,849,718      branches                  #  404.226 M/sec                  
       203,317,123      branch-misses             #    4.87% of all branches        

      33.912008461 seconds time elapsed


real	0m2.789s
user	0m0.661s
sys	0m0.212s

 Performance counter stats for './__run 7':

        855.028134      task-clock (msec)         #    0.306 CPUs utilized          
             1,058      context-switches          #    0.001 M/sec                  
                12      cpu-migrations            #    0.014 K/sec                  
               265      page-faults               #    0.310 K/sec                  
     2,227,508,624      cycles                    #    2.605 GHz                    
       877,624,415      stalled-cycles-frontend   #   39.40% frontend cycles idle   
     3,862,311,588      instructions              #    1.73  insn per cycle         
                                                  #    0.23  stalled cycles per insn
       346,320,464      branches                  #  405.040 M/sec                  
        16,214,098      branch-misses             #    4.68% of all branches        

       2.793465758 seconds time elapsed


real	0m8.891s
user	0m1.860s
sys	0m0.667s

 Performance counter stats for './__run 8':

       2476.022493      task-clock (msec)         #    0.278 CPUs utilized          
             3,369      context-switches          #    0.001 M/sec                  
                31      cpu-migrations            #    0.013 K/sec                  
               263      page-faults               #    0.106 K/sec                  
     7,242,787,840      cycles                    #    2.925 GHz                    
     2,928,409,560      stalled-cycles-frontend   #   40.43% frontend cycles idle   
    12,314,499,455      instructions              #    1.70  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     1,102,381,183      branches                  #  445.223 M/sec                  
        53,418,289      branch-misses             #    4.85% of all branches        

       8.895549566 seconds time elapsed


real	0m6.977s
user	0m1.514s
sys	0m0.643s

 Performance counter stats for './__run 9':

       2108.181792      task-clock (msec)         #    0.302 CPUs utilized          
             2,652      context-switches          #    0.001 M/sec                  
                21      cpu-migrations            #    0.010 K/sec                  
               266      page-faults               #    0.126 K/sec                  
     5,678,072,821      cycles                    #    2.693 GHz                    
     2,275,904,034      stalled-cycles-frontend   #   40.08% frontend cycles idle   
     9,713,031,947      instructions              #    1.71  insn per cycle         
                                                  #    0.23  stalled cycles per insn
       869,518,647      branches                  #  412.450 M/sec                  
        42,514,700      branch-misses             #    4.89% of all branches        

       6.980116775 seconds time elapsed


real	0m8.542s
user	0m1.686s
sys	0m0.649s

 Performance counter stats for './__run 10':

       2289.983875      task-clock (msec)         #    0.268 CPUs utilized          
             3,180      context-switches          #    0.001 M/sec                  
                35      cpu-migrations            #    0.015 K/sec                  
               264      page-faults               #    0.115 K/sec                  
     6,784,021,728      cycles                    #    2.962 GHz                    
     2,733,608,547      stalled-cycles-frontend   #   40.29% frontend cycles idle   
    11,571,970,152      instructions              #    1.71  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     1,034,864,493      branches                  #  451.909 M/sec                  
        49,981,743      branch-misses             #    4.83% of all branches        

       8.545808108 seconds time elapsed

