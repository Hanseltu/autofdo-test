
real	0m5.074s
user	0m4.436s
sys	0m0.536s

 Performance counter stats for './__run 1':

       4971.213876      task-clock (msec)         #    0.979 CPUs utilized          
             3,476      context-switches          #    0.699 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            33,958      page-faults               #    0.007 M/sec                  
    16,187,603,790      cycles                    #    3.256 GHz                    
     4,989,727,553      stalled-cycles-frontend   #   30.82% frontend cycles idle   
    28,491,953,405      instructions              #    1.76  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     4,840,544,039      branches                  #  973.715 M/sec                  
       199,348,572      branch-misses             #    4.12% of all branches        

       5.076631851 seconds time elapsed


real	0m3.387s
user	0m2.887s
sys	0m0.384s

 Performance counter stats for './__run 2':

       3270.233603      task-clock (msec)         #    0.965 CPUs utilized          
             2,406      context-switches          #    0.736 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            22,381      page-faults               #    0.007 M/sec                  
    10,637,348,674      cycles                    #    3.253 GHz                    
     3,294,201,781      stalled-cycles-frontend   #   30.97% frontend cycles idle   
    18,692,208,425      instructions              #    1.76  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     3,175,738,553      branches                  #  971.104 M/sec                  
       130,958,574      branch-misses             #    4.12% of all branches        

       3.389450645 seconds time elapsed


real	0m0.135s
user	0m0.114s
sys	0m0.020s

 Performance counter stats for './__run 3':

        135.987149      task-clock (msec)         #    0.986 CPUs utilized          
                91      context-switches          #    0.669 K/sec                  
                 1      cpu-migrations            #    0.007 K/sec                  
             1,108      page-faults               #    0.008 M/sec                  
       409,273,846      cycles                    #    3.010 GHz                    
       138,526,864      stalled-cycles-frontend   #   33.85% frontend cycles idle   
       690,500,763      instructions              #    1.69  insn per cycle         
                                                  #    0.20  stalled cycles per insn
       117,479,211      branches                  #  863.899 M/sec                  
         4,848,021      branch-misses             #    4.13% of all branches        

       0.137856130 seconds time elapsed


real	0m1.737s
user	0m1.557s
sys	0m0.169s

 Performance counter stats for './__run 4':

       1726.960407      task-clock (msec)         #    0.993 CPUs utilized          
             1,274      context-switches          #    0.738 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
            11,949      page-faults               #    0.007 M/sec                  
     5,621,704,156      cycles                    #    3.255 GHz                    
     1,745,639,209      stalled-cycles-frontend   #   31.05% frontend cycles idle   
     9,862,958,561      instructions              #    1.75  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     1,675,785,848      branches                  #  970.367 M/sec                  
        69,023,316      branch-misses             #    4.12% of all branches        

       1.739930108 seconds time elapsed


real	0m4.087s
user	0m3.506s
sys	0m0.466s

 Performance counter stats for './__run 5':

       3971.644417      task-clock (msec)         #    0.971 CPUs utilized          
             2,787      context-switches          #    0.702 K/sec                  
                 4      cpu-migrations            #    0.001 K/sec                  
            27,103      page-faults               #    0.007 M/sec                  
    12,890,756,390      cycles                    #    3.246 GHz                    
     3,979,317,087      stalled-cycles-frontend   #   30.87% frontend cycles idle   
    22,684,846,236      instructions              #    1.76  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     3,854,023,011      branches                  #  970.385 M/sec                  
       158,794,963      branch-misses             #    4.12% of all branches        

       4.090590206 seconds time elapsed


real	0m3.972s
user	0m3.476s
sys	0m0.375s

 Performance counter stats for './__run 6':

       3851.765005      task-clock (msec)         #    0.969 CPUs utilized          
             1,939      context-switches          #    0.503 K/sec                  
                 5      cpu-migrations            #    0.001 K/sec                  
            26,338      page-faults               #    0.007 M/sec                  
    12,512,174,611      cycles                    #    3.248 GHz                    
     3,860,136,002      stalled-cycles-frontend   #   30.85% frontend cycles idle   
    22,026,070,282      instructions              #    1.76  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     3,741,894,739      branches                  #  971.475 M/sec                  
       154,080,520      branch-misses             #    4.12% of all branches        

       3.974815859 seconds time elapsed


real	0m2.025s
user	0m1.736s
sys	0m0.248s

 Performance counter stats for './__run 7':

       1984.693058      task-clock (msec)         #    0.978 CPUs utilized          
             1,454      context-switches          #    0.733 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
            13,634      page-faults               #    0.007 M/sec                  
     6,449,602,435      cycles                    #    3.250 GHz                    
     2,017,586,236      stalled-cycles-frontend   #   31.28% frontend cycles idle   
    11,288,450,095      instructions              #    1.75  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     1,917,985,811      branches                  #  966.389 M/sec                  
        79,081,905      branch-misses             #    4.12% of all branches        

       2.028683169 seconds time elapsed


real	0m2.858s
user	0m2.472s
sys	0m0.315s

 Performance counter stats for './__run 8':

       2787.161758      task-clock (msec)         #    0.974 CPUs utilized          
             2,046      context-switches          #    0.734 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            19,075      page-faults               #    0.007 M/sec                  
     9,050,451,305      cycles                    #    3.247 GHz                    
     2,805,653,495      stalled-cycles-frontend   #   31.00% frontend cycles idle   
    15,897,855,774      instructions              #    1.76  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     2,701,061,777      branches                  #  969.108 M/sec                  
       111,343,792      branch-misses             #    4.12% of all branches        

       2.860956535 seconds time elapsed


real	0m2.680s
user	0m2.361s
sys	0m0.280s

 Performance counter stats for './__run 9':

       2642.665179      task-clock (msec)         #    0.985 CPUs utilized          
             1,839      context-switches          #    0.696 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
            18,106      page-faults               #    0.007 M/sec                  
     8,579,157,728      cycles                    #    3.246 GHz                    
     2,658,247,297      stalled-cycles-frontend   #   30.98% frontend cycles idle   
    15,071,036,067      instructions              #    1.76  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     2,560,576,102      branches                  #  968.937 M/sec                  
       105,537,409      branch-misses             #    4.12% of all branches        

       2.683817833 seconds time elapsed


real	0m3.894s
user	0m3.272s
sys	0m0.486s

 Performance counter stats for './__run 10':

       3756.955376      task-clock (msec)         #    0.964 CPUs utilized          
             2,760      context-switches          #    0.735 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            25,618      page-faults               #    0.007 M/sec                  
    12,218,780,873      cycles                    #    3.252 GHz                    
     3,795,549,807      stalled-cycles-frontend   #   31.06% frontend cycles idle   
    21,434,503,837      instructions              #    1.75  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     3,641,689,325      branches                  #  969.319 M/sec                  
       150,015,680      branch-misses             #    4.12% of all branches        

       3.896908415 seconds time elapsed

