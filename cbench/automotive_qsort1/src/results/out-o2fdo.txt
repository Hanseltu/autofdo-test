
real	0m5.378s
user	0m5.309s
sys	0m0.068s

 Performance counter stats for './__run 1':

       5379.718239      task-clock (msec)         #    1.000 CPUs utilized          
                 9      context-switches          #    0.002 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
               476      page-faults               #    0.088 K/sec                  
    17,324,396,946      cycles                    #    3.220 GHz                    
     4,780,349,432      stalled-cycles-frontend   #   27.59% frontend cycles idle   
    32,307,055,005      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     8,144,430,792      branches                  # 1513.914 M/sec                  
       160,937,626      branch-misses             #    1.98% of all branches        

       5.380911646 seconds time elapsed


real	0m5.444s
user	0m5.337s
sys	0m0.104s

 Performance counter stats for './__run 1':

       5442.637462      task-clock (msec)         #    0.999 CPUs utilized          
               211      context-switches          #    0.039 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.087 K/sec                  
    17,529,351,615      cycles                    #    3.221 GHz                    
     4,948,982,287      stalled-cycles-frontend   #   28.23% frontend cycles idle   
    32,308,974,849      instructions              #    1.84  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     8,144,825,049      branches                  # 1496.485 M/sec                  
       160,965,400      branch-misses             #    1.98% of all branches        

       5.446343090 seconds time elapsed


real	0m5.413s
user	0m5.322s
sys	0m0.088s

 Performance counter stats for './__run 1':

       5412.522394      task-clock (msec)         #    0.999 CPUs utilized          
                85      context-switches          #    0.016 K/sec                  
                 4      cpu-migrations            #    0.001 K/sec                  
               478      page-faults               #    0.088 K/sec                  
    17,380,464,224      cycles                    #    3.211 GHz                    
     4,875,958,118      stalled-cycles-frontend   #   28.05% frontend cycles idle   
    32,308,670,880      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     8,144,727,105      branches                  # 1504.793 M/sec                  
       160,956,729      branch-misses             #    1.98% of all branches        

       5.415560285 seconds time elapsed


real	0m10.096s
user	0m9.931s
sys	0m0.164s

 Performance counter stats for './__run 2':

      10097.410489      task-clock (msec)         #    1.000 CPUs utilized          
                 8      context-switches          #    0.001 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               477      page-faults               #    0.047 K/sec                  
    32,515,571,388      cycles                    #    3.220 GHz                    
     9,035,540,867      stalled-cycles-frontend   #   27.79% frontend cycles idle   
    60,463,593,924      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    15,242,796,649      branches                  # 1509.575 M/sec                  
       301,061,301      branch-misses             #    1.98% of all branches        

      10.098039721 seconds time elapsed


real	0m10.165s
user	0m9.971s
sys	0m0.192s

 Performance counter stats for './__run 2':

      10164.892035      task-clock (msec)         #    1.000 CPUs utilized          
               137      context-switches          #    0.013 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               471      page-faults               #    0.046 K/sec                  
    32,725,237,361      cycles                    #    3.219 GHz                    
     9,225,966,577      stalled-cycles-frontend   #   28.19% frontend cycles idle   
    60,487,701,762      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    15,246,954,539      branches                  # 1499.962 M/sec                  
       301,110,520      branch-misses             #    1.97% of all branches        

      10.167842330 seconds time elapsed


real	0m10.051s
user	0m9.938s
sys	0m0.112s

 Performance counter stats for './__run 2':

      10051.998839      task-clock (msec)         #    1.000 CPUs utilized          
                27      context-switches          #    0.003 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               477      page-faults               #    0.047 K/sec                  
    32,422,078,333      cycles                    #    3.225 GHz                    
     8,942,483,101      stalled-cycles-frontend   #   27.58% frontend cycles idle   
    60,464,721,128      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    15,242,996,440      branches                  # 1516.414 M/sec                  
       301,034,242      branch-misses             #    1.97% of all branches        

      10.053017765 seconds time elapsed


real	0m7.210s
user	0m7.055s
sys	0m0.152s

 Performance counter stats for './__run 3':

       7209.632759      task-clock (msec)         #    1.000 CPUs utilized          
               135      context-switches          #    0.019 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               479      page-faults               #    0.066 K/sec                  
    23,246,155,483      cycles                    #    3.224 GHz                    
     6,422,892,492      stalled-cycles-frontend   #   27.63% frontend cycles idle   
    43,314,985,876      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    10,919,468,831      branches                  # 1514.567 M/sec                  
       215,852,721      branch-misses             #    1.98% of all branches        

       7.212309771 seconds time elapsed


real	0m7.189s
user	0m7.112s
sys	0m0.076s

 Performance counter stats for './__run 3':

       7190.512162      task-clock (msec)         #    1.000 CPUs utilized          
                15      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.066 K/sec                  
    23,179,717,636      cycles                    #    3.224 GHz                    
     6,326,437,764      stalled-cycles-frontend   #   27.29% frontend cycles idle   
    43,313,666,824      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    10,919,169,701      branches                  # 1518.552 M/sec                  
       215,738,049      branch-misses             #    1.98% of all branches        

       7.191288630 seconds time elapsed


real	0m7.258s
user	0m7.167s
sys	0m0.088s

 Performance counter stats for './__run 3':

       7258.705103      task-clock (msec)         #    1.000 CPUs utilized          
                81      context-switches          #    0.011 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
               477      page-faults               #    0.066 K/sec                  
    23,413,438,006      cycles                    #    3.226 GHz                    
     6,603,419,515      stalled-cycles-frontend   #   28.20% frontend cycles idle   
    43,314,500,770      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    10,919,336,233      branches                  # 1504.309 M/sec                  
       215,758,963      branch-misses             #    1.98% of all branches        

       7.260695268 seconds time elapsed


real	0m6.305s
user	0m6.201s
sys	0m0.104s

 Performance counter stats for './__run 4':

       6306.950878      task-clock (msec)         #    1.000 CPUs utilized          
                10      context-switches          #    0.002 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               470      page-faults               #    0.075 K/sec                  
    20,329,850,062      cycles                    #    3.223 GHz                    
     5,707,079,817      stalled-cycles-frontend   #   28.07% frontend cycles idle   
    37,595,984,069      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     9,477,750,276      branches                  # 1502.747 M/sec                  
       190,746,634      branch-misses             #    2.01% of all branches        

       6.307809929 seconds time elapsed


real	0m6.342s
user	0m6.261s
sys	0m0.080s

 Performance counter stats for './__run 4':

       6344.509871      task-clock (msec)         #    1.000 CPUs utilized          
                16      context-switches          #    0.003 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               477      page-faults               #    0.075 K/sec                  
    20,457,188,636      cycles                    #    3.224 GHz                    
     5,877,933,011      stalled-cycles-frontend   #   28.73% frontend cycles idle   
    37,596,943,874      instructions              #    1.84  insn per cycle         
                                                  #    0.16  stalled cycles per insn
     9,477,914,043      branches                  # 1493.876 M/sec                  
       187,359,351      branch-misses             #    1.98% of all branches        

       6.345485961 seconds time elapsed


real	0m6.282s
user	0m6.222s
sys	0m0.060s

 Performance counter stats for './__run 4':

       6285.244426      task-clock (msec)         #    1.000 CPUs utilized          
                 9      context-switches          #    0.001 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.076 K/sec                  
    20,265,106,481      cycles                    #    3.224 GHz                    
     5,621,189,569      stalled-cycles-frontend   #   27.74% frontend cycles idle   
    37,595,897,440      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     9,477,735,521      branches                  # 1507.934 M/sec                  
       187,142,352      branch-misses             #    1.97% of all branches        

       6.286362091 seconds time elapsed


real	0m8.922s
user	0m8.763s
sys	0m0.156s

 Performance counter stats for './__run 5':

       8921.163147      task-clock (msec)         #    1.000 CPUs utilized          
               169      context-switches          #    0.019 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               473      page-faults               #    0.053 K/sec                  
    28,747,168,890      cycles                    #    3.222 GHz                    
     7,922,131,609      stalled-cycles-frontend   #   27.56% frontend cycles idle   
    53,606,337,379      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    13,513,899,564      branches                  # 1514.814 M/sec                  
       266,878,093      branch-misses             #    1.97% of all branches        

       8.924578626 seconds time elapsed


real	0m8.895s
user	0m8.786s
sys	0m0.108s

 Performance counter stats for './__run 5':

       8896.859070      task-clock (msec)         #    1.000 CPUs utilized          
                24      context-switches          #    0.003 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               474      page-faults               #    0.053 K/sec                  
    28,657,940,622      cycles                    #    3.221 GHz                    
     7,836,260,858      stalled-cycles-frontend   #   27.34% frontend cycles idle   
    53,604,485,298      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    13,513,486,864      branches                  # 1518.905 M/sec                  
       266,720,861      branch-misses             #    1.97% of all branches        

       8.897876633 seconds time elapsed


real	0m8.994s
user	0m8.885s
sys	0m0.108s

 Performance counter stats for './__run 5':

       8995.535518      task-clock (msec)         #    1.000 CPUs utilized          
                44      context-switches          #    0.005 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
               476      page-faults               #    0.053 K/sec                  
    28,987,197,823      cycles                    #    3.222 GHz                    
     8,178,038,605      stalled-cycles-frontend   #   28.21% frontend cycles idle   
    53,605,063,000      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    13,513,603,950      branches                  # 1502.257 M/sec                  
       266,907,452      branch-misses             #    1.98% of all branches        

       8.996993872 seconds time elapsed


real	0m30.990s
user	0m30.560s
sys	0m0.417s

 Performance counter stats for './__run 6':

      30979.965831      task-clock (msec)         #    1.000 CPUs utilized          
               660      context-switches          #    0.021 K/sec                  
                 8      cpu-migrations            #    0.000 K/sec                  
               474      page-faults               #    0.015 K/sec                  
    99,724,610,253      cycles                    #    3.219 GHz                    
    27,578,239,894      stalled-cycles-frontend   #   27.65% frontend cycles idle   
   185,820,419,071      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    46,845,344,508      branches                  # 1512.117 M/sec                  
       924,865,603      branch-misses             #    1.97% of all branches        

      30.993253534 seconds time elapsed


real	0m31.108s
user	0m30.664s
sys	0m0.432s

 Performance counter stats for './__run 6':

      31103.799552      task-clock (msec)         #    1.000 CPUs utilized          
               339      context-switches          #    0.011 K/sec                  
                 6      cpu-migrations            #    0.000 K/sec                  
               478      page-faults               #    0.015 K/sec                  
   100,198,888,531      cycles                    #    3.221 GHz                    
    28,039,502,247      stalled-cycles-frontend   #   27.98% frontend cycles idle   
   185,829,490,277      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    46,846,773,629      branches                  # 1506.143 M/sec                  
       925,391,907      branch-misses             #    1.98% of all branches        

      31.110611610 seconds time elapsed


real	0m31.010s
user	0m30.637s
sys	0m0.372s

 Performance counter stats for './__run 6':

      31011.239378      task-clock (msec)         #    1.000 CPUs utilized          
                35      context-switches          #    0.001 K/sec                  
                 5      cpu-migrations            #    0.000 K/sec                  
               469      page-faults               #    0.015 K/sec                  
   100,105,575,478      cycles                    #    3.228 GHz                    
    27,974,461,256      stalled-cycles-frontend   #   27.94% frontend cycles idle   
   185,811,832,665      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    46,843,592,532      branches                  # 1510.536 M/sec                  
       924,997,220      branch-misses             #    1.97% of all branches        

      31.012466189 seconds time elapsed


real	0m7.796s
user	0m7.594s
sys	0m0.200s

 Performance counter stats for './__run 7':

       7796.529215      task-clock (msec)         #    1.000 CPUs utilized          
               126      context-switches          #    0.016 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
               476      page-faults               #    0.061 K/sec                  
    25,053,568,099      cycles                    #    3.213 GHz                    
     6,983,410,947      stalled-cycles-frontend   #   27.87% frontend cycles idle   
    46,478,767,388      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    11,715,403,561      branches                  # 1502.643 M/sec                  
       231,291,582      branch-misses             #    1.97% of all branches        

       7.799185970 seconds time elapsed


real	0m7.701s
user	0m7.629s
sys	0m0.072s

 Performance counter stats for './__run 7':

       7703.671019      task-clock (msec)         #    1.000 CPUs utilized          
                15      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               473      page-faults               #    0.061 K/sec                  
    24,804,779,309      cycles                    #    3.220 GHz                    
     6,747,930,716      stalled-cycles-frontend   #   27.20% frontend cycles idle   
    46,458,615,776      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    11,711,986,106      branches                  # 1520.312 M/sec                  
       231,261,831      branch-misses             #    1.97% of all branches        

       7.704530055 seconds time elapsed


real	0m7.769s
user	0m7.661s
sys	0m0.108s

 Performance counter stats for './__run 7':

       7771.408306      task-clock (msec)         #    1.000 CPUs utilized          
                10      context-switches          #    0.001 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               473      page-faults               #    0.061 K/sec                  
    25,057,596,381      cycles                    #    3.224 GHz                    
     7,038,865,734      stalled-cycles-frontend   #   28.09% frontend cycles idle   
    46,457,940,541      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    11,711,876,357      branches                  # 1507.047 M/sec                  
       231,331,813      branch-misses             #    1.98% of all branches        

       7.772280935 seconds time elapsed


real	0m10.927s
user	0m10.767s
sys	0m0.156s

 Performance counter stats for './__run 8':

      10926.392442      task-clock (msec)         #    1.000 CPUs utilized          
               157      context-switches          #    0.014 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               478      page-faults               #    0.044 K/sec                  
    35,183,859,085      cycles                    #    3.220 GHz                    
     9,577,703,796      stalled-cycles-frontend   #   27.22% frontend cycles idle   
    65,898,723,131      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    16,612,830,937      branches                  # 1520.431 M/sec                  
       327,969,773      branch-misses             #    1.97% of all branches        

      10.929603423 seconds time elapsed


real	0m11.036s
user	0m10.893s
sys	0m0.140s

 Performance counter stats for './__run 8':

      11036.602286      task-clock (msec)         #    1.000 CPUs utilized          
               125      context-switches          #    0.011 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.043 K/sec                  
    35,586,597,725      cycles                    #    3.224 GHz                    
     9,991,911,485      stalled-cycles-frontend   #   28.08% frontend cycles idle   
    65,900,562,216      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    16,613,083,337      branches                  # 1505.272 M/sec                  
       328,197,183      branch-misses             #    1.98% of all branches        

      11.039258448 seconds time elapsed


real	0m10.961s
user	0m10.737s
sys	0m0.220s

 Performance counter stats for './__run 8':

      10960.577521      task-clock (msec)         #    1.000 CPUs utilized          
               182      context-switches          #    0.017 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
               474      page-faults               #    0.043 K/sec                  
    35,282,823,473      cycles                    #    3.219 GHz                    
     9,674,050,324      stalled-cycles-frontend   #   27.42% frontend cycles idle   
    65,898,749,660      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    16,612,833,941      branches                  # 1515.690 M/sec                  
       328,000,890      branch-misses             #    1.97% of all branches        

      10.964271760 seconds time elapsed


real	0m6.093s
user	0m5.969s
sys	0m0.120s

 Performance counter stats for './__run 9':

       6093.232651      task-clock (msec)         #    1.000 CPUs utilized          
               128      context-switches          #    0.021 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               478      page-faults               #    0.078 K/sec                  
    19,624,016,439      cycles                    #    3.221 GHz                    
     5,399,870,799      stalled-cycles-frontend   #   27.52% frontend cycles idle   
    36,600,775,197      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     9,226,529,977      branches                  # 1514.226 M/sec                  
       182,212,507      branch-misses             #    1.97% of all branches        

       6.095799794 seconds time elapsed


real	0m6.190s
user	0m6.109s
sys	0m0.080s

 Performance counter stats for './__run 9':

       6191.491391      task-clock (msec)         #    1.000 CPUs utilized          
                14      context-switches          #    0.002 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
               474      page-faults               #    0.077 K/sec                  
    19,889,818,011      cycles                    #    3.212 GHz                    
     5,675,725,712      stalled-cycles-frontend   #   28.54% frontend cycles idle   
    36,612,505,667      instructions              #    1.84  insn per cycle         
                                                  #    0.16  stalled cycles per insn
     9,228,475,660      branches                  # 1490.509 M/sec                  
       182,966,553      branch-misses             #    1.98% of all branches        

       6.192276935 seconds time elapsed


real	0m6.153s
user	0m6.069s
sys	0m0.084s

 Performance counter stats for './__run 9':

       6156.107789      task-clock (msec)         #    1.000 CPUs utilized          
                30      context-switches          #    0.005 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.077 K/sec                  
    19,823,439,195      cycles                    #    3.220 GHz                    
     5,667,660,587      stalled-cycles-frontend   #   28.59% frontend cycles idle   
    36,598,071,056      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     9,226,033,525      branches                  # 1498.680 M/sec                  
       182,187,120      branch-misses             #    1.97% of all branches        

       6.157042884 seconds time elapsed


real	0m7.060s
user	0m6.947s
sys	0m0.112s

 Performance counter stats for './__run 10':

       7062.558694      task-clock (msec)         #    1.000 CPUs utilized          
                25      context-switches          #    0.004 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
               474      page-faults               #    0.067 K/sec                  
    22,736,583,083      cycles                    #    3.219 GHz                    
     6,325,864,925      stalled-cycles-frontend   #   27.82% frontend cycles idle   
    42,028,904,122      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    10,595,193,362      branches                  # 1500.192 M/sec                  
       218,090,784      branch-misses             #    2.06% of all branches        

       7.063718480 seconds time elapsed


real	0m6.995s
user	0m6.923s
sys	0m0.072s

 Performance counter stats for './__run 10':

       6997.900313      task-clock (msec)         #    1.000 CPUs utilized          
                 9      context-switches          #    0.001 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               475      page-faults               #    0.068 K/sec                  
    22,587,294,175      cycles                    #    3.228 GHz                    
     6,269,183,513      stalled-cycles-frontend   #   27.76% frontend cycles idle   
    42,027,824,409      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    10,595,000,700      branches                  # 1514.026 M/sec                  
       209,497,311      branch-misses             #    1.98% of all branches        

       6.998503857 seconds time elapsed


real	0m7.039s
user	0m6.904s
sys	0m0.132s

 Performance counter stats for './__run 10':

       7038.333087      task-clock (msec)         #    0.999 CPUs utilized          
               205      context-switches          #    0.029 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               476      page-faults               #    0.068 K/sec                  
    22,669,928,349      cycles                    #    3.221 GHz                    
     6,350,280,643      stalled-cycles-frontend   #   28.01% frontend cycles idle   
    42,030,340,030      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
    10,595,509,397      branches                  # 1505.400 M/sec                  
       209,310,940      branch-misses             #    1.98% of all branches        

       7.042340821 seconds time elapsed

