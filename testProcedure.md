##  Test steps in compiler optimal in cbenchmark

* Test Machine : ubuntu 16.04

* GCC Version : 5.4.0

all commands are in the path of .../cbench/automotive_bitcount/src

### step 1 : (-O2)

'''
gcc -O2 \*.c

./__run 1

./__run 2
'''

#### result
