
real	0m16.279s
user	0m8.121s
sys	0m1.390s

 Performance counter stats for './__run 1':

       9409.410483      task-clock (msec)         #    0.578 CPUs utilized          
            31,749      context-switches          #    0.003 M/sec                  
               306      cpu-migrations            #    0.033 K/sec                  
            17,194      page-faults               #    0.002 M/sec                  
    28,853,722,115      cycles                    #    3.066 GHz                    
    18,040,243,073      stalled-cycles-frontend   #   62.52% frontend cycles idle   
    29,164,333,030      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     4,169,232,450      branches                  #  443.092 M/sec                  
       248,714,160      branch-misses             #    5.97% of all branches        

      16.281750320 seconds time elapsed


real	0m16.814s
user	0m8.314s
sys	0m1.301s

 Performance counter stats for './__run 1':

       9525.128876      task-clock (msec)         #    0.566 CPUs utilized          
            29,126      context-switches          #    0.003 M/sec                  
               312      cpu-migrations            #    0.033 K/sec                  
            17,193      page-faults               #    0.002 M/sec                  
    29,204,703,948      cycles                    #    3.066 GHz                    
    18,373,462,942      stalled-cycles-frontend   #   62.91% frontend cycles idle   
    29,154,684,088      instructions              #    1.00  insn per cycle         
                                                  #    0.63  stalled cycles per insn
     4,166,935,675      branches                  #  437.468 M/sec                  
       248,826,243      branch-misses             #    5.97% of all branches        

      16.821794581 seconds time elapsed


real	0m18.554s
user	0m8.326s
sys	0m1.360s

 Performance counter stats for './__run 1':

       9589.588841      task-clock (msec)         #    0.517 CPUs utilized          
            32,195      context-switches          #    0.003 M/sec                  
               345      cpu-migrations            #    0.036 K/sec                  
            17,194      page-faults               #    0.002 M/sec                  
    29,394,606,397      cycles                    #    3.065 GHz                    
    18,469,171,927      stalled-cycles-frontend   #   62.83% frontend cycles idle   
    29,165,333,781      instructions              #    0.99  insn per cycle         
                                                  #    0.63  stalled cycles per insn
     4,169,490,539      branches                  #  434.793 M/sec                  
       248,648,225      branch-misses             #    5.96% of all branches        

      18.558151104 seconds time elapsed


real	0m15.758s
user	0m5.814s
sys	0m0.923s

 Performance counter stats for './__run 2':

       6667.212858      task-clock (msec)         #    0.423 CPUs utilized          
            22,136      context-switches          #    0.003 M/sec                  
               301      cpu-migrations            #    0.045 K/sec                  
            12,231      page-faults               #    0.002 M/sec                  
    20,404,301,099      cycles                    #    3.060 GHz                    
    12,755,854,866      stalled-cycles-frontend   #   62.52% frontend cycles idle   
    20,619,618,040      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     2,948,035,782      branches                  #  442.169 M/sec                  
       175,819,483      branch-misses             #    5.96% of all branches        

      15.766047947 seconds time elapsed


real	0m15.336s
user	0m5.783s
sys	0m0.886s

 Performance counter stats for './__run 2':

       6610.923553      task-clock (msec)         #    0.431 CPUs utilized          
            19,723      context-switches          #    0.003 M/sec                  
               293      cpu-migrations            #    0.044 K/sec                  
            12,236      page-faults               #    0.002 M/sec                  
    20,274,034,293      cycles                    #    3.067 GHz                    
    12,629,559,651      stalled-cycles-frontend   #   62.29% frontend cycles idle   
    20,611,272,400      instructions              #    1.02  insn per cycle         
                                                  #    0.61  stalled cycles per insn
     2,946,011,124      branches                  #  445.628 M/sec                  
       175,613,296      branch-misses             #    5.96% of all branches        

      15.339646301 seconds time elapsed


real	0m16.099s
user	0m5.800s
sys	0m0.931s

 Performance counter stats for './__run 2':

       6668.717178      task-clock (msec)         #    0.414 CPUs utilized          
            19,034      context-switches          #    0.003 M/sec                  
               304      cpu-migrations            #    0.046 K/sec                  
            12,237      page-faults               #    0.002 M/sec                  
    20,447,173,273      cycles                    #    3.066 GHz                    
    12,720,593,473      stalled-cycles-frontend   #   62.21% frontend cycles idle   
    20,601,490,796      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     2,944,349,571      branches                  #  441.517 M/sec                  
       175,624,649      branch-misses             #    5.96% of all branches        

      16.104037408 seconds time elapsed


real	0m0.077s
user	0m0.049s
sys	0m0.008s

 Performance counter stats for './__run 3':

         60.947658      task-clock (msec)         #    0.746 CPUs utilized          
               126      context-switches          #    0.002 M/sec                  
                 2      cpu-migrations            #    0.033 K/sec                  
               386      page-faults               #    0.006 M/sec                  
       187,377,042      cycles                    #    3.074 GHz                    
       118,397,369      stalled-cycles-frontend   #   63.19% frontend cycles idle   
       184,368,550      instructions              #    0.98  insn per cycle         
                                                  #    0.64  stalled cycles per insn
        26,647,774      branches                  #  437.224 M/sec                  
         1,567,549      branch-misses             #    5.88% of all branches        

       0.081735116 seconds time elapsed


real	0m0.070s
user	0m0.048s
sys	0m0.004s

 Performance counter stats for './__run 3':

         53.866401      task-clock (msec)         #    0.731 CPUs utilized          
               101      context-switches          #    0.002 M/sec                  
                 5      cpu-migrations            #    0.093 K/sec                  
               385      page-faults               #    0.007 M/sec                  
       164,509,487      cycles                    #    3.054 GHz                    
        93,933,116      stalled-cycles-frontend   #   57.10% frontend cycles idle   
       184,295,387      instructions              #    1.12  insn per cycle         
                                                  #    0.51  stalled cycles per insn
        26,632,643      branches                  #  494.420 M/sec                  
         1,558,678      branch-misses             #    5.85% of all branches        

       0.073666059 seconds time elapsed


real	0m0.153s
user	0m0.054s
sys	0m0.004s

 Performance counter stats for './__run 3':

         59.878896      task-clock (msec)         #    0.383 CPUs utilized          
               310      context-switches          #    0.005 M/sec                  
                 8      cpu-migrations            #    0.134 K/sec                  
               387      page-faults               #    0.006 M/sec                  
       182,080,049      cycles                    #    3.041 GHz                    
       111,150,459      stalled-cycles-frontend   #   61.04% frontend cycles idle   
       185,182,198      instructions              #    1.02  insn per cycle         
                                                  #    0.60  stalled cycles per insn
        26,815,734      branches                  #  447.833 M/sec                  
         1,575,585      branch-misses             #    5.88% of all branches        

       0.156439518 seconds time elapsed


real	0m3.662s
user	0m0.997s
sys	0m0.161s

 Performance counter stats for './__run 4':

       1150.633109      task-clock (msec)         #    0.313 CPUs utilized          
             3,471      context-switches          #    0.003 M/sec                  
                58      cpu-migrations            #    0.050 K/sec                  
             2,339      page-faults               #    0.002 M/sec                  
     3,519,262,176      cycles                    #    3.059 GHz                    
     2,183,866,432      stalled-cycles-frontend   #   62.05% frontend cycles idle   
     3,550,470,847      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
       507,764,588      branches                  #  441.291 M/sec                  
        30,258,653      branch-misses             #    5.96% of all branches        

       3.677582437 seconds time elapsed


real	0m3.057s
user	0m1.053s
sys	0m0.138s

 Performance counter stats for './__run 4':

       1183.962808      task-clock (msec)         #    0.387 CPUs utilized          
             3,093      context-switches          #    0.003 M/sec                  
                61      cpu-migrations            #    0.052 K/sec                  
             2,337      page-faults               #    0.002 M/sec                  
     3,628,618,290      cycles                    #    3.065 GHz                    
     2,309,845,861      stalled-cycles-frontend   #   63.66% frontend cycles idle   
     3,547,743,915      instructions              #    0.98  insn per cycle         
                                                  #    0.65  stalled cycles per insn
       507,230,285      branches                  #  428.417 M/sec                  
        30,303,121      branch-misses             #    5.97% of all branches        

       3.061150539 seconds time elapsed


real	0m2.825s
user	0m0.993s
sys	0m0.153s

 Performance counter stats for './__run 4':

       1141.565274      task-clock (msec)         #    0.403 CPUs utilized          
             2,561      context-switches          #    0.002 M/sec                  
                49      cpu-migrations            #    0.043 K/sec                  
             2,343      page-faults               #    0.002 M/sec                  
     3,503,847,137      cycles                    #    3.069 GHz                    
     2,186,219,932      stalled-cycles-frontend   #   62.39% frontend cycles idle   
     3,545,383,533      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
       506,783,122      branches                  #  443.937 M/sec                  
        30,215,028      branch-misses             #    5.96% of all branches        

       2.831090056 seconds time elapsed


real	0m50.926s
user	0m19.812s
sys	0m2.882s

 Performance counter stats for './__run 5':

      22457.584857      task-clock (msec)         #    0.441 CPUs utilized          
            68,940      context-switches          #    0.003 M/sec                  
               913      cpu-migrations            #    0.041 K/sec                  
            40,691      page-faults               #    0.002 M/sec                  
    68,848,517,379      cycles                    #    3.066 GHz                    
    42,998,192,546      stalled-cycles-frontend   #   62.45% frontend cycles idle   
    69,654,230,358      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     9,955,419,370      branches                  #  443.299 M/sec                  
       593,539,667      branch-misses             #    5.96% of all branches        

      50.928505810 seconds time elapsed


real	0m50.136s
user	0m19.546s
sys	0m3.242s

 Performance counter stats for './__run 5':

      22562.684979      task-clock (msec)         #    0.450 CPUs utilized          
            72,498      context-switches          #    0.003 M/sec                  
               922      cpu-migrations            #    0.041 K/sec                  
            40,692      page-faults               #    0.002 M/sec                  
    69,117,099,217      cycles                    #    3.063 GHz                    
    43,262,947,413      stalled-cycles-frontend   #   62.59% frontend cycles idle   
    69,670,147,379      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     9,958,819,445      branches                  #  441.385 M/sec                  
       594,497,446      branch-misses             #    5.97% of all branches        

      50.139815833 seconds time elapsed


real	0m50.328s
user	0m19.428s
sys	0m2.594s

 Performance counter stats for './__run 5':

      21914.070690      task-clock (msec)         #    0.435 CPUs utilized          
            33,570      context-switches          #    0.002 M/sec                  
               947      cpu-migrations            #    0.043 K/sec                  
            40,691      page-faults               #    0.002 M/sec                  
    67,399,280,707      cycles                    #    3.076 GHz                    
    41,475,873,295      stalled-cycles-frontend   #   61.54% frontend cycles idle   
    69,536,977,894      instructions              #    1.03  insn per cycle         
                                                  #    0.60  stalled cycles per insn
     9,929,621,071      branches                  #  453.116 M/sec                  
       593,353,589      branch-misses             #    5.98% of all branches        

      50.336074981 seconds time elapsed


real	0m30.677s
user	0m11.770s
sys	0m1.329s

 Performance counter stats for './__run 6':

      13060.657104      task-clock (msec)         #    0.426 CPUs utilized          
            10,152      context-switches          #    0.777 K/sec                  
               602      cpu-migrations            #    0.046 K/sec                  
            24,676      page-faults               #    0.002 M/sec                  
    40,248,507,129      cycles                    #    3.082 GHz                    
    24,698,317,422      stalled-cycles-frontend   #   61.36% frontend cycles idle   
    41,955,609,819      instructions              #    1.04  insn per cycle         
                                                  #    0.59  stalled cycles per insn
     5,989,312,458      branches                  #  458.577 M/sec                  
       357,498,602      branch-misses             #    5.97% of all branches        

      30.683615961 seconds time elapsed


real	0m30.768s
user	0m11.801s
sys	0m1.317s

 Performance counter stats for './__run 6':

      13087.837724      task-clock (msec)         #    0.425 CPUs utilized          
             8,789      context-switches          #    0.672 K/sec                  
               568      cpu-migrations            #    0.043 K/sec                  
            24,674      page-faults               #    0.002 M/sec                  
    40,344,642,276      cycles                    #    3.083 GHz                    
    24,972,075,436      stalled-cycles-frontend   #   61.90% frontend cycles idle   
    41,942,955,531      instructions              #    1.04  insn per cycle         
                                                  #    0.60  stalled cycles per insn
     5,986,655,148      branches                  #  457.421 M/sec                  
       356,897,121      branch-misses             #    5.96% of all branches        

      30.779443111 seconds time elapsed


real	0m26.662s
user	0m11.982s
sys	0m1.299s

 Performance counter stats for './__run 6':

      13248.559016      task-clock (msec)         #    0.497 CPUs utilized          
             9,252      context-switches          #    0.698 K/sec                  
               566      cpu-migrations            #    0.043 K/sec                  
            24,674      page-faults               #    0.002 M/sec                  
    40,836,031,442      cycles                    #    3.082 GHz                    
    25,405,957,101      stalled-cycles-frontend   #   62.21% frontend cycles idle   
    41,937,821,621      instructions              #    1.03  insn per cycle         
                                                  #    0.61  stalled cycles per insn
     5,985,869,200      branches                  #  451.813 M/sec                  
       356,630,701      branch-misses             #    5.96% of all branches        

      26.667844777 seconds time elapsed


real	0m2.026s
user	0m1.073s
sys	0m0.133s

 Performance counter stats for './__run 7':

       1203.331772      task-clock (msec)         #    0.593 CPUs utilized          
               869      context-switches          #    0.722 K/sec                  
                47      cpu-migrations            #    0.039 K/sec                  
             2,502      page-faults               #    0.002 M/sec                  
     3,707,722,431      cycles                    #    3.081 GHz                    
     2,284,321,611      stalled-cycles-frontend   #   61.61% frontend cycles idle   
     3,814,795,266      instructions              #    1.03  insn per cycle         
                                                  #    0.60  stalled cycles per insn
       544,803,957      branches                  #  452.746 M/sec                  
        32,514,083      branch-misses             #    5.97% of all branches        

       2.029251725 seconds time elapsed


real	0m2.323s
user	0m1.071s
sys	0m0.142s

 Performance counter stats for './__run 7':

       1213.722223      task-clock (msec)         #    0.521 CPUs utilized          
             1,066      context-switches          #    0.878 K/sec                  
                54      cpu-migrations            #    0.044 K/sec                  
             2,500      page-faults               #    0.002 M/sec                  
     3,737,356,895      cycles                    #    3.079 GHz                    
     2,323,460,145      stalled-cycles-frontend   #   62.17% frontend cycles idle   
     3,818,877,172      instructions              #    1.02  insn per cycle         
                                                  #    0.61  stalled cycles per insn
       545,563,018      branches                  #  449.496 M/sec                  
        32,512,724      branch-misses             #    5.96% of all branches        

       2.329531662 seconds time elapsed


real	0m2.004s
user	0m1.045s
sys	0m0.124s

 Performance counter stats for './__run 7':

       1169.072980      task-clock (msec)         #    0.582 CPUs utilized          
               895      context-switches          #    0.766 K/sec                  
                67      cpu-migrations            #    0.057 K/sec                  
             2,495      page-faults               #    0.002 M/sec                  
     3,603,351,287      cycles                    #    3.082 GHz                    
     2,193,589,060      stalled-cycles-frontend   #   60.88% frontend cycles idle   
     3,815,350,533      instructions              #    1.06  insn per cycle         
                                                  #    0.57  stalled cycles per insn
       544,903,118      branches                  #  466.098 M/sec                  
        32,450,166      branch-misses             #    5.96% of all branches        

       2.009705124 seconds time elapsed


real	0m6.446s
user	0m3.157s
sys	0m0.360s

 Performance counter stats for './__run 8':

       3507.540562      task-clock (msec)         #    0.544 CPUs utilized          
             2,708      context-switches          #    0.772 K/sec                  
               178      cpu-migrations            #    0.051 K/sec                  
             6,821      page-faults               #    0.002 M/sec                  
    10,808,526,767      cycles                    #    3.082 GHz                    
     6,587,180,797      stalled-cycles-frontend   #   60.94% frontend cycles idle   
    11,246,341,231      instructions              #    1.04  insn per cycle         
                                                  #    0.59  stalled cycles per insn
     1,605,680,493      branches                  #  457.780 M/sec                  
        95,953,324      branch-misses             #    5.98% of all branches        

       6.450781480 seconds time elapsed


real	0m8.805s
user	0m3.128s
sys	0m0.337s

 Performance counter stats for './__run 8':

       3459.772218      task-clock (msec)         #    0.393 CPUs utilized          
             2,548      context-switches          #    0.736 K/sec                  
               151      cpu-migrations            #    0.044 K/sec                  
             6,816      page-faults               #    0.002 M/sec                  
    10,659,975,480      cycles                    #    3.081 GHz                    
     6,481,170,567      stalled-cycles-frontend   #   60.80% frontend cycles idle   
    11,244,122,234      instructions              #    1.05  insn per cycle         
                                                  #    0.58  stalled cycles per insn
     1,605,264,683      branches                  #  463.980 M/sec                  
        95,844,757      branch-misses             #    5.97% of all branches        

       8.809341791 seconds time elapsed


real	0m6.384s
user	0m3.198s
sys	0m0.386s

 Performance counter stats for './__run 8':

       3573.003463      task-clock (msec)         #    0.559 CPUs utilized          
             2,458      context-switches          #    0.688 K/sec                  
               152      cpu-migrations            #    0.043 K/sec                  
             6,817      page-faults               #    0.002 M/sec                  
    11,011,078,342      cycles                    #    3.082 GHz                    
     6,898,340,047      stalled-cycles-frontend   #   62.65% frontend cycles idle   
    11,240,769,404      instructions              #    1.02  insn per cycle         
                                                  #    0.61  stalled cycles per insn
     1,604,657,675      branches                  #  449.106 M/sec                  
        95,816,272      branch-misses             #    5.97% of all branches        

       6.387353555 seconds time elapsed


real	0m6.050s
user	0m2.897s
sys	0m0.299s

 Performance counter stats for './__run 9':

       3190.678536      task-clock (msec)         #    0.527 CPUs utilized          
             2,351      context-switches          #    0.737 K/sec                  
               132      cpu-migrations            #    0.041 K/sec                  
             6,109      page-faults               #    0.002 M/sec                  
     9,834,617,576      cycles                    #    3.082 GHz                    
     6,199,769,824      stalled-cycles-frontend   #   63.04% frontend cycles idle   
    10,019,648,290      instructions              #    1.02  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     1,430,505,306      branches                  #  448.339 M/sec                  
        85,298,278      branch-misses             #    5.96% of all branches        

       6.056895295 seconds time elapsed


real	0m5.921s
user	0m2.845s
sys	0m0.286s

 Performance counter stats for './__run 9':

       3124.985480      task-clock (msec)         #    0.527 CPUs utilized          
             1,956      context-switches          #    0.626 K/sec                  
               145      cpu-migrations            #    0.046 K/sec                  
             6,108      page-faults               #    0.002 M/sec                  
     9,633,014,201      cycles                    #    3.083 GHz                    
     5,989,517,061      stalled-cycles-frontend   #   62.18% frontend cycles idle   
    10,016,061,130      instructions              #    1.04  insn per cycle         
                                                  #    0.60  stalled cycles per insn
     1,429,775,820      branches                  #  457.530 M/sec                  
        85,018,930      branch-misses             #    5.95% of all branches        

       5.929618123 seconds time elapsed


real	0m8.017s
user	0m2.865s
sys	0m0.363s

 Performance counter stats for './__run 9':

       3221.598009      task-clock (msec)         #    0.402 CPUs utilized          
             2,781      context-switches          #    0.863 K/sec                  
               130      cpu-migrations            #    0.040 K/sec                  
             6,108      page-faults               #    0.002 M/sec                  
     9,923,606,195      cycles                    #    3.080 GHz                    
     6,239,223,989      stalled-cycles-frontend   #   62.87% frontend cycles idle   
    10,024,986,496      instructions              #    1.01  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     1,431,513,252      branches                  #  444.349 M/sec                  
        85,328,067      branch-misses             #    5.96% of all branches        

       8.022536193 seconds time elapsed


real	0m9.615s
user	0m4.866s
sys	0m0.577s

 Performance counter stats for './__run 10':

       5428.412480      task-clock (msec)         #    0.564 CPUs utilized          
             4,338      context-switches          #    0.799 K/sec                  
               254      cpu-migrations            #    0.047 K/sec                  
            10,087      page-faults               #    0.002 M/sec                  
    16,723,575,277      cycles                    #    3.081 GHz                    
    10,565,116,915      stalled-cycles-frontend   #   63.17% frontend cycles idle   
    16,859,580,265      instructions              #    1.01  insn per cycle         
                                                  #    0.63  stalled cycles per insn
     2,406,932,429      branches                  #  443.395 M/sec                  
       143,323,181      branch-misses             #    5.95% of all branches        

       9.623821107 seconds time elapsed


real	0m8.957s
user	0m4.735s
sys	0m0.594s

 Performance counter stats for './__run 10':

       5314.941439      task-clock (msec)         #    0.593 CPUs utilized          
             3,743      context-switches          #    0.704 K/sec                  
               218      cpu-migrations            #    0.041 K/sec                  
            10,086      page-faults               #    0.002 M/sec                  
    16,379,650,089      cycles                    #    3.082 GHz                    
    10,187,240,853      stalled-cycles-frontend   #   62.19% frontend cycles idle   
    16,850,988,810      instructions              #    1.03  insn per cycle         
                                                  #    0.60  stalled cycles per insn
     2,405,400,283      branches                  #  452.573 M/sec                  
       143,533,774      branch-misses             #    5.97% of all branches        

       8.961619065 seconds time elapsed


real	0m10.086s
user	0m4.684s
sys	0m0.681s

 Performance counter stats for './__run 10':

       5351.098023      task-clock (msec)         #    0.530 CPUs utilized          
             4,206      context-switches          #    0.786 K/sec                  
               285      cpu-migrations            #    0.053 K/sec                  
            10,081      page-faults               #    0.002 M/sec                  
    16,476,935,558      cycles                    #    3.079 GHz                    
    10,285,107,184      stalled-cycles-frontend   #   62.42% frontend cycles idle   
    16,860,115,970      instructions              #    1.02  insn per cycle         
                                                  #    0.61  stalled cycles per insn
     2,407,061,160      branches                  #  449.826 M/sec                  
       143,652,334      branch-misses             #    5.97% of all branches        

      10.092708672 seconds time elapsed

