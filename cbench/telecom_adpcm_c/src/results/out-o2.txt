
real	0m2.937s
user	0m2.933s
sys	0m0.004s

 Performance counter stats for './__run 1':

       2938.990021      task-clock (msec)         #    1.000 CPUs utilized          
                 9      context-switches          #    0.003 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.086 K/sec                  
     9,588,988,177      cycles                    #    3.263 GHz                    
     1,548,368,885      stalled-cycles-frontend   #   16.15% frontend cycles idle   
    23,597,072,249      instructions              #    2.46  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     4,856,747,610      branches                  # 1652.523 M/sec                  
        49,363,522      branch-misses             #    1.02% of all branches        

       2.939876835 seconds time elapsed


real	0m0.077s
user	0m0.064s
sys	0m0.000s

 Performance counter stats for './__run 2':

         66.293186      task-clock (msec)         #    0.832 CPUs utilized          
                 8      context-switches          #    0.121 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.004 M/sec                  
       181,629,164      cycles                    #    2.740 GHz                    
        45,455,092      stalled-cycles-frontend   #   25.03% frontend cycles idle   
       360,743,018      instructions              #    1.99  insn per cycle         
                                                  #    0.13  stalled cycles per insn
        74,209,092      branches                  # 1119.408 M/sec                  
         1,939,045      branch-misses             #    2.61% of all branches        

       0.079643937 seconds time elapsed


real	0m0.796s
user	0m0.796s
sys	0m0.000s

 Performance counter stats for './__run 3':

        798.195082      task-clock (msec)         #    0.999 CPUs utilized          
                 7      context-switches          #    0.009 K/sec                  
                 3      cpu-migrations            #    0.004 K/sec                  
               255      page-faults               #    0.319 K/sec                  
     2,589,033,447      cycles                    #    3.244 GHz                    
       437,128,842      stalled-cycles-frontend   #   16.88% frontend cycles idle   
     6,223,424,390      instructions              #    2.40  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     1,280,875,183      branches                  # 1604.714 M/sec                  
        15,491,313      branch-misses             #    1.21% of all branches        

       0.798912308 seconds time elapsed


real	0m0.427s
user	0m0.422s
sys	0m0.000s

 Performance counter stats for './__run 4':

        424.692186      task-clock (msec)         #    0.986 CPUs utilized          
                 7      context-switches          #    0.016 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               254      page-faults               #    0.598 K/sec                  
     1,382,469,509      cycles                    #    3.255 GHz                    
       243,274,498      stalled-cycles-frontend   #   17.60% frontend cycles idle   
     3,272,909,658      instructions              #    2.37  insn per cycle         
                                                  #    0.07  stalled cycles per insn
       673,595,018      branches                  # 1586.078 M/sec                  
         8,799,379      branch-misses             #    1.31% of all branches        

       0.430550815 seconds time elapsed


real	0m0.471s
user	0m0.462s
sys	0m0.000s

 Performance counter stats for './__run 5':

        464.548319      task-clock (msec)         #    0.980 CPUs utilized          
                 8      context-switches          #    0.017 K/sec                  
                 2      cpu-migrations            #    0.004 K/sec                  
               254      page-faults               #    0.547 K/sec                  
     1,513,265,900      cycles                    #    3.257 GHz                    
       265,709,351      stalled-cycles-frontend   #   17.56% frontend cycles idle   
     3,589,132,337      instructions              #    2.37  insn per cycle         
                                                  #    0.07  stalled cycles per insn
       738,679,615      branches                  # 1590.103 M/sec                  
         9,545,799      branch-misses             #    1.29% of all branches        

       0.474231206 seconds time elapsed


real	0m0.183s
user	0m0.182s
sys	0m0.000s

 Performance counter stats for './__run 6':

        184.506052      task-clock (msec)         #    0.994 CPUs utilized          
                 7      context-switches          #    0.038 K/sec                  
                 1      cpu-migrations            #    0.005 K/sec                  
               252      page-faults               #    0.001 M/sec                  
       595,427,259      cycles                    #    3.227 GHz                    
       112,842,896      stalled-cycles-frontend   #   18.95% frontend cycles idle   
     1,359,335,213      instructions              #    2.28  insn per cycle         
                                                  #    0.08  stalled cycles per insn
       279,740,974      branches                  # 1516.162 M/sec                  
         4,460,704      branch-misses             #    1.59% of all branches        

       0.185550276 seconds time elapsed


real	0m0.381s
user	0m0.376s
sys	0m0.004s

 Performance counter stats for './__run 7':

        382.456179      task-clock (msec)         #    0.997 CPUs utilized          
                 9      context-switches          #    0.024 K/sec                  
                 1      cpu-migrations            #    0.003 K/sec                  
               255      page-faults               #    0.667 K/sec                  
     1,246,627,822      cycles                    #    3.260 GHz                    
       221,456,356      stalled-cycles-frontend   #   17.76% frontend cycles idle   
     2,943,361,099      instructions              #    2.36  insn per cycle         
                                                  #    0.08  stalled cycles per insn
       605,763,575      branches                  # 1583.877 M/sec                  
         8,032,787      branch-misses             #    1.33% of all branches        

       0.383503461 seconds time elapsed


real	0m0.660s
user	0m0.655s
sys	0m0.004s

 Performance counter stats for './__run 8':

        662.790884      task-clock (msec)         #    0.999 CPUs utilized          
                 9      context-switches          #    0.014 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               255      page-faults               #    0.385 K/sec                  
     2,161,248,374      cycles                    #    3.261 GHz                    
       369,706,467      stalled-cycles-frontend   #   17.11% frontend cycles idle   
     5,183,975,177      instructions              #    2.40  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     1,066,936,191      branches                  # 1609.763 M/sec                  
        12,930,606      branch-misses             #    1.21% of all branches        

       0.663394725 seconds time elapsed


real	0m2.727s
user	0m2.727s
sys	0m0.000s

 Performance counter stats for './__run 9':

       2729.574564      task-clock (msec)         #    1.000 CPUs utilized          
                12      context-switches          #    0.004 K/sec                  
                 2      cpu-migrations            #    0.001 K/sec                  
               254      page-faults               #    0.093 K/sec                  
     8,902,543,886      cycles                    #    3.262 GHz                    
     1,445,995,692      stalled-cycles-frontend   #   16.24% frontend cycles idle   
    21,873,036,811      instructions              #    2.46  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     4,501,906,763      branches                  # 1649.307 M/sec                  
        46,021,773      branch-misses             #    1.02% of all branches        

       2.730208052 seconds time elapsed


real	0m1.541s
user	0m1.536s
sys	0m0.004s

 Performance counter stats for './__run 10':

       1542.722008      task-clock (msec)         #    0.999 CPUs utilized          
                10      context-switches          #    0.006 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
               251      page-faults               #    0.163 K/sec                  
     5,053,348,577      cycles                    #    3.276 GHz                    
       834,553,507      stalled-cycles-frontend   #   16.51% frontend cycles idle   
    12,321,483,280      instructions              #    2.44  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     2,535,991,118      branches                  # 1643.842 M/sec                  
        27,506,450      branch-misses             #    1.08% of all branches        

       1.543907738 seconds time elapsed

