
real	0m0.004s
user	0m0.002s
sys	0m0.000s

 Performance counter stats for './__run 1':

          5.224162      task-clock (msec)         #    0.695 CPUs utilized          
                11      context-switches          #    0.002 M/sec                  
                 2      cpu-migrations            #    0.383 K/sec                  
               429      page-faults               #    0.082 M/sec                  
         7,790,197      cycles                    #    1.491 GHz                    
         5,101,990      stalled-cycles-frontend   #   65.49% frontend cycles idle   
         5,881,470      instructions              #    0.75  insn per cycle         
                                                  #    0.87  stalled cycles per insn
         1,154,205      branches                  #  220.936 M/sec                  
            49,228      branch-misses             #    4.27% of all branches        

       0.007516616 seconds time elapsed


real	0m0.002s
user	0m0.001s
sys	0m0.000s

 Performance counter stats for './__run 2':

          3.949260      task-clock (msec)         #    0.900 CPUs utilized          
                 3      context-switches          #    0.760 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               432      page-faults               #    0.109 M/sec                  
         7,460,697      cycles                    #    1.889 GHz                    
         4,860,686      stalled-cycles-frontend   #   65.15% frontend cycles idle   
         5,639,165      instructions              #    0.76  insn per cycle         
                                                  #    0.86  stalled cycles per insn
         1,113,418      branches                  #  281.931 M/sec                  
            45,494      branch-misses             #    4.09% of all branches        

       0.004385741 seconds time elapsed


real	0m0.001s
user	0m0.001s
sys	0m0.000s

 Performance counter stats for './__run 3':

          2.657144      task-clock (msec)         #    0.910 CPUs utilized          
                 1      context-switches          #    0.376 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               433      page-faults               #    0.163 M/sec                  
         8,027,315      cycles                    #    3.021 GHz                    
         5,390,684      stalled-cycles-frontend   #   67.15% frontend cycles idle   
         5,759,798      instructions              #    0.72  insn per cycle         
                                                  #    0.94  stalled cycles per insn
         1,146,241      branches                  #  431.381 M/sec                  
            45,399      branch-misses             #    3.96% of all branches        

       0.002919392 seconds time elapsed


real	0m0.001s
user	0m0.001s
sys	0m0.000s

 Performance counter stats for './__run 4':

          2.701689      task-clock (msec)         #    0.898 CPUs utilized          
                 2      context-switches          #    0.740 K/sec                  
                 1      cpu-migrations            #    0.370 K/sec                  
               428      page-faults               #    0.158 M/sec                  
         8,208,088      cycles                    #    3.038 GHz                    
         5,524,668      stalled-cycles-frontend   #   67.31% frontend cycles idle   
         5,820,415      instructions              #    0.71  insn per cycle         
                                                  #    0.95  stalled cycles per insn
         1,161,874      branches                  #  430.055 M/sec                  
            44,615      branch-misses             #    3.84% of all branches        

       0.003007466 seconds time elapsed


real	0m0.001s
user	0m0.001s
sys	0m0.000s

 Performance counter stats for './__run 5':

          2.608642      task-clock (msec)         #    0.915 CPUs utilized          
                 1      context-switches          #    0.383 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               428      page-faults               #    0.164 M/sec                  
         8,037,774      cycles                    #    3.081 GHz                    
         5,270,302      stalled-cycles-frontend   #   65.57% frontend cycles idle   
         6,033,543      instructions              #    0.75  insn per cycle         
                                                  #    0.87  stalled cycles per insn
         1,204,179      branches                  #  461.611 M/sec                  
            45,523      branch-misses             #    3.78% of all branches        

       0.002851431 seconds time elapsed


real	0m0.001s
user	0m0.001s
sys	0m0.000s

 Performance counter stats for './__run 6':

          2.935566      task-clock (msec)         #    0.903 CPUs utilized          
                 3      context-switches          #    0.001 M/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               432      page-faults               #    0.147 M/sec                  
         8,603,824      cycles                    #    2.931 GHz                    
         5,730,064      stalled-cycles-frontend   #   66.60% frontend cycles idle   
         6,257,811      instructions              #    0.73  insn per cycle         
                                                  #    0.92  stalled cycles per insn
         1,249,357      branches                  #  425.593 M/sec                  
            47,291      branch-misses             #    3.79% of all branches        

       0.003249892 seconds time elapsed


real	0m0.001s
user	0m0.000s
sys	0m0.001s

 Performance counter stats for './__run 7':

          3.071721      task-clock (msec)         #    0.917 CPUs utilized          
                 2      context-switches          #    0.651 K/sec                  
                 1      cpu-migrations            #    0.326 K/sec                  
               432      page-faults               #    0.141 M/sec                  
         8,237,289      cycles                    #    2.682 GHz                    
         5,316,435      stalled-cycles-frontend   #   64.54% frontend cycles idle   
         6,401,628      instructions              #    0.78  insn per cycle         
                                                  #    0.83  stalled cycles per insn
         1,282,854      branches                  #  417.634 M/sec                  
            46,677      branch-misses             #    3.64% of all branches        

       0.003348424 seconds time elapsed


real	0m0.001s
user	0m0.000s
sys	0m0.001s

 Performance counter stats for './__run 8':

          3.591194      task-clock (msec)         #    0.927 CPUs utilized          
                 1      context-switches          #    0.278 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               430      page-faults               #    0.120 M/sec                  
         8,517,282      cycles                    #    2.372 GHz                    
         5,530,270      stalled-cycles-frontend   #   64.93% frontend cycles idle   
         6,547,870      instructions              #    0.77  insn per cycle         
                                                  #    0.84  stalled cycles per insn
         1,313,684      branches                  #  365.807 M/sec                  
            48,016      branch-misses             #    3.66% of all branches        

       0.003875954 seconds time elapsed


real	0m0.001s
user	0m0.001s
sys	0m0.000s

 Performance counter stats for './__run 9':

          3.981832      task-clock (msec)         #    0.920 CPUs utilized          
                 3      context-switches          #    0.753 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               429      page-faults               #    0.108 M/sec                  
         8,470,264      cycles                    #    2.127 GHz                    
         5,397,122      stalled-cycles-frontend   #   63.72% frontend cycles idle   
         6,737,734      instructions              #    0.80  insn per cycle         
                                                  #    0.80  stalled cycles per insn
         1,353,777      branches                  #  339.988 M/sec                  
            49,413      branch-misses             #    3.65% of all branches        

       0.004326798 seconds time elapsed


real	0m0.001s
user	0m0.002s
sys	0m0.000s

 Performance counter stats for './__run 10':

          4.421668      task-clock (msec)         #    0.926 CPUs utilized          
                 1      context-switches          #    0.226 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               433      page-faults               #    0.098 M/sec                  
         8,515,669      cycles                    #    1.926 GHz                    
         5,394,764      stalled-cycles-frontend   #   63.35% frontend cycles idle   
         6,869,397      instructions              #    0.81  insn per cycle         
                                                  #    0.79  stalled cycles per insn
         1,384,654      branches                  #  313.152 M/sec                  
            49,579      branch-misses             #    3.58% of all branches        

       0.004773102 seconds time elapsed

