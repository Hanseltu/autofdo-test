
real	0m26.928s
user	0m6.186s
sys	0m6.182s

 Performance counter stats for './__run 1':

      11774.324949      task-clock (msec)         #    0.437 CPUs utilized          
           134,368      context-switches          #    0.011 M/sec                  
               574      cpu-migrations            #    0.049 K/sec                  
               254      page-faults               #    0.022 K/sec                  
    35,586,885,626      cycles                    #    3.022 GHz                    
    19,212,996,730      stalled-cycles-frontend   #   53.99% frontend cycles idle   
    45,388,422,889      instructions              #    1.28  insn per cycle         
                                                  #    0.42  stalled cycles per insn
     4,507,371,907      branches                  #  382.814 M/sec                  
        30,736,574      branch-misses             #    0.68% of all branches        

      26.930065667 seconds time elapsed


real	0m21.934s
user	0m4.964s
sys	0m5.579s

 Performance counter stats for './__run 1':

      10153.404855      task-clock (msec)         #    0.463 CPUs utilized          
           129,966      context-switches          #    0.013 M/sec                  
               509      cpu-migrations            #    0.050 K/sec                  
               250      page-faults               #    0.025 K/sec                  
    31,163,648,466      cycles                    #    3.069 GHz                    
    14,951,661,767      stalled-cycles-frontend   #   47.98% frontend cycles idle   
    45,296,739,039      instructions              #    1.45  insn per cycle         
                                                  #    0.33  stalled cycles per insn
     4,490,715,665      branches                  #  442.287 M/sec                  
        27,599,659      branch-misses             #    0.61% of all branches        

      21.936099694 seconds time elapsed


real	0m21.024s
user	0m4.867s
sys	0m5.079s

 Performance counter stats for './__run 1':

       9579.829725      task-clock (msec)         #    0.456 CPUs utilized          
           129,958      context-switches          #    0.014 M/sec                  
               341      cpu-migrations            #    0.036 K/sec                  
               251      page-faults               #    0.026 K/sec                  
    30,349,747,819      cycles                    #    3.168 GHz                    
    14,158,462,655      stalled-cycles-frontend   #   46.65% frontend cycles idle   
    45,296,929,747      instructions              #    1.49  insn per cycle         
                                                  #    0.31  stalled cycles per insn
     4,490,721,156      branches                  #  468.768 M/sec                  
        27,296,245      branch-misses             #    0.61% of all branches        

      21.026466073 seconds time elapsed


real	0m1.889s
user	0m0.379s
sys	0m0.383s

 Performance counter stats for './__run 2':

        733.935478      task-clock (msec)         #    0.388 CPUs utilized          
             9,426      context-switches          #    0.013 M/sec                  
                12      cpu-migrations            #    0.016 K/sec                  
               251      page-faults               #    0.342 K/sec                  
     2,223,962,623      cycles                    #    3.030 GHz                    
     1,045,491,827      stalled-cycles-frontend   #   47.01% frontend cycles idle   
     3,295,322,364      instructions              #    1.48  insn per cycle         
                                                  #    0.32  stalled cycles per insn
       327,133,572      branches                  #  445.725 M/sec                  
         2,099,650      branch-misses             #    0.64% of all branches        

       1.891462405 seconds time elapsed


real	0m1.672s
user	0m0.433s
sys	0m0.385s

 Performance counter stats for './__run 2':

        783.832156      task-clock (msec)         #    0.468 CPUs utilized          
             9,447      context-switches          #    0.012 M/sec                  
                28      cpu-migrations            #    0.036 K/sec                  
               252      page-faults               #    0.321 K/sec                  
     2,233,271,597      cycles                    #    2.849 GHz                    
     1,054,445,830      stalled-cycles-frontend   #   47.22% frontend cycles idle   
     3,295,849,489      instructions              #    1.48  insn per cycle         
                                                  #    0.32  stalled cycles per insn
       327,191,796      branches                  #  417.426 M/sec                  
         2,022,121      branch-misses             #    0.62% of all branches        

       1.675082166 seconds time elapsed


real	0m1.652s
user	0m0.420s
sys	0m0.499s

 Performance counter stats for './__run 2':

        870.534860      task-clock (msec)         #    0.526 CPUs utilized          
             9,433      context-switches          #    0.011 M/sec                  
                15      cpu-migrations            #    0.017 K/sec                  
               250      page-faults               #    0.287 K/sec                  
     2,198,410,238      cycles                    #    2.525 GHz                    
     1,019,373,513      stalled-cycles-frontend   #   46.37% frontend cycles idle   
     3,297,183,287      instructions              #    1.50  insn per cycle         
                                                  #    0.31  stalled cycles per insn
       327,479,078      branches                  #  376.181 M/sec                  
         1,978,835      branch-misses             #    0.60% of all branches        

       1.654839562 seconds time elapsed


real	0m0.696s
user	0m0.183s
sys	0m0.191s

 Performance counter stats for './__run 3':

        356.998665      task-clock (msec)         #    0.511 CPUs utilized          
             3,942      context-switches          #    0.011 M/sec                  
                15      cpu-migrations            #    0.042 K/sec                  
               248      page-faults               #    0.695 K/sec                  
       929,248,146      cycles                    #    2.603 GHz                    
       434,948,949      stalled-cycles-frontend   #   46.81% frontend cycles idle   
     1,380,638,830      instructions              #    1.49  insn per cycle         
                                                  #    0.32  stalled cycles per insn
       137,404,355      branches                  #  384.888 M/sec                  
           892,678      branch-misses             #    0.65% of all branches        

       0.698665682 seconds time elapsed


real	0m0.680s
user	0m0.205s
sys	0m0.170s

 Performance counter stats for './__run 3':

        359.693530      task-clock (msec)         #    0.527 CPUs utilized          
             3,953      context-switches          #    0.011 M/sec                  
                10      cpu-migrations            #    0.028 K/sec                  
               249      page-faults               #    0.692 K/sec                  
       921,410,588      cycles                    #    2.562 GHz                    
       427,523,274      stalled-cycles-frontend   #   46.40% frontend cycles idle   
     1,380,966,103      instructions              #    1.50  insn per cycle         
                                                  #    0.31  stalled cycles per insn
       137,453,031      branches                  #  382.139 M/sec                  
           863,864      branch-misses             #    0.63% of all branches        

       0.683131304 seconds time elapsed


real	0m0.635s
user	0m0.199s
sys	0m0.183s

 Performance counter stats for './__run 3':

        357.387632      task-clock (msec)         #    0.560 CPUs utilized          
             3,937      context-switches          #    0.011 M/sec                  
                 4      cpu-migrations            #    0.011 K/sec                  
               252      page-faults               #    0.705 K/sec                  
       937,809,692      cycles                    #    2.624 GHz                    
       443,585,092      stalled-cycles-frontend   #   47.30% frontend cycles idle   
     1,380,840,606      instructions              #    1.47  insn per cycle         
                                                  #    0.32  stalled cycles per insn
       137,416,884      branches                  #  384.504 M/sec                  
           843,179      branch-misses             #    0.61% of all branches        

       0.637929912 seconds time elapsed


real	0m0.038s
user	0m0.004s
sys	0m0.011s

 Performance counter stats for './__run 4':

         16.549198      task-clock (msec)         #    0.411 CPUs utilized          
               130      context-switches          #    0.008 M/sec                  
                 3      cpu-migrations            #    0.181 K/sec                  
               247      page-faults               #    0.015 M/sec                  
        38,589,404      cycles                    #    2.332 GHz                    
        20,462,802      stalled-cycles-frontend   #   53.03% frontend cycles idle   
        49,287,550      instructions              #    1.28  insn per cycle         
                                                  #    0.42  stalled cycles per insn
         5,452,277      branches                  #  329.459 M/sec                  
            86,177      branch-misses             #    1.58% of all branches        

       0.040295267 seconds time elapsed


real	0m0.024s
user	0m0.007s
sys	0m0.007s

 Performance counter stats for './__run 4':

         16.417844      task-clock (msec)         #    0.605 CPUs utilized          
               127      context-switches          #    0.008 M/sec                  
                 3      cpu-migrations            #    0.183 K/sec                  
               251      page-faults               #    0.015 M/sec                  
        37,411,100      cycles                    #    2.279 GHz                    
        19,263,764      stalled-cycles-frontend   #   51.49% frontend cycles idle   
        49,205,094      instructions              #    1.32  insn per cycle         
                                                  #    0.39  stalled cycles per insn
         5,438,794      branches                  #  331.273 M/sec                  
            81,051      branch-misses             #    1.49% of all branches        

       0.027133300 seconds time elapsed


real	0m0.021s
user	0m0.003s
sys	0m0.009s

 Performance counter stats for './__run 4':

         13.520527      task-clock (msec)         #    0.570 CPUs utilized          
               126      context-switches          #    0.009 M/sec                  
                 1      cpu-migrations            #    0.074 K/sec                  
               252      page-faults               #    0.019 M/sec                  
        37,376,082      cycles                    #    2.764 GHz                    
        19,246,935      stalled-cycles-frontend   #   51.50% frontend cycles idle   
        49,189,426      instructions              #    1.32  insn per cycle         
                                                  #    0.39  stalled cycles per insn
         5,434,929      branches                  #  401.976 M/sec                  
            75,689      branch-misses             #    1.39% of all branches        

       0.023733308 seconds time elapsed


real	0m0.349s
user	0m0.110s
sys	0m0.096s

 Performance counter stats for './__run 5':

        197.863621      task-clock (msec)         #    0.561 CPUs utilized          
             1,985      context-switches          #    0.010 M/sec                  
                 1      cpu-migrations            #    0.005 K/sec                  
               252      page-faults               #    0.001 M/sec                  
       470,865,340      cycles                    #    2.380 GHz                    
       220,982,703      stalled-cycles-frontend   #   46.93% frontend cycles idle   
       697,791,440      instructions              #    1.48  insn per cycle         
                                                  #    0.32  stalled cycles per insn
        69,731,648      branches                  #  352.423 M/sec                  
           460,834      branch-misses             #    0.66% of all branches        

       0.352857542 seconds time elapsed


real	0m0.434s
user	0m0.103s
sys	0m0.099s

 Performance counter stats for './__run 5':

        193.410252      task-clock (msec)         #    0.442 CPUs utilized          
             1,988      context-switches          #    0.010 M/sec                  
                 4      cpu-migrations            #    0.021 K/sec                  
               249      page-faults               #    0.001 M/sec                  
       468,237,304      cycles                    #    2.421 GHz                    
       218,150,903      stalled-cycles-frontend   #   46.59% frontend cycles idle   
       697,908,151      instructions              #    1.49  insn per cycle         
                                                  #    0.31  stalled cycles per insn
        69,776,123      branches                  #  360.767 M/sec                  
           492,321      branch-misses             #    0.71% of all branches        

       0.437240779 seconds time elapsed


real	0m0.386s
user	0m0.104s
sys	0m0.113s

 Performance counter stats for './__run 5':

        207.919825      task-clock (msec)         #    0.535 CPUs utilized          
             1,985      context-switches          #    0.010 M/sec                  
                28      cpu-migrations            #    0.135 K/sec                  
               251      page-faults               #    0.001 M/sec                  
       489,741,771      cycles                    #    2.355 GHz                    
       238,701,200      stalled-cycles-frontend   #   48.74% frontend cycles idle   
       697,774,598      instructions              #    1.42  insn per cycle         
                                                  #    0.34  stalled cycles per insn
        69,759,557      branches                  #  335.512 M/sec                  
           557,313      branch-misses             #    0.80% of all branches        

       0.388335586 seconds time elapsed


real	0m0.030s
user	0m0.015s
sys	0m0.005s

 Performance counter stats for './__run 6':

         20.610994      task-clock (msec)         #    0.627 CPUs utilized          
               141      context-switches          #    0.007 M/sec                  
                 3      cpu-migrations            #    0.146 K/sec                  
               253      page-faults               #    0.012 M/sec                  
        42,231,520      cycles                    #    2.049 GHz                    
        21,912,711      stalled-cycles-frontend   #   51.89% frontend cycles idle   
        55,261,539      instructions              #    1.31  insn per cycle         
                                                  #    0.40  stalled cycles per insn
         6,087,427      branches                  #  295.349 M/sec                  
            86,456      branch-misses             #    1.42% of all branches        

       0.032855294 seconds time elapsed


real	0m0.041s
user	0m0.013s
sys	0m0.006s

 Performance counter stats for './__run 6':

         20.310619      task-clock (msec)         #    0.457 CPUs utilized          
               143      context-switches          #    0.007 M/sec                  
                 4      cpu-migrations            #    0.197 K/sec                  
               249      page-faults               #    0.012 M/sec                  
        42,865,273      cycles                    #    2.110 GHz                    
        22,506,219      stalled-cycles-frontend   #   52.50% frontend cycles idle   
        55,247,701      instructions              #    1.29  insn per cycle         
                                                  #    0.41  stalled cycles per insn
         6,086,655      branches                  #  299.678 M/sec                  
            92,988      branch-misses             #    1.53% of all branches        

       0.044447249 seconds time elapsed


real	0m0.025s
user	0m0.007s
sys	0m0.007s

 Performance counter stats for './__run 6':

         16.500297      task-clock (msec)         #    0.584 CPUs utilized          
               142      context-switches          #    0.009 M/sec                  
                 3      cpu-migrations            #    0.182 K/sec                  
               251      page-faults               #    0.015 M/sec                  
        43,994,813      cycles                    #    2.666 GHz                    
        23,603,895      stalled-cycles-frontend   #   53.65% frontend cycles idle   
        55,211,127      instructions              #    1.25  insn per cycle         
                                                  #    0.43  stalled cycles per insn
         6,080,629      branches                  #  368.516 M/sec                  
            90,392      branch-misses             #    1.49% of all branches        

       0.028274350 seconds time elapsed


real	0m1.676s
user	0m0.461s
sys	0m0.402s

 Performance counter stats for './__run 7':

        832.536431      task-clock (msec)         #    0.495 CPUs utilized          
            10,092      context-switches          #    0.012 M/sec                  
                76      cpu-migrations            #    0.091 K/sec                  
               249      page-faults               #    0.299 K/sec                  
     2,397,269,266      cycles                    #    2.879 GHz                    
     1,135,373,604      stalled-cycles-frontend   #   47.36% frontend cycles idle   
     3,522,454,359      instructions              #    1.47  insn per cycle         
                                                  #    0.32  stalled cycles per insn
       349,891,062      branches                  #  420.271 M/sec                  
         2,342,348      branch-misses             #    0.67% of all branches        

       1.681574670 seconds time elapsed


real	0m1.653s
user	0m0.369s
sys	0m0.471s

 Performance counter stats for './__run 7':

        805.414455      task-clock (msec)         #    0.486 CPUs utilized          
            10,093      context-switches          #    0.013 M/sec                  
                34      cpu-migrations            #    0.042 K/sec                  
               250      page-faults               #    0.310 K/sec                  
     2,368,604,320      cycles                    #    2.941 GHz                    
     1,107,857,341      stalled-cycles-frontend   #   46.77% frontend cycles idle   
     3,521,934,179      instructions              #    1.49  insn per cycle         
                                                  #    0.31  stalled cycles per insn
       349,743,950      branches                  #  434.241 M/sec                  
         2,177,130      branch-misses             #    0.62% of all branches        

       1.655558985 seconds time elapsed


real	0m1.772s
user	0m0.428s
sys	0m0.505s

 Performance counter stats for './__run 7':

        890.266715      task-clock (msec)         #    0.502 CPUs utilized          
            10,084      context-switches          #    0.011 M/sec                  
                13      cpu-migrations            #    0.015 K/sec                  
               249      page-faults               #    0.280 K/sec                  
     2,348,420,109      cycles                    #    2.638 GHz                    
     1,088,226,461      stalled-cycles-frontend   #   46.34% frontend cycles idle   
     3,523,501,207      instructions              #    1.50  insn per cycle         
                                                  #    0.31  stalled cycles per insn
       349,997,981      branches                  #  393.138 M/sec                  
         2,086,138      branch-misses             #    0.60% of all branches        

       1.774301887 seconds time elapsed


real	0m9.898s
user	0m2.315s
sys	0m2.612s

 Performance counter stats for './__run 8':

       4736.303201      task-clock (msec)         #    0.478 CPUs utilized          
            62,012      context-switches          #    0.013 M/sec                  
                39      cpu-migrations            #    0.008 K/sec                  
               252      page-faults               #    0.053 K/sec                  
    14,376,926,210      cycles                    #    3.035 GHz                    
     6,650,613,798      stalled-cycles-frontend   #   46.26% frontend cycles idle   
    21,613,720,298      instructions              #    1.50  insn per cycle         
                                                  #    0.31  stalled cycles per insn
     2,143,045,470      branches                  #  452.472 M/sec                  
        12,701,202      branch-misses             #    0.59% of all branches        

       9.901742626 seconds time elapsed


real	0m9.849s
user	0m2.236s
sys	0m2.442s

 Performance counter stats for './__run 8':

       4511.646291      task-clock (msec)         #    0.458 CPUs utilized          
            62,017      context-switches          #    0.014 M/sec                  
                41      cpu-migrations            #    0.009 K/sec                  
               253      page-faults               #    0.056 K/sec                  
    14,360,720,223      cycles                    #    3.183 GHz                    
     6,643,568,799      stalled-cycles-frontend   #   46.26% frontend cycles idle   
    21,610,873,891      instructions              #    1.50  insn per cycle         
                                                  #    0.31  stalled cycles per insn
     2,142,620,396      branches                  #  474.909 M/sec                  
        12,244,938      branch-misses             #    0.57% of all branches        

       9.852775927 seconds time elapsed


real	0m9.829s
user	0m2.227s
sys	0m2.478s

 Performance counter stats for './__run 8':

       4534.054625      task-clock (msec)         #    0.461 CPUs utilized          
            62,013      context-switches          #    0.014 M/sec                  
                95      cpu-migrations            #    0.021 K/sec                  
               250      page-faults               #    0.055 K/sec                  
    14,377,286,874      cycles                    #    3.171 GHz                    
     6,655,411,045      stalled-cycles-frontend   #   46.29% frontend cycles idle   
    21,611,553,338      instructions              #    1.50  insn per cycle         
                                                  #    0.31  stalled cycles per insn
     2,142,760,383      branches                  #  472.593 M/sec                  
        12,376,188      branch-misses             #    0.58% of all branches        

       9.832290027 seconds time elapsed


real	0m0.639s
user	0m0.152s
sys	0m0.152s

 Performance counter stats for './__run 9':

        295.465857      task-clock (msec)         #    0.460 CPUs utilized          
             3,958      context-switches          #    0.013 M/sec                  
                21      cpu-migrations            #    0.071 K/sec                  
               254      page-faults               #    0.860 K/sec                  
       932,338,717      cycles                    #    3.155 GHz                    
       436,203,280      stalled-cycles-frontend   #   46.79% frontend cycles idle   
     1,385,340,274      instructions              #    1.49  insn per cycle         
                                                  #    0.31  stalled cycles per insn
       138,035,274      branches                  #  467.178 M/sec                  
           891,065      branch-misses             #    0.65% of all branches        

       0.641971859 seconds time elapsed


real	0m0.649s
user	0m0.121s
sys	0m0.182s

 Performance counter stats for './__run 9':

        295.448516      task-clock (msec)         #    0.453 CPUs utilized          
             3,960      context-switches          #    0.013 M/sec                  
                18      cpu-migrations            #    0.061 K/sec                  
               249      page-faults               #    0.843 K/sec                  
       934,132,514      cycles                    #    3.162 GHz                    
       438,249,543      stalled-cycles-frontend   #   46.92% frontend cycles idle   
     1,385,358,144      instructions              #    1.48  insn per cycle         
                                                  #    0.32  stalled cycles per insn
       138,018,190      branches                  #  467.148 M/sec                  
           885,873      branch-misses             #    0.64% of all branches        

       0.652313685 seconds time elapsed


real	0m0.639s
user	0m0.103s
sys	0m0.202s

 Performance counter stats for './__run 9':

        297.289802      task-clock (msec)         #    0.463 CPUs utilized          
             3,964      context-switches          #    0.013 M/sec                  
                10      cpu-migrations            #    0.034 K/sec                  
               248      page-faults               #    0.834 K/sec                  
       940,500,736      cycles                    #    3.164 GHz                    
       444,783,822      stalled-cycles-frontend   #   47.29% frontend cycles idle   
     1,385,190,698      instructions              #    1.47  insn per cycle         
                                                  #    0.32  stalled cycles per insn
       137,990,265      branches                  #  464.161 M/sec                  
           868,833      branch-misses             #    0.63% of all branches        

       0.642381421 seconds time elapsed


real	0m2.437s
user	0m0.588s
sys	0m0.524s

 Performance counter stats for './__run 10':

       1072.804796      task-clock (msec)         #    0.440 CPUs utilized          
            13,938      context-switches          #    0.013 M/sec                  
                16      cpu-migrations            #    0.015 K/sec                  
               251      page-faults               #    0.234 K/sec                  
     3,322,711,346      cycles                    #    3.097 GHz                    
     1,577,052,894      stalled-cycles-frontend   #   47.46% frontend cycles idle   
     4,861,989,195      instructions              #    1.46  insn per cycle         
                                                  #    0.32  stalled cycles per insn
       482,613,799      branches                  #  449.862 M/sec                  
         2,894,615      branch-misses             #    0.60% of all branches        

       2.440615436 seconds time elapsed


real	0m2.407s
user	0m0.607s
sys	0m0.632s

 Performance counter stats for './__run 10':

       1186.719742      task-clock (msec)         #    0.492 CPUs utilized          
            13,937      context-switches          #    0.012 M/sec                  
                 8      cpu-migrations            #    0.007 K/sec                  
               254      page-faults               #    0.214 K/sec                  
     3,217,384,847      cycles                    #    2.711 GHz                    
     1,479,967,243      stalled-cycles-frontend   #   46.00% frontend cycles idle   
     4,863,623,826      instructions              #    1.51  insn per cycle         
                                                  #    0.30  stalled cycles per insn
       483,005,330      branches                  #  407.009 M/sec                  
         2,832,691      branch-misses             #    0.59% of all branches        

       2.410705007 seconds time elapsed


real	0m2.140s
user	0m0.520s
sys	0m0.535s

 Performance counter stats for './__run 10':

       1020.163698      task-clock (msec)         #    0.476 CPUs utilized          
            13,942      context-switches          #    0.014 M/sec                  
                 6      cpu-migrations            #    0.006 K/sec                  
               252      page-faults               #    0.247 K/sec                  
     3,255,748,923      cycles                    #    3.191 GHz                    
     1,519,848,021      stalled-cycles-frontend   #   46.68% frontend cycles idle   
     4,861,279,213      instructions              #    1.49  insn per cycle         
                                                  #    0.31  stalled cycles per insn
       482,467,091      branches                  #  472.931 M/sec                  
         2,876,228      branch-misses             #    0.60% of all branches        

       2.142739528 seconds time elapsed

