
real	0m7.143s
user	0m4.985s
sys	0m0.751s

 Performance counter stats for './__run 1':

       5671.416861      task-clock (msec)         #    0.794 CPUs utilized          
             6,786      context-switches          #    0.001 M/sec                  
                20      cpu-migrations            #    0.004 K/sec                  
             1,597      page-faults               #    0.282 K/sec                  
    17,861,285,531      cycles                    #    3.149 GHz                    
     5,677,451,081      stalled-cycles-frontend   #   31.79% frontend cycles idle   
    29,933,587,846      instructions              #    1.68  insn per cycle         
                                                  #    0.19  stalled cycles per insn
     5,462,052,745      branches                  #  963.084 M/sec                  
       270,046,641      branch-misses             #    4.94% of all branches        

       7.146383122 seconds time elapsed


real	0m5.031s
user	0m3.508s
sys	0m0.547s

 Performance counter stats for './__run 2':

       4011.486877      task-clock (msec)         #    0.797 CPUs utilized          
             4,818      context-switches          #    0.001 M/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
             1,221      page-faults               #    0.304 K/sec                  
    12,692,736,043      cycles                    #    3.164 GHz                    
     4,025,946,300      stalled-cycles-frontend   #   31.72% frontend cycles idle   
    21,300,821,469      instructions              #    1.68  insn per cycle         
                                                  #    0.19  stalled cycles per insn
     3,886,826,445      branches                  #  968.924 M/sec                  
       191,765,786      branch-misses             #    4.93% of all branches        

       5.034500367 seconds time elapsed


real	0m0.054s
user	0m0.038s
sys	0m0.008s

 Performance counter stats for './__run 3':

         48.084735      task-clock (msec)         #    0.846 CPUs utilized          
                48      context-switches          #    0.998 K/sec                  
                 1      cpu-migrations            #    0.021 K/sec                  
               296      page-faults               #    0.006 M/sec                  
       143,498,155      cycles                    #    2.984 GHz                    
        57,233,301      stalled-cycles-frontend   #   39.88% frontend cycles idle   
       213,586,321      instructions              #    1.49  insn per cycle         
                                                  #    0.27  stalled cycles per insn
        39,070,482      branches                  #  812.534 M/sec                  
         1,931,315      branch-misses             #    4.94% of all branches        

       0.056849554 seconds time elapsed


real	0m0.785s
user	0m0.572s
sys	0m0.066s

 Performance counter stats for './__run 4':

        633.487186      task-clock (msec)         #    0.804 CPUs utilized          
               764      context-switches          #    0.001 M/sec                  
                 3      cpu-migrations            #    0.005 K/sec                  
               435      page-faults               #    0.687 K/sec                  
     2,007,838,389      cycles                    #    3.170 GHz                    
       632,975,133      stalled-cycles-frontend   #   31.53% frontend cycles idle   
     3,380,133,872      instructions              #    1.68  insn per cycle         
                                                  #    0.19  stalled cycles per insn
       616,863,771      branches                  #  973.759 M/sec                  
        30,425,492      branch-misses             #    4.93% of all branches        

       0.787588567 seconds time elapsed


real	0m17.121s
user	0m11.989s
sys	0m1.627s

 Performance counter stats for './__run 5':

      13464.614638      task-clock (msec)         #    0.786 CPUs utilized          
            16,177      context-switches          #    0.001 M/sec                  
                35      cpu-migrations            #    0.003 K/sec                  
             3,409      page-faults               #    0.253 K/sec                  
    42,657,654,038      cycles                    #    3.168 GHz                    
    13,584,434,892      stalled-cycles-frontend   #   31.85% frontend cycles idle   
    71,456,476,297      instructions              #    1.68  insn per cycle         
                                                  #    0.19  stalled cycles per insn
    13,038,744,842      branches                  #  968.371 M/sec                  
       643,342,699      branch-misses             #    4.93% of all branches        

      17.124600640 seconds time elapsed


real	0m8.656s
user	0m6.046s
sys	0m0.929s

 Performance counter stats for './__run 6':

       6902.065636      task-clock (msec)         #    0.797 CPUs utilized          
             8,166      context-switches          #    0.001 M/sec                  
                28      cpu-migrations            #    0.004 K/sec                  
             1,861      page-faults               #    0.270 K/sec                  
    21,774,740,380      cycles                    #    3.155 GHz                    
     7,144,895,461      stalled-cycles-frontend   #   32.81% frontend cycles idle   
    36,020,762,774      instructions              #    1.65  insn per cycle         
                                                  #    0.20  stalled cycles per insn
     6,572,763,881      branches                  #  952.289 M/sec                  
       324,626,937      branch-misses             #    4.94% of all branches        

       8.659153444 seconds time elapsed


real	0m0.890s
user	0m0.665s
sys	0m0.067s

 Performance counter stats for './__run 7':

        726.352528      task-clock (msec)         #    0.812 CPUs utilized          
               868      context-switches          #    0.001 M/sec                  
                 4      cpu-migrations            #    0.006 K/sec                  
               455      page-faults               #    0.626 K/sec                  
     2,296,476,021      cycles                    #    3.162 GHz                    
       733,931,542      stalled-cycles-frontend   #   31.96% frontend cycles idle   
     3,837,856,747      instructions              #    1.67  insn per cycle         
                                                  #    0.19  stalled cycles per insn
       700,410,233      branches                  #  964.284 M/sec                  
        34,565,708      branch-misses             #    4.94% of all branches        

       0.894225853 seconds time elapsed


real	0m2.314s
user	0m1.684s
sys	0m0.213s

 Performance counter stats for './__run 8':

       1877.259595      task-clock (msec)         #    0.810 CPUs utilized          
             2,266      context-switches          #    0.001 M/sec                  
                 5      cpu-migrations            #    0.003 K/sec                  
               730      page-faults               #    0.389 K/sec                  
     5,961,848,617      cycles                    #    3.176 GHz                    
     1,889,894,022      stalled-cycles-frontend   #   31.70% frontend cycles idle   
    10,003,920,962      instructions              #    1.68  insn per cycle         
                                                  #    0.19  stalled cycles per insn
     1,825,516,104      branches                  #  972.437 M/sec                  
        90,115,257      branch-misses             #    4.94% of all branches        

       2.316419348 seconds time elapsed


real	0m2.268s
user	0m1.565s
sys	0m0.234s

 Performance counter stats for './__run 9':

       1784.279141      task-clock (msec)         #    0.786 CPUs utilized          
             2,095      context-switches          #    0.001 M/sec                  
                 7      cpu-migrations            #    0.004 K/sec                  
               690      page-faults               #    0.387 K/sec                  
     5,624,502,817      cycles                    #    3.152 GHz                    
     1,872,745,331      stalled-cycles-frontend   #   33.30% frontend cycles idle   
     9,255,253,122      instructions              #    1.65  insn per cycle         
                                                  #    0.20  stalled cycles per insn
     1,688,951,876      branches                  #  946.574 M/sec                  
        83,498,441      branch-misses             #    4.94% of all branches        

       2.270653076 seconds time elapsed


real	0m5.496s
user	0m2.490s
sys	0m0.418s

 Performance counter stats for './__run 10':

       2881.730307      task-clock (msec)         #    0.524 CPUs utilized          
             3,417      context-switches          #    0.001 M/sec                  
                39      cpu-migrations            #    0.014 K/sec                  
               930      page-faults               #    0.323 K/sec                  
     9,012,551,287      cycles                    #    3.127 GHz                    
     3,066,263,809      stalled-cycles-frontend   #   34.02% frontend cycles idle   
    14,674,920,889      instructions              #    1.63  insn per cycle         
                                                  #    0.21  stalled cycles per insn
     2,677,896,334      branches                  #  929.267 M/sec                  
       132,654,416      branch-misses             #    4.95% of all branches        

       5.499293124 seconds time elapsed

