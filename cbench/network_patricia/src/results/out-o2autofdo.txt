
real	0m2.606s
user	0m2.292s
sys	0m0.307s

 Performance counter stats for './__run 1':

       2600.596782      task-clock (msec)         #    0.997 CPUs utilized          
               293      context-switches          #    0.113 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
           122,717      page-faults               #    0.047 M/sec                  
     8,479,981,719      cycles                    #    3.261 GHz                    
     2,380,571,049      stalled-cycles-frontend   #   28.07% frontend cycles idle   
    15,683,762,043      instructions              #    1.85  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     3,155,446,148      branches                  # 1213.355 M/sec                  
        26,037,793      branch-misses             #    0.83% of all branches        

       2.607383268 seconds time elapsed


real	0m1.550s
user	0m1.367s
sys	0m0.179s

 Performance counter stats for './__run 2':

       1549.001321      task-clock (msec)         #    0.997 CPUs utilized          
               191      context-switches          #    0.123 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            73,731      page-faults               #    0.048 M/sec                  
     5,029,812,590      cycles                    #    3.247 GHz                    
     1,403,239,967      stalled-cycles-frontend   #   27.90% frontend cycles idle   
     9,414,335,150      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     1,894,185,268      branches                  # 1222.843 M/sec                  
        12,607,767      branch-misses             #    0.67% of all branches        

       1.553475607 seconds time elapsed


real	0m1.033s
user	0m0.918s
sys	0m0.112s

 Performance counter stats for './__run 3':

       1032.451652      task-clock (msec)         #    0.997 CPUs utilized          
               127      context-switches          #    0.123 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
            49,240      page-faults               #    0.048 M/sec                  
     3,340,143,135      cycles                    #    3.235 GHz                    
       922,940,230      stalled-cycles-frontend   #   27.63% frontend cycles idle   
     6,278,407,864      instructions              #    1.88  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     1,263,227,315      branches                  # 1223.522 M/sec                  
         8,285,639      branch-misses             #    0.66% of all branches        

       1.035578379 seconds time elapsed


real	0m0.774s
user	0m0.631s
sys	0m0.140s

 Performance counter stats for './__run 4':

        774.669981      task-clock (msec)         #    0.997 CPUs utilized          
                83      context-switches          #    0.107 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            36,994      page-faults               #    0.048 M/sec                  
     2,518,126,143      cycles                    #    3.251 GHz                    
       704,398,383      stalled-cycles-frontend   #   27.97% frontend cycles idle   
     4,709,772,689      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
       947,725,809      branches                  # 1223.393 M/sec                  
         6,315,090      branch-misses             #    0.67% of all branches        

       0.776870876 seconds time elapsed


real	0m0.527s
user	0m0.453s
sys	0m0.072s

 Performance counter stats for './__run 5':

        527.746294      task-clock (msec)         #    0.996 CPUs utilized          
                68      context-switches          #    0.129 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            24,748      page-faults               #    0.047 M/sec                  
     1,691,841,130      cycles                    #    3.206 GHz                    
       480,502,745      stalled-cycles-frontend   #   28.40% frontend cycles idle   
     3,141,760,627      instructions              #    1.86  insn per cycle         
                                                  #    0.15  stalled cycles per insn
       632,200,366      branches                  # 1197.925 M/sec                  
         4,216,522      branch-misses             #    0.67% of all branches        

       0.529817391 seconds time elapsed


real	0m0.267s
user	0m0.226s
sys	0m0.040s

 Performance counter stats for './__run 6':

        268.827575      task-clock (msec)         #    0.996 CPUs utilized          
                25      context-switches          #    0.093 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            12,501      page-faults               #    0.047 M/sec                  
       852,336,712      cycles                    #    3.171 GHz                    
       246,284,744      stalled-cycles-frontend   #   28.90% frontend cycles idle   
     1,573,877,747      instructions              #    1.85  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       316,731,559      branches                  # 1178.196 M/sec                  
         2,124,711      branch-misses             #    0.67% of all branches        

       0.269934203 seconds time elapsed


real	0m0.223s
user	0m0.191s
sys	0m0.032s

 Performance counter stats for './__run 7':

        225.825552      task-clock (msec)         #    0.996 CPUs utilized          
                17      context-switches          #    0.075 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            10,054      page-faults               #    0.045 M/sec                  
       695,896,021      cycles                    #    3.082 GHz                    
       207,297,519      stalled-cycles-frontend   #   29.79% frontend cycles idle   
     1,260,083,610      instructions              #    1.81  insn per cycle         
                                                  #    0.16  stalled cycles per insn
       253,534,942      branches                  # 1122.703 M/sec                  
         2,129,385      branch-misses             #    0.84% of all branches        

       0.226684304 seconds time elapsed


real	0m0.117s
user	0m0.104s
sys	0m0.012s

 Performance counter stats for './__run 8':

        118.997514      task-clock (msec)         #    0.995 CPUs utilized          
                14      context-switches          #    0.118 K/sec                  
                 1      cpu-migrations            #    0.008 K/sec                  
             5,153      page-faults               #    0.043 M/sec                  
       363,270,843      cycles                    #    3.053 GHz                    
       117,633,413      stalled-cycles-frontend   #   32.38% frontend cycles idle   
       633,493,089      instructions              #    1.74  insn per cycle         
                                                  #    0.19  stalled cycles per insn
       127,481,603      branches                  # 1071.296 M/sec                  
         1,083,761      branch-misses             #    0.85% of all branches        

       0.119651102 seconds time elapsed


real	0m0.091s
user	0m0.079s
sys	0m0.012s

 Performance counter stats for './__run 9':

         94.066302      task-clock (msec)         #    0.991 CPUs utilized          
                16      context-switches          #    0.170 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             3,931      page-faults               #    0.042 M/sec                  
       274,878,795      cycles                    #    2.922 GHz                    
        90,929,411      stalled-cycles-frontend   #   33.08% frontend cycles idle   
       476,762,401      instructions              #    1.73  insn per cycle         
                                                  #    0.19  stalled cycles per insn
        95,958,901      branches                  # 1020.120 M/sec                  
           715,287      branch-misses             #    0.75% of all branches        

       0.094942049 seconds time elapsed


real	0m0.061s
user	0m0.056s
sys	0m0.004s

 Performance counter stats for './__run 10':

         63.289948      task-clock (msec)         #    0.991 CPUs utilized          
                 4      context-switches          #    0.063 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             2,704      page-faults               #    0.043 M/sec                  
       186,305,478      cycles                    #    2.944 GHz                    
        62,542,065      stalled-cycles-frontend   #   33.57% frontend cycles idle   
       320,001,150      instructions              #    1.72  insn per cycle         
                                                  #    0.20  stalled cycles per insn
        64,417,151      branches                  # 1017.810 M/sec                  
           483,423      branch-misses             #    0.75% of all branches        

       0.063847786 seconds time elapsed

