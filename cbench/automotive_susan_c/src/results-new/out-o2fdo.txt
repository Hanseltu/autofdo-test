
real	0m4.756s
user	0m3.518s
sys	0m0.499s

 Performance counter stats for './__run 1':

       4020.022467      task-clock (msec)         #    0.845 CPUs utilized          
               753      context-switches          #    0.187 K/sec                  
                 4      cpu-migrations            #    0.001 K/sec                  
           188,606      page-faults               #    0.047 M/sec                  
    12,981,755,018      cycles                    #    3.229 GHz                    
     3,732,771,046      stalled-cycles-frontend   #   28.75% frontend cycles idle   
    32,115,318,798      instructions              #    2.47  insn per cycle         
                                                  #    0.12  stalled cycles per insn
     1,414,567,332      branches                  #  351.880 M/sec                  
        53,127,232      branch-misses             #    3.76% of all branches        

       4.759475465 seconds time elapsed


real	0m4.248s
user	0m3.061s
sys	0m0.444s

 Performance counter stats for './__run 2':

       3506.758718      task-clock (msec)         #    0.825 CPUs utilized          
               703      context-switches          #    0.200 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
           166,768      page-faults               #    0.048 M/sec                  
    11,359,237,639      cycles                    #    3.239 GHz                    
     3,184,885,445      stalled-cycles-frontend   #   28.04% frontend cycles idle   
    28,377,717,767      instructions              #    2.50  insn per cycle         
                                                  #    0.11  stalled cycles per insn
     1,249,312,214      branches                  #  356.258 M/sec                  
        47,060,486      branch-misses             #    3.77% of all branches        

       4.250246263 seconds time elapsed


real	0m1.297s
user	0m0.943s
sys	0m0.172s

 Performance counter stats for './__run 3':

       1117.428477      task-clock (msec)         #    0.860 CPUs utilized          
               222      context-switches          #    0.199 K/sec                  
                 2      cpu-migrations            #    0.002 K/sec                  
            53,004      page-faults               #    0.047 M/sec                  
     3,581,609,797      cycles                    #    3.205 GHz                    
     1,011,065,841      stalled-cycles-frontend   #   28.23% frontend cycles idle   
     8,917,620,084      instructions              #    2.49  insn per cycle         
                                                  #    0.11  stalled cycles per insn
       393,678,639      branches                  #  352.308 M/sec                  
        14,810,863      branch-misses             #    3.76% of all branches        

       1.300014170 seconds time elapsed


real	0m17.274s
user	0m12.834s
sys	0m1.718s

 Performance counter stats for './__run 4':

      14554.362537      task-clock (msec)         #    0.842 CPUs utilized          
             2,397      context-switches          #    0.165 K/sec                  
                12      cpu-migrations            #    0.001 K/sec                  
           658,527      page-faults               #    0.045 M/sec                  
    46,968,479,893      cycles                    #    3.227 GHz                    
    14,373,904,241      stalled-cycles-frontend   #   30.60% frontend cycles idle   
   112,485,840,617      instructions              #    2.39  insn per cycle         
                                                  #    0.13  stalled cycles per insn
     4,946,471,859      branches                  #  339.862 M/sec                  
       186,305,276      branch-misses             #    3.77% of all branches        

      17.276481105 seconds time elapsed


real	0m1.833s
user	0m1.413s
sys	0m0.193s

 Performance counter stats for './__run 5':

       1608.540602      task-clock (msec)         #    0.876 CPUs utilized          
               271      context-switches          #    0.168 K/sec                  
                 3      cpu-migrations            #    0.002 K/sec                  
            76,284      page-faults               #    0.047 M/sec                  
     5,168,292,244      cycles                    #    3.213 GHz                    
     1,452,701,329      stalled-cycles-frontend   #   28.11% frontend cycles idle   
    12,897,582,707      instructions              #    2.50  insn per cycle         
                                                  #    0.11  stalled cycles per insn
       568,281,104      branches                  #  353.290 M/sec                  
        21,348,603      branch-misses             #    3.76% of all branches        

       1.835784056 seconds time elapsed


real	0m1.435s
user	0m0.974s
sys	0m0.180s

 Performance counter stats for './__run 6':

       1156.074313      task-clock (msec)         #    0.804 CPUs utilized          
               222      context-switches          #    0.192 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
            52,290      page-faults               #    0.045 M/sec                  
     3,679,393,392      cycles                    #    3.183 GHz                    
     1,145,358,694      stalled-cycles-frontend   #   31.13% frontend cycles idle   
     8,793,984,808      instructions              #    2.39  insn per cycle         
                                                  #    0.13  stalled cycles per insn
       388,002,651      branches                  #  335.621 M/sec                  
        14,635,277      branch-misses             #    3.77% of all branches        

       1.438291683 seconds time elapsed


real	0m6.225s
user	0m4.302s
sys	0m0.625s

 Performance counter stats for './__run 7':

       4929.618763      task-clock (msec)         #    0.791 CPUs utilized          
               871      context-switches          #    0.177 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
           232,766      page-faults               #    0.047 M/sec                  
    15,909,648,617      cycles                    #    3.227 GHz                    
     4,481,725,428      stalled-cycles-frontend   #   28.17% frontend cycles idle   
    39,665,512,276      instructions              #    2.49  insn per cycle         
                                                  #    0.11  stalled cycles per insn
     1,745,627,323      branches                  #  354.110 M/sec                  
        65,644,702      branch-misses             #    3.76% of all branches        

       6.228591456 seconds time elapsed


real	0m23.686s
user	0m15.909s
sys	0m2.469s

 Performance counter stats for './__run 8':

      18382.304733      task-clock (msec)         #    0.776 CPUs utilized          
             3,248      context-switches          #    0.177 K/sec                  
                39      cpu-migrations            #    0.002 K/sec                  
           844,765      page-faults               #    0.046 M/sec                  
    58,832,806,453      cycles                    #    3.201 GHz                    
    17,266,240,600      stalled-cycles-frontend   #   29.35% frontend cycles idle   
   144,345,760,934      instructions              #    2.45  insn per cycle         
                                                  #    0.12  stalled cycles per insn
     6,348,495,479      branches                  #  345.359 M/sec                  
       239,211,493      branch-misses             #    3.77% of all branches        

      23.689053526 seconds time elapsed


real	0m0.873s
user	0m0.658s
sys	0m0.088s

 Performance counter stats for './__run 9':

        749.700571      task-clock (msec)         #    0.855 CPUs utilized          
               147      context-switches          #    0.196 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
            35,732      page-faults               #    0.048 M/sec                  
     2,405,339,107      cycles                    #    3.208 GHz                    
       686,152,081      stalled-cycles-frontend   #   28.53% frontend cycles idle   
     5,961,380,622      instructions              #    2.48  insn per cycle         
                                                  #    0.12  stalled cycles per insn
       263,511,113      branches                  #  351.488 M/sec                  
         9,915,771      branch-misses             #    3.76% of all branches        

       0.876897136 seconds time elapsed


real	0m14.825s
user	0m10.334s
sys	0m1.420s

 Performance counter stats for './__run 10':

      11756.315933      task-clock (msec)         #    0.793 CPUs utilized          
             2,351      context-switches          #    0.200 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
           558,925      page-faults               #    0.048 M/sec                  
    38,146,652,061      cycles                    #    3.245 GHz                    
    10,630,546,711      stalled-cycles-frontend   #   27.87% frontend cycles idle   
    95,492,173,691      instructions              #    2.50  insn per cycle         
                                                  #    0.11  stalled cycles per insn
     4,207,193,223      branches                  #  357.867 M/sec                  
       158,017,817      branch-misses             #    3.76% of all branches        

      14.828608601 seconds time elapsed

