
real	0m30.006s
user	0m7.154s
sys	0m2.127s

 Performance counter stats for './__run 1':

       9212.968664      task-clock (msec)         #    0.307 CPUs utilized          
            11,981      context-switches          #    0.001 M/sec                  
             2,111      cpu-migrations            #    0.229 K/sec                  
             1,598      page-faults               #    0.173 K/sec                  
    27,789,464,698      cycles                    #    3.016 GHz                    
    16,909,592,819      stalled-cycles-frontend   #   60.85% frontend cycles idle   
    30,082,255,534      instructions              #    1.08  insn per cycle         
                                                  #    0.56  stalled cycles per insn
     5,479,686,579      branches                  #  594.780 M/sec                  
       285,592,280      branch-misses             #    5.21% of all branches        

      30.008276367 seconds time elapsed


real	0m22.176s
user	0m7.050s
sys	0m2.027s

 Performance counter stats for './__run 1':

       9019.247960      task-clock (msec)         #    0.407 CPUs utilized          
            11,279      context-switches          #    0.001 M/sec                  
             1,245      cpu-migrations            #    0.138 K/sec                  
             1,596      page-faults               #    0.177 K/sec                  
    27,617,859,221      cycles                    #    3.062 GHz                    
    16,722,608,491      stalled-cycles-frontend   #   60.55% frontend cycles idle   
    30,050,027,770      instructions              #    1.09  insn per cycle         
                                                  #    0.56  stalled cycles per insn
     5,473,598,442      branches                  #  606.880 M/sec                  
       285,227,883      branch-misses             #    5.21% of all branches        

      22.179086577 seconds time elapsed


real	0m21.518s
user	0m7.190s
sys	0m2.435s

 Performance counter stats for './__run 1':

       9574.685291      task-clock (msec)         #    0.445 CPUs utilized          
            12,562      context-switches          #    0.001 M/sec                  
             1,032      cpu-migrations            #    0.108 K/sec                  
             1,597      page-faults               #    0.167 K/sec                  
    29,415,124,558      cycles                    #    3.072 GHz                    
    18,781,576,930      stalled-cycles-frontend   #   63.85% frontend cycles idle   
    30,065,403,905      instructions              #    1.02  insn per cycle         
                                                  #    0.62  stalled cycles per insn
     5,476,619,504      branches                  #  571.990 M/sec                  
       283,164,343      branch-misses             #    5.17% of all branches        

      21.520012036 seconds time elapsed


real	0m12.778s
user	0m4.857s
sys	0m1.193s

 Performance counter stats for './__run 2':

       6009.278712      task-clock (msec)         #    0.470 CPUs utilized          
             8,011      context-switches          #    0.001 M/sec                  
               900      cpu-migrations            #    0.150 K/sec                  
             1,219      page-faults               #    0.203 K/sec                  
    18,298,432,233      cycles                    #    3.045 GHz                    
    10,476,138,830      stalled-cycles-frontend   #   57.25% frontend cycles idle   
    21,388,845,758      instructions              #    1.17  insn per cycle         
                                                  #    0.49  stalled cycles per insn
     3,895,951,005      branches                  #  648.323 M/sec                  
       202,506,350      branch-misses             #    5.20% of all branches        

      12.779769268 seconds time elapsed


real	0m33.928s
user	0m4.897s
sys	0m1.230s

 Performance counter stats for './__run 2':

       6067.569822      task-clock (msec)         #    0.179 CPUs utilized          
             8,698      context-switches          #    0.001 M/sec                  
             1,580      cpu-migrations            #    0.260 K/sec                  
             1,222      page-faults               #    0.201 K/sec                  
    18,122,990,489      cycles                    #    2.987 GHz                    
    10,277,397,884      stalled-cycles-frontend   #   56.71% frontend cycles idle   
    21,416,293,274      instructions              #    1.18  insn per cycle         
                                                  #    0.48  stalled cycles per insn
     3,901,021,729      branches                  #  642.930 M/sec                  
       204,106,119      branch-misses             #    5.23% of all branches        

      33.932566793 seconds time elapsed


real	0m28.237s
user	0m5.006s
sys	0m1.435s

 Performance counter stats for './__run 2':

       6380.598587      task-clock (msec)         #    0.226 CPUs utilized          
             7,277      context-switches          #    0.001 M/sec                  
             1,464      cpu-migrations            #    0.229 K/sec                  
             1,221      page-faults               #    0.191 K/sec                  
    18,896,891,467      cycles                    #    2.962 GHz                    
    10,871,368,651      stalled-cycles-frontend   #   57.53% frontend cycles idle   
    21,401,656,079      instructions              #    1.13  insn per cycle         
                                                  #    0.51  stalled cycles per insn
     3,898,364,256      branches                  #  610.972 M/sec                  
       204,566,960      branch-misses             #    5.25% of all branches        

      28.239733345 seconds time elapsed


real	0m0.175s
user	0m0.040s
sys	0m0.018s

 Performance counter stats for './__run 3':

         59.864096      task-clock (msec)         #    0.336 CPUs utilized          
                50      context-switches          #    0.835 K/sec                  
                 8      cpu-migrations            #    0.134 K/sec                  
               298      page-faults               #    0.005 M/sec                  
       161,070,862      cycles                    #    2.691 GHz                    
        75,716,770      stalled-cycles-frontend   #   47.01% frontend cycles idle   
       214,363,025      instructions              #    1.33  insn per cycle         
                                                  #    0.35  stalled cycles per insn
        39,143,805      branches                  #  653.878 M/sec                  
         2,034,453      branch-misses             #    5.20% of all branches        

       0.178019395 seconds time elapsed


real	0m0.718s
user	0m0.047s
sys	0m0.030s

 Performance counter stats for './__run 3':

         78.620849      task-clock (msec)         #    0.109 CPUs utilized          
                58      context-switches          #    0.738 K/sec                  
                 8      cpu-migrations            #    0.102 K/sec                  
               298      page-faults               #    0.004 M/sec                  
       177,045,047      cycles                    #    2.252 GHz                    
        94,605,668      stalled-cycles-frontend   #   53.44% frontend cycles idle   
       214,276,383      instructions              #    1.21  insn per cycle         
                                                  #    0.44  stalled cycles per insn
        39,127,061      branches                  #  497.668 M/sec                  
         2,063,864      branch-misses             #    5.27% of all branches        

       0.720760547 seconds time elapsed


real	0m0.324s
user	0m0.048s
sys	0m0.015s

 Performance counter stats for './__run 3':

         65.120583      task-clock (msec)         #    0.199 CPUs utilized          
                51      context-switches          #    0.783 K/sec                  
                 5      cpu-migrations            #    0.077 K/sec                  
               297      page-faults               #    0.005 M/sec                  
       173,744,486      cycles                    #    2.668 GHz                    
        92,025,388      stalled-cycles-frontend   #   52.97% frontend cycles idle   
       214,226,402      instructions              #    1.23  insn per cycle         
                                                  #    0.43  stalled cycles per insn
        39,118,646      branches                  #  600.711 M/sec                  
         2,024,394      branch-misses             #    5.18% of all branches        

       0.326982789 seconds time elapsed


real	0m3.663s
user	0m0.825s
sys	0m0.206s

 Performance counter stats for './__run 4':

       1021.918410      task-clock (msec)         #    0.279 CPUs utilized          
               959      context-switches          #    0.938 K/sec                  
               120      cpu-migrations            #    0.117 K/sec                  
               439      page-faults               #    0.430 K/sec                  
     2,819,997,827      cycles                    #    2.760 GHz                    
     1,542,144,820      stalled-cycles-frontend   #   54.69% frontend cycles idle   
     3,394,010,563      instructions              #    1.20  insn per cycle         
                                                  #    0.45  stalled cycles per insn
       618,294,452      branches                  #  605.033 M/sec                  
        32,374,619      branch-misses             #    5.24% of all branches        

       3.666332534 seconds time elapsed


real	0m4.076s
user	0m0.812s
sys	0m0.186s

 Performance counter stats for './__run 4':

        989.805900      task-clock (msec)         #    0.243 CPUs utilized          
               970      context-switches          #    0.980 K/sec                  
               140      cpu-migrations            #    0.141 K/sec                  
               438      page-faults               #    0.443 K/sec                  
     2,678,139,622      cycles                    #    2.706 GHz                    
     1,375,251,016      stalled-cycles-frontend   #   51.35% frontend cycles idle   
     3,396,005,659      instructions              #    1.27  insn per cycle         
                                                  #    0.40  stalled cycles per insn
       618,673,383      branches                  #  625.045 M/sec                  
        32,196,484      branch-misses             #    5.20% of all branches        

       4.078855015 seconds time elapsed


real	0m3.296s
user	0m0.803s
sys	0m0.228s

 Performance counter stats for './__run 4':

       1023.018164      task-clock (msec)         #    0.310 CPUs utilized          
               934      context-switches          #    0.913 K/sec                  
                69      cpu-migrations            #    0.067 K/sec                  
               437      page-faults               #    0.427 K/sec                  
     2,698,847,202      cycles                    #    2.638 GHz                    
     1,413,220,834      stalled-cycles-frontend   #   52.36% frontend cycles idle   
     3,393,901,725      instructions              #    1.26  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       618,268,568      branches                  #  604.357 M/sec                  
        32,081,053      branch-misses             #    5.19% of all branches        

       3.298797713 seconds time elapsed


real	1m9.580s
user	0m17.055s
sys	0m4.226s

 Performance counter stats for './__run 5':

      21075.336266      task-clock (msec)         #    0.303 CPUs utilized          
            23,038      context-switches          #    0.001 M/sec                  
             3,339      cpu-migrations            #    0.158 K/sec                  
             3,406      page-faults               #    0.162 K/sec                  
    59,816,183,456      cycles                    #    2.838 GHz                    
    33,018,342,156      stalled-cycles-frontend   #   55.20% frontend cycles idle   
    71,752,754,509      instructions              #    1.20  insn per cycle         
                                                  #    0.46  stalled cycles per insn
    13,069,248,101      branches                  #  620.121 M/sec                  
       681,681,635      branch-misses             #    5.22% of all branches        

      69.582982088 seconds time elapsed


real	1m13.129s
user	0m16.543s
sys	0m4.443s

 Performance counter stats for './__run 5':

      20790.456205      task-clock (msec)         #    0.284 CPUs utilized          
            24,420      context-switches          #    0.001 M/sec                  
             5,148      cpu-migrations            #    0.248 K/sec                  
             3,410      page-faults               #    0.164 K/sec                  
    62,627,549,185      cycles                    #    3.012 GHz                    
    36,470,195,732      stalled-cycles-frontend   #   58.23% frontend cycles idle   
    71,789,288,729      instructions              #    1.15  insn per cycle         
                                                  #    0.51  stalled cycles per insn
    13,075,991,905      branches                  #  628.942 M/sec                  
       686,166,594      branch-misses             #    5.25% of all branches        

      73.131928777 seconds time elapsed


real	1m8.282s
user	0m16.614s
sys	0m4.196s

 Performance counter stats for './__run 5':

      20608.152068      task-clock (msec)         #    0.302 CPUs utilized          
            24,184      context-switches          #    0.001 M/sec                  
             5,231      cpu-migrations            #    0.254 K/sec                  
             3,406      page-faults               #    0.165 K/sec                  
    61,658,366,393      cycles                    #    2.992 GHz                    
    35,430,535,300      stalled-cycles-frontend   #   57.46% frontend cycles idle   
    71,817,987,216      instructions              #    1.16  insn per cycle         
                                                  #    0.49  stalled cycles per insn
    13,081,330,753      branches                  #  634.765 M/sec                  
       685,618,069      branch-misses             #    5.24% of all branches        

      68.284611094 seconds time elapsed


real	0m34.244s
user	0m8.246s
sys	0m2.196s

 Performance counter stats for './__run 6':

      10346.909765      task-clock (msec)         #    0.302 CPUs utilized          
            12,021      context-switches          #    0.001 M/sec                  
             2,685      cpu-migrations            #    0.259 K/sec                  
             1,859      page-faults               #    0.180 K/sec                  
    30,926,502,171      cycles                    #    2.989 GHz                    
    17,681,977,565      stalled-cycles-frontend   #   57.17% frontend cycles idle   
    36,193,199,989      instructions              #    1.17  insn per cycle         
                                                  #    0.49  stalled cycles per insn
     6,592,474,130      branches                  #  637.144 M/sec                  
       346,083,325      branch-misses             #    5.25% of all branches        

      34.246702893 seconds time elapsed


real	0m33.605s
user	0m8.459s
sys	0m1.981s

 Performance counter stats for './__run 6':

      10345.811074      task-clock (msec)         #    0.308 CPUs utilized          
            12,359      context-switches          #    0.001 M/sec                  
             2,635      cpu-migrations            #    0.255 K/sec                  
             1,860      page-faults               #    0.180 K/sec                  
    30,937,498,251      cycles                    #    2.990 GHz                    
    17,693,929,951      stalled-cycles-frontend   #   57.19% frontend cycles idle   
    36,198,972,837      instructions              #    1.17  insn per cycle         
                                                  #    0.49  stalled cycles per insn
     6,593,518,064      branches                  #  637.313 M/sec                  
       345,574,818      branch-misses             #    5.24% of all branches        

      33.607457908 seconds time elapsed


real	0m32.645s
user	0m8.491s
sys	0m2.028s

 Performance counter stats for './__run 6':

      10424.540085      task-clock (msec)         #    0.319 CPUs utilized          
            12,335      context-switches          #    0.001 M/sec                  
             2,748      cpu-migrations            #    0.264 K/sec                  
             1,864      page-faults               #    0.179 K/sec                  
    31,251,002,232      cycles                    #    2.998 GHz                    
    18,023,807,539      stalled-cycles-frontend   #   57.67% frontend cycles idle   
    36,204,141,867      instructions              #    1.16  insn per cycle         
                                                  #    0.50  stalled cycles per insn
     6,594,657,628      branches                  #  632.609 M/sec                  
       345,929,757      branch-misses             #    5.25% of all branches        

      32.647980917 seconds time elapsed


real	0m3.617s
user	0m0.879s
sys	0m0.244s

 Performance counter stats for './__run 7':

       1113.784411      task-clock (msec)         #    0.308 CPUs utilized          
             1,410      context-switches          #    0.001 M/sec                  
               290      cpu-migrations            #    0.260 K/sec                  
               459      page-faults               #    0.412 K/sec                  
     3,353,452,823      cycles                    #    3.011 GHz                    
     1,953,533,596      stalled-cycles-frontend   #   58.25% frontend cycles idle   
     3,856,656,392      instructions              #    1.15  insn per cycle         
                                                  #    0.51  stalled cycles per insn
       702,600,010      branches                  #  630.822 M/sec                  
        36,767,145      branch-misses             #    5.23% of all branches        

       3.619287574 seconds time elapsed


real	0m3.662s
user	0m0.940s
sys	0m0.190s

 Performance counter stats for './__run 7':

       1121.196794      task-clock (msec)         #    0.306 CPUs utilized          
             1,332      context-switches          #    0.001 M/sec                  
               253      cpu-migrations            #    0.226 K/sec                  
               453      page-faults               #    0.404 K/sec                  
     3,347,599,494      cycles                    #    2.986 GHz                    
     1,950,358,273      stalled-cycles-frontend   #   58.26% frontend cycles idle   
     3,854,298,766      instructions              #    1.15  insn per cycle         
                                                  #    0.51  stalled cycles per insn
       702,163,113      branches                  #  626.262 M/sec                  
        36,776,554      branch-misses             #    5.24% of all branches        

       3.664222029 seconds time elapsed


real	0m3.189s
user	0m0.907s
sys	0m0.216s

 Performance counter stats for './__run 7':

       1115.126830      task-clock (msec)         #    0.349 CPUs utilized          
             1,170      context-switches          #    0.001 M/sec                  
               206      cpu-migrations            #    0.185 K/sec                  
               457      page-faults               #    0.410 K/sec                  
     3,311,238,882      cycles                    #    2.969 GHz                    
     1,918,391,177      stalled-cycles-frontend   #   57.94% frontend cycles idle   
     3,853,931,627      instructions              #    1.16  insn per cycle         
                                                  #    0.50  stalled cycles per insn
       702,062,976      branches                  #  629.581 M/sec                  
        36,651,691      branch-misses             #    5.22% of all branches        

       3.191464323 seconds time elapsed


real	0m9.738s
user	0m2.367s
sys	0m0.542s

 Performance counter stats for './__run 8':

       2884.423778      task-clock (msec)         #    0.296 CPUs utilized          
             3,270      context-switches          #    0.001 M/sec                  
               685      cpu-migrations            #    0.237 K/sec                  
               728      page-faults               #    0.252 K/sec                  
     8,615,160,626      cycles                    #    2.987 GHz                    
     4,929,209,279      stalled-cycles-frontend   #   57.22% frontend cycles idle   
    10,050,919,142      instructions              #    1.17  insn per cycle         
                                                  #    0.49  stalled cycles per insn
     1,830,802,501      branches                  #  634.720 M/sec                  
        95,914,142      branch-misses             #    5.24% of all branches        

       9.742017567 seconds time elapsed


real	0m8.224s
user	0m2.325s
sys	0m0.642s

 Performance counter stats for './__run 8':

       2942.679625      task-clock (msec)         #    0.358 CPUs utilized          
             3,570      context-switches          #    0.001 M/sec                  
               701      cpu-migrations            #    0.238 K/sec                  
               728      page-faults               #    0.247 K/sec                  
     8,814,068,342      cycles                    #    2.995 GHz                    
     5,161,895,873      stalled-cycles-frontend   #   58.56% frontend cycles idle   
    10,051,079,416      instructions              #    1.14  insn per cycle         
                                                  #    0.51  stalled cycles per insn
     1,830,881,161      branches                  #  622.182 M/sec                  
        95,788,779      branch-misses             #    5.23% of all branches        

       8.226531715 seconds time elapsed


real	0m9.911s
user	0m2.341s
sys	0m0.546s

 Performance counter stats for './__run 8':

       2859.559702      task-clock (msec)         #    0.288 CPUs utilized          
             3,355      context-switches          #    0.001 M/sec                  
               698      cpu-migrations            #    0.244 K/sec                  
               725      page-faults               #    0.254 K/sec                  
     8,556,956,935      cycles                    #    2.992 GHz                    
     4,873,775,409      stalled-cycles-frontend   #   56.96% frontend cycles idle   
    10,050,821,553      instructions              #    1.17  insn per cycle         
                                                  #    0.48  stalled cycles per insn
     1,830,811,155      branches                  #  640.242 M/sec                  
        95,872,523      branch-misses             #    5.24% of all branches        

       9.913404113 seconds time elapsed


real	0m7.804s
user	0m2.079s
sys	0m0.601s

 Performance counter stats for './__run 9':

       2657.701236      task-clock (msec)         #    0.340 CPUs utilized          
             3,144      context-switches          #    0.001 M/sec                  
               635      cpu-migrations            #    0.239 K/sec                  
               694      page-faults               #    0.261 K/sec                  
     7,967,402,099      cycles                    #    2.998 GHz                    
     4,567,489,592      stalled-cycles-frontend   #   57.33% frontend cycles idle   
     9,299,290,913      instructions              #    1.17  insn per cycle         
                                                  #    0.49  stalled cycles per insn
     1,693,941,458      branches                  #  637.371 M/sec                  
        88,601,035      branch-misses             #    5.23% of all branches        

       7.807890249 seconds time elapsed


real	0m7.647s
user	0m2.174s
sys	0m0.557s

 Performance counter stats for './__run 9':

       2710.243527      task-clock (msec)         #    0.354 CPUs utilized          
             2,980      context-switches          #    0.001 M/sec                  
               570      cpu-migrations            #    0.210 K/sec                  
               691      page-faults               #    0.255 K/sec                  
     8,098,878,522      cycles                    #    2.988 GHz                    
     4,707,152,829      stalled-cycles-frontend   #   58.12% frontend cycles idle   
     9,303,204,872      instructions              #    1.15  insn per cycle         
                                                  #    0.51  stalled cycles per insn
     1,694,677,884      branches                  #  625.286 M/sec                  
        88,631,002      branch-misses             #    5.23% of all branches        

       7.650787333 seconds time elapsed


real	0m8.059s
user	0m2.147s
sys	0m0.580s

 Performance counter stats for './__run 9':

       2704.596575      task-clock (msec)         #    0.335 CPUs utilized          
             3,029      context-switches          #    0.001 M/sec                  
               637      cpu-migrations            #    0.236 K/sec                  
               688      page-faults               #    0.254 K/sec                  
     8,069,140,820      cycles                    #    2.983 GHz                    
     4,696,224,897      stalled-cycles-frontend   #   58.20% frontend cycles idle   
     9,297,326,857      instructions              #    1.15  insn per cycle         
                                                  #    0.51  stalled cycles per insn
     1,693,554,421      branches                  #  626.176 M/sec                  
        88,624,997      branch-misses             #    5.23% of all branches        

       8.061641075 seconds time elapsed


real	0m13.606s
user	0m3.544s
sys	0m0.899s

 Performance counter stats for './__run 10':

       4407.088280      task-clock (msec)         #    0.324 CPUs utilized          
             5,320      context-switches          #    0.001 M/sec                  
             1,067      cpu-migrations            #    0.242 K/sec                  
               933      page-faults               #    0.212 K/sec                  
    13,401,882,271      cycles                    #    3.041 GHz                    
     8,118,418,667      stalled-cycles-frontend   #   60.58% frontend cycles idle   
    14,746,455,233      instructions              #    1.10  insn per cycle         
                                                  #    0.55  stalled cycles per insn
     2,686,158,559      branches                  #  609.509 M/sec                  
       141,374,810      branch-misses             #    5.26% of all branches        

      13.608804838 seconds time elapsed


real	0m16.502s
user	0m3.631s
sys	0m0.898s

 Performance counter stats for './__run 10':

       4491.864442      task-clock (msec)         #    0.272 CPUs utilized          
             5,704      context-switches          #    0.001 M/sec                  
             1,325      cpu-migrations            #    0.295 K/sec                  
               929      page-faults               #    0.207 K/sec                  
    13,677,493,860      cycles                    #    3.045 GHz                    
     8,509,016,144      stalled-cycles-frontend   #   62.21% frontend cycles idle   
    14,777,969,949      instructions              #    1.08  insn per cycle         
                                                  #    0.58  stalled cycles per insn
     2,692,923,615      branches                  #  599.511 M/sec                  
       141,355,065      branch-misses             #    5.25% of all branches        

      16.505008426 seconds time elapsed


real	0m15.015s
user	0m3.724s
sys	0m0.782s

 Performance counter stats for './__run 10':

       4473.226753      task-clock (msec)         #    0.298 CPUs utilized          
             5,595      context-switches          #    0.001 M/sec                  
             1,306      cpu-migrations            #    0.292 K/sec                  
               931      page-faults               #    0.208 K/sec                  
    13,662,292,851      cycles                    #    3.054 GHz                    
     8,536,878,015      stalled-cycles-frontend   #   62.48% frontend cycles idle   
    14,756,746,478      instructions              #    1.08  insn per cycle         
                                                  #    0.58  stalled cycles per insn
     2,687,968,913      branches                  #  600.902 M/sec                  
       141,220,310      branch-misses             #    5.25% of all branches        

      15.018516477 seconds time elapsed

