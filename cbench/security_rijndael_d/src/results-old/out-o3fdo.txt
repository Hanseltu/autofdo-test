
real	0m32.886s
user	0m6.146s
sys	0m7.420s

 Performance counter stats for './__run 1':

      12723.995437      task-clock (msec)         #    0.387 CPUs utilized          
           143,280      context-switches          #    0.011 M/sec                  
               419      cpu-migrations            #    0.033 K/sec                  
               248      page-faults               #    0.019 K/sec                  
    33,630,823,975      cycles                    #    2.643 GHz                    
    17,497,172,198      stalled-cycles-frontend   #   52.03% frontend cycles idle   
    44,868,716,219      instructions              #    1.33  insn per cycle         
                                                  #    0.39  stalled cycles per insn
     4,110,314,356      branches                  #  323.036 M/sec                  
        31,311,779      branch-misses             #    0.76% of all branches        

      32.888409869 seconds time elapsed


real	0m42.670s
user	0m6.665s
sys	0m9.037s

 Performance counter stats for './__run 1':

      14794.574140      task-clock (msec)         #    0.347 CPUs utilized          
           199,894      context-switches          #    0.014 M/sec                  
             1,608      cpu-migrations            #    0.109 K/sec                  
               250      page-faults               #    0.017 K/sec                  
    42,162,223,275      cycles                    #    2.850 GHz                    
    25,062,250,813      stalled-cycles-frontend   #   59.44% frontend cycles idle   
    46,334,329,190      instructions              #    1.10  insn per cycle         
                                                  #    0.54  stalled cycles per insn
     4,395,979,142      branches                  #  297.135 M/sec                  
        44,144,128      branch-misses             #    1.00% of all branches        

      42.671928390 seconds time elapsed


real	0m34.950s
user	0m6.703s
sys	0m9.110s

 Performance counter stats for './__run 1':

      14896.975723      task-clock (msec)         #    0.426 CPUs utilized          
           212,451      context-switches          #    0.014 M/sec                  
               442      cpu-migrations            #    0.030 K/sec                  
               249      page-faults               #    0.017 K/sec                  
    42,896,993,685      cycles                    #    2.880 GHz                    
    25,633,451,909      stalled-cycles-frontend   #   59.76% frontend cycles idle   
    46,606,455,340      instructions              #    1.09  insn per cycle         
                                                  #    0.55  stalled cycles per insn
     4,447,761,238      branches                  #  298.568 M/sec                  
        42,086,036      branch-misses             #    0.95% of all branches        

      34.952763851 seconds time elapsed


real	0m2.514s
user	0m0.483s
sys	0m0.706s

 Performance counter stats for './__run 2':

       1120.793909      task-clock (msec)         #    0.445 CPUs utilized          
            17,344      context-switches          #    0.015 M/sec                  
                29      cpu-migrations            #    0.026 K/sec                  
               251      page-faults               #    0.224 K/sec                  
     3,257,894,940      cycles                    #    2.907 GHz                    
     1,948,883,369      stalled-cycles-frontend   #   59.82% frontend cycles idle   
     3,517,747,403      instructions              #    1.08  insn per cycle         
                                                  #    0.55  stalled cycles per insn
       337,880,764      branches                  #  301.466 M/sec                  
         3,266,827      branch-misses             #    0.97% of all branches        

       2.517004740 seconds time elapsed


real	0m2.468s
user	0m0.567s
sys	0m0.623s

 Performance counter stats for './__run 2':

       1120.059622      task-clock (msec)         #    0.453 CPUs utilized          
            15,833      context-switches          #    0.014 M/sec                  
                32      cpu-migrations            #    0.029 K/sec                  
               253      page-faults               #    0.226 K/sec                  
     3,223,199,156      cycles                    #    2.878 GHz                    
     1,919,447,855      stalled-cycles-frontend   #   59.55% frontend cycles idle   
     3,496,574,169      instructions              #    1.08  insn per cycle         
                                                  #    0.55  stalled cycles per insn
       333,861,412      branches                  #  298.075 M/sec                  
         3,180,278      branch-misses             #    0.95% of all branches        

       2.469812409 seconds time elapsed


real	0m2.910s
user	0m0.503s
sys	0m0.698s

 Performance counter stats for './__run 2':

       1133.813655      task-clock (msec)         #    0.389 CPUs utilized          
            15,687      context-switches          #    0.014 M/sec                  
               111      cpu-migrations            #    0.098 K/sec                  
               250      page-faults               #    0.220 K/sec                  
     3,230,467,882      cycles                    #    2.849 GHz                    
     1,929,861,430      stalled-cycles-frontend   #   59.74% frontend cycles idle   
     3,496,513,009      instructions              #    1.08  insn per cycle         
                                                  #    0.55  stalled cycles per insn
       333,839,204      branches                  #  294.439 M/sec                  
         3,421,193      branch-misses             #    1.02% of all branches        

       2.911805917 seconds time elapsed


real	0m1.243s
user	0m0.192s
sys	0m0.312s

 Performance counter stats for './__run 3':

        482.482843      task-clock (msec)         #    0.387 CPUs utilized          
             4,150      context-switches          #    0.009 M/sec                  
                16      cpu-migrations            #    0.033 K/sec                  
               251      page-faults               #    0.520 K/sec                  
     1,266,613,027      cycles                    #    2.625 GHz                    
       740,231,112      stalled-cycles-frontend   #   58.44% frontend cycles idle   
     1,423,803,989      instructions              #    1.12  insn per cycle         
                                                  #    0.52  stalled cycles per insn
       133,365,950      branches                  #  276.416 M/sec                  
         1,157,091      branch-misses             #    0.87% of all branches        

       1.246089378 seconds time elapsed


real	0m0.981s
user	0m0.221s
sys	0m0.274s

 Performance counter stats for './__run 3':

        473.915334      task-clock (msec)         #    0.482 CPUs utilized          
             4,106      context-switches          #    0.009 M/sec                  
                15      cpu-migrations            #    0.032 K/sec                  
               250      page-faults               #    0.528 K/sec                  
     1,259,960,985      cycles                    #    2.659 GHz                    
       733,923,971      stalled-cycles-frontend   #   58.25% frontend cycles idle   
     1,421,903,279      instructions              #    1.13  insn per cycle         
                                                  #    0.52  stalled cycles per insn
       133,120,714      branches                  #  280.896 M/sec                  
         1,147,688      branch-misses             #    0.86% of all branches        

       0.983200588 seconds time elapsed


real	0m0.980s
user	0m0.238s
sys	0m0.217s

 Performance counter stats for './__run 3':

        437.244770      task-clock (msec)         #    0.445 CPUs utilized          
             4,108      context-switches          #    0.009 M/sec                  
                13      cpu-migrations            #    0.030 K/sec                  
               251      page-faults               #    0.574 K/sec                  
     1,253,586,468      cycles                    #    2.867 GHz                    
       728,782,804      stalled-cycles-frontend   #   58.14% frontend cycles idle   
     1,420,872,522      instructions              #    1.13  insn per cycle         
                                                  #    0.51  stalled cycles per insn
       132,898,522      branches                  #  303.945 M/sec                  
         1,119,613      branch-misses             #    0.84% of all branches        

       0.982376233 seconds time elapsed


real	0m0.025s
user	0m0.008s
sys	0m0.006s

 Performance counter stats for './__run 4':

         15.427510      task-clock (msec)         #    0.575 CPUs utilized          
               130      context-switches          #    0.008 M/sec                  
                 2      cpu-migrations            #    0.130 K/sec                  
               251      page-faults               #    0.016 M/sec                  
        43,678,675      cycles                    #    2.831 GHz                    
        24,615,892      stalled-cycles-frontend   #   56.36% frontend cycles idle   
        51,005,579      instructions              #    1.17  insn per cycle         
                                                  #    0.48  stalled cycles per insn
         5,357,424      branches                  #  347.264 M/sec                  
            81,994      branch-misses             #    1.53% of all branches        

       0.026809981 seconds time elapsed


real	0m0.036s
user	0m0.008s
sys	0m0.008s

 Performance counter stats for './__run 4':

         17.964576      task-clock (msec)         #    0.471 CPUs utilized          
               139      context-switches          #    0.008 M/sec                  
                 6      cpu-migrations            #    0.334 K/sec                  
               251      page-faults               #    0.014 M/sec                  
        50,677,839      cycles                    #    2.821 GHz                    
        31,266,722      stalled-cycles-frontend   #   61.70% frontend cycles idle   
        51,036,152      instructions              #    1.01  insn per cycle         
                                                  #    0.61  stalled cycles per insn
         5,360,914      branches                  #  298.416 M/sec                  
            90,113      branch-misses             #    1.68% of all branches        

       0.038146837 seconds time elapsed


real	0m0.029s
user	0m0.007s
sys	0m0.011s

 Performance counter stats for './__run 4':

         20.161890      task-clock (msec)         #    0.636 CPUs utilized          
               130      context-switches          #    0.006 M/sec                  
                 1      cpu-migrations            #    0.050 K/sec                  
               251      page-faults               #    0.012 M/sec                  
        43,715,411      cycles                    #    2.168 GHz                    
        24,695,616      stalled-cycles-frontend   #   56.49% frontend cycles idle   
        50,959,842      instructions              #    1.17  insn per cycle         
                                                  #    0.48  stalled cycles per insn
         5,350,018      branches                  #  265.353 M/sec                  
            84,122      branch-misses             #    1.57% of all branches        

       0.031701933 seconds time elapsed


real	0m0.554s
user	0m0.116s
sys	0m0.133s

 Performance counter stats for './__run 5':

        235.792615      task-clock (msec)         #    0.424 CPUs utilized          
             3,909      context-switches          #    0.017 M/sec                  
                24      cpu-migrations            #    0.102 K/sec                  
               251      page-faults               #    0.001 M/sec                  
       696,282,439      cycles                    #    2.953 GHz                    
       419,994,259      stalled-cycles-frontend   #   60.32% frontend cycles idle   
       750,673,651      instructions              #    1.08  insn per cycle         
                                                  #    0.56  stalled cycles per insn
        72,956,030      branches                  #  309.408 M/sec                  
           797,408      branch-misses             #    1.09% of all branches        

       0.555771446 seconds time elapsed


real	0m0.523s
user	0m0.149s
sys	0m0.098s

 Performance counter stats for './__run 5':

        237.832506      task-clock (msec)         #    0.452 CPUs utilized          
             2,241      context-switches          #    0.009 M/sec                  
                29      cpu-migrations            #    0.122 K/sec                  
               254      page-faults               #    0.001 M/sec                  
       647,361,988      cycles                    #    2.722 GHz                    
       379,030,138      stalled-cycles-frontend   #   58.55% frontend cycles idle   
       727,885,264      instructions              #    1.12  insn per cycle         
                                                  #    0.52  stalled cycles per insn
        68,636,957      branches                  #  288.594 M/sec                  
           676,286      branch-misses             #    0.99% of all branches        

       0.526591410 seconds time elapsed


real	0m0.541s
user	0m0.105s
sys	0m0.146s

 Performance counter stats for './__run 5':

        237.998434      task-clock (msec)         #    0.438 CPUs utilized          
             3,943      context-switches          #    0.017 M/sec                  
                 8      cpu-migrations            #    0.034 K/sec                  
               247      page-faults               #    0.001 M/sec                  
       702,303,881      cycles                    #    2.951 GHz                    
       423,625,259      stalled-cycles-frontend   #   60.32% frontend cycles idle   
       750,557,924      instructions              #    1.07  insn per cycle         
                                                  #    0.56  stalled cycles per insn
        72,927,740      branches                  #  306.421 M/sec                  
           748,239      branch-misses             #    1.03% of all branches        

       0.543332361 seconds time elapsed


real	0m0.029s
user	0m0.009s
sys	0m0.009s

 Performance counter stats for './__run 6':

         18.812189      task-clock (msec)         #    0.601 CPUs utilized          
               292      context-switches          #    0.016 M/sec                  
                 1      cpu-migrations            #    0.053 K/sec                  
               248      page-faults               #    0.013 M/sec                  
        54,736,504      cycles                    #    2.910 GHz                    
        32,635,524      stalled-cycles-frontend   #   59.62% frontend cycles idle   
        59,562,717      instructions              #    1.09  insn per cycle         
                                                  #    0.55  stalled cycles per insn
         6,375,290      branches                  #  338.891 M/sec                  
           101,442      branch-misses             #    1.59% of all branches        

       0.031289517 seconds time elapsed


real	0m0.030s
user	0m0.017s
sys	0m0.003s

 Performance counter stats for './__run 6':

         21.779586      task-clock (msec)         #    0.660 CPUs utilized          
               171      context-switches          #    0.008 M/sec                  
                 5      cpu-migrations            #    0.230 K/sec                  
               247      page-faults               #    0.011 M/sec                  
        57,324,124      cycles                    #    2.632 GHz                    
        35,352,805      stalled-cycles-frontend   #   61.67% frontend cycles idle   
        57,982,488      instructions              #    1.01  insn per cycle         
                                                  #    0.61  stalled cycles per insn
         6,070,767      branches                  #  278.737 M/sec                  
           104,724      branch-misses             #    1.73% of all branches        

       0.032976536 seconds time elapsed


real	0m0.031s
user	0m0.010s
sys	0m0.009s

 Performance counter stats for './__run 6':

         21.046445      task-clock (msec)         #    0.618 CPUs utilized          
               220      context-switches          #    0.010 M/sec                  
                 6      cpu-migrations            #    0.285 K/sec                  
               249      page-faults               #    0.012 M/sec                  
        56,890,907      cycles                    #    2.703 GHz                    
        34,883,719      stalled-cycles-frontend   #   61.32% frontend cycles idle   
        58,584,087      instructions              #    1.03  insn per cycle         
                                                  #    0.60  stalled cycles per insn
         6,189,635      branches                  #  294.094 M/sec                  
           114,064      branch-misses             #    1.84% of all branches        

       0.034077781 seconds time elapsed


real	0m2.798s
user	0m0.552s
sys	0m0.746s

 Performance counter stats for './__run 7':

       1229.999433      task-clock (msec)         #    0.439 CPUs utilized          
            15,494      context-switches          #    0.013 M/sec                  
                64      cpu-migrations            #    0.052 K/sec                  
               252      page-faults               #    0.205 K/sec                  
     3,490,458,807      cycles                    #    2.838 GHz                    
     2,081,929,199      stalled-cycles-frontend   #   59.65% frontend cycles idle   
     3,788,480,237      instructions              #    1.09  insn per cycle         
                                                  #    0.55  stalled cycles per insn
       359,565,474      branches                  #  292.330 M/sec                  
         3,407,965      branch-misses             #    0.95% of all branches        

       2.800611200 seconds time elapsed


real	0m2.630s
user	0m0.562s
sys	0m0.714s

 Performance counter stats for './__run 7':

       1207.154041      task-clock (msec)         #    0.459 CPUs utilized          
            16,604      context-switches          #    0.014 M/sec                  
                95      cpu-migrations            #    0.079 K/sec                  
               250      page-faults               #    0.207 K/sec                  
     3,465,163,176      cycles                    #    2.871 GHz                    
     2,063,214,995      stalled-cycles-frontend   #   59.54% frontend cycles idle   
     3,802,554,480      instructions              #    1.10  insn per cycle         
                                                  #    0.54  stalled cycles per insn
       362,355,651      branches                  #  300.173 M/sec                  
         3,544,235      branch-misses             #    0.98% of all branches        

       2.632206844 seconds time elapsed


real	0m2.721s
user	0m0.522s
sys	0m0.745s

 Performance counter stats for './__run 7':

       1196.639743      task-clock (msec)         #    0.440 CPUs utilized          
            16,664      context-switches          #    0.014 M/sec                  
                76      cpu-migrations            #    0.064 K/sec                  
               253      page-faults               #    0.211 K/sec                  
     3,499,899,728      cycles                    #    2.925 GHz                    
     2,088,965,695      stalled-cycles-frontend   #   59.69% frontend cycles idle   
     3,803,328,850      instructions              #    1.09  insn per cycle         
                                                  #    0.55  stalled cycles per insn
       362,589,548      branches                  #  303.006 M/sec                  
         3,562,786      branch-misses             #    0.98% of all branches        

       2.722653700 seconds time elapsed


real	0m16.815s
user	0m3.452s
sys	0m4.397s

 Performance counter stats for './__run 8':

       7408.634998      task-clock (msec)         #    0.441 CPUs utilized          
           102,077      context-switches          #    0.014 M/sec                  
               423      cpu-migrations            #    0.057 K/sec                  
               253      page-faults               #    0.034 K/sec                  
    21,252,392,092      cycles                    #    2.869 GHz                    
    12,664,233,242      stalled-cycles-frontend   #   59.59% frontend cycles idle   
    23,180,493,179      instructions              #    1.09  insn per cycle         
                                                  #    0.55  stalled cycles per insn
     2,207,264,000      branches                  #  297.931 M/sec                  
        21,211,728      branch-misses             #    0.96% of all branches        

      16.817123178 seconds time elapsed


real	0m17.347s
user	0m3.250s
sys	0m4.706s

 Performance counter stats for './__run 8':

       7505.758633      task-clock (msec)         #    0.433 CPUs utilized          
           103,255      context-switches          #    0.014 M/sec                  
               671      cpu-migrations            #    0.089 K/sec                  
               249      page-faults               #    0.033 K/sec                  
    21,658,339,301      cycles                    #    2.886 GHz                    
    13,056,128,950      stalled-cycles-frontend   #   60.28% frontend cycles idle   
    23,200,934,168      instructions              #    1.07  insn per cycle         
                                                  #    0.56  stalled cycles per insn
     2,211,241,895      branches                  #  294.606 M/sec                  
        23,302,794      branch-misses             #    1.05% of all branches        

      17.350120261 seconds time elapsed


real	0m17.161s
user	0m3.204s
sys	0m4.625s

 Performance counter stats for './__run 8':

       7385.524482      task-clock (msec)         #    0.430 CPUs utilized          
           100,680      context-switches          #    0.014 M/sec                  
               649      cpu-migrations            #    0.088 K/sec                  
               248      page-faults               #    0.034 K/sec                  
    21,410,227,608      cycles                    #    2.899 GHz                    
    12,817,981,026      stalled-cycles-frontend   #   59.87% frontend cycles idle   
    23,156,678,220      instructions              #    1.08  insn per cycle         
                                                  #    0.55  stalled cycles per insn
     2,202,841,389      branches                  #  298.265 M/sec                  
        22,154,046      branch-misses             #    1.01% of all branches        

      17.163779296 seconds time elapsed


real	0m1.058s
user	0m0.211s
sys	0m0.278s

 Performance counter stats for './__run 9':

        463.568323      task-clock (msec)         #    0.437 CPUs utilized          
             6,559      context-switches          #    0.014 M/sec                  
                24      cpu-migrations            #    0.052 K/sec                  
               252      page-faults               #    0.544 K/sec                  
     1,358,039,292      cycles                    #    2.930 GHz                    
       812,127,023      stalled-cycles-frontend   #   59.80% frontend cycles idle   
     1,472,157,967      instructions              #    1.08  insn per cycle         
                                                  #    0.55  stalled cycles per insn
       141,014,449      branches                  #  304.193 M/sec                  
         1,428,539      branch-misses             #    1.01% of all branches        

       1.061126493 seconds time elapsed


real	0m1.021s
user	0m0.229s
sys	0m0.273s

 Performance counter stats for './__run 9':

        481.069777      task-clock (msec)         #    0.470 CPUs utilized          
             5,158      context-switches          #    0.011 M/sec                  
                36      cpu-migrations            #    0.075 K/sec                  
               252      page-faults               #    0.524 K/sec                  
     1,335,315,155      cycles                    #    2.776 GHz                    
       793,302,155      stalled-cycles-frontend   #   59.41% frontend cycles idle   
     1,454,765,881      instructions              #    1.09  insn per cycle         
                                                  #    0.55  stalled cycles per insn
       137,644,339      branches                  #  286.121 M/sec                  
         1,322,382      branch-misses             #    0.96% of all branches        

       1.024625843 seconds time elapsed


real	0m1.011s
user	0m0.253s
sys	0m0.253s

 Performance counter stats for './__run 9':

        481.632282      task-clock (msec)         #    0.475 CPUs utilized          
             4,797      context-switches          #    0.010 M/sec                  
                44      cpu-migrations            #    0.091 K/sec                  
               248      page-faults               #    0.515 K/sec                  
     1,317,582,754      cycles                    #    2.736 GHz                    
       780,038,315      stalled-cycles-frontend   #   59.20% frontend cycles idle   
     1,447,560,349      instructions              #    1.10  insn per cycle         
                                                  #    0.54  stalled cycles per insn
       136,346,256      branches                  #  283.092 M/sec                  
         1,301,088      branch-misses             #    0.95% of all branches        

       1.014930115 seconds time elapsed


real	0m3.708s
user	0m0.760s
sys	0m1.018s

 Performance counter stats for './__run 10':

       1678.426016      task-clock (msec)         #    0.452 CPUs utilized          
            23,911      context-switches          #    0.014 M/sec                  
               174      cpu-migrations            #    0.104 K/sec                  
               250      page-faults               #    0.149 K/sec                  
     4,875,016,934      cycles                    #    2.905 GHz                    
     2,940,631,308      stalled-cycles-frontend   #   60.32% frontend cycles idle   
     5,209,817,844      instructions              #    1.07  insn per cycle         
                                                  #    0.56  stalled cycles per insn
       497,996,579      branches                  #  296.705 M/sec                  
         5,200,651      branch-misses             #    1.04% of all branches        

       3.710125633 seconds time elapsed


real	0m3.868s
user	0m0.721s
sys	0m1.089s

 Performance counter stats for './__run 10':

       1709.697204      task-clock (msec)         #    0.442 CPUs utilized          
            23,762      context-switches          #    0.014 M/sec                  
               213      cpu-migrations            #    0.125 K/sec                  
               251      page-faults               #    0.147 K/sec                  
     4,932,593,619      cycles                    #    2.885 GHz                    
     2,990,716,851      stalled-cycles-frontend   #   60.63% frontend cycles idle   
     5,209,986,143      instructions              #    1.06  insn per cycle         
                                                  #    0.57  stalled cycles per insn
       498,014,562      branches                  #  291.288 M/sec                  
         5,347,420      branch-misses             #    1.07% of all branches        

       3.871180813 seconds time elapsed


real	0m3.825s
user	0m0.789s
sys	0m0.968s

 Performance counter stats for './__run 10':

       1659.926956      task-clock (msec)         #    0.434 CPUs utilized          
            22,890      context-switches          #    0.014 M/sec                  
                81      cpu-migrations            #    0.049 K/sec                  
               246      page-faults               #    0.148 K/sec                  
     4,747,937,788      cycles                    #    2.860 GHz                    
     2,826,854,394      stalled-cycles-frontend   #   59.54% frontend cycles idle   
     5,196,557,853      instructions              #    1.09  insn per cycle         
                                                  #    0.54  stalled cycles per insn
       495,439,669      branches                  #  298.471 M/sec                  
         4,731,162      branch-misses             #    0.95% of all branches        

       3.828333520 seconds time elapsed

