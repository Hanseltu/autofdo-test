
real	0m5.946s
user	0m5.508s
sys	0m0.192s

 Performance counter stats for './__run 1':

       5696.156179      task-clock (msec)         #    0.957 CPUs utilized          
             2,127      context-switches          #    0.373 K/sec                  
                 5      cpu-migrations            #    0.001 K/sec                  
            17,191      page-faults               #    0.003 M/sec                  
    18,564,096,557      cycles                    #    3.259 GHz                    
     6,824,863,155      stalled-cycles-frontend   #   36.76% frontend cycles idle   
    28,916,817,714      instructions              #    1.56  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     4,512,389,574      branches                  #  792.182 M/sec                  
       247,303,308      branch-misses             #    5.48% of all branches        

       5.950483690 seconds time elapsed


real	0m4.236s
user	0m3.920s
sys	0m0.120s

 Performance counter stats for './__run 2':

       4037.818302      task-clock (msec)         #    0.953 CPUs utilized          
             1,506      context-switches          #    0.373 K/sec                  
                 7      cpu-migrations            #    0.002 K/sec                  
            12,233      page-faults               #    0.003 M/sec                  
    13,125,094,797      cycles                    #    3.251 GHz                    
     4,837,101,834      stalled-cycles-frontend   #   36.85% frontend cycles idle   
    20,438,813,934      instructions              #    1.56  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     3,189,580,697      branches                  #  789.927 M/sec                  
       174,859,616      branch-misses             #    5.48% of all branches        

       4.238216987 seconds time elapsed


real	0m0.058s
user	0m0.042s
sys	0m0.000s

 Performance counter stats for './__run 3':

         44.783947      task-clock (msec)         #    0.735 CPUs utilized          
                18      context-switches          #    0.402 K/sec                  
                 1      cpu-migrations            #    0.022 K/sec                  
               385      page-faults               #    0.009 M/sec                  
       137,189,300      cycles                    #    3.063 GHz                    
        63,255,548      stalled-cycles-frontend   #   46.11% frontend cycles idle   
       183,195,753      instructions              #    1.34  insn per cycle         
                                                  #    0.35  stalled cycles per insn
        28,829,409      branches                  #  643.744 M/sec                  
         1,578,776      branch-misses             #    5.48% of all branches        

       0.060911671 seconds time elapsed


real	0m0.728s
user	0m0.648s
sys	0m0.049s

 Performance counter stats for './__run 4':

        698.688976      task-clock (msec)         #    0.956 CPUs utilized          
               262      context-switches          #    0.375 K/sec                  
                 3      cpu-migrations            #    0.004 K/sec                  
             2,340      page-faults               #    0.003 M/sec                  
     2,275,344,620      cycles                    #    3.257 GHz                    
       846,428,035      stalled-cycles-frontend   #   37.20% frontend cycles idle   
     3,520,833,927      instructions              #    1.55  insn per cycle         
                                                  #    0.24  stalled cycles per insn
       549,665,676      branches                  #  786.710 M/sec                  
        30,136,015      branch-misses             #    5.48% of all branches        

       0.730885975 seconds time elapsed


real	0m14.159s
user	0m13.109s
sys	0m0.501s

 Performance counter stats for './__run 5':

      13597.241628      task-clock (msec)         #    0.960 CPUs utilized          
             5,094      context-switches          #    0.375 K/sec                  
                11      cpu-migrations            #    0.001 K/sec                  
            40,689      page-faults               #    0.003 M/sec                  
    44,297,433,925      cycles                    #    3.258 GHz                    
    16,277,254,334      stalled-cycles-frontend   #   36.75% frontend cycles idle   
    69,087,544,981      instructions              #    1.56  insn per cycle         
                                                  #    0.24  stalled cycles per insn
    10,780,751,984      branches                  #  792.863 M/sec                  
       590,645,984      branch-misses             #    5.48% of all branches        

      14.162001887 seconds time elapsed


real	0m8.596s
user	0m7.838s
sys	0m0.382s

 Performance counter stats for './__run 6':

       8213.559485      task-clock (msec)         #    0.955 CPUs utilized          
             3,080      context-switches          #    0.375 K/sec                  
                 9      cpu-migrations            #    0.001 K/sec                  
            24,676      page-faults               #    0.003 M/sec                  
    26,734,161,538      cycles                    #    3.255 GHz                    
     9,822,821,012      stalled-cycles-frontend   #   36.74% frontend cycles idle   
    41,706,698,167      instructions              #    1.56  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     6,508,292,589      branches                  #  792.384 M/sec                  
       356,611,043      branch-misses             #    5.48% of all branches        

       8.598939440 seconds time elapsed


real	0m0.799s
user	0m0.717s
sys	0m0.040s

 Performance counter stats for './__run 7':

        759.571304      task-clock (msec)         #    0.947 CPUs utilized          
               284      context-switches          #    0.374 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
             2,501      page-faults               #    0.003 M/sec                  
     2,469,350,713      cycles                    #    3.251 GHz                    
       911,963,714      stalled-cycles-frontend   #   36.93% frontend cycles idle   
     3,794,925,649      instructions              #    1.54  insn per cycle         
                                                  #    0.24  stalled cycles per insn
       592,461,339      branches                  #  779.994 M/sec                  
        32,455,908      branch-misses             #    5.48% of all branches        

       0.802303263 seconds time elapsed


real	0m2.363s
user	0m2.152s
sys	0m0.080s

 Performance counter stats for './__run 8':

       2231.954241      task-clock (msec)         #    0.943 CPUs utilized          
               828      context-switches          #    0.371 K/sec                  
                 8      cpu-migrations            #    0.004 K/sec                  
             6,823      page-faults               #    0.003 M/sec                  
     7,264,408,240      cycles                    #    3.255 GHz                    
     2,675,051,576      stalled-cycles-frontend   #   36.82% frontend cycles idle   
    11,180,130,698      instructions              #    1.54  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     1,744,858,678      branches                  #  781.763 M/sec                  
        95,626,299      branch-misses             #    5.48% of all branches        

       2.366281112 seconds time elapsed


real	0m2.029s
user	0m1.891s
sys	0m0.072s

 Performance counter stats for './__run 9':

       1962.844448      task-clock (msec)         #    0.966 CPUs utilized          
               735      context-switches          #    0.374 K/sec                  
                 5      cpu-migrations            #    0.003 K/sec                  
             6,109      page-faults               #    0.003 M/sec                  
     6,408,741,756      cycles                    #    3.265 GHz                    
     2,366,685,982      stalled-cycles-frontend   #   36.93% frontend cycles idle   
     9,963,120,453      instructions              #    1.55  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     1,554,953,532      branches                  #  792.194 M/sec                  
        85,217,927      branch-misses             #    5.48% of all branches        

       2.031582908 seconds time elapsed


real	0m3.477s
user	0m3.180s
sys	0m0.124s

 Performance counter stats for './__run 10':

       3303.362268      task-clock (msec)         #    0.949 CPUs utilized          
             1,235      context-switches          #    0.374 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
            10,082      page-faults               #    0.003 M/sec                  
    10,752,660,829      cycles                    #    3.255 GHz                    
     3,955,251,415      stalled-cycles-frontend   #   36.78% frontend cycles idle   
    16,760,154,408      instructions              #    1.56  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     2,615,557,501      branches                  #  791.786 M/sec                  
       143,288,398      branch-misses             #    5.48% of all branches        

       3.480410938 seconds time elapsed

