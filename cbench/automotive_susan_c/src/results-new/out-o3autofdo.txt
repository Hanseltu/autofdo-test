
real	0m5.585s
user	0m3.502s
sys	0m0.532s

 Performance counter stats for './__run 1':

       4037.128994      task-clock (msec)         #    0.721 CPUs utilized          
               770      context-switches          #    0.191 K/sec                  
                 7      cpu-migrations            #    0.002 K/sec                  
           188,608      page-faults               #    0.047 M/sec                  
    13,053,080,696      cycles                    #    3.233 GHz                    
     3,803,151,892      stalled-cycles-frontend   #   29.14% frontend cycles idle   
    32,117,923,288      instructions              #    2.46  insn per cycle         
                                                  #    0.12  stalled cycles per insn
     1,414,944,361      branches                  #  350.483 M/sec                  
        52,274,386      branch-misses             #    3.69% of all branches        

       5.599520168 seconds time elapsed


real	0m5.043s
user	0m2.988s
sys	0m0.613s

 Performance counter stats for './__run 2':

       3603.167317      task-clock (msec)         #    0.714 CPUs utilized          
               668      context-switches          #    0.185 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
           166,769      page-faults               #    0.046 M/sec                  
    11,611,167,062      cycles                    #    3.222 GHz                    
     3,434,787,600      stalled-cycles-frontend   #   29.58% frontend cycles idle   
    28,378,633,362      instructions              #    2.44  insn per cycle         
                                                  #    0.12  stalled cycles per insn
     1,249,498,013      branches                  #  346.778 M/sec                  
        46,295,839      branch-misses             #    3.71% of all branches        

       5.045493347 seconds time elapsed


real	0m1.359s
user	0m1.031s
sys	0m0.105s

 Performance counter stats for './__run 3':

       1139.900352      task-clock (msec)         #    0.836 CPUs utilized          
               180      context-switches          #    0.158 K/sec                  
                 2      cpu-migrations            #    0.002 K/sec                  
            53,007      page-faults               #    0.047 M/sec                  
     3,665,997,008      cycles                    #    3.216 GHz                    
     1,096,044,736      stalled-cycles-frontend   #   29.90% frontend cycles idle   
     8,914,205,040      instructions              #    2.43  insn per cycle         
                                                  #    0.12  stalled cycles per insn
       392,826,296      branches                  #  344.615 M/sec                  
        14,538,889      branch-misses             #    3.70% of all branches        

       1.363491914 seconds time elapsed


real	0m19.025s
user	0m12.395s
sys	0m1.737s

 Performance counter stats for './__run 4':

      14133.364728      task-clock (msec)         #    0.743 CPUs utilized          
             2,781      context-switches          #    0.197 K/sec                  
                 6      cpu-migrations            #    0.000 K/sec                  
           658,527      page-faults               #    0.047 M/sec                  
    45,607,899,211      cycles                    #    3.227 GHz                    
    13,205,442,046      stalled-cycles-frontend   #   28.95% frontend cycles idle   
   112,523,813,700      instructions              #    2.47  insn per cycle         
                                                  #    0.12  stalled cycles per insn
     4,953,503,305      branches                  #  350.483 M/sec                  
       183,143,585      branch-misses             #    3.70% of all branches        

      19.027920580 seconds time elapsed


real	0m2.144s
user	0m1.400s
sys	0m0.201s

 Performance counter stats for './__run 5':

       1604.183111      task-clock (msec)         #    0.747 CPUs utilized          
               318      context-switches          #    0.198 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
            76,288      page-faults               #    0.048 M/sec                  
     5,165,726,294      cycles                    #    3.220 GHz                    
     1,450,724,187      stalled-cycles-frontend   #   28.08% frontend cycles idle   
    12,900,695,690      instructions              #    2.50  insn per cycle         
                                                  #    0.11  stalled cycles per insn
       568,777,332      branches                  #  354.559 M/sec                  
        21,044,551      branch-misses             #    3.70% of all branches        

       2.147127280 seconds time elapsed


real	0m1.657s
user	0m0.971s
sys	0m0.152s

 Performance counter stats for './__run 6':

       1125.889840      task-clock (msec)         #    0.678 CPUs utilized          
               218      context-switches          #    0.194 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
            52,287      page-faults               #    0.046 M/sec                  
     3,600,572,768      cycles                    #    3.198 GHz                    
     1,062,800,046      stalled-cycles-frontend   #   29.52% frontend cycles idle   
     8,794,297,230      instructions              #    2.44  insn per cycle         
                                                  #    0.12  stalled cycles per insn
       388,036,497      branches                  #  344.649 M/sec                  
        14,325,265      branch-misses             #    3.69% of all branches        

       1.660064468 seconds time elapsed


real	0m6.723s
user	0m4.340s
sys	0m0.709s

 Performance counter stats for './__run 7':

       5052.205664      task-clock (msec)         #    0.751 CPUs utilized          
               909      context-switches          #    0.180 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
           232,768      page-faults               #    0.046 M/sec                  
    16,285,632,991      cycles                    #    3.223 GHz                    
     4,855,128,570      stalled-cycles-frontend   #   29.81% frontend cycles idle   
    39,669,031,490      instructions              #    2.44  insn per cycle         
                                                  #    0.12  stalled cycles per insn
     1,745,909,848      branches                  #  345.574 M/sec                  
        64,684,351      branch-misses             #    3.70% of all branches        

       6.726552903 seconds time elapsed


real	0m24.706s
user	0m15.595s
sys	0m2.339s

 Performance counter stats for './__run 8':

      17935.625658      task-clock (msec)         #    0.726 CPUs utilized          
             3,505      context-switches          #    0.195 K/sec                  
                13      cpu-migrations            #    0.001 K/sec                  
           844,768      page-faults               #    0.047 M/sec                  
    57,965,800,060      cycles                    #    3.232 GHz                    
    16,398,549,422      stalled-cycles-frontend   #   28.29% frontend cycles idle   
   144,383,806,381      instructions              #    2.49  insn per cycle         
                                                  #    0.11  stalled cycles per insn
     6,357,453,569      branches                  #  354.460 M/sec                  
       234,936,801      branch-misses             #    3.70% of all branches        

      24.709891396 seconds time elapsed


real	0m1.012s
user	0m0.676s
sys	0m0.072s

 Performance counter stats for './__run 9':

        751.417132      task-clock (msec)         #    0.740 CPUs utilized          
               133      context-switches          #    0.177 K/sec                  
                 2      cpu-migrations            #    0.003 K/sec                  
            35,726      page-faults               #    0.048 M/sec                  
     2,402,588,490      cycles                    #    3.197 GHz                    
       683,823,063      stalled-cycles-frontend   #   28.46% frontend cycles idle   
     5,961,286,564      instructions              #    2.48  insn per cycle         
                                                  #    0.11  stalled cycles per insn
       263,449,837      branches                  #  350.604 M/sec                  
         9,737,784      branch-misses             #    3.70% of all branches        

       1.015685587 seconds time elapsed


real	0m17.458s
user	0m10.501s
sys	0m1.571s

 Performance counter stats for './__run 10':

      12073.026152      task-clock (msec)         #    0.691 CPUs utilized          
             2,319      context-switches          #    0.192 K/sec                  
                10      cpu-migrations            #    0.001 K/sec                  
           558,927      page-faults               #    0.046 M/sec                  
    38,944,830,548      cycles                    #    3.226 GHz                    
    11,453,050,669      stalled-cycles-frontend   #   29.41% frontend cycles idle   
    95,469,277,869      instructions              #    2.45  insn per cycle         
                                                  #    0.12  stalled cycles per insn
     4,200,771,164      branches                  #  347.947 M/sec                  
       156,012,553      branch-misses             #    3.71% of all branches        

      17.461178366 seconds time elapsed

