
real	0m3.944s
user	0m3.744s
sys	0m0.140s

 Performance counter stats for './__run 1':

       3887.937841      task-clock (msec)         #    0.985 CPUs utilized          
               228      context-switches          #    0.059 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
             1,063      page-faults               #    0.273 K/sec                  
    12,647,626,626      cycles                    #    3.253 GHz                    
     3,251,269,037      stalled-cycles-frontend   #   25.71% frontend cycles idle   
    32,703,613,293      instructions              #    2.59  insn per cycle         
                                                  #    0.10  stalled cycles per insn
     1,721,486,008      branches                  #  442.776 M/sec                  
        42,771,142      branch-misses             #    2.48% of all branches        

       3.947887205 seconds time elapsed


real	0m3.247s
user	0m3.040s
sys	0m0.124s

 Performance counter stats for './__run 2':

       3166.469357      task-clock (msec)         #    0.974 CPUs utilized          
               307      context-switches          #    0.097 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,058      page-faults               #    0.334 K/sec                  
    10,316,755,965      cycles                    #    3.258 GHz                    
     2,674,918,775      stalled-cycles-frontend   #   25.93% frontend cycles idle   
    26,592,427,262      instructions              #    2.58  insn per cycle         
                                                  #    0.10  stalled cycles per insn
     1,401,108,879      branches                  #  442.483 M/sec                  
        34,858,504      branch-misses             #    2.49% of all branches        

       3.249521170 seconds time elapsed


real	0m0.873s
user	0m0.824s
sys	0m0.048s

 Performance counter stats for './__run 3':

        874.386192      task-clock (msec)         #    0.998 CPUs utilized          
                84      context-switches          #    0.096 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,061      page-faults               #    0.001 M/sec                  
     2,836,594,305      cycles                    #    3.244 GHz                    
       791,478,423      stalled-cycles-frontend   #   27.90% frontend cycles idle   
     7,098,804,046      instructions              #    2.50  insn per cycle         
                                                  #    0.11  stalled cycles per insn
       375,708,588      branches                  #  429.683 M/sec                  
         9,344,671      branch-misses             #    2.49% of all branches        

       0.875902243 seconds time elapsed


real	0m12.891s
user	0m12.229s
sys	0m0.476s

 Performance counter stats for './__run 4':

      12708.621365      task-clock (msec)         #    0.986 CPUs utilized          
             1,218      context-switches          #    0.096 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,059      page-faults               #    0.083 K/sec                  
    41,485,317,229      cycles                    #    3.264 GHz                    
    10,697,003,437      stalled-cycles-frontend   #   25.79% frontend cycles idle   
   107,138,701,205      instructions              #    2.58  insn per cycle         
                                                  #    0.10  stalled cycles per insn
     5,642,313,047      branches                  #  443.975 M/sec                  
       140,240,503      branch-misses             #    2.49% of all branches        

      12.893961244 seconds time elapsed


real	0m1.591s
user	0m1.480s
sys	0m0.040s

 Performance counter stats for './__run 5':

       1522.987542      task-clock (msec)         #    0.955 CPUs utilized          
               145      context-switches          #    0.095 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,061      page-faults               #    0.697 K/sec                  
     4,957,103,202      cycles                    #    3.255 GHz                    
     1,285,264,700      stalled-cycles-frontend   #   25.93% frontend cycles idle   
    12,775,326,718      instructions              #    2.58  insn per cycle         
                                                  #    0.10  stalled cycles per insn
       673,671,630      branches                  #  442.336 M/sec                  
        16,762,146      branch-misses             #    2.49% of all branches        

       1.594063530 seconds time elapsed


real	0m0.708s
user	0m0.679s
sys	0m0.028s

 Performance counter stats for './__run 6':

        710.017611      task-clock (msec)         #    0.998 CPUs utilized          
                68      context-switches          #    0.096 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,058      page-faults               #    0.001 M/sec                  
     2,281,851,067      cycles                    #    3.214 GHz                    
       594,674,267      stalled-cycles-frontend   #   26.06% frontend cycles idle   
     5,867,171,705      instructions              #    2.57  insn per cycle         
                                                  #    0.10  stalled cycles per insn
       310,025,564      branches                  #  436.645 M/sec                  
         7,722,319      branch-misses             #    2.49% of all branches        

       0.711319892 seconds time elapsed


real	0m5.053s
user	0m4.769s
sys	0m0.148s

 Performance counter stats for './__run 7':

       4920.339173      task-clock (msec)         #    0.973 CPUs utilized          
               453      context-switches          #    0.092 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
             1,064      page-faults               #    0.216 K/sec                  
    16,017,781,537      cycles                    #    3.255 GHz                    
     4,104,180,455      stalled-cycles-frontend   #   25.62% frontend cycles idle   
    41,459,543,104      instructions              #    2.59  insn per cycle         
                                                  #    0.10  stalled cycles per insn
     2,183,915,280      branches                  #  443.855 M/sec                  
        54,284,920      branch-misses             #    2.49% of all branches        

       5.056002948 seconds time elapsed


real	0m15.903s
user	0m14.936s
sys	0m0.512s

 Performance counter stats for './__run 8':

      15453.009944      task-clock (msec)         #    0.971 CPUs utilized          
             1,504      context-switches          #    0.097 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
             1,062      page-faults               #    0.069 K/sec                  
    50,403,920,358      cycles                    #    3.262 GHz                    
    12,936,188,467      stalled-cycles-frontend   #   25.67% frontend cycles idle   
   130,394,902,669      instructions              #    2.59  insn per cycle         
                                                  #    0.10  stalled cycles per insn
     6,866,075,288      branches                  #  444.320 M/sec                  
       170,683,276      branch-misses             #    2.49% of all branches        

      15.906519992 seconds time elapsed


real	0m0.488s
user	0m0.463s
sys	0m0.024s

 Performance counter stats for './__run 9':

        490.301217      task-clock (msec)         #    0.998 CPUs utilized          
                47      context-switches          #    0.096 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
             1,058      page-faults               #    0.002 M/sec                  
     1,572,926,572      cycles                    #    3.208 GHz                    
       413,462,623      stalled-cycles-frontend   #   26.29% frontend cycles idle   
     4,031,327,929      instructions              #    2.56  insn per cycle         
                                                  #    0.10  stalled cycles per insn
       213,472,976      branches                  #  435.391 M/sec                  
         5,316,118      branch-misses             #    2.49% of all branches        

       0.491254975 seconds time elapsed


real	0m11.379s
user	0m10.881s
sys	0m0.324s

 Performance counter stats for './__run 10':

      11207.829744      task-clock (msec)         #    0.985 CPUs utilized          
               932      context-switches          #    0.083 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
             1,059      page-faults               #    0.094 K/sec                  
    36,556,478,231      cycles                    #    3.262 GHz                    
     9,393,720,804      stalled-cycles-frontend   #   25.70% frontend cycles idle   
    94,532,812,180      instructions              #    2.59  insn per cycle         
                                                  #    0.10  stalled cycles per insn
     4,977,092,288      branches                  #  444.073 M/sec                  
       123,675,849      branch-misses             #    2.48% of all branches        

      11.382212781 seconds time elapsed

