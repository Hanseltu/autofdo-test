
real	0m6.728s
user	0m5.437s
sys	0m0.439s

 Performance counter stats for './__run 1':

       5863.812459      task-clock (msec)         #    0.871 CPUs utilized          
             5,028      context-switches          #    0.857 K/sec                  
                19      cpu-migrations            #    0.003 K/sec                  
               294      page-faults               #    0.050 K/sec                  
    19,114,545,484      cycles                    #    3.260 GHz                    
     6,693,702,140      stalled-cycles-frontend   #   35.02% frontend cycles idle   
    29,550,368,582      instructions              #    1.55  insn per cycle         
                                                  #    0.23  stalled cycles per insn
     4,754,180,110      branches                  #  810.766 M/sec                  
       264,191,405      branch-misses             #    5.56% of all branches        

       6.731014915 seconds time elapsed


real	0m3.177s
user	0m2.761s
sys	0m0.193s

 Performance counter stats for './__run 2':

       2948.880558      task-clock (msec)         #    0.927 CPUs utilized          
             2,519      context-switches          #    0.854 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
               295      page-faults               #    0.100 K/sec                  
     9,603,412,845      cycles                    #    3.257 GHz                    
     3,391,761,217      stalled-cycles-frontend   #   35.32% frontend cycles idle   
    14,778,940,597      instructions              #    1.54  insn per cycle         
                                                  #    0.23  stalled cycles per insn
     2,377,854,824      branches                  #  806.358 M/sec                  
       132,102,837      branch-misses             #    5.56% of all branches        

       3.179698672 seconds time elapsed


real	0m0.029s
user	0m0.022s
sys	0m0.005s

 Performance counter stats for './__run 3':

         28.843801      task-clock (msec)         #    0.920 CPUs utilized          
                22      context-switches          #    0.763 K/sec                  
                 1      cpu-migrations            #    0.035 K/sec                  
               297      page-faults               #    0.010 M/sec                  
        86,028,412      cycles                    #    2.983 GHz                    
        33,859,182      stalled-cycles-frontend   #   39.36% frontend cycles idle   
       123,767,501      instructions              #    1.44  insn per cycle         
                                                  #    0.27  stalled cycles per insn
        20,130,932      branches                  #  697.929 M/sec                  
         1,117,098      branch-misses             #    5.55% of all branches        

       0.031354410 seconds time elapsed


real	0m0.710s
user	0m0.544s
sys	0m0.049s

 Performance counter stats for './__run 4':

        594.092779      task-clock (msec)         #    0.834 CPUs utilized          
               506      context-switches          #    0.852 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               296      page-faults               #    0.498 K/sec                  
     1,929,435,802      cycles                    #    3.248 GHz                    
       685,200,645      stalled-cycles-frontend   #   35.51% frontend cycles idle   
     2,960,217,144      instructions              #    1.53  insn per cycle         
                                                  #    0.23  stalled cycles per insn
       476,462,434      branches                  #  802.000 M/sec                  
        26,530,078      branch-misses             #    5.57% of all branches        

       0.712462198 seconds time elapsed


real	0m15.685s
user	0m13.297s
sys	0m1.049s

 Performance counter stats for './__run 5':

      14311.432509      task-clock (msec)         #    0.912 CPUs utilized          
            12,112      context-switches          #    0.846 K/sec                  
                15      cpu-migrations            #    0.001 K/sec                  
               298      page-faults               #    0.021 K/sec                  
    46,405,742,965      cycles                    #    3.243 GHz                    
    16,606,897,041      stalled-cycles-frontend   #   35.79% frontend cycles idle   
    70,923,705,786      instructions              #    1.53  insn per cycle         
                                                  #    0.23  stalled cycles per insn
    11,410,645,940      branches                  #  797.310 M/sec                  
       634,501,097      branch-misses             #    5.56% of all branches        

      15.688476370 seconds time elapsed


real	0m7.733s
user	0m6.585s
sys	0m0.522s

 Performance counter stats for './__run 6':

       7092.491416      task-clock (msec)         #    0.917 CPUs utilized          
             6,037      context-switches          #    0.851 K/sec                  
                 7      cpu-migrations            #    0.001 K/sec                  
               299      page-faults               #    0.042 K/sec                  
    23,053,632,422      cycles                    #    3.250 GHz                    
     8,156,717,243      stalled-cycles-frontend   #   35.38% frontend cycles idle   
    35,463,145,347      instructions              #    1.54  insn per cycle         
                                                  #    0.23  stalled cycles per insn
     5,705,579,123      branches                  #  804.453 M/sec                  
       316,911,485      branch-misses             #    5.55% of all branches        

       7.736576169 seconds time elapsed


real	0m0.644s
user	0m0.548s
sys	0m0.051s

 Performance counter stats for './__run 7':

        599.543912      task-clock (msec)         #    0.926 CPUs utilized          
               508      context-switches          #    0.847 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
               295      page-faults               #    0.492 K/sec                  
     1,931,020,952      cycles                    #    3.221 GHz                    
       686,493,586      stalled-cycles-frontend   #   35.55% frontend cycles idle   
     2,961,087,914      instructions              #    1.53  insn per cycle         
                                                  #    0.23  stalled cycles per insn
       476,635,299      branches                  #  794.996 M/sec                  
        26,465,680      branch-misses             #    5.55% of all branches        

       0.647235262 seconds time elapsed


real	0m2.630s
user	0m2.187s
sys	0m0.190s

 Performance counter stats for './__run 8':

       2374.012414      task-clock (msec)         #    0.902 CPUs utilized          
             2,018      context-switches          #    0.850 K/sec                  
                10      cpu-migrations            #    0.004 K/sec                  
               300      page-faults               #    0.126 K/sec                  
     7,704,554,561      cycles                    #    3.245 GHz                    
     2,734,859,093      stalled-cycles-frontend   #   35.50% frontend cycles idle   
    11,825,524,236      instructions              #    1.53  insn per cycle         
                                                  #    0.23  stalled cycles per insn
     1,902,764,240      branches                  #  801.497 M/sec                  
       105,795,598      branch-misses             #    5.56% of all branches        

       2.633368130 seconds time elapsed


real	0m1.968s
user	0m1.679s
sys	0m0.121s

 Performance counter stats for './__run 9':

       1797.952580      task-clock (msec)         #    0.912 CPUs utilized          
             1,506      context-switches          #    0.838 K/sec                  
                13      cpu-migrations            #    0.007 K/sec                  
               295      page-faults               #    0.164 K/sec                  
     5,808,355,941      cycles                    #    3.231 GHz                    
     2,082,231,079      stalled-cycles-frontend   #   35.85% frontend cycles idle   
     8,870,136,826      instructions              #    1.53  insn per cycle         
                                                  #    0.23  stalled cycles per insn
     1,427,282,378      branches                  #  793.838 M/sec                  
        79,455,668      branch-misses             #    5.57% of all branches        

       1.970671542 seconds time elapsed


real	0m3.443s
user	0m2.843s
sys	0m0.286s

 Performance counter stats for './__run 10':

       3124.018597      task-clock (msec)         #    0.906 CPUs utilized          
             2,530      context-switches          #    0.810 K/sec                  
                17      cpu-migrations            #    0.005 K/sec                  
               299      page-faults               #    0.096 K/sec                  
    10,069,485,382      cycles                    #    3.223 GHz                    
     3,852,152,195      stalled-cycles-frontend   #   38.26% frontend cycles idle   
    14,805,705,207      instructions              #    1.47  insn per cycle         
                                                  #    0.26  stalled cycles per insn
     2,382,599,178      branches                  #  762.671 M/sec                  
       132,939,741      branch-misses             #    5.58% of all branches        

       3.446932699 seconds time elapsed

