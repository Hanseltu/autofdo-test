
real	0m7.562s
user	0m7.561s
sys	0m0.000s

 Performance counter stats for './__run 1':

       7562.902725      task-clock (msec)         #    1.000 CPUs utilized          
                12      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.034 K/sec                  
    23,391,751,844      cycles                    #    3.093 GHz                    
    13,930,205,766      stalled-cycles-frontend   #   59.55% frontend cycles idle   
    33,457,834,607      instructions              #    1.43  insn per cycle         
                                                  #    0.42  stalled cycles per insn
     1,712,821,118      branches                  #  226.477 M/sec                  
           106,955      branch-misses             #    0.01% of all branches        

       7.563354052 seconds time elapsed


real	0m7.626s
user	0m7.618s
sys	0m0.008s

 Performance counter stats for './__run 1':

       7627.423192      task-clock (msec)         #    1.000 CPUs utilized          
                14      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.033 K/sec                  
    23,591,214,087      cycles                    #    3.093 GHz                    
    14,114,729,928      stalled-cycles-frontend   #   59.83% frontend cycles idle   
    33,475,686,539      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
     1,715,849,677      branches                  #  224.958 M/sec                  
           125,364      branch-misses             #    0.01% of all branches        

       7.627918663 seconds time elapsed


real	0m7.559s
user	0m7.558s
sys	0m0.000s

 Performance counter stats for './__run 1':

       7559.948704      task-clock (msec)         #    1.000 CPUs utilized          
                14      context-switches          #    0.002 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
               252      page-faults               #    0.033 K/sec                  
    23,382,540,096      cycles                    #    3.093 GHz                    
    13,922,727,869      stalled-cycles-frontend   #   59.54% frontend cycles idle   
    33,455,804,537      instructions              #    1.43  insn per cycle         
                                                  #    0.42  stalled cycles per insn
     1,712,487,515      branches                  #  226.521 M/sec                  
           105,956      branch-misses             #    0.01% of all branches        

       7.560477092 seconds time elapsed


real	0m0.549s
user	0m0.549s
sys	0m0.000s

 Performance counter stats for './__run 2':

        550.326146      task-clock (msec)         #    1.000 CPUs utilized          
                 3      context-switches          #    0.005 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.460 K/sec                  
     1,702,092,974      cycles                    #    3.093 GHz                    
     1,015,399,458      stalled-cycles-frontend   #   59.66% frontend cycles idle   
     2,422,234,104      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       124,893,579      branches                  #  226.945 M/sec                  
            58,361      branch-misses             #    0.05% of all branches        

       0.550566463 seconds time elapsed


real	0m0.548s
user	0m0.548s
sys	0m0.000s

 Performance counter stats for './__run 2':

        549.900265      task-clock (msec)         #    0.999 CPUs utilized          
                 3      context-switches          #    0.005 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.462 K/sec                  
     1,700,925,786      cycles                    #    3.093 GHz                    
     1,012,733,948      stalled-cycles-frontend   #   59.54% frontend cycles idle   
     2,421,156,494      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       124,714,117      branches                  #  226.794 M/sec                  
            76,722      branch-misses             #    0.06% of all branches        

       0.550282421 seconds time elapsed


real	0m0.549s
user	0m0.549s
sys	0m0.000s

 Performance counter stats for './__run 2':

        550.570047      task-clock (msec)         #    1.000 CPUs utilized          
                 4      context-switches          #    0.007 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               256      page-faults               #    0.465 K/sec                  
     1,702,842,075      cycles                    #    3.093 GHz                    
     1,016,005,283      stalled-cycles-frontend   #   59.67% frontend cycles idle   
     2,422,271,018      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       124,900,274      branches                  #  226.856 M/sec                  
            59,217      branch-misses             #    0.05% of all branches        

       0.550830740 seconds time elapsed


real	0m0.254s
user	0m0.250s
sys	0m0.004s

 Performance counter stats for './__run 3':

        255.801673      task-clock (msec)         #    0.999 CPUs utilized          
                 2      context-switches          #    0.008 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.989 K/sec                  
       791,298,828      cycles                    #    3.093 GHz                    
       472,118,870      stalled-cycles-frontend   #   59.66% frontend cycles idle   
     1,120,643,327      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
        58,220,162      branches                  #  227.599 M/sec                  
            66,295      branch-misses             #    0.11% of all branches        

       0.256091189 seconds time elapsed


real	0m0.256s
user	0m0.252s
sys	0m0.004s

 Performance counter stats for './__run 3':

        257.625090      task-clock (msec)         #    0.999 CPUs utilized          
                 3      context-switches          #    0.012 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.974 K/sec                  
       796,939,406      cycles                    #    3.093 GHz                    
       477,828,352      stalled-cycles-frontend   #   59.96% frontend cycles idle   
     1,121,848,602      instructions              #    1.41  insn per cycle         
                                                  #    0.43  stalled cycles per insn
        58,420,913      branches                  #  226.767 M/sec                  
            57,796      branch-misses             #    0.10% of all branches        

       0.257940552 seconds time elapsed


real	0m0.254s
user	0m0.254s
sys	0m0.000s

 Performance counter stats for './__run 3':

        256.483803      task-clock (msec)         #    0.999 CPUs utilized          
                 1      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.990 K/sec                  
       793,316,561      cycles                    #    3.093 GHz                    
       473,645,326      stalled-cycles-frontend   #   59.70% frontend cycles idle   
     1,120,662,248      instructions              #    1.41  insn per cycle         
                                                  #    0.42  stalled cycles per insn
        58,223,178      branches                  #  227.005 M/sec                  
            65,402      branch-misses             #    0.11% of all branches        

       0.256764202 seconds time elapsed


real	0m0.008s
user	0m0.008s
sys	0m0.000s

 Performance counter stats for './__run 4':

          9.379496      task-clock (msec)         #    0.979 CPUs utilized          
                 3      context-switches          #    0.320 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.027 M/sec                  
        28,965,921      cycles                    #    3.088 GHz                    
        17,797,955      stalled-cycles-frontend   #   61.44% frontend cycles idle   
        35,742,674      instructions              #    1.23  insn per cycle         
                                                  #    0.50  stalled cycles per insn
         2,761,390      branches                  #  294.407 M/sec                  
            54,233      branch-misses             #    1.96% of all branches        

       0.009585027 seconds time elapsed


real	0m0.008s
user	0m0.007s
sys	0m0.000s

 Performance counter stats for './__run 4':

          9.272337      task-clock (msec)         #    0.974 CPUs utilized          
                 3      context-switches          #    0.324 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.027 M/sec                  
        28,639,074      cycles                    #    3.089 GHz                    
        17,493,109      stalled-cycles-frontend   #   61.08% frontend cycles idle   
        35,771,729      instructions              #    1.25  insn per cycle         
                                                  #    0.49  stalled cycles per insn
         2,767,020      branches                  #  298.417 M/sec                  
            53,886      branch-misses             #    1.95% of all branches        

       0.009516943 seconds time elapsed


real	0m0.008s
user	0m0.003s
sys	0m0.006s

 Performance counter stats for './__run 4':

         10.316703      task-clock (msec)         #    0.969 CPUs utilized          
                 3      context-switches          #    0.291 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.025 M/sec                  
        32,109,965      cycles                    #    3.112 GHz                    
        21,037,657      stalled-cycles-frontend   #   65.52% frontend cycles idle   
        35,728,422      instructions              #    1.11  insn per cycle         
                                                  #    0.59  stalled cycles per insn
         2,760,386      branches                  #  267.565 M/sec                  
            56,179      branch-misses             #    2.04% of all branches        

       0.010642511 seconds time elapsed


real	0m0.123s
user	0m0.123s
sys	0m0.000s

 Performance counter stats for './__run 5':

        125.032252      task-clock (msec)         #    0.998 CPUs utilized          
                 2      context-switches          #    0.016 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               252      page-faults               #    0.002 M/sec                  
       386,668,155      cycles                    #    3.093 GHz                    
       230,642,433      stalled-cycles-frontend   #   59.65% frontend cycles idle   
       545,066,874      instructions              #    1.41  insn per cycle         
                                                  #    0.42  stalled cycles per insn
        28,846,252      branches                  #  230.710 M/sec                  
            59,712      branch-misses             #    0.21% of all branches        

       0.125266751 seconds time elapsed


real	0m0.123s
user	0m0.123s
sys	0m0.000s

 Performance counter stats for './__run 5':

        124.670370      task-clock (msec)         #    0.998 CPUs utilized          
                 4      context-switches          #    0.032 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               250      page-faults               #    0.002 M/sec                  
       385,542,911      cycles                    #    3.092 GHz                    
       230,255,134      stalled-cycles-frontend   #   59.72% frontend cycles idle   
       545,025,574      instructions              #    1.41  insn per cycle         
                                                  #    0.42  stalled cycles per insn
        28,837,953      branches                  #  231.314 M/sec                  
            57,307      branch-misses             #    0.20% of all branches        

       0.124896716 seconds time elapsed


real	0m0.123s
user	0m0.119s
sys	0m0.004s

 Performance counter stats for './__run 5':

        125.622996      task-clock (msec)         #    0.998 CPUs utilized          
                 1      context-switches          #    0.008 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.002 M/sec                  
       388,587,599      cycles                    #    3.093 GHz                    
       233,118,082      stalled-cycles-frontend   #   59.99% frontend cycles idle   
       544,998,667      instructions              #    1.40  insn per cycle         
                                                  #    0.43  stalled cycles per insn
        28,833,230      branches                  #  229.522 M/sec                  
            57,605      branch-misses             #    0.20% of all branches        

       0.125905987 seconds time elapsed


real	0m0.009s
user	0m0.009s
sys	0m0.000s

 Performance counter stats for './__run 6':

         11.059106      task-clock (msec)         #    0.982 CPUs utilized          
                 3      context-switches          #    0.271 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               255      page-faults               #    0.023 M/sec                  
        34,162,609      cycles                    #    3.089 GHz                    
        20,788,054      stalled-cycles-frontend   #   60.85% frontend cycles idle   
        43,548,309      instructions              #    1.27  insn per cycle         
                                                  #    0.48  stalled cycles per insn
         3,218,939      branches                  #  291.067 M/sec                  
            54,878      branch-misses             #    1.70% of all branches        

       0.011266678 seconds time elapsed


real	0m0.010s
user	0m0.007s
sys	0m0.003s

 Performance counter stats for './__run 6':

         11.883268      task-clock (msec)         #    0.980 CPUs utilized          
                 3      context-switches          #    0.252 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               257      page-faults               #    0.022 M/sec                  
        36,700,572      cycles                    #    3.088 GHz                    
        23,040,601      stalled-cycles-frontend   #   62.78% frontend cycles idle   
        43,512,027      instructions              #    1.19  insn per cycle         
                                                  #    0.53  stalled cycles per insn
         3,213,962      branches                  #  270.461 M/sec                  
            56,040      branch-misses             #    1.74% of all branches        

       0.012119886 seconds time elapsed


real	0m0.009s
user	0m0.009s
sys	0m0.000s

 Performance counter stats for './__run 6':

         11.064766      task-clock (msec)         #    0.980 CPUs utilized          
                 4      context-switches          #    0.362 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.023 M/sec                  
        34,174,339      cycles                    #    3.089 GHz                    
        20,788,440      stalled-cycles-frontend   #   60.83% frontend cycles idle   
        43,539,385      instructions              #    1.27  insn per cycle         
                                                  #    0.48  stalled cycles per insn
         3,218,982      branches                  #  290.922 M/sec                  
            55,333      branch-misses             #    1.72% of all branches        

       0.011289630 seconds time elapsed


real	0m0.546s
user	0m0.546s
sys	0m0.000s

 Performance counter stats for './__run 7':

        548.522908      task-clock (msec)         #    0.999 CPUs utilized          
                 1      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               257      page-faults               #    0.469 K/sec                  
     1,696,572,675      cycles                    #    3.093 GHz                    
     1,011,000,023      stalled-cycles-frontend   #   59.59% frontend cycles idle   
     2,421,853,415      instructions              #    1.43  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       124,869,340      branches                  #  227.647 M/sec                  
            61,260      branch-misses             #    0.05% of all branches        

       0.548830448 seconds time elapsed


real	0m0.548s
user	0m0.548s
sys	0m0.000s

 Performance counter stats for './__run 7':

        550.234050      task-clock (msec)         #    0.999 CPUs utilized          
                 2      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               252      page-faults               #    0.458 K/sec                  
     1,701,975,767      cycles                    #    3.093 GHz                    
     1,013,800,193      stalled-cycles-frontend   #   59.57% frontend cycles idle   
     2,421,905,528      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       124,880,228      branches                  #  226.958 M/sec                  
            72,891      branch-misses             #    0.06% of all branches        

       0.550544898 seconds time elapsed


real	0m0.547s
user	0m0.546s
sys	0m0.000s

 Performance counter stats for './__run 7':

        548.455958      task-clock (msec)         #    0.999 CPUs utilized          
                 6      context-switches          #    0.011 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               252      page-faults               #    0.459 K/sec                  
     1,696,289,048      cycles                    #    3.093 GHz                    
     1,010,148,402      stalled-cycles-frontend   #   59.55% frontend cycles idle   
     2,421,905,567      instructions              #    1.43  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       124,877,460      branches                  #  227.689 M/sec                  
            61,834      branch-misses             #    0.05% of all branches        

       0.548910493 seconds time elapsed


real	0m3.370s
user	0m3.370s
sys	0m0.000s

 Performance counter stats for './__run 8':

       3372.015329      task-clock (msec)         #    1.000 CPUs utilized          
                 5      context-switches          #    0.001 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.075 K/sec                  
    10,429,457,321      cycles                    #    3.093 GHz                    
     6,215,689,640      stalled-cycles-frontend   #   59.60% frontend cycles idle   
    14,875,009,276      instructions              #    1.43  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       762,296,981      branches                  #  226.066 M/sec                  
            91,191      branch-misses             #    0.01% of all branches        

       3.372340781 seconds time elapsed


real	0m3.393s
user	0m3.393s
sys	0m0.000s

 Performance counter stats for './__run 8':

       3395.019075      task-clock (msec)         #    1.000 CPUs utilized          
                 9      context-switches          #    0.003 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.075 K/sec                  
    10,500,572,939      cycles                    #    3.093 GHz                    
     6,271,367,802      stalled-cycles-frontend   #   59.72% frontend cycles idle   
    14,874,013,379      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       762,139,553      branches                  #  224.488 M/sec                  
           119,267      branch-misses             #    0.02% of all branches        

       3.395416231 seconds time elapsed


real	0m3.383s
user	0m3.383s
sys	0m0.000s

 Performance counter stats for './__run 8':

       3384.712545      task-clock (msec)         #    1.000 CPUs utilized          
                 7      context-switches          #    0.002 K/sec                  
                 2      cpu-migrations            #    0.001 K/sec                  
               252      page-faults               #    0.074 K/sec                  
    10,477,463,730      cycles                    #    3.096 GHz                    
     6,258,823,598      stalled-cycles-frontend   #   59.74% frontend cycles idle   
    14,873,927,420      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       762,115,115      branches                  #  225.164 M/sec                  
           100,035      branch-misses             #    0.01% of all branches        

       3.385111214 seconds time elapsed


real	0m0.256s
user	0m0.256s
sys	0m0.000s

 Performance counter stats for './__run 9':

        258.517014      task-clock (msec)         #    0.999 CPUs utilized          
                 2      context-switches          #    0.008 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.971 K/sec                  
       799,792,057      cycles                    #    3.094 GHz                    
       480,060,766      stalled-cycles-frontend   #   60.02% frontend cycles idle   
     1,122,859,113      instructions              #    1.40  insn per cycle         
                                                  #    0.43  stalled cycles per insn
        58,637,232      branches                  #  226.822 M/sec                  
            61,995      branch-misses             #    0.11% of all branches        

       0.258849442 seconds time elapsed


real	0m0.253s
user	0m0.253s
sys	0m0.000s

 Performance counter stats for './__run 9':

        255.276670      task-clock (msec)         #    0.999 CPUs utilized          
                 3      context-switches          #    0.012 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               250      page-faults               #    0.979 K/sec                  
       789,508,401      cycles                    #    3.093 GHz                    
       470,831,190      stalled-cycles-frontend   #   59.64% frontend cycles idle   
     1,121,628,751      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
        58,430,752      branches                  #  228.892 M/sec                  
            60,286      branch-misses             #    0.10% of all branches        

       0.255540147 seconds time elapsed


real	0m0.265s
user	0m0.261s
sys	0m0.004s

 Performance counter stats for './__run 9':

        266.836101      task-clock (msec)         #    0.999 CPUs utilized          
                 6      context-switches          #    0.022 K/sec                  
                 2      cpu-migrations            #    0.007 K/sec                  
               254      page-faults               #    0.952 K/sec                  
       845,295,611      cycles                    #    3.168 GHz                    
       523,573,385      stalled-cycles-frontend   #   61.94% frontend cycles idle   
     1,121,680,422      instructions              #    1.33  insn per cycle         
                                                  #    0.47  stalled cycles per insn
        58,439,487      branches                  #  219.009 M/sec                  
            62,933      branch-misses             #    0.11% of all branches        

       0.267161809 seconds time elapsed


real	0m0.845s
user	0m0.840s
sys	0m0.004s

 Performance counter stats for './__run 10':

        846.666791      task-clock (msec)         #    0.999 CPUs utilized          
                 5      context-switches          #    0.006 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               251      page-faults               #    0.296 K/sec                  
     2,618,935,584      cycles                    #    3.093 GHz                    
     1,560,706,575      stalled-cycles-frontend   #   59.59% frontend cycles idle   
     3,723,340,074      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       191,565,540      branches                  #  226.258 M/sec                  
            92,079      branch-misses             #    0.05% of all branches        

       0.847353382 seconds time elapsed


real	0m0.843s
user	0m0.843s
sys	0m0.000s

 Performance counter stats for './__run 10':

        845.307847      task-clock (msec)         #    1.000 CPUs utilized          
                 1      context-switches          #    0.001 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               254      page-faults               #    0.300 K/sec                  
     2,614,837,574      cycles                    #    3.093 GHz                    
     1,559,715,666      stalled-cycles-frontend   #   59.65% frontend cycles idle   
     3,724,131,509      instructions              #    1.42  insn per cycle         
                                                  #    0.42  stalled cycles per insn
       191,689,956      branches                  #  226.769 M/sec                  
            66,402      branch-misses             #    0.03% of all branches        

       0.845668954 seconds time elapsed


real	0m0.891s
user	0m0.843s
sys	0m0.048s

 Performance counter stats for './__run 10':

        892.851861      task-clock (msec)         #    1.000 CPUs utilized          
                 6      context-switches          #    0.007 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
               253      page-faults               #    0.283 K/sec                  
     2,761,472,039      cycles                    #    3.093 GHz                    
     1,689,097,913      stalled-cycles-frontend   #   61.17% frontend cycles idle   
     3,743,941,056      instructions              #    1.36  insn per cycle         
                                                  #    0.45  stalled cycles per insn
       195,003,423      branches                  #  218.405 M/sec                  
            69,289      branch-misses             #    0.04% of all branches        

       0.893149308 seconds time elapsed

