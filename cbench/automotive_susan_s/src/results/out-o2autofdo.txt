
real	0m3.653s
user	0m3.617s
sys	0m0.032s

 Performance counter stats for './__run 1':

       3652.670133      task-clock (msec)         #    0.999 CPUs utilized          
               134      context-switches          #    0.037 K/sec                  
                 2      cpu-migrations            #    0.001 K/sec                  
             3,833      page-faults               #    0.001 M/sec                  
    11,661,459,827      cycles                    #    3.193 GHz                    
     2,368,436,377      stalled-cycles-frontend   #   20.31% frontend cycles idle   
    37,355,707,336      instructions              #    3.20  insn per cycle         
                                                  #    0.06  stalled cycles per insn
     1,851,097,335      branches                  #  506.779 M/sec                  
           522,351      branch-misses             #    0.03% of all branches        

       3.656107160 seconds time elapsed


real	0m3.677s
user	0m3.633s
sys	0m0.040s

 Performance counter stats for './__run 1':

       3675.690401      task-clock (msec)         #    0.999 CPUs utilized          
               109      context-switches          #    0.030 K/sec                  
                 2      cpu-migrations            #    0.001 K/sec                  
             3,830      page-faults               #    0.001 M/sec                  
    11,755,508,357      cycles                    #    3.198 GHz                    
     2,447,114,021      stalled-cycles-frontend   #   20.82% frontend cycles idle   
    37,354,895,094      instructions              #    3.18  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     1,850,953,161      branches                  #  503.566 M/sec                  
           517,371      branch-misses             #    0.03% of all branches        

       3.678516068 seconds time elapsed


real	0m3.690s
user	0m3.664s
sys	0m0.024s

 Performance counter stats for './__run 1':

       3690.265855      task-clock (msec)         #    0.999 CPUs utilized          
                49      context-switches          #    0.013 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
             3,825      page-faults               #    0.001 M/sec                  
    11,809,377,698      cycles                    #    3.200 GHz                    
     2,491,396,883      stalled-cycles-frontend   #   21.10% frontend cycles idle   
    37,354,991,289      instructions              #    3.16  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     1,850,937,472      branches                  #  501.573 M/sec                  
           556,281      branch-misses             #    0.03% of all branches        

       3.692426790 seconds time elapsed


real	0m3.011s
user	0m2.975s
sys	0m0.036s

 Performance counter stats for './__run 2':

       3012.852512      task-clock (msec)         #    1.000 CPUs utilized          
                 8      context-switches          #    0.003 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
             3,271      page-faults               #    0.001 M/sec                  
     9,622,118,926      cycles                    #    3.194 GHz                    
     1,862,062,619      stalled-cycles-frontend   #   19.35% frontend cycles idle   
    31,256,024,718      instructions              #    3.25  insn per cycle         
                                                  #    0.06  stalled cycles per insn
     1,548,730,397      branches                  #  514.041 M/sec                  
           420,888      branch-misses             #    0.03% of all branches        

       3.013406413 seconds time elapsed


real	0m3.005s
user	0m2.969s
sys	0m0.032s

 Performance counter stats for './__run 2':

       3004.016957      task-clock (msec)         #    0.999 CPUs utilized          
               128      context-switches          #    0.043 K/sec                  
                 4      cpu-migrations            #    0.001 K/sec                  
             3,269      page-faults               #    0.001 M/sec                  
     9,607,088,050      cycles                    #    3.198 GHz                    
     1,848,660,583      stalled-cycles-frontend   #   19.24% frontend cycles idle   
    31,257,802,144      instructions              #    3.25  insn per cycle         
                                                  #    0.06  stalled cycles per insn
     1,549,066,662      branches                  #  515.665 M/sec                  
           457,689      branch-misses             #    0.03% of all branches        

       3.007203267 seconds time elapsed


real	0m3.083s
user	0m3.049s
sys	0m0.032s

 Performance counter stats for './__run 2':

       3084.248600      task-clock (msec)         #    1.000 CPUs utilized          
                61      context-switches          #    0.020 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
             3,270      page-faults               #    0.001 M/sec                  
     9,874,468,489      cycles                    #    3.202 GHz                    
     2,082,992,710      stalled-cycles-frontend   #   21.09% frontend cycles idle   
    31,257,502,204      instructions              #    3.17  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     1,549,006,227      branches                  #  502.231 M/sec                  
           467,531      branch-misses             #    0.03% of all branches        

       3.085746404 seconds time elapsed


real	0m1.309s
user	0m1.288s
sys	0m0.020s

 Performance counter stats for './__run 3':

       1310.648212      task-clock (msec)         #    0.999 CPUs utilized          
                38      context-switches          #    0.029 K/sec                  
                 3      cpu-migrations            #    0.002 K/sec                  
             1,660      page-faults               #    0.001 M/sec                  
     4,208,546,934      cycles                    #    3.211 GHz                    
       807,294,341      stalled-cycles-frontend   #   19.18% frontend cycles idle   
    13,726,179,245      instructions              #    3.26  insn per cycle         
                                                  #    0.06  stalled cycles per insn
       680,730,146      branches                  #  519.384 M/sec                  
           221,359      branch-misses             #    0.03% of all branches        

       1.312047063 seconds time elapsed


real	0m1.309s
user	0m1.295s
sys	0m0.012s

 Performance counter stats for './__run 3':

       1308.559321      task-clock (msec)         #    0.998 CPUs utilized          
                14      context-switches          #    0.011 K/sec                  
                 3      cpu-migrations            #    0.002 K/sec                  
             1,660      page-faults               #    0.001 M/sec                  
     4,205,764,262      cycles                    #    3.214 GHz                    
       801,254,458      stalled-cycles-frontend   #   19.05% frontend cycles idle   
    13,725,158,285      instructions              #    3.26  insn per cycle         
                                                  #    0.06  stalled cycles per insn
       680,545,635      branches                  #  520.072 M/sec                  
           205,103      branch-misses             #    0.03% of all branches        

       1.311440005 seconds time elapsed


real	0m1.354s
user	0m1.346s
sys	0m0.008s

 Performance counter stats for './__run 3':

       1356.625620      task-clock (msec)         #    1.000 CPUs utilized          
                 4      context-switches          #    0.003 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
             1,659      page-faults               #    0.001 M/sec                  
     4,328,410,898      cycles                    #    3.191 GHz                    
       903,672,639      stalled-cycles-frontend   #   20.88% frontend cycles idle   
    13,725,502,288      instructions              #    3.17  insn per cycle         
                                                  #    0.07  stalled cycles per insn
       680,600,760      branches                  #  501.687 M/sec                  
           213,233      branch-misses             #    0.03% of all branches        

       1.356968083 seconds time elapsed


real	0m14.802s
user	0m14.634s
sys	0m0.160s

 Performance counter stats for './__run 4':

      14797.068338      task-clock (msec)         #    1.000 CPUs utilized          
               270      context-switches          #    0.018 K/sec                  
                 5      cpu-migrations            #    0.000 K/sec                  
            14,189      page-faults               #    0.959 K/sec                  
    47,360,359,325      cycles                    #    3.201 GHz                    
     9,943,692,150      stalled-cycles-frontend   #   21.00% frontend cycles idle   
   150,184,834,408      instructions              #    3.17  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     7,441,498,037      branches                  #  502.904 M/sec                  
         1,841,591      branch-misses             #    0.02% of all branches        

      14.804352133 seconds time elapsed


real	0m14.618s
user	0m14.500s
sys	0m0.112s

 Performance counter stats for './__run 4':

      14615.186051      task-clock (msec)         #    0.999 CPUs utilized          
               297      context-switches          #    0.020 K/sec                  
                 6      cpu-migrations            #    0.000 K/sec                  
            14,192      page-faults               #    0.971 K/sec                  
    46,823,130,237      cycles                    #    3.204 GHz                    
     9,480,499,911      stalled-cycles-frontend   #   20.25% frontend cycles idle   
   150,170,513,162      instructions              #    3.21  insn per cycle         
                                                  #    0.06  stalled cycles per insn
     7,439,287,960      branches                  #  509.011 M/sec                  
         1,928,517      branch-misses             #    0.03% of all branches        

      14.623664165 seconds time elapsed


real	0m14.733s
user	0m14.561s
sys	0m0.160s

 Performance counter stats for './__run 4':

      14725.244523      task-clock (msec)         #    0.999 CPUs utilized          
               441      context-switches          #    0.030 K/sec                  
                 5      cpu-migrations            #    0.000 K/sec                  
            14,188      page-faults               #    0.964 K/sec                  
    47,138,561,143      cycles                    #    3.201 GHz                    
     9,762,344,739      stalled-cycles-frontend   #   20.71% frontend cycles idle   
   150,168,761,495      instructions              #    3.19  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     7,438,905,255      branches                  #  505.180 M/sec                  
         1,948,984      branch-misses             #    0.03% of all branches        

      14.736511579 seconds time elapsed


real	0m1.337s
user	0m1.328s
sys	0m0.008s

 Performance counter stats for './__run 5':

       1338.647438      task-clock (msec)         #    0.999 CPUs utilized          
                 9      context-switches          #    0.007 K/sec                  
                 2      cpu-migrations            #    0.001 K/sec                  
             1,664      page-faults               #    0.001 M/sec                  
     4,269,358,183      cycles                    #    3.189 GHz                    
       856,407,088      stalled-cycles-frontend   #   20.06% frontend cycles idle   
    13,725,512,884      instructions              #    3.21  insn per cycle         
                                                  #    0.06  stalled cycles per insn
       680,620,761      branches                  #  508.439 M/sec                  
           208,344      branch-misses             #    0.03% of all branches        

       1.340013001 seconds time elapsed


real	0m1.375s
user	0m1.356s
sys	0m0.016s

 Performance counter stats for './__run 5':

       1374.556456      task-clock (msec)         #    0.998 CPUs utilized          
                26      context-switches          #    0.019 K/sec                  
                 3      cpu-migrations            #    0.002 K/sec                  
             1,662      page-faults               #    0.001 M/sec                  
     4,407,096,273      cycles                    #    3.206 GHz                    
       975,418,154      stalled-cycles-frontend   #   22.13% frontend cycles idle   
    13,726,853,357      instructions              #    3.11  insn per cycle         
                                                  #    0.07  stalled cycles per insn
       680,871,812      branches                  #  495.339 M/sec                  
           224,445      branch-misses             #    0.03% of all branches        

       1.377497721 seconds time elapsed


real	0m1.360s
user	0m1.347s
sys	0m0.012s

 Performance counter stats for './__run 5':

       1361.206236      task-clock (msec)         #    0.999 CPUs utilized          
                 6      context-switches          #    0.004 K/sec                  
                 3      cpu-migrations            #    0.002 K/sec                  
             1,661      page-faults               #    0.001 M/sec                  
     4,353,803,175      cycles                    #    3.198 GHz                    
       929,386,522      stalled-cycles-frontend   #   21.35% frontend cycles idle   
    13,725,904,532      instructions              #    3.15  insn per cycle         
                                                  #    0.07  stalled cycles per insn
       680,680,266      branches                  #  500.057 M/sec                  
           216,062      branch-misses             #    0.03% of all branches        

       1.362376354 seconds time elapsed


real	0m1.333s
user	0m1.320s
sys	0m0.012s

 Performance counter stats for './__run 6':

       1334.473238      task-clock (msec)         #    0.999 CPUs utilized          
                32      context-switches          #    0.024 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
             1,661      page-faults               #    0.001 M/sec                  
     4,251,100,083      cycles                    #    3.186 GHz                    
       840,433,326      stalled-cycles-frontend   #   19.77% frontend cycles idle   
    13,727,050,879      instructions              #    3.23  insn per cycle         
                                                  #    0.06  stalled cycles per insn
       680,913,137      branches                  #  510.249 M/sec                  
           231,481      branch-misses             #    0.03% of all branches        

       1.335587381 seconds time elapsed


real	0m1.377s
user	0m1.365s
sys	0m0.012s

 Performance counter stats for './__run 6':

       1379.389294      task-clock (msec)         #    0.999 CPUs utilized          
                22      context-switches          #    0.016 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,663      page-faults               #    0.001 M/sec                  
     4,415,760,942      cycles                    #    3.201 GHz                    
       983,655,515      stalled-cycles-frontend   #   22.28% frontend cycles idle   
    13,726,823,875      instructions              #    3.11  insn per cycle         
                                                  #    0.07  stalled cycles per insn
       680,865,628      branches                  #  493.599 M/sec                  
           230,599      branch-misses             #    0.03% of all branches        

       1.380113958 seconds time elapsed


real	0m1.313s
user	0m1.304s
sys	0m0.008s

 Performance counter stats for './__run 6':

       1314.529856      task-clock (msec)         #    0.999 CPUs utilized          
                52      context-switches          #    0.040 K/sec                  
                 1      cpu-migrations            #    0.001 K/sec                  
             1,659      page-faults               #    0.001 M/sec                  
     4,178,330,446      cycles                    #    3.179 GHz                    
       780,882,152      stalled-cycles-frontend   #   18.69% frontend cycles idle   
    13,726,800,191      instructions              #    3.29  insn per cycle         
                                                  #    0.06  stalled cycles per insn
       680,866,933      branches                  #  517.955 M/sec                  
           213,693      branch-misses             #    0.03% of all branches        

       1.316124296 seconds time elapsed


real	0m5.228s
user	0m5.179s
sys	0m0.044s

 Performance counter stats for './__run 7':

       5226.604837      task-clock (msec)         #    0.999 CPUs utilized          
               223      context-switches          #    0.043 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             5,297      page-faults               #    0.001 M/sec                  
    16,741,992,442      cycles                    #    3.203 GHz                    
     3,456,051,320      stalled-cycles-frontend   #   20.64% frontend cycles idle   
    53,364,590,034      instructions              #    3.19  insn per cycle         
                                                  #    0.06  stalled cycles per insn
     2,644,304,783      branches                  #  505.932 M/sec                  
           754,048      branch-misses             #    0.03% of all branches        

       5.230874335 seconds time elapsed


real	0m5.225s
user	0m5.180s
sys	0m0.044s

 Performance counter stats for './__run 7':

       5226.501349      task-clock (msec)         #    1.000 CPUs utilized          
                26      context-switches          #    0.005 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
             5,298      page-faults               #    0.001 M/sec                  
    16,725,782,619      cycles                    #    3.200 GHz                    
     3,436,561,376      stalled-cycles-frontend   #   20.55% frontend cycles idle   
    53,362,388,750      instructions              #    3.19  insn per cycle         
                                                  #    0.06  stalled cycles per insn
     2,643,843,997      branches                  #  505.853 M/sec                  
           713,678      branch-misses             #    0.03% of all branches        

       5.227776297 seconds time elapsed


real	0m5.405s
user	0m5.353s
sys	0m0.048s

 Performance counter stats for './__run 7':

       5405.067177      task-clock (msec)         #    0.999 CPUs utilized          
               140      context-switches          #    0.026 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
             5,303      page-faults               #    0.981 K/sec                  
    17,282,271,784      cycles                    #    3.197 GHz                    
     3,933,819,486      stalled-cycles-frontend   #   22.76% frontend cycles idle   
    53,373,812,123      instructions              #    3.09  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     2,645,812,122      branches                  #  489.506 M/sec                  
           757,097      branch-misses             #    0.03% of all branches        

       5.408556759 seconds time elapsed


real	0m22.517s
user	0m22.351s
sys	0m0.164s

 Performance counter stats for './__run 8':

      22517.943510      task-clock (msec)         #    1.000 CPUs utilized          
                44      context-switches          #    0.002 K/sec                  
                 4      cpu-migrations            #    0.000 K/sec                  
            21,684      page-faults               #    0.963 K/sec                  
    72,232,816,434      cycles                    #    3.208 GHz                    
    14,604,512,112      stalled-cycles-frontend   #   20.22% frontend cycles idle   
   231,710,586,759      instructions              #    3.21  insn per cycle         
                                                  #    0.06  stalled cycles per insn
    11,475,271,701      branches                  #  509.606 M/sec                  
         2,787,525      branch-misses             #    0.02% of all branches        

      22.520551481 seconds time elapsed


real	0m22.609s
user	0m22.403s
sys	0m0.192s

 Performance counter stats for './__run 8':

      22599.255300      task-clock (msec)         #    0.999 CPUs utilized          
               542      context-switches          #    0.024 K/sec                  
                 4      cpu-migrations            #    0.000 K/sec                  
            21,685      page-faults               #    0.960 K/sec                  
    72,376,666,189      cycles                    #    3.203 GHz                    
    14,740,914,331      stalled-cycles-frontend   #   20.37% frontend cycles idle   
   231,733,361,213      instructions              #    3.20  insn per cycle         
                                                  #    0.06  stalled cycles per insn
    11,479,589,810      branches                  #  507.963 M/sec                  
         3,018,498      branch-misses             #    0.03% of all branches        

      22.612650255 seconds time elapsed


real	0m22.683s
user	0m22.508s
sys	0m0.168s

 Performance counter stats for './__run 8':

      22678.829011      task-clock (msec)         #    1.000 CPUs utilized          
               297      context-switches          #    0.013 K/sec                  
                 4      cpu-migrations            #    0.000 K/sec                  
            21,681      page-faults               #    0.956 K/sec                  
    72,588,905,953      cycles                    #    3.201 GHz                    
    14,924,519,423      stalled-cycles-frontend   #   20.56% frontend cycles idle   
   231,727,298,532      instructions              #    3.19  insn per cycle         
                                                  #    0.06  stalled cycles per insn
    11,478,272,769      branches                  #  506.123 M/sec                  
         2,921,438      branch-misses             #    0.03% of all branches        

      22.686001602 seconds time elapsed


real	0m0.652s
user	0m0.643s
sys	0m0.008s

 Performance counter stats for './__run 9':

        654.201345      task-clock (msec)         #    0.998 CPUs utilized          
                18      context-switches          #    0.028 K/sec                  
                 1      cpu-migrations            #    0.002 K/sec                  
             1,029      page-faults               #    0.002 M/sec                  
     2,071,735,047      cycles                    #    3.167 GHz                    
       373,278,034      stalled-cycles-frontend   #   18.02% frontend cycles idle   
     6,867,033,754      instructions              #    3.31  insn per cycle         
                                                  #    0.05  stalled cycles per insn
       341,170,277      branches                  #  521.507 M/sec                  
           138,796      branch-misses             #    0.04% of all branches        

       0.655221712 seconds time elapsed


real	0m0.717s
user	0m0.716s
sys	0m0.000s

 Performance counter stats for './__run 9':

        718.679634      task-clock (msec)         #    0.997 CPUs utilized          
                35      context-switches          #    0.049 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,029      page-faults               #    0.001 M/sec                  
     2,290,793,924      cycles                    #    3.188 GHz                    
       563,361,088      stalled-cycles-frontend   #   24.59% frontend cycles idle   
     6,866,635,129      instructions              #    3.00  insn per cycle         
                                                  #    0.08  stalled cycles per insn
       341,097,798      branches                  #  474.617 M/sec                  
           148,821      branch-misses             #    0.04% of all branches        

       0.720944013 seconds time elapsed


real	0m0.671s
user	0m0.661s
sys	0m0.008s

 Performance counter stats for './__run 9':

        671.685614      task-clock (msec)         #    0.996 CPUs utilized          
                42      context-switches          #    0.063 K/sec                  
                 2      cpu-migrations            #    0.003 K/sec                  
             1,034      page-faults               #    0.002 M/sec                  
     2,135,614,138      cycles                    #    3.179 GHz                    
       428,859,797      stalled-cycles-frontend   #   20.08% frontend cycles idle   
     6,866,598,181      instructions              #    3.22  insn per cycle         
                                                  #    0.06  stalled cycles per insn
       341,096,206      branches                  #  507.821 M/sec                  
           136,719      branch-misses             #    0.04% of all branches        

       0.674424645 seconds time elapsed


real	0m11.341s
user	0m11.235s
sys	0m0.096s

 Performance counter stats for './__run 10':

      11333.915751      task-clock (msec)         #    0.999 CPUs utilized          
               220      context-switches          #    0.019 K/sec                  
                 4      cpu-migrations            #    0.000 K/sec                  
            11,112      page-faults               #    0.980 K/sec                  
    36,324,392,995      cycles                    #    3.205 GHz                    
     7,323,927,171      stalled-cycles-frontend   #   20.16% frontend cycles idle   
   116,631,634,343      instructions              #    3.21  insn per cycle         
                                                  #    0.06  stalled cycles per insn
     5,778,033,432      branches                  #  509.800 M/sec                  
         1,559,316      branch-misses             #    0.03% of all branches        

      11.343201186 seconds time elapsed


real	0m11.484s
user	0m11.345s
sys	0m0.136s

 Performance counter stats for './__run 10':

      11485.537266      task-clock (msec)         #    1.000 CPUs utilized          
               194      context-switches          #    0.017 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
            11,112      page-faults               #    0.967 K/sec                  
    36,775,412,971      cycles                    #    3.202 GHz                    
     7,734,964,659      stalled-cycles-frontend   #   21.03% frontend cycles idle   
   116,631,967,792      instructions              #    3.17  insn per cycle         
                                                  #    0.07  stalled cycles per insn
     5,778,113,159      branches                  #  503.077 M/sec                  
         1,498,435      branch-misses             #    0.03% of all branches        

      11.488727288 seconds time elapsed


real	0m11.531s
user	0m11.168s
sys	0m0.104s

 Performance counter stats for './__run 10':

      11274.775843      task-clock (msec)         #    0.978 CPUs utilized          
               193      context-switches          #    0.017 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
            11,107      page-faults               #    0.985 K/sec                  
    36,120,847,919      cycles                    #    3.204 GHz                    
     7,154,729,081      stalled-cycles-frontend   #   19.81% frontend cycles idle   
   116,631,269,236      instructions              #    3.23  insn per cycle         
                                                  #    0.06  stalled cycles per insn
     5,777,988,708      branches                  #  512.470 M/sec                  
         1,515,729      branch-misses             #    0.03% of all branches        

      11.533634594 seconds time elapsed

