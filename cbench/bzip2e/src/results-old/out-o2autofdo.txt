
real	0m5.340s
user	0m4.843s
sys	0m0.144s

 Performance counter stats for './__run 1':

       4988.558661      task-clock (msec)         #    0.934 CPUs utilized          
                66      context-switches          #    0.013 K/sec                  
                 4      cpu-migrations            #    0.001 K/sec                  
            63,178      page-faults               #    0.013 M/sec                  
    15,439,507,443      cycles                    #    3.095 GHz                    
     6,825,114,324      stalled-cycles-frontend   #   44.21% frontend cycles idle   
    15,579,728,362      instructions              #    1.01  insn per cycle         
                                                  #    0.44  stalled cycles per insn
     2,096,609,303      branches                  #  420.284 M/sec                  
       198,147,572      branch-misses             #    9.45% of all branches        

       5.342431878 seconds time elapsed


real	0m5.045s
user	0m4.744s
sys	0m0.108s

 Performance counter stats for './__run 1':

       4853.653810      task-clock (msec)         #    0.962 CPUs utilized          
                93      context-switches          #    0.019 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            63,173      page-faults               #    0.013 M/sec                  
    15,005,656,392      cycles                    #    3.092 GHz                    
     6,345,128,697      stalled-cycles-frontend   #   42.28% frontend cycles idle   
    15,532,282,977      instructions              #    1.04  insn per cycle         
                                                  #    0.41  stalled cycles per insn
     2,088,333,180      branches                  #  430.260 M/sec                  
       197,893,342      branch-misses             #    9.48% of all branches        

       5.047717384 seconds time elapsed


real	0m5.097s
user	0m4.807s
sys	0m0.100s

 Performance counter stats for './__run 1':

       4909.160851      task-clock (msec)         #    0.963 CPUs utilized          
               116      context-switches          #    0.024 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            63,179      page-faults               #    0.013 M/sec                  
    15,176,969,300      cycles                    #    3.092 GHz                    
     6,537,822,010      stalled-cycles-frontend   #   43.08% frontend cycles idle   
    15,532,938,013      instructions              #    1.02  insn per cycle         
                                                  #    0.42  stalled cycles per insn
     2,088,392,114      branches                  #  425.407 M/sec                  
       198,024,456      branch-misses             #    9.48% of all branches        

       5.100120033 seconds time elapsed


real	0m45.025s
user	0m42.148s
sys	0m1.012s

 Performance counter stats for './__run 2':

      43162.490806      task-clock (msec)         #    0.959 CPUs utilized          
               695      context-switches          #    0.016 K/sec                  
                 9      cpu-migrations            #    0.000 K/sec                  
           550,190      page-faults               #    0.013 M/sec                  
   133,405,778,435      cycles                    #    3.091 GHz                    
    57,896,092,533      stalled-cycles-frontend   #   43.40% frontend cycles idle   
   135,792,103,381      instructions              #    1.02  insn per cycle         
                                                  #    0.43  stalled cycles per insn
    18,247,644,391      branches                  #  422.766 M/sec                  
     1,732,381,217      branch-misses             #    9.49% of all branches        

      45.026822726 seconds time elapsed


real	0m46.395s
user	0m41.444s
sys	0m1.048s

 Performance counter stats for './__run 2':

      42493.261035      task-clock (msec)         #    0.916 CPUs utilized          
               699      context-switches          #    0.016 K/sec                  
                 7      cpu-migrations            #    0.000 K/sec                  
           550,186      page-faults               #    0.013 M/sec                  
   131,399,721,610      cycles                    #    3.092 GHz                    
    55,876,141,629      stalled-cycles-frontend   #   42.52% frontend cycles idle   
   135,793,385,750      instructions              #    1.03  insn per cycle         
                                                  #    0.41  stalled cycles per insn
    18,249,616,987      branches                  #  429.471 M/sec                  
     1,732,440,892      branch-misses             #    9.49% of all branches        

      46.397111980 seconds time elapsed


real	0m43.363s
user	0m41.578s
sys	0m0.984s

 Performance counter stats for './__run 2':

      42564.507543      task-clock (msec)         #    0.982 CPUs utilized          
               441      context-switches          #    0.010 K/sec                  
                 6      cpu-migrations            #    0.000 K/sec                  
           550,186      page-faults               #    0.013 M/sec                  
   131,611,203,582      cycles                    #    3.092 GHz                    
    56,118,301,899      stalled-cycles-frontend   #   42.64% frontend cycles idle   
   135,840,750,035      instructions              #    1.03  insn per cycle         
                                                  #    0.41  stalled cycles per insn
    18,258,735,320      branches                  #  428.966 M/sec                  
     1,732,371,243      branch-misses             #    9.49% of all branches        

      43.364589032 seconds time elapsed


real	0m2.933s
user	0m2.236s
sys	0m0.144s

 Performance counter stats for './__run 3':

       2379.802146      task-clock (msec)         #    0.811 CPUs utilized          
                96      context-switches          #    0.040 K/sec                  
                 5      cpu-migrations            #    0.002 K/sec                  
            47,743      page-faults               #    0.020 M/sec                  
     7,355,478,346      cycles                    #    3.091 GHz                    
     3,084,939,948      stalled-cycles-frontend   #   41.94% frontend cycles idle   
     8,655,099,132      instructions              #    1.18  insn per cycle         
                                                  #    0.36  stalled cycles per insn
     1,164,174,255      branches                  #  489.190 M/sec                  
        97,730,285      branch-misses             #    8.39% of all branches        

       2.935411343 seconds time elapsed


real	0m2.331s
user	0m2.142s
sys	0m0.100s

 Performance counter stats for './__run 3':

       2244.428249      task-clock (msec)         #    0.962 CPUs utilized          
                78      context-switches          #    0.035 K/sec                  
                 2      cpu-migrations            #    0.001 K/sec                  
            47,744      page-faults               #    0.021 M/sec                  
     6,940,765,067      cycles                    #    3.092 GHz                    
     2,704,821,782      stalled-cycles-frontend   #   38.97% frontend cycles idle   
     8,555,464,806      instructions              #    1.23  insn per cycle         
                                                  #    0.32  stalled cycles per insn
     1,146,581,290      branches                  #  510.857 M/sec                  
        97,484,719      branch-misses             #    8.50% of all branches        

       2.333632451 seconds time elapsed


real	0m2.366s
user	0m2.177s
sys	0m0.108s

 Performance counter stats for './__run 3':

       2286.528683      task-clock (msec)         #    0.966 CPUs utilized          
                30      context-switches          #    0.013 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            47,743      page-faults               #    0.021 M/sec                  
     7,071,803,047      cycles                    #    3.093 GHz                    
     2,847,502,932      stalled-cycles-frontend   #   40.27% frontend cycles idle   
     8,554,269,704      instructions              #    1.21  insn per cycle         
                                                  #    0.33  stalled cycles per insn
     1,146,139,835      branches                  #  501.258 M/sec                  
        97,479,627      branch-misses             #    8.51% of all branches        

       2.368046596 seconds time elapsed


real	0m22.602s
user	0m21.653s
sys	0m0.861s

 Performance counter stats for './__run 4':

      22517.704484      task-clock (msec)         #    0.996 CPUs utilized          
               276      context-switches          #    0.012 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
           431,333      page-faults               #    0.019 M/sec                  
    71,563,243,656      cycles                    #    3.178 GHz                    
    36,497,791,937      stalled-cycles-frontend   #   51.00% frontend cycles idle   
    77,681,094,617      instructions              #    1.09  insn per cycle         
                                                  #    0.47  stalled cycles per insn
    10,405,056,983      branches                  #  462.083 M/sec                  
       885,527,693      branch-misses             #    8.51% of all branches        

      22.604442235 seconds time elapsed


real	0m21.022s
user	0m19.763s
sys	0m0.716s

 Performance counter stats for './__run 4':

      20479.019351      task-clock (msec)         #    0.974 CPUs utilized          
               653      context-switches          #    0.032 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
           431,329      page-faults               #    0.021 M/sec                  
    63,329,371,819      cycles                    #    3.092 GHz                    
    24,891,550,347      stalled-cycles-frontend   #   39.30% frontend cycles idle   
    77,729,209,841      instructions              #    1.23  insn per cycle         
                                                  #    0.32  stalled cycles per insn
    10,413,969,181      branches                  #  508.519 M/sec                  
       885,728,575      branch-misses             #    8.51% of all branches        

      21.024809965 seconds time elapsed


real	0m23.889s
user	0m20.356s
sys	0m0.856s

 Performance counter stats for './__run 4':

      21215.235325      task-clock (msec)         #    0.888 CPUs utilized          
               513      context-switches          #    0.024 K/sec                  
                11      cpu-migrations            #    0.001 K/sec                  
           431,332      page-faults               #    0.020 M/sec                  
    66,056,615,892      cycles                    #    3.114 GHz                    
    28,680,515,634      stalled-cycles-frontend   #   43.42% frontend cycles idle   
    77,777,950,053      instructions              #    1.18  insn per cycle         
                                                  #    0.37  stalled cycles per insn
    10,420,635,006      branches                  #  491.186 M/sec                  
       886,034,656      branch-misses             #    8.50% of all branches        

      23.891690536 seconds time elapsed


real	0m5.806s
user	0m4.603s
sys	0m0.140s

 Performance counter stats for './__run 5':

       4744.905069      task-clock (msec)         #    0.817 CPUs utilized          
                63      context-switches          #    0.013 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
            51,921      page-faults               #    0.011 M/sec                  
    15,260,704,101      cycles                    #    3.216 GHz                    
     8,583,334,442      stalled-cycles-frontend   #   56.24% frontend cycles idle   
    19,606,748,753      instructions              #    1.28  insn per cycle         
                                                  #    0.44  stalled cycles per insn
     2,951,149,799      branches                  #  621.962 M/sec                  
        72,103,184      branch-misses             #    2.44% of all branches        

       5.808104744 seconds time elapsed


real	0m3.594s
user	0m3.309s
sys	0m0.136s

 Performance counter stats for './__run 5':

       3446.138305      task-clock (msec)         #    0.958 CPUs utilized          
                99      context-switches          #    0.029 K/sec                  
                 2      cpu-migrations            #    0.001 K/sec                  
            51,920      page-faults               #    0.015 M/sec                  
    10,657,287,886      cycles                    #    3.093 GHz                    
     3,248,887,585      stalled-cycles-frontend   #   30.49% frontend cycles idle   
    19,558,609,361      instructions              #    1.84  insn per cycle         
                                                  #    0.17  stalled cycles per insn
     2,942,526,457      branches                  #  853.862 M/sec                  
        72,485,176      branch-misses             #    2.46% of all branches        

       3.596671837 seconds time elapsed


real	0m4.889s
user	0m4.509s
sys	0m0.216s

 Performance counter stats for './__run 5':

       4726.427948      task-clock (msec)         #    0.966 CPUs utilized          
                19      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            51,921      page-faults               #    0.011 M/sec                  
    15,203,526,506      cycles                    #    3.217 GHz                    
     8,556,170,295      stalled-cycles-frontend   #   56.28% frontend cycles idle   
    19,557,074,907      instructions              #    1.29  insn per cycle         
                                                  #    0.44  stalled cycles per insn
     2,942,495,955      branches                  #  622.562 M/sec                  
        71,896,362      branch-misses             #    2.44% of all branches        

       4.891081771 seconds time elapsed


real	0m3.929s
user	0m3.688s
sys	0m0.136s

 Performance counter stats for './__run 6':

       3825.897648      task-clock (msec)         #    0.973 CPUs utilized          
                14      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            83,582      page-faults               #    0.022 M/sec                  
    11,833,179,741      cycles                    #    3.093 GHz                    
     4,398,557,998      stalled-cycles-frontend   #   37.17% frontend cycles idle   
    16,992,641,753      instructions              #    1.44  insn per cycle         
                                                  #    0.26  stalled cycles per insn
     2,477,957,139      branches                  #  647.680 M/sec                  
       137,743,540      branch-misses             #    5.56% of all branches        

       3.930484988 seconds time elapsed


real	0m4.505s
user	0m3.684s
sys	0m0.148s

 Performance counter stats for './__run 6':

       3834.311190      task-clock (msec)         #    0.851 CPUs utilized          
                83      context-switches          #    0.022 K/sec                  
                 3      cpu-migrations            #    0.001 K/sec                  
            83,577      page-faults               #    0.022 M/sec                  
    11,855,175,035      cycles                    #    3.092 GHz                    
     4,416,433,238      stalled-cycles-frontend   #   37.25% frontend cycles idle   
    16,991,184,080      instructions              #    1.43  insn per cycle         
                                                  #    0.26  stalled cycles per insn
     2,477,919,767      branches                  #  646.249 M/sec                  
       137,829,280      branch-misses             #    5.56% of all branches        

       4.507985479 seconds time elapsed


real	0m4.816s
user	0m4.495s
sys	0m0.216s

 Performance counter stats for './__run 6':

       4712.495708      task-clock (msec)         #    0.978 CPUs utilized          
                15      context-switches          #    0.003 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            83,581      page-faults               #    0.018 M/sec                  
    15,032,216,108      cycles                    #    3.190 GHz                    
     8,479,402,316      stalled-cycles-frontend   #   56.41% frontend cycles idle   
    17,011,654,766      instructions              #    1.13  insn per cycle         
                                                  #    0.50  stalled cycles per insn
     2,480,754,953      branches                  #  526.421 M/sec                  
       138,390,682      branch-misses             #    5.58% of all branches        

       4.818253868 seconds time elapsed


real	0m0.242s
user	0m0.118s
sys	0m0.004s

 Performance counter stats for './__run 7':

        123.199967      task-clock (msec)         #    0.504 CPUs utilized          
                 7      context-switches          #    0.057 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             2,041      page-faults               #    0.017 M/sec                  
       380,970,161      cycles                    #    3.092 GHz                    
       130,746,665      stalled-cycles-frontend   #   34.32% frontend cycles idle   
       664,526,099      instructions              #    1.74  insn per cycle         
                                                  #    0.20  stalled cycles per insn
       100,466,517      branches                  #  815.475 M/sec                  
         2,478,682      branch-misses             #    2.47% of all branches        

       0.244258032 seconds time elapsed


real	0m0.125s
user	0m0.121s
sys	0m0.004s

 Performance counter stats for './__run 7':

        127.193179      task-clock (msec)         #    0.998 CPUs utilized          
                 3      context-switches          #    0.024 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             2,044      page-faults               #    0.016 M/sec                  
       393,349,751      cycles                    #    3.093 GHz                    
       145,057,999      stalled-cycles-frontend   #   36.88% frontend cycles idle   
       658,801,009      instructions              #    1.67  insn per cycle         
                                                  #    0.22  stalled cycles per insn
        99,423,055      branches                  #  781.670 M/sec                  
         2,474,051      branch-misses             #    2.49% of all branches        

       0.127440172 seconds time elapsed


real	0m0.163s
user	0m0.154s
sys	0m0.008s

 Performance counter stats for './__run 7':

        164.525196      task-clock (msec)         #    0.990 CPUs utilized          
                24      context-switches          #    0.146 K/sec                  
                 2      cpu-migrations            #    0.012 K/sec                  
             2,041      page-faults               #    0.012 M/sec                  
       524,258,185      cycles                    #    3.186 GHz                    
       299,321,382      stalled-cycles-frontend   #   57.09% frontend cycles idle   
       658,128,411      instructions              #    1.26  insn per cycle         
                                                  #    0.45  stalled cycles per insn
        99,322,264      branches                  #  603.690 M/sec                  
         2,461,276      branch-misses             #    2.48% of all branches        

       0.166139850 seconds time elapsed


real	0m0.052s
user	0m0.052s
sys	0m0.000s

 Performance counter stats for './__run 8':

         53.864438      task-clock (msec)         #    0.983 CPUs utilized          
                 5      context-switches          #    0.093 K/sec                  
                 1      cpu-migrations            #    0.019 K/sec                  
             1,440      page-faults               #    0.027 M/sec                  
       166,618,182      cycles                    #    3.093 GHz                    
        64,496,116      stalled-cycles-frontend   #   38.71% frontend cycles idle   
       232,840,647      instructions              #    1.40  insn per cycle         
                                                  #    0.28  stalled cycles per insn
        34,296,269      branches                  #  636.715 M/sec                  
         1,885,617      branch-misses             #    5.50% of all branches        

       0.054793930 seconds time elapsed


real	0m0.063s
user	0m0.058s
sys	0m0.004s

 Performance counter stats for './__run 8':

         64.819978      task-clock (msec)         #    0.982 CPUs utilized          
                10      context-switches          #    0.154 K/sec                  
                 1      cpu-migrations            #    0.015 K/sec                  
             1,440      page-faults               #    0.022 M/sec                  
       205,080,971      cycles                    #    3.164 GHz                    
       116,543,037      stalled-cycles-frontend   #   56.83% frontend cycles idle   
       232,675,986      instructions              #    1.13  insn per cycle         
                                                  #    0.50  stalled cycles per insn
        34,266,112      branches                  #  528.635 M/sec                  
         1,894,230      branch-misses             #    5.53% of all branches        

       0.065998194 seconds time elapsed


real	0m0.069s
user	0m0.063s
sys	0m0.004s

 Performance counter stats for './__run 8':

         70.007474      task-clock (msec)         #    0.981 CPUs utilized          
                12      context-switches          #    0.171 K/sec                  
                 3      cpu-migrations            #    0.043 K/sec                  
             1,439      page-faults               #    0.021 M/sec                  
       224,758,661      cycles                    #    3.210 GHz                    
       139,632,593      stalled-cycles-frontend   #   62.13% frontend cycles idle   
       232,755,105      instructions              #    1.04  insn per cycle         
                                                  #    0.60  stalled cycles per insn
        34,279,819      branches                  #  489.659 M/sec                  
         1,891,797      branch-misses             #    5.52% of all branches        

       0.071368608 seconds time elapsed


real	0m4.672s
user	0m4.514s
sys	0m0.156s

 Performance counter stats for './__run 9':

       4671.603281      task-clock (msec)         #    0.999 CPUs utilized          
               120      context-switches          #    0.026 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            20,678      page-faults               #    0.004 M/sec                  
    14,701,670,361      cycles                    #    3.147 GHz                    
     6,030,310,675      stalled-cycles-frontend   #   41.02% frontend cycles idle   
    21,426,677,585      instructions              #    1.46  insn per cycle         
                                                  #    0.28  stalled cycles per insn
     3,255,661,569      branches                  #  696.905 M/sec                  
       169,982,194      branch-misses             #    5.22% of all branches        

       4.674267445 seconds time elapsed


real	0m4.242s
user	0m3.973s
sys	0m0.120s

 Performance counter stats for './__run 9':

       4094.971520      task-clock (msec)         #    0.965 CPUs utilized          
                43      context-switches          #    0.011 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            20,675      page-faults               #    0.005 M/sec                  
    12,927,274,457      cycles                    #    3.157 GHz                    
     3,843,065,716      stalled-cycles-frontend   #   29.73% frontend cycles idle   
    21,442,184,972      instructions              #    1.66  insn per cycle         
                                                  #    0.18  stalled cycles per insn
     3,258,623,976      branches                  #  795.762 M/sec                  
       170,258,055      branch-misses             #    5.22% of all branches        

       4.244861575 seconds time elapsed


real	0m5.107s
user	0m4.825s
sys	0m0.152s

 Performance counter stats for './__run 9':

       4979.231444      task-clock (msec)         #    0.974 CPUs utilized          
                95      context-switches          #    0.019 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            20,678      page-faults               #    0.004 M/sec                  
    15,529,905,548      cycles                    #    3.119 GHz                    
     6,894,807,809      stalled-cycles-frontend   #   44.40% frontend cycles idle   
    21,442,784,424      instructions              #    1.38  insn per cycle         
                                                  #    0.32  stalled cycles per insn
     3,258,740,330      branches                  #  654.467 M/sec                  
       170,269,510      branch-misses             #    5.23% of all branches        

       5.109592077 seconds time elapsed


real	0m4.865s
user	0m4.579s
sys	0m0.180s

 Performance counter stats for './__run 10':

       4761.501779      task-clock (msec)         #    0.978 CPUs utilized          
               122      context-switches          #    0.026 K/sec                  
                 2      cpu-migrations            #    0.000 K/sec                  
            82,730      page-faults               #    0.017 M/sec                  
    14,799,580,336      cycles                    #    3.108 GHz                    
     7,554,974,104      stalled-cycles-frontend   #   51.05% frontend cycles idle   
    16,784,414,163      instructions              #    1.13  insn per cycle         
                                                  #    0.45  stalled cycles per insn
     2,299,830,540      branches                  #  483.005 M/sec                  
       172,212,450      branch-misses             #    7.49% of all branches        

       4.868380857 seconds time elapsed


real	0m5.034s
user	0m4.703s
sys	0m0.216s

 Performance counter stats for './__run 10':

       4920.535533      task-clock (msec)         #    0.977 CPUs utilized          
                82      context-switches          #    0.017 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            82,728      page-faults               #    0.017 M/sec                  
    15,339,624,720      cycles                    #    3.117 GHz                    
     8,227,179,807      stalled-cycles-frontend   #   53.63% frontend cycles idle   
    16,782,434,852      instructions              #    1.09  insn per cycle         
                                                  #    0.49  stalled cycles per insn
     2,299,462,750      branches                  #  467.320 M/sec                  
       172,254,626      branch-misses             #    7.49% of all branches        

       5.036505719 seconds time elapsed


real	0m5.034s
user	0m4.663s
sys	0m0.188s

 Performance counter stats for './__run 10':

       4853.527316      task-clock (msec)         #    0.963 CPUs utilized          
                28      context-switches          #    0.006 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            82,733      page-faults               #    0.017 M/sec                  
    15,108,098,067      cycles                    #    3.113 GHz                    
     7,974,760,020      stalled-cycles-frontend   #   52.78% frontend cycles idle   
    16,780,241,476      instructions              #    1.11  insn per cycle         
                                                  #    0.48  stalled cycles per insn
     2,298,992,178      branches                  #  473.675 M/sec                  
       172,111,385      branch-misses             #    7.49% of all branches        

       5.037457257 seconds time elapsed

