
real	0m4.108s
user	0m4.003s
sys	0m0.104s

 Performance counter stats for './__run 1':

       4108.343509      task-clock (msec)         #    0.994 CPUs utilized          
                41      context-switches          #    0.010 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            63,178      page-faults               #    0.015 M/sec                  
    13,416,431,590      cycles                    #    3.266 GHz                    
     4,671,399,028      stalled-cycles-frontend   #   34.82% frontend cycles idle   
    15,373,050,214      instructions              #    1.15  insn per cycle         
                                                  #    0.30  stalled cycles per insn
     1,988,238,805      branches                  #  483.951 M/sec                  
       205,447,643      branch-misses             #   10.33% of all branches        

       4.132115723 seconds time elapsed


real	0m36.089s
user	0m34.907s
sys	0m0.972s

 Performance counter stats for './__run 2':

      35880.608498      task-clock (msec)         #    0.994 CPUs utilized          
               115      context-switches          #    0.003 K/sec                  
                 3      cpu-migrations            #    0.000 K/sec                  
           550,188      page-faults               #    0.015 M/sec                  
   117,266,605,666      cycles                    #    3.268 GHz                    
    40,828,443,888      stalled-cycles-frontend   #   34.82% frontend cycles idle   
   134,429,586,460      instructions              #    1.15  insn per cycle         
                                                  #    0.30  stalled cycles per insn
    17,381,807,346      branches                  #  484.435 M/sec                  
     1,797,001,690      branch-misses             #   10.34% of all branches        

      36.091834504 seconds time elapsed


real	0m2.401s
user	0m1.978s
sys	0m0.076s

 Performance counter stats for './__run 3':

       2056.003277      task-clock (msec)         #    0.855 CPUs utilized          
                30      context-switches          #    0.015 K/sec                  
                 2      cpu-migrations            #    0.001 K/sec                  
            47,743      page-faults               #    0.023 M/sec                  
     6,665,724,316      cycles                    #    3.242 GHz                    
     2,322,429,938      stalled-cycles-frontend   #   34.84% frontend cycles idle   
     8,602,712,551      instructions              #    1.29  insn per cycle         
                                                  #    0.27  stalled cycles per insn
     1,114,264,313      branches                  #  541.956 M/sec                  
       102,520,841      branch-misses             #    9.20% of all branches        

       2.403834957 seconds time elapsed


real	0m18.373s
user	0m17.668s
sys	0m0.640s

 Performance counter stats for './__run 4':

      18310.713203      task-clock (msec)         #    0.996 CPUs utilized          
                75      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
           431,333      page-faults               #    0.024 M/sec                  
    59,911,993,122      cycles                    #    3.272 GHz                    
    20,715,583,482      stalled-cycles-frontend   #   34.58% frontend cycles idle   
    77,408,380,861      instructions              #    1.29  insn per cycle         
                                                  #    0.27  stalled cycles per insn
     9,984,579,415      branches                  #  545.286 M/sec                  
       929,996,966      branch-misses             #    9.31% of all branches        

      18.376606349 seconds time elapsed


real	0m4.356s
user	0m3.145s
sys	0m0.157s

 Performance counter stats for './__run 5':

       3302.660736      task-clock (msec)         #    0.758 CPUs utilized          
                69      context-switches          #    0.021 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            51,919      page-faults               #    0.016 M/sec                  
    10,688,876,734      cycles                    #    3.236 GHz                    
     3,037,293,090      stalled-cycles-frontend   #   28.42% frontend cycles idle   
    20,016,827,346      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
     3,042,714,396      branches                  #  921.292 M/sec                  
        75,010,989      branch-misses             #    2.47% of all branches        

       4.358977293 seconds time elapsed


real	0m3.434s
user	0m3.249s
sys	0m0.148s

 Performance counter stats for './__run 6':

       3399.962574      task-clock (msec)         #    0.989 CPUs utilized          
                25      context-switches          #    0.007 K/sec                  
                 1      cpu-migrations            #    0.000 K/sec                  
            83,582      page-faults               #    0.025 M/sec                  
    11,089,202,356      cycles                    #    3.262 GHz                    
     3,622,184,564      stalled-cycles-frontend   #   32.66% frontend cycles idle   
    16,921,566,093      instructions              #    1.53  insn per cycle         
                                                  #    0.21  stalled cycles per insn
     2,405,227,635      branches                  #  707.428 M/sec                  
       143,653,609      branch-misses             #    5.97% of all branches        

       3.437559514 seconds time elapsed


real	0m0.218s
user	0m0.120s
sys	0m0.000s

 Performance counter stats for './__run 7':

        122.211529      task-clock (msec)         #    0.554 CPUs utilized          
                 7      context-switches          #    0.057 K/sec                  
                 2      cpu-migrations            #    0.016 K/sec                  
             2,044      page-faults               #    0.017 M/sec                  
       362,589,237      cycles                    #    2.967 GHz                    
       103,052,894      stalled-cycles-frontend   #   28.42% frontend cycles idle   
       677,937,465      instructions              #    1.87  insn per cycle         
                                                  #    0.15  stalled cycles per insn
       103,478,775      branches                  #  846.719 M/sec                  
         2,559,298      branch-misses             #    2.47% of all branches        

       0.220759647 seconds time elapsed


real	0m0.056s
user	0m0.052s
sys	0m0.004s

 Performance counter stats for './__run 8':

         59.086996      task-clock (msec)         #    0.994 CPUs utilized          
                 1      context-switches          #    0.017 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,442      page-faults               #    0.024 M/sec                  
       168,140,526      cycles                    #    2.846 GHz                    
        66,646,114      stalled-cycles-frontend   #   39.64% frontend cycles idle   
       231,822,025      instructions              #    1.38  insn per cycle         
                                                  #    0.29  stalled cycles per insn
        33,314,759      branches                  #  563.826 M/sec                  
         1,969,097      branch-misses             #    5.91% of all branches        

       0.059471370 seconds time elapsed


real	0m3.820s
user	0m3.739s
sys	0m0.080s

 Performance counter stats for './__run 9':

       3821.134046      task-clock (msec)         #    1.000 CPUs utilized          
                 9      context-switches          #    0.002 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            20,681      page-faults               #    0.005 M/sec                  
    12,448,780,812      cycles                    #    3.258 GHz                    
     3,026,907,350      stalled-cycles-frontend   #   24.31% frontend cycles idle   
    21,736,183,469      instructions              #    1.75  insn per cycle         
                                                  #    0.14  stalled cycles per insn
     3,453,135,243      branches                  #  903.694 M/sec                  
       169,281,478      branch-misses             #    4.90% of all branches        

       3.821824880 seconds time elapsed


real	0m3.904s
user	0m3.617s
sys	0m0.132s

 Performance counter stats for './__run 10':

       3751.723807      task-clock (msec)         #    0.960 CPUs utilized          
                14      context-switches          #    0.004 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
            82,732      page-faults               #    0.022 M/sec                  
    12,218,406,990      cycles                    #    3.257 GHz                    
     4,240,819,867      stalled-cycles-frontend   #   34.71% frontend cycles idle   
    16,885,271,074      instructions              #    1.38  insn per cycle         
                                                  #    0.25  stalled cycles per insn
     2,255,884,330      branches                  #  601.293 M/sec                  
       180,111,710      branch-misses             #    7.98% of all branches        

       3.907845964 seconds time elapsed

