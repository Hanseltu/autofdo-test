
real	0m8.209s
user	0m5.385s
sys	0m1.347s

 Performance counter stats for './__run 1':

       6680.128148      task-clock (msec)         #    0.813 CPUs utilized          
             7,267      context-switches          #    0.001 M/sec                  
               329      cpu-migrations            #    0.049 K/sec                  
             1,592      page-faults               #    0.238 K/sec                  
    20,605,728,797      cycles                    #    3.085 GHz                    
     8,949,425,410      stalled-cycles-frontend   #   43.43% frontend cycles idle   
    30,324,820,233      instructions              #    1.47  insn per cycle         
                                                  #    0.30  stalled cycles per insn
     5,411,874,755      branches                  #  810.145 M/sec                  
       278,458,509      branch-misses             #    5.15% of all branches        

       8.212230286 seconds time elapsed


real	0m8.140s
user	0m5.337s
sys	0m1.201s

 Performance counter stats for './__run 1':

       6483.392087      task-clock (msec)         #    0.796 CPUs utilized          
             7,145      context-switches          #    0.001 M/sec                  
               325      cpu-migrations            #    0.050 K/sec                  
             1,594      page-faults               #    0.246 K/sec                  
    20,022,050,246      cycles                    #    3.088 GHz                    
     8,302,959,441      stalled-cycles-frontend   #   41.47% frontend cycles idle   
    30,320,211,915      instructions              #    1.51  insn per cycle         
                                                  #    0.27  stalled cycles per insn
     5,410,919,794      branches                  #  834.582 M/sec                  
       277,122,973      branch-misses             #    5.12% of all branches        

       8.142954088 seconds time elapsed


real	0m7.786s
user	0m5.251s
sys	0m1.049s

 Performance counter stats for './__run 1':

       6244.733056      task-clock (msec)         #    0.802 CPUs utilized          
             7,099      context-switches          #    0.001 M/sec                  
               295      cpu-migrations            #    0.047 K/sec                  
             1,597      page-faults               #    0.256 K/sec                  
    19,327,940,270      cycles                    #    3.095 GHz                    
     7,570,332,789      stalled-cycles-frontend   #   39.17% frontend cycles idle   
    30,318,921,687      instructions              #    1.57  insn per cycle         
                                                  #    0.25  stalled cycles per insn
     5,410,682,213      branches                  #  866.439 M/sec                  
       276,732,464      branch-misses             #    5.11% of all branches        

       7.788666302 seconds time elapsed


real	0m6.028s
user	0m3.879s
sys	0m0.823s

 Performance counter stats for './__run 2':

       4669.362510      task-clock (msec)         #    0.774 CPUs utilized          
             5,165      context-switches          #    0.001 M/sec                  
               224      cpu-migrations            #    0.048 K/sec                  
             1,214      page-faults               #    0.260 K/sec                  
    14,478,742,928      cycles                    #    3.101 GHz                    
     6,223,077,035      stalled-cycles-frontend   #   42.98% frontend cycles idle   
    21,577,150,669      instructions              #    1.49  insn per cycle         
                                                  #    0.29  stalled cycles per insn
     3,850,675,750      branches                  #  824.668 M/sec                  
       198,117,420      branch-misses             #    5.15% of all branches        

       6.030949935 seconds time elapsed


real	0m5.597s
user	0m3.756s
sys	0m0.742s

 Performance counter stats for './__run 2':

       4458.615190      task-clock (msec)         #    0.796 CPUs utilized          
             4,917      context-switches          #    0.001 M/sec                  
               151      cpu-migrations            #    0.034 K/sec                  
             1,221      page-faults               #    0.274 K/sec                  
    13,838,347,800      cycles                    #    3.104 GHz                    
     5,493,805,713      stalled-cycles-frontend   #   39.70% frontend cycles idle   
    21,574,476,379      instructions              #    1.56  insn per cycle         
                                                  #    0.25  stalled cycles per insn
     3,850,126,442      branches                  #  863.525 M/sec                  
       197,199,495      branch-misses             #    5.12% of all branches        

       5.600165667 seconds time elapsed


real	0m5.894s
user	0m3.976s
sys	0m0.824s

 Performance counter stats for './__run 2':

       4767.588518      task-clock (msec)         #    0.809 CPUs utilized          
             5,328      context-switches          #    0.001 M/sec                  
               198      cpu-migrations            #    0.042 K/sec                  
             1,217      page-faults               #    0.255 K/sec                  
    14,834,566,534      cycles                    #    3.112 GHz                    
     6,607,969,929      stalled-cycles-frontend   #   44.54% frontend cycles idle   
    21,578,095,904      instructions              #    1.45  insn per cycle         
                                                  #    0.31  stalled cycles per insn
     3,850,881,237      branches                  #  807.721 M/sec                  
       198,106,303      branch-misses             #    5.14% of all branches        

       5.896418933 seconds time elapsed


real	0m0.063s
user	0m0.032s
sys	0m0.016s

 Performance counter stats for './__run 3':

         50.317965      task-clock (msec)         #    0.753 CPUs utilized          
                56      context-switches          #    0.001 M/sec                  
                 3      cpu-migrations            #    0.060 K/sec                  
               297      page-faults               #    0.006 M/sec                  
       150,174,503      cycles                    #    2.985 GHz                    
        65,505,861      stalled-cycles-frontend   #   43.62% frontend cycles idle   
       216,335,558      instructions              #    1.44  insn per cycle         
                                                  #    0.30  stalled cycles per insn
        38,725,936      branches                  #  769.624 M/sec                  
         1,970,772      branch-misses             #    5.09% of all branches        

       0.066791078 seconds time elapsed


real	0m0.068s
user	0m0.035s
sys	0m0.014s

 Performance counter stats for './__run 3':

         50.652595      task-clock (msec)         #    0.711 CPUs utilized          
                55      context-switches          #    0.001 M/sec                  
                 2      cpu-migrations            #    0.039 K/sec                  
               299      page-faults               #    0.006 M/sec                  
       152,972,229      cycles                    #    3.020 GHz                    
        69,668,849      stalled-cycles-frontend   #   45.54% frontend cycles idle   
       216,330,403      instructions              #    1.41  insn per cycle         
                                                  #    0.32  stalled cycles per insn
        38,726,340      branches                  #  764.548 M/sec                  
         1,984,647      branch-misses             #    5.12% of all branches        

       0.071245835 seconds time elapsed


real	0m0.057s
user	0m0.043s
sys	0m0.005s

 Performance counter stats for './__run 3':

         49.283622      task-clock (msec)         #    0.825 CPUs utilized          
                54      context-switches          #    0.001 M/sec                  
                 8      cpu-migrations            #    0.162 K/sec                  
               299      page-faults               #    0.006 M/sec                  
       146,369,745      cycles                    #    2.970 GHz                    
        62,162,255      stalled-cycles-frontend   #   42.47% frontend cycles idle   
       216,300,104      instructions              #    1.48  insn per cycle         
                                                  #    0.29  stalled cycles per insn
        38,719,770      branches                  #  785.652 M/sec                  
         1,990,889      branch-misses             #    5.14% of all branches        

       0.059736073 seconds time elapsed


real	0m0.853s
user	0m0.602s
sys	0m0.109s

 Performance counter stats for './__run 4':

        707.369595      task-clock (msec)         #    0.827 CPUs utilized          
               772      context-switches          #    0.001 M/sec                  
                21      cpu-migrations            #    0.030 K/sec                  
               437      page-faults               #    0.618 K/sec                  
     2,181,666,728      cycles                    #    3.084 GHz                    
       852,814,599      stalled-cycles-frontend   #   39.09% frontend cycles idle   
     3,424,025,809      instructions              #    1.57  insn per cycle         
                                                  #    0.25  stalled cycles per insn
       611,157,631      branches                  #  863.986 M/sec                  
        31,153,702      branch-misses             #    5.10% of all branches        

       0.855808999 seconds time elapsed


real	0m0.854s
user	0m0.591s
sys	0m0.123s

 Performance counter stats for './__run 4':

        710.097744      task-clock (msec)         #    0.829 CPUs utilized          
               776      context-switches          #    0.001 M/sec                  
                18      cpu-migrations            #    0.025 K/sec                  
               438      page-faults               #    0.617 K/sec                  
     2,218,130,816      cycles                    #    3.124 GHz                    
       895,407,123      stalled-cycles-frontend   #   40.37% frontend cycles idle   
     3,424,219,761      instructions              #    1.54  insn per cycle         
                                                  #    0.26  stalled cycles per insn
       611,191,733      branches                  #  860.715 M/sec                  
        31,306,212      branch-misses             #    5.12% of all branches        

       0.857055438 seconds time elapsed


real	0m0.841s
user	0m0.609s
sys	0m0.086s

 Performance counter stats for './__run 4':

        691.154818      task-clock (msec)         #    0.820 CPUs utilized          
               781      context-switches          #    0.001 M/sec                  
                24      cpu-migrations            #    0.035 K/sec                  
               437      page-faults               #    0.632 K/sec                  
     2,146,123,772      cycles                    #    3.105 GHz                    
       812,887,639      stalled-cycles-frontend   #   37.88% frontend cycles idle   
     3,424,049,416      instructions              #    1.60  insn per cycle         
                                                  #    0.24  stalled cycles per insn
       611,161,121      branches                  #  884.261 M/sec                  
        31,150,089      branch-misses             #    5.10% of all branches        

       0.843126598 seconds time elapsed


real	0m18.778s
user	0m12.802s
sys	0m2.429s

 Performance counter stats for './__run 5':

      15097.504737      task-clock (msec)         #    0.804 CPUs utilized          
            16,744      context-switches          #    0.001 M/sec                  
               527      cpu-migrations            #    0.035 K/sec                  
             3,407      page-faults               #    0.226 K/sec                  
    46,833,478,666      cycles                    #    3.102 GHz                    
    18,897,513,799      stalled-cycles-frontend   #   40.35% frontend cycles idle   
    72,383,271,546      instructions              #    1.55  insn per cycle         
                                                  #    0.26  stalled cycles per insn
    12,917,188,909      branches                  #  855.584 M/sec                  
       660,983,446      branch-misses             #    5.12% of all branches        

      18.780648545 seconds time elapsed


real	0m18.584s
user	0m12.638s
sys	0m2.431s

 Performance counter stats for './__run 5':

      14936.097086      task-clock (msec)         #    0.804 CPUs utilized          
            16,772      context-switches          #    0.001 M/sec                  
               635      cpu-migrations            #    0.043 K/sec                  
             3,409      page-faults               #    0.228 K/sec                  
    45,956,955,960      cycles                    #    3.077 GHz                    
    17,993,801,901      stalled-cycles-frontend   #   39.15% frontend cycles idle   
    72,375,153,186      instructions              #    1.57  insn per cycle         
                                                  #    0.25  stalled cycles per insn
    12,915,839,234      branches                  #  864.740 M/sec                  
       660,165,935      branch-misses             #    5.11% of all branches        

      18.586727892 seconds time elapsed


real	0m33.682s
user	0m14.012s
sys	0m2.929s

 Performance counter stats for './__run 5':

      16786.510257      task-clock (msec)         #    0.498 CPUs utilized          
            18,846      context-switches          #    0.001 M/sec                  
             2,208      cpu-migrations            #    0.132 K/sec                  
             3,410      page-faults               #    0.203 K/sec                  
    50,847,252,943      cycles                    #    3.029 GHz                    
    23,228,549,545      stalled-cycles-frontend   #   45.68% frontend cycles idle   
    72,447,999,159      instructions              #    1.42  insn per cycle         
                                                  #    0.32  stalled cycles per insn
    12,929,439,325      branches                  #  770.228 M/sec                  
       674,383,861      branch-misses             #    5.22% of all branches        

      33.685031248 seconds time elapsed


real	0m32.045s
user	0m7.920s
sys	0m2.493s

 Performance counter stats for './__run 6':

      10303.462068      task-clock (msec)         #    0.321 CPUs utilized          
            11,327      context-switches          #    0.001 M/sec                  
             2,372      cpu-migrations            #    0.230 K/sec                  
             1,856      page-faults               #    0.180 K/sec                  
    30,163,825,602      cycles                    #    2.928 GHz                    
    16,448,728,885      stalled-cycles-frontend   #   54.53% frontend cycles idle   
    36,584,236,643      instructions              #    1.21  insn per cycle         
                                                  #    0.45  stalled cycles per insn
     6,529,800,477      branches                  #  633.748 M/sec                  
       347,645,345      branch-misses             #    5.32% of all branches        

      32.048560107 seconds time elapsed


real	0m32.902s
user	0m7.767s
sys	0m2.326s

 Performance counter stats for './__run 6':

       9980.347500      task-clock (msec)         #    0.303 CPUs utilized          
            11,155      context-switches          #    0.001 M/sec                  
             2,188      cpu-migrations            #    0.219 K/sec                  
             1,860      page-faults               #    0.186 K/sec                  
    28,978,981,049      cycles                    #    2.904 GHz                    
    15,272,749,641      stalled-cycles-frontend   #   52.70% frontend cycles idle   
    36,584,587,145      instructions              #    1.26  insn per cycle         
                                                  #    0.42  stalled cycles per insn
     6,529,759,154      branches                  #  654.262 M/sec                  
       347,234,541      branch-misses             #    5.32% of all branches        

      32.905150169 seconds time elapsed


real	0m31.624s
user	0m7.709s
sys	0m2.030s

 Performance counter stats for './__run 6':

       9618.373457      task-clock (msec)         #    0.304 CPUs utilized          
            10,978      context-switches          #    0.001 M/sec                  
             2,453      cpu-migrations            #    0.255 K/sec                  
             1,861      page-faults               #    0.193 K/sec                  
    27,642,981,231      cycles                    #    2.874 GHz                    
    13,822,202,840      stalled-cycles-frontend   #   50.00% frontend cycles idle   
    36,586,462,820      instructions              #    1.32  insn per cycle         
                                                  #    0.38  stalled cycles per insn
     6,530,040,918      branches                  #  678.913 M/sec                  
       346,916,142      branch-misses             #    5.31% of all branches        

      31.627150086 seconds time elapsed


real	0m1.386s
user	0m0.772s
sys	0m0.128s

 Performance counter stats for './__run 7':

        890.183141      task-clock (msec)         #    0.641 CPUs utilized          
               912      context-switches          #    0.001 M/sec                  
                14      cpu-migrations            #    0.016 K/sec                  
               456      page-faults               #    0.512 K/sec                  
     2,628,666,954      cycles                    #    2.953 GHz                    
     1,128,267,475      stalled-cycles-frontend   #   42.92% frontend cycles idle   
     3,894,334,348      instructions              #    1.48  insn per cycle         
                                                  #    0.29  stalled cycles per insn
       695,191,806      branches                  #  780.954 M/sec                  
        35,658,499      branch-misses             #    5.13% of all branches        

       1.389416027 seconds time elapsed


real	0m1.018s
user	0m0.709s
sys	0m0.142s

 Performance counter stats for './__run 7':

        844.988123      task-clock (msec)         #    0.826 CPUs utilized          
               890      context-switches          #    0.001 M/sec                  
                18      cpu-migrations            #    0.021 K/sec                  
               452      page-faults               #    0.535 K/sec                  
     2,527,062,642      cycles                    #    2.991 GHz                    
     1,024,585,370      stalled-cycles-frontend   #   40.54% frontend cycles idle   
     3,890,106,443      instructions              #    1.54  insn per cycle         
                                                  #    0.26  stalled cycles per insn
       694,392,322      branches                  #  821.778 M/sec                  
        35,529,321      branch-misses             #    5.12% of all branches        

       1.022552516 seconds time elapsed


real	0m1.021s
user	0m0.670s
sys	0m0.181s

 Performance counter stats for './__run 7':

        842.456366      task-clock (msec)         #    0.823 CPUs utilized          
               883      context-switches          #    0.001 M/sec                  
                17      cpu-migrations            #    0.020 K/sec                  
               453      page-faults               #    0.538 K/sec                  
     2,504,587,632      cycles                    #    2.973 GHz                    
     1,001,559,910      stalled-cycles-frontend   #   39.99% frontend cycles idle   
     3,887,775,395      instructions              #    1.55  insn per cycle         
                                                  #    0.26  stalled cycles per insn
       693,940,551      branches                  #  823.711 M/sec                  
        35,459,604      branch-misses             #    5.11% of all branches        

       1.023566147 seconds time elapsed


real	0m2.697s
user	0m1.797s
sys	0m0.371s

 Performance counter stats for './__run 8':

       2150.462227      task-clock (msec)         #    0.796 CPUs utilized          
             2,413      context-switches          #    0.001 M/sec                  
                73      cpu-migrations            #    0.034 K/sec                  
               727      page-faults               #    0.338 K/sec                  
     6,587,241,132      cycles                    #    3.063 GHz                    
     2,672,382,668      stalled-cycles-frontend   #   40.57% frontend cycles idle   
    10,134,401,392      instructions              #    1.54  insn per cycle         
                                                  #    0.26  stalled cycles per insn
     1,808,691,989      branches                  #  841.071 M/sec                  
        92,466,875      branch-misses             #    5.11% of all branches        

       2.700084091 seconds time elapsed


real	0m2.699s
user	0m1.761s
sys	0m0.389s

 Performance counter stats for './__run 8':

       2133.775469      task-clock (msec)         #    0.790 CPUs utilized          
             2,322      context-switches          #    0.001 M/sec                  
               104      cpu-migrations            #    0.049 K/sec                  
               723      page-faults               #    0.339 K/sec                  
     6,512,841,507      cycles                    #    3.052 GHz                    
     2,585,863,809      stalled-cycles-frontend   #   39.70% frontend cycles idle   
    10,132,164,873      instructions              #    1.56  insn per cycle         
                                                  #    0.26  stalled cycles per insn
     1,808,288,946      branches                  #  847.460 M/sec                  
        92,479,912      branch-misses             #    5.11% of all branches        

       2.702311950 seconds time elapsed


real	0m2.684s
user	0m1.818s
sys	0m0.350s

 Performance counter stats for './__run 8':

       2151.389643      task-clock (msec)         #    0.801 CPUs utilized          
             2,347      context-switches          #    0.001 M/sec                  
                87      cpu-migrations            #    0.040 K/sec                  
               725      page-faults               #    0.337 K/sec                  
     6,633,711,459      cycles                    #    3.083 GHz                    
     2,730,100,877      stalled-cycles-frontend   #   41.15% frontend cycles idle   
    10,132,012,550      instructions              #    1.53  insn per cycle         
                                                  #    0.27  stalled cycles per insn
     1,808,260,478      branches                  #  840.508 M/sec                  
        92,698,351      branch-misses             #    5.13% of all branches        

       2.686965715 seconds time elapsed


real	0m2.313s
user	0m1.652s
sys	0m0.262s

 Performance counter stats for './__run 9':

       1900.516039      task-clock (msec)         #    0.821 CPUs utilized          
             2,152      context-switches          #    0.001 M/sec                  
                55      cpu-migrations            #    0.029 K/sec                  
               696      page-faults               #    0.366 K/sec                  
     5,893,886,933      cycles                    #    3.101 GHz                    
     2,268,186,236      stalled-cycles-frontend   #   38.48% frontend cycles idle   
     9,373,339,092      instructions              #    1.59  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     1,672,869,831      branches                  #  880.219 M/sec                  
        85,296,438      branch-misses             #    5.10% of all branches        

       2.316256102 seconds time elapsed


real	0m2.687s
user	0m1.714s
sys	0m0.334s

 Performance counter stats for './__run 9':

       2031.166400      task-clock (msec)         #    0.755 CPUs utilized          
             2,243      context-switches          #    0.001 M/sec                  
               101      cpu-migrations            #    0.050 K/sec                  
               692      page-faults               #    0.341 K/sec                  
     6,228,696,695      cycles                    #    3.067 GHz                    
     2,634,839,414      stalled-cycles-frontend   #   42.30% frontend cycles idle   
     9,374,151,731      instructions              #    1.50  insn per cycle         
                                                  #    0.28  stalled cycles per insn
     1,673,038,241      branches                  #  823.683 M/sec                  
        86,021,421      branch-misses             #    5.14% of all branches        

       2.690470274 seconds time elapsed


real	0m2.317s
user	0m1.655s
sys	0m0.267s

 Performance counter stats for './__run 9':

       1909.499275      task-clock (msec)         #    0.822 CPUs utilized          
             2,131      context-switches          #    0.001 M/sec                  
                63      cpu-migrations            #    0.033 K/sec                  
               695      page-faults               #    0.364 K/sec                  
     5,880,003,789      cycles                    #    3.079 GHz                    
     2,231,154,334      stalled-cycles-frontend   #   37.94% frontend cycles idle   
     9,373,544,406      instructions              #    1.59  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     1,672,900,339      branches                  #  876.094 M/sec                  
        85,291,463      branch-misses             #    5.10% of all branches        

       2.322516728 seconds time elapsed


real	0m3.826s
user	0m2.609s
sys	0m0.488s

 Performance counter stats for './__run 10':

       3072.546742      task-clock (msec)         #    0.802 CPUs utilized          
             3,391      context-switches          #    0.001 M/sec                  
               130      cpu-migrations            #    0.042 K/sec                  
               930      page-faults               #    0.303 K/sec                  
     9,473,303,560      cycles                    #    3.083 GHz                    
     3,708,664,870      stalled-cycles-frontend   #   39.15% frontend cycles idle   
    14,860,930,064      instructions              #    1.57  insn per cycle         
                                                  #    0.25  stalled cycles per insn
     2,652,180,281      branches                  #  863.186 M/sec                  
       135,701,203      branch-misses             #    5.12% of all branches        

       3.830550898 seconds time elapsed


real	0m4.053s
user	0m2.620s
sys	0m0.485s

 Performance counter stats for './__run 10':

       3079.627233      task-clock (msec)         #    0.759 CPUs utilized          
             3,522      context-switches          #    0.001 M/sec                  
               118      cpu-migrations            #    0.038 K/sec                  
               929      page-faults               #    0.302 K/sec                  
     9,519,591,121      cycles                    #    3.091 GHz                    
     3,759,147,293      stalled-cycles-frontend   #   39.49% frontend cycles idle   
    14,861,219,302      instructions              #    1.56  insn per cycle         
                                                  #    0.25  stalled cycles per insn
     2,652,239,769      branches                  #  861.221 M/sec                  
       135,544,516      branch-misses             #    5.11% of all branches        

       4.056561413 seconds time elapsed


real	0m3.847s
user	0m2.574s
sys	0m0.488s

 Performance counter stats for './__run 10':

       3036.271597      task-clock (msec)         #    0.789 CPUs utilized          
             3,433      context-switches          #    0.001 M/sec                  
               106      cpu-migrations            #    0.035 K/sec                  
               930      page-faults               #    0.306 K/sec                  
     9,369,335,256      cycles                    #    3.086 GHz                    
     3,596,193,744      stalled-cycles-frontend   #   38.38% frontend cycles idle   
    14,860,518,177      instructions              #    1.59  insn per cycle         
                                                  #    0.24  stalled cycles per insn
     2,652,097,320      branches                  #  873.472 M/sec                  
       135,476,158      branch-misses             #    5.11% of all branches        

       3.849868262 seconds time elapsed

